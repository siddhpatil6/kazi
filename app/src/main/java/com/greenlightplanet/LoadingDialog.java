package com.greenlightplanet;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;

import retrofit2.Call;


public class LoadingDialog {

    static ProgressDialog progressDialog;


    public static void showLoadingDialog(Context context, String message) {

        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(message);

        progressDialog.setCancelable(true);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {

                //calll.cancel();

            }
        });


        progressDialog.show();

    }

    public static void cancelLoading() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.cancel();

    }




}
