package com.greenlightplanet;

import java.util.ArrayList;

/**
 * Created by prateek on 14-06-2018.
 */

public interface CallBackVolley {

    void onSuccess(ArrayList<String> result);
}
