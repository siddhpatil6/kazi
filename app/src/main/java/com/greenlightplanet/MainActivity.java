package com.greenlightplanet;

import android.app.Activity;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.greenlightplanet.kazi.R;

import java.util.ArrayList;

public class MainActivity extends Activity {
    private ArrayList<String> agentList, agent_txt_list;
    Button btn;
    String[] splitedValue;
    ArrayList<String> spinnerArrayList, spinnerArraylistPhone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn = (Button) findViewById(R.id.button);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new GetAgentNames().execute();
                //agentList = new GetData().getAgentList_Kenya();
            }
        });
    }

    private class GetAgentNames extends AsyncTask<Void, Void, ArrayList<String>> {
        @Override
        protected ArrayList<String> doInBackground(Void... params) {
          //  agentList = new GetData().getAgentList_sms_Kenya();
            ;
            return agentList;

        }

        @Override
        protected void onPostExecute(ArrayList<String> strings) {
            spinnerArrayList = new ArrayList<String>();
            spinnerArraylistPhone = new ArrayList<String>();
            String txt_phone,name;



            if (agentList.toString().contains(",")) {


                for (int i = 0; i < agentList.size(); i++) {
                    splitedValue = agentList.get(i).split("-");
                    spinnerArrayList.add(splitedValue[0]);
                    spinnerArraylistPhone.add(splitedValue[1]);
                    name = splitedValue[0];
                    txt_phone = splitedValue[1];

                //   new SendSmsEos(txt_phone,name).execute();

;                    Log.d("check#######", spinnerArrayList.toString() + "Phone" + spinnerArraylistPhone.toString());
                }


            }
        }
    }


    private class SendSmsEos extends AsyncTask<String, Void, String> {

        String[] phonesplit_value;
        String phone_number,txt_otp,txt_name;

        public SendSmsEos(String txt_phone,String name) {

            phone_number = txt_phone;
            txt_name = name;
        }


        @Override
        protected String doInBackground(String... strings) {

            try {

                if (new GetData().isNetworkAvailable(MainActivity.this)) {

// checking the connection first

                    //  new HttpAsyncTask().execute("http://smsc.txtnation.com:8091/sms/send_sms.php?src=440000000000&dst=447777000000&type=0&dr=1&user=Sunking&password=Android12&msg=txtNation%20best%20to%20test%20at%20the%20end%20of%20the%20road.");

                    //return  new SendSms().sendSMS("Sunking:", txt_phone, "0", "1", "greenlight001", "5ea6Rds3", "Hi "+ txt_name+" enter your OTP code: "+ txt_otp +" in Kazi app");
                    // for update msg for limited Eo'// STOPSHIP: 24-07-2017
                      return  new SendSms().sendSMS("Sunking:", phone_number, "0", "1", "greenlight001", "5ea6Rds3", "Dear Eo "+ txt_name+", you can now update the Kazi app from the Play Store. Go to https://play.google.com/store/apps/details?id=com.greenlightplanet.kazi");

                }
                else {


                }


            } catch (Exception e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPreExecute() {

        }



        @Override
        protected void onPostExecute(String v) {

            // Testing response
            //  v = "111";

            if (v.equalsIgnoreCase("400")){
                Toast.makeText(getBaseContext(),"ERROR Sorry you will not receive any OTP ! ERROR", Toast.LENGTH_LONG).show();
            }else if (v.equalsIgnoreCase("200")){
                Toast.makeText(getBaseContext(), " You have received OTP successfully ! Please check the sms from Sunking " , Toast.LENGTH_LONG).show();

              //  Toast.makeText(getBaseContext(), "Hey this is only for trial: Your OTP is !" + String.valueOf(otp), Toast.LENGTH_LONG).show();

            }
            else if (v.equalsIgnoreCase("500")){
                Toast.makeText(getBaseContext(), " Server Error  !", Toast.LENGTH_LONG).show();
            }
            else if (v.equalsIgnoreCase("60")){

                Toast.makeText(getBaseContext()," Error Please Consult to your Area manager ! Charging fail ",Toast.LENGTH_LONG).show();
            }
            else if (v.equalsIgnoreCase("110")){

                Toast.makeText(getBaseContext()," Error Please Consult to your Area manager ! Invalid Source Address",Toast.LENGTH_LONG).show();
            }
            else if (v.equalsIgnoreCase("111")){

                Toast.makeText(getBaseContext()," Error Please Consult to your Area manager ! Check your Mobile number ",Toast.LENGTH_LONG).show();
            }

            else {
                Toast.makeText(getBaseContext(), " Please check your connection !", Toast.LENGTH_LONG).show();
            }


        }

    }
}
