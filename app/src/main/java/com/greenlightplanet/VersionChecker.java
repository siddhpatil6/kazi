package com.greenlightplanet;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.jsoup.Jsoup;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by prateek on 09-08-2017.
 */

public class VersionChecker extends AsyncTask<String, String, String> {

    String newVersion;
    public ProgressDialog dialog;

    Context mContext;


    VersionChecker(Context context){
        this.mContext = context;
    }

    @Override
    protected void onPreExecute() {
       /* dialog.setMessage("Doing something, please wait.");
        dialog.show();*/


        super.onPreExecute();
        dialog = new ProgressDialog(mContext);
        dialog.setMessage("Doing something, please wait...");
        dialog.setIndeterminate(false);
        dialog.setCancelable(true);
        dialog.show();

    }

    @Override
    protected void onPostExecute(String s) {
        if (dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    @Override
    protected String doInBackground(String... params) {
                //.timeout(30000)

        String line = "";
        String result = "";
        int responseCode = 0;
        try {
          //  newVersion = Jsoup.connect("https://play.google.com/store/apps/details?id=development.avenues.dc&hl=en")
           // newVersion = Jsoup.connect("https://play.google.com/store/apps/details?id=com.greenlightplanet.kazi&hl=en")
           /*  newVersion = Jsoup.connect("http://13.126.146.126/Sunking_EB/versionCheck.php")
                    .timeout(3000)
                    .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                    .referrer("http://www.google.com")
                    .get()
                    .select("div[itemprop=softwareVersion]")
                    .first()
                    .ownText();*/

     String url = "http://13.126.146.126/Sunking_EB/versionCheck.php";


            URL tmUrl = new URL(url);

            HttpURLConnection connection = (HttpURLConnection) tmUrl.openConnection();
            connection.setRequestMethod("GET");
            connection.connect();
//testing

            responseCode = connection.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {


                BufferedReader reader = null;

                InputStream inputStream = connection.getInputStream();
                StringBuffer buffer = new StringBuffer();
                if (inputStream == null) {
                    // Nothing to do.
                    return null;
                }
                reader = new BufferedReader(new InputStreamReader(inputStream));

                while ((line = reader.readLine()) != null) {
                    long elapsedTime = System.currentTimeMillis();
                    long currentTime = 0;
                    //result += responseCode+line;
                    result += responseCode;

                    if (elapsedTime - currentTime >= 5000) {
                        return result;
                    }
                    buffer.append(line + "\n");
                    inputStream.close();

                    return result;
                }

                if (buffer.length() == 0) {
                    return result;
                }

                Log.d("Test", buffer.toString());
                //return buffer.toString();
                return buffer.toString();
            } else if (responseCode == HttpURLConnection.HTTP_BAD_REQUEST){

                Log.i("SMS Check", "Unsuccessful HTTP Response Code: " + responseCode);
                return result;
            }else if (responseCode == HttpURLConnection.HTTP_SERVER_ERROR){

                return result;
            }
            else {

                Log.i("SMS Check", "Unsuccessful HTTP Response Code: " + responseCode);
                return result;
            }
        } catch (MalformedURLException e) {
            Log.e("Exception", "Error processing Places API URL", e);
            return result;
        } catch (IOException e) {
            Log.e("Exception", "Error connecting to Places API", e);
            return result;
        } catch (Exception e) {
            return result;
        }


    }





}


