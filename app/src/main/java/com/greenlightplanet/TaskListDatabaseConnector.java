package com.greenlightplanet;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import android.Manifest;
import android.content.ContentValues;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.provider.CallLog;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

public class TaskListDatabaseConnector {

    private static final String DATABASE_NAME = "Tasks";
    private SQLiteDatabase database;
    private DatabaseOpenHelper databaseOpenHelper;
    private static final int CALL_DURATION = 10; // in seconds
    private static final int DAY_LOOKBACK = -3; //
    private static final int KENYA_PHONE_NUMBER_DIGITS = 9;
public String callCount = "";
    public String  callcount = "";
    public String first_call = "";
    public String sec_call = "";
    public String visit_call = "";
    public String repo_call = "";


    /*
     * Initializes the database connector
     * @param context: context in which the database is contained, use getApplicationContext()
     */
    public TaskListDatabaseConnector(Context context) {
        this.databaseOpenHelper = new DatabaseOpenHelper(context, DATABASE_NAME, null,13);

    }

    /*
     * Opens the database
     */
    public void open() throws SQLException {
        this.database = this.databaseOpenHelper.getWritableDatabase();
    }

    /*
     * Closes the database if it is open
     */
    public void close() {
        if (this.database != null) {
            this.database.close();
        }
    }

    public void insertTaskList(HashMap<String, HashMap<String, String>> taskListData, String Count) {
        ContentValues taskList = new ContentValues(); //create object be put into the database
        this.open(); //open the database

        Log.d("TASKKLISST",taskListData.toString());

        Log.d("DataBase******", Count);
        database.delete("tasks", null, null);
        for (String taskID : taskListData.keySet()) { //cycle through each ticker symbol
            taskList.put("task_id", taskID);
            taskList.put("call_count", Count);
            for (String transactionDataPoint : taskListData.get(taskID).keySet()) {
                taskList.put(transactionDataPoint, taskListData.get(taskID).get(transactionDataPoint));
                taskList.put("ticket_created", false);
                taskList.put("call_duration", "");
                taskList.put("ticket_solved", false);
                taskList.put("call_complete", false);
                taskList.put("next_payment_date", "");
                taskList.put("task_complete", false);
            }
            System.out.println("INserting Data" + taskList);
            this.database.insert("tasks", null, taskList); //inserts values into database

        }
        this.close(); //close the database
    }

    public void insertOtpCheck(String agentName,String mobile_no,Integer check){

        ContentValues otpCheck = new ContentValues();
        this.open();
        otpCheck.put("agent_name",agentName);
        otpCheck.put("phone_number",mobile_no);
        otpCheck.put("check_otp",check);


        System.out.println("INSerting Otp Check"+ otpCheck);
        this.database.insert("OtpCheck",null,otpCheck);        // inserting OTP check into database

        this.close();
    }

    public void insertLogs(String eo_name,String activity_page,String action,String httpCall,String elapseTime,String timeStamp,String country){

        ContentValues logs = new ContentValues();
        this.open();
        logs.put("eo_name",eo_name);
        logs.put("activity_page",activity_page);
        logs.put("action",action);
        logs.put("httpCall",httpCall);
        logs.put("elaspse_time",elapseTime);
        logs.put("timeStamp",timeStamp);
        logs.put("country",country);
        System.out.println("INSerting logs into database"+ logs);
        this.database.insert("logs",null,logs);        // inserting logs check into database
        this.close();
    }



    public int deleteLogs(){

        ContentValues logs = new ContentValues();
        this.open();
        System.out.println("Deleting logs into database"+ logs);
       int del =  this.database.delete("logs",null,null);        // deleting logs check into database
        this.close();


       // / database.execSQL("delete from "+ "logs");

        return del;
    }
    public void updateTaskComplete(String taskID) {
        ContentValues taskComplete = new ContentValues();
        this.open();
        taskComplete.put("task_complete", true);
        database.update("tasks", taskComplete, "task_id=?", new String[]{taskID});
        this.close();
    }

    public void updateNextPaymentDue(String taskID, String nextPaymentDueDate) {
        ContentValues taskComplete = new ContentValues();
        this.open();
        taskComplete.put("next_payment_date", nextPaymentDueDate);
        database.update("tasks", taskComplete, "task_id=?", new String[]{taskID});
        this.close();
    }

    public void checkCallComplete(Context context) {
        ContentValues callComplete = new ContentValues();
        callComplete.put("call_complete", true);
        this.open();
        Cursor data = this.database.query("tasks", new String[]{"task_id,owner_msisdn"}, "task_complete=0 AND ticket_type LIKE '%Call'", null, null, null, null);
        while (data.moveToNext()) {
            int call_duration = checkCallLog(data.getString(1), context);
            if (call_duration >= CALL_DURATION) {
                callComplete.put("call_duration", Integer.toString(call_duration));
                String taskID = data.getString(0);
                database.update("tasks", callComplete, "task_id=?", new String[]{taskID});
            }
        }
        this.close();
    }


    public int checkCallLog(String phoneNumber, Context context) {

        String[] strFields = {CallLog.Calls.DURATION, CallLog.Calls.NUMBER};
        String mSelectionClause = CallLog.Calls.DATE + " >= ? AND " + CallLog.Calls.TYPE + " = ?";
        String[] mSelectionArgs = {createDateFilter().toString(), Integer.toString(CallLog.Calls.OUTGOING_TYPE)};
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_CALL_LOG) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.

        }

        Cursor callLog = context.getContentResolver().query(CallLog.Calls.CONTENT_URI, strFields, mSelectionClause, mSelectionArgs, null);
        int callDuration = 0;
       try {

           if (callLog.getCount() > 0) {
               while (callLog.moveToNext()) {
                   if (callLog.getString(1).length() >= KENYA_PHONE_NUMBER_DIGITS) {
                       if (right(callLog.getString(1), KENYA_PHONE_NUMBER_DIGITS).equals(right(phoneNumber, KENYA_PHONE_NUMBER_DIGITS))) {
                           int tempCallDuration = Integer.parseInt(callLog.getString(0));
                           if (tempCallDuration > callDuration) {
                               callDuration = tempCallDuration;
                           }
                       }
                   }
               }
           }
       } catch (NullPointerException en){
           en.printStackTrace();
       } catch (Exception e){
           e.printStackTrace();
       }
        callLog.close();
        return callDuration;
    }






    /*public int checkCallLog(String phoneNumber, Context context) {
        String[] strFields = {CallLog.Calls.DURATION, CallLog.Calls.NUMBER}; // calls Number prateek
        String mSelectionClause = CallLog.Calls.DATE + " >= ? AND " + CallLog.Calls.TYPE + " = ?";
        String[] mSelectionArgs = {createDateFilter().toString(), Integer.toString(CallLog.Calls.OUTGOING_TYPE)};
        //  Cursor callLog = context.getContentResolver().query(CallLog.Calls.CONTENT_URI,strFields, mSelectionClause,mSelectionArgs, null);
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_CALL_LOG) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.

        }
        Cursor callLog = context.getContentResolver().query(CallLog.Calls.CONTENT_URI, strFields, mSelectionClause, mSelectionArgs, null);
        int callDuration = 0;
        if (callLog.getCount() > 0) {
            while (callLog.moveToNext()) {
                if (callLog.getString(1).length() >= KENYA_PHONE_NUMBER_DIGITS) {
                    if (right(callLog.getString(1), KENYA_PHONE_NUMBER_DIGITS).equals(right(phoneNumber, KENYA_PHONE_NUMBER_DIGITS))) {
                        int tempCallDuration = Integer.parseInt(callLog.getString(0));
                        if (tempCallDuration > callDuration) {
                            callDuration = tempCallDuration;
                        }
                    }
                }
            }
            // this.close(); // Check the data base close or not
        }
        return callDuration;
    }
*/

    private static String right(String value, int length) {
        return value.substring(value.length() - length);
    }

    public static Long createDateFilter() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, DAY_LOOKBACK);
        return calendar.getTimeInMillis();

    }

    public Cursor getCustomerData(String taskID) {
        return this.database.query("tasks", null, "task_id=?", new String[]{taskID}, null, null, null);


    }


    public Cursor getOTPCheck(String agent_name){

        return this.database.query("OtpCheck",new String[]{"check_otp"},"agent_name=?",new String[]{agent_name},null,null,null);

    }

    public String getOtpCheck(String agent_name){
        this.open();

        Cursor data = this.database.query("OtpCheck",new String[]{"check_otp"},"agent_name=?",new String[]{agent_name},null,null,null);
        data.moveToNext();
        String OtpCheck = data.getString(0);
        this.close();
        return OtpCheck;
    }



    public Cursor getOTPChecks(String agent_name){

        return this.database.query("OtpCheck",new String[]{"check_otp"},"agent_name=?",new String[]{agent_name},null,null,null);

    }
    public Cursor getCountTasktype() {


        return this.database.query("tasks", new String[]{"ticket_type"}, null, null, null, null, null);
       // return this.database.query("tasks", null, "ticket_id=?", new String[]{owner_name}, null, null, null);
    }


    public String getPhoneNumber(String taskID) {
        this.open();
        Cursor data = this.database.query("tasks", new String[]{"owner_msisdn"}, "task_id=?", new String[]{taskID}, null, null, null);
        data.moveToNext();
        String phoneNumber = data.getString(0);
        this.close();
        return phoneNumber;
    }





    public ArrayList<HashMap<String, String>> getTaskListData(String[] listParameters, Context context) {
        checkCallComplete(context);
        ArrayList<HashMap<String, String>> listData = new ArrayList<HashMap<String, String>>();
        ArrayList<HashMap<String, String>> calData = new ArrayList<HashMap<String, String>>();
        int caldatacheck ;
        this.open();

        Cursor data = this.database.query("tasks", new String[]{"account_number,task_id, call_complete,ticket_type"}, "task_complete=0", null, null, null, "ticket_type");
        Cursor callData = getCallCount();
        while (callData.moveToNext()) {
            callCount = callData.getString(0);
        }

        if(callCount.length()>0) {
            String call[] = callCount.split(",");
            callcount = call[0];
            first_call = call[1];
            sec_call = call[2];
            visit_call = call[3];
            repo_call = call[4];
            //  call_count = "8";
            int number = Integer.parseInt(callcount.toString());
            int total_count = Integer.parseInt(callcount.toString());
            int firstcall = Integer.parseInt(first_call.toString());
            int secondcall = Integer.parseInt(sec_call.toString());
            int Visitcall = Integer.parseInt(visit_call.toString());
            int Repocalls = Integer.parseInt(repo_call.toString());
            int num = 0;

            int i = 0; // 1st call;
            int j  = 0; // 2nd call;
            int k = 0; // Visit call;
            int l = 0; // Repo call;
            // Check testing

  /*  firstcall = 0;
    secondcall = 2;
    Visitcall = 8;
    Repocalls = 0;*/

  /*firstcall = 5;
    secondcall = 5;
    Visitcall = 6;
    Repocalls = 3;
    number = 18;*/

            int callsize = calData.size();
            int listsizee = listData.size();


            if (firstcall >= 20 && secondcall >=10 && Visitcall >=10 && Repocalls >=10){
                i=20;j=10;k=10;l=10;
            }else if (firstcall >= 13 && secondcall >=13&& Visitcall >= 12 && Repocalls >=12){
                i=13;j=13;k=12;l=12;}
            else if (firstcall >= 26 && secondcall >=25 && Visitcall >=0 && Repocalls >=0){
                i=25;j=25;k=0;l=0;
            }else if (firstcall >= 26 && secondcall >=0&& Visitcall >= 25 && Repocalls >=0 ){
                i = 25;j=0;k=25;l=0;
            }else if (firstcall >= 26 && secondcall >=0&& Visitcall >= 0 && Repocalls >=25){
                i =25;j=0;k=0;l=25;
            }else if (firstcall >= 25 && secondcall >=26&& Visitcall >= 0 && Repocalls >=0){
                i = 25;j=25;k=0;l=0;
            }else if (firstcall >= 0 && secondcall >=26&& Visitcall >= 25 && Repocalls >=0 ){
                i =0;j=25;k=25;l=0;
            }else if (firstcall >= 0 && secondcall >=26&& Visitcall >= 0 && Repocalls >=25 ){
                i=0;j=25;k=0;l=25;
            }else if (firstcall >= 25 && secondcall >=0&& Visitcall >= 26 && Repocalls >=0 ){
                i = 25;j=0;k=25;l=0;
            }else if (firstcall >= 0 && secondcall >=25&& Visitcall >= 26 && Repocalls >=0 ){
                i = 0;j=25;k=25;l=0;
            }else if (firstcall >= 0 && secondcall >=0&& Visitcall >= 26 && Repocalls >=26 ){
                i = 0;j=0;k=25;l=25;
            }else if (firstcall >=25 && secondcall >=0&& Visitcall >= 0&& Repocalls >=26 ){
                i = 25;j=0;k=0;l=25;
            }else if (firstcall >= 0 && secondcall >=25&& Visitcall >= 0 && Repocalls >=26 ){
                i = 0;j=25;k=0;l=25;
            }else if (firstcall >= 0 && secondcall >=0&& Visitcall >= 25 && Repocalls >=26 ){
                i = 0;j=0;k=25;l=25;
            }else if (firstcall >= 13 && secondcall >=13&& Visitcall >= 24 && Repocalls >=0 ){
                i =13;j=13;k=24;l=0;
            }else if (firstcall >= 13 && secondcall >=13&& Visitcall >= 0 && Repocalls >=24 ){
                i = 13;j=13;k=0;l=24;
            }else if (firstcall >= 24 && secondcall >=13&& Visitcall >= 13 && Repocalls >=0 ){
                i =24;j= 13;k=13;l=0;
            }else if (firstcall >= 0 && secondcall >=24&& Visitcall >= 13 && Repocalls >=13 ){
                i = 0;j=24;k=13;l=13;
            }else if (firstcall >= 26 && secondcall >=8&& Visitcall >= 8 && Repocalls >=8 ){
                i =26;j=8;k=8;l=8;
            }else if (firstcall >= 8 && secondcall >=26&& Visitcall >=8 && Repocalls >=8 ){
                i = 8;j=26;k=8;l=8;
            }
            else if (firstcall >= 8 && secondcall >=8&& Visitcall >=26 && Repocalls >=8 ){
                i = 8;j=8;k=26;l=8;
            }
            else if (firstcall >= 8 && secondcall >=8&& Visitcall >=8 && Repocalls >=26 ){
                i = 8;j=8;k=8;l=26;
            }else if (firstcall >= 34 && secondcall >=0&& Visitcall >=8 && Repocalls >=8 ){
                i = 34;j=0;k=8;l=8;
            }
            else if (firstcall >= 8 && secondcall >=34&& Visitcall >=0 && Repocalls >=8 ){
                i = 8;j=34;k=0;l=8;
            }
            else if (firstcall >= 8 && secondcall >=8&& Visitcall >=34 && Repocalls >=0 ){
                i = 8;j=8;k=34;l=0;
            }
            else if (firstcall >= 0 && secondcall >=8&& Visitcall >=8 && Repocalls >=34 ){
                i = 0;j=8;k=8;l=34;
            }
            else if (firstcall >= 20 && secondcall >=30&& Visitcall >=0 && Repocalls >=0 ){
                i = 20;j=30;k=0;l=0;
            }
            else if (firstcall >= 30 && secondcall >=20&& Visitcall >=0 && Repocalls >=0 ){
                i = 30;j=20;k=0;l=0;
            }
            else if (firstcall >= 0 && secondcall >=0&& Visitcall >=20 && Repocalls >=30 ){
                i = 0;j=0;k=20;l=30;
            }
            else if (firstcall >= 0 && secondcall >=0&& Visitcall >=30 && Repocalls >=20 ){
                i = 0;j=0;k=30;l=20;
            }
            else if (firstcall >= 0 && secondcall >=20&& Visitcall >=30 && Repocalls >=0 ){
                i = 0;j=20;k=30;l=0;
            }
            else if (firstcall >= 0 && secondcall >=30&& Visitcall >=20 && Repocalls >=0 ){
                i = 0;j=30;k=20;l=0;
            }
            else if (firstcall >= 20 && secondcall >=0&& Visitcall >=0 && Repocalls >=30 ){
                i = 20;j=0;k=0;l=30;
            }
            else if (firstcall >= 30 && secondcall >=0&& Visitcall >=0 && Repocalls >=20 ){
                i = 30;j=0;k=0;l=20;
            }
            else if (firstcall >=41 && secondcall >=10 && Visitcall >=0 && Repocalls >=0 ){
                i=40;j=10;k=0;l=0;
            } else if (firstcall >=10 && secondcall >=41 && Visitcall >=0 && Repocalls >=0 ){
                i=10;j=40;k=0;l=0;
            } else if (firstcall >=0 && secondcall >=0 && Visitcall >=41 && Repocalls >=10 ){
                i=10;j=0;k=40;l=10;
            }else if (firstcall >=10 && secondcall >=0 && Visitcall >=0 && Repocalls >=41){
                i=10;j=40;k=0;l=0;
            }else if (firstcall >= 36&& secondcall >=15 && Visitcall >=0 && Repocalls >=0 ){
                i=36;j=15;k=0;l=0;
            }else if (firstcall >= 15&& secondcall >=36 && Visitcall >=0 && Repocalls >=0 ){
                i=15;j=36;k=0;l=0;
            }else if (firstcall >= 0&& secondcall >=15 && Visitcall >=36 && Repocalls >=0 ){
                i=0;j=15;k=36;l=0;
            }else if (firstcall >= 51&& secondcall >= 0 && Visitcall >= 0 && Repocalls >=0){
                i = 50;j=0;k=0;l=0;
            }else if (firstcall >= 0&& secondcall >= 51 && Visitcall >= 0 && Repocalls >=0){
                i = 0;j=50;k=0;l=0;
            }else if (firstcall >= 0&& secondcall >= 0 && Visitcall >= 51 && Repocalls >=0){
                i = 0;j=0;k=50;l=0;
            }else if (firstcall >= 0&& secondcall >= 0 && Visitcall >= 0 && Repocalls >=51){
                i = 0;j=0;k=0;l=50;
            }else if (firstcall >= 22&& secondcall >= 22 && Visitcall >= 2 && Repocalls >=10){
                i= 22 ; j= 22 ;k=1;l=9 ;
            }
            else if (firstcall >= 2&& secondcall >= 22 && Visitcall >= 22 && Repocalls >=10){
                i= 1 ; j= 22 ;k=22;l=9 ;
            }
            else if (firstcall >= 22&& secondcall >= 2 && Visitcall >= 22 && Repocalls >=10){
                i= 22 ; j= 1 ;k=22;l=9 ;
            }
            else if (firstcall >= 22&& secondcall >= 22 && Visitcall >= 10 && Repocalls >=2){
                i= 22 ; j= 22 ;k=9;l=1 ;
            }
            else if (firstcall >= 22&& secondcall >= 10 && Visitcall >= 22 && Repocalls >=2){
                i= 22 ; j= 9 ;k=22;l=1 ;
            }  else if (firstcall >= 10&& secondcall >= 22 && Visitcall >= 22 && Repocalls >=2){
                i= 9 ; j= 22 ;k=22;l=1 ;
            }





/*



if (firstcall >= 5 && secondcall >=3 && Visitcall >= 1 && Repocalls >=1){
    i =5;j = 3;l = 1;k= 1;num = i+j+k+l;
}else if ( firstcall <=0 && secondcall >= 8 && Visitcall >=1 && Repocalls >=1){
    i=0;j = 8;l= 1;k = 1;
}else if (firstcall <=5 && secondcall <=3 && Visitcall<=1&& Repocalls<=2 ){
    i = 4;j= 2;k = 1;l =2;
}else if (firstcall >= 10 && secondcall >= 2&& Visitcall>=1&& Repocalls>=1){
    i=6;j=2;k= 1;l =1;
}else if (firstcall>= 10 && secondcall<= 0&& Visitcall>=1 && Repocalls >=1){
    i=8;k=1;j=0;l=1;
}else if (firstcall>=6 && secondcall>= 3&& Visitcall<=0 && Repocalls >=1){
    i=6;j=3;k=0;l=1;
}else if (firstcall >=7 && secondcall>=2&&Visitcall>=1&&Repocalls<=0){
    i=7;j=2;k=1;l=0;
}else if (firstcall >= 5 && secondcall >=2&&Visitcall>=1 &&Repocalls>=1){
    i = 5;j=3;k=1;l=1;
}else if (firstcall<=0 && secondcall >=3 && Visitcall>=5&&Repocalls>=1){
    i =0;j=3;k=5;l=1;
}
else if (firstcall<=0 && secondcall >=3 && Visitcall>=1&&Repocalls>=5){
 i = 0;j=3;k=1;l=5;
}
else if (firstcall >=10&& secondcall<=0&&Visitcall<=0&&Repocalls<=0){
    i = 10;j=0;k=0;l=0;
} else if (firstcall <=0&& secondcall>=10&&Visitcall<=0&&Repocalls<=0){
    i = 0;j=10;k=0;l=0;
} else if (firstcall <=0&& secondcall<=0&&Visitcall>=10&&Repocalls<=0){
    i = 0;j=0;k=10;l=0;
} else if (firstcall <=0&& secondcall<=0&&Visitcall<=0&&Repocalls>=10){
    i = 0;j=0;k=0;l=10;
}else if (firstcall>=8&& secondcall>=2&& Visitcall<=0&& Repocalls<=0){
    i=8;j=2;k=0;l=0;
}
else if (firstcall<=0&&secondcall<=0&&Visitcall<=8&& Repocalls<=2){
    i =0;j=0;k=8;l=2;
}

else if (firstcall<=0&&secondcall<=0&&Visitcall<=2&& Repocalls<=8){
    i =0;j=0;k=2;l=8;
}
else if (firstcall>=8&& secondcall<=0&&Visitcall>=2&&Repocalls>=0){
    i =8;j=0;k=2;l=0;
}else if (firstcall>=8&& secondcall<=0&&Visitcall<=0&&Repocalls>=2){
    i =8;j=0;k=0;l=2;
}else if (firstcall>=2&&secondcall>=8&&Visitcall<=0&&Repocalls<=0){
    i =2;j=8;k=0;l=0;
}else if (firstcall<=0&& secondcall>=8&& Visitcall>=2&&Repocalls<=0){
    i=0;j=8;k=2;l=0;
}else if (firstcall<=0&& secondcall>=8&& Visitcall<=0&&Repocalls>=2){
    i=0;j=8;k=0;l=2;
}else if (firstcall>=2&& secondcall<=0&& Visitcall>=8&&Repocalls<=0){
    i=2;j=0;k=8;l=0;
}else if (firstcall<=0&& secondcall>=2&& Visitcall>=8&&Repocalls<=0){
    i=0;j=2;k=8;l=0;
} else if (firstcall<=0&& secondcall<=0&& Visitcall>=8&&Repocalls>=2){
    i=0;j=0;k=8;l=2;
} else if (firstcall>=2&& secondcall<=0&& Visitcall<=0&&Repocalls>=8){
    i=2;j=0;k=0;l=8;
} else if (firstcall<=0&& secondcall>=2&& Visitcall<=0&&Repocalls>=8){
    i=0;j=2;k=0;l=8;
}
else if (firstcall<=0&& secondcall<=0&& Visitcall>=2&&Repocalls>=8){
    i=0;j=0;k=2;l=8;
}else if (firstcall>=8 && secondcall>= 1&& Visitcall<=0 && Repocalls >=1){
    i=8;j=1;k=0;l=1;
}else if (firstcall>=4 && secondcall >=3 && Visitcall >=3 && Repocalls<=0){
    i=4;j=3;k=3;l=0;
}else if (firstcall<=0 && secondcall>=4 && Visitcall>=3 && Repocalls >=3){
    i=0;j=4;k=3;l=3;
}else if (firstcall>=4 && secondcall<=0 && Visitcall >=3 && Repocalls >=3){
i=4;j=0;k=3;l=3;
}else if (firstcall>=4 && secondcall>=3 && Visitcall<=0 && Repocalls>=3){
    i=4;j=3;k=0;l=3;
}
else if (firstcall>=11 && secondcall>=1 && Visitcall<=0 && Repocalls<=0){
    i=9;j=1;k=0;l=0;
}else if (firstcall>=11 && secondcall<=0 && Visitcall>=1 && Repocalls<=0){
    i=9;j=0;k=1;l=0;
}else if (firstcall>=11 && secondcall<=0 && Visitcall<=0 && Repocalls>=1){
    i=9;j=0;k=0;l=1;
}

*/






            if (number > 51) {


                while (data.moveToNext()) {


                    HashMap<String, String> item = new HashMap<String, String>();

                    //((String.valueOf(firstcall) != null && firstcall >= 8) && (listData.size() < i))

//5
                    if ((String.valueOf(firstcall) != null && firstcall >= 3) && (listData.size() < i)) {
                        if ((listData.size() < i) && (data.getString(3).equalsIgnoreCase("1st Call") || listData.size() > i)) {
                            Log.d("Item", String.valueOf(item.size()));
                            item.put(listParameters[0], data.getString(0));
                            item.put(listParameters[1], data.getString(1));
                            item.put(listParameters[2], data.getString(2));
                            item.put(listParameters[3], data.getString(3)); // 1st Call
                            listData.add(item);
                            calData.add(item);
                        } else {
                            item.put(listParameters[0], data.getString(0));
                            item.put(listParameters[1], data.getString(1));
                            item.put(listParameters[2], data.getString(2));
                            item.put(listParameters[3], data.getString(3)); // 1st Call
                            listData.add(item);
                        }
//8
                    } else if ((String.valueOf(secondcall) != null && secondcall >= 1) && (calData.size()) < i+j) {

                        if ((listData.size()+1 > firstcall) && (data.getString(3).equalsIgnoreCase("2nd Call")) && (calData.size() < i+j)) {
                            Log.d("Item", String.valueOf(item.size()));
                            item.put(listParameters[0], data.getString(0));
                            item.put(listParameters[1], data.getString(1));
                            item.put(listParameters[2], data.getString(2));
                            item.put(listParameters[3], data.getString(3)); // 2nd Call
                            listData.add(item);
                            calData.add(item);
                        } else {
                            item.put(listParameters[0], data.getString(0));
                            item.put(listParameters[1], data.getString(1));
                            item.put(listParameters[2], data.getString(2));
                            item.put(listParameters[3], data.getString(3)); // 2nd Call
                            listData.add(item);
                        }
//9
                    } else if ((String.valueOf(Repocalls) != null && Repocalls >= 1) && (calData.size()) < i+j+l) {
                        if (listData.size()+1 > firstcall + secondcall && (data.getString(3).equalsIgnoreCase("Repo")) && (calData.size() < i+j+l)) {

                            item.put(listParameters[0], data.getString(0));
                            item.put(listParameters[1], data.getString(1));
                            item.put(listParameters[2], data.getString(2));
                            item.put(listParameters[3], data.getString(3)); // Repo Call
                            listData.add(item);
                            calData.add(item);
                        } else {
                            item.put(listParameters[0], data.getString(0));
                            item.put(listParameters[1], data.getString(1));
                            item.put(listParameters[2], data.getString(2));
                            item.put(listParameters[3], data.getString(3)); // Repo Call
                            listData.add(item);

                        }
                        //10
                    } else if ((String.valueOf(Visitcall) != null && Visitcall >= 1) && (calData.size()) < i+j+k+l) {
                        if (listData.size()+1 < total_count && (data.getString(3).equalsIgnoreCase("Visit")) && (calData.size() < i+j+k+l)) {

                            item.put(listParameters[0], data.getString(0));
                            item.put(listParameters[1], data.getString(1));
                            item.put(listParameters[2], data.getString(2));
                            item.put(listParameters[3], data.getString(3)); // Visit Call
                            listData.add(item);
                            calData.add(item);
                        } else {
                            item.put(listParameters[0], data.getString(0));
                            item.put(listParameters[1], data.getString(1));
                            item.put(listParameters[2], data.getString(2));
                            item.put(listParameters[3], data.getString(3)); // Visit Call
                            listData.add(item);

                        }
                    }


                }



                Log.d("CA****** Data********", callCount.toString());
            } else {

                while (data.moveToNext()) {
                    HashMap<String, String> item = new HashMap<String, String>();
                    item.put(listParameters[0], data.getString(0));
                    item.put(listParameters[1], data.getString(1));
                    item.put(listParameters[2], data.getString(2));
                    item.put(listParameters[3], data.getString(3));
                    listData.add(item);
                    calData.add(item);
                }
                Log.d("LIst Data ", listData.toString());

            }

        }else {
            while (data.moveToNext()) {
                HashMap<String, String> item = new HashMap<String, String>();
                item.put(listParameters[0], data.getString(0));
                item.put(listParameters[1], data.getString(1));
                item.put(listParameters[2], data.getString(2));
                item.put(listParameters[3], data.getString(3));
                listData.add(item);
                calData.add(item);
            }

        }


        this.close();
        return calData;
    }





/*    public ArrayList<HashMap<String, String>> getTaskListData(String[] listParameters, Context context) {
        checkCallComplete(context);
        ArrayList<HashMap<String, String>> listData = new ArrayList<HashMap<String, String>>();
        ArrayList<HashMap<String, String>> calData = new ArrayList<HashMap<String, String>>();
        int caldatacheck ;
        this.open();

        Cursor data = this.database.query("tasks", new String[]{"account_number,task_id, call_complete,ticket_type"}, "task_complete=0", null, null, null, "ticket_type");
        Cursor callData = getCallCount();
        while (callData.moveToNext()) {
            callCount = callData.getString(0);
        }

if(callCount.length()>0) {
    String call[] = callCount.split(",");
    callcount = call[0];
    first_call = call[1];
    sec_call = call[2];
    visit_call = call[3];
    repo_call = call[4];
    //  call_count = "8";
    int number = Integer.parseInt(callcount.toString());
    int total_count = Integer.parseInt(callcount.toString());
    int firstcall = Integer.parseInt(first_call.toString());
    int secondcall = Integer.parseInt(sec_call.toString());
    int Visitcall = Integer.parseInt(visit_call.toString());
    int Repocalls = Integer.parseInt(repo_call.toString());
int num = 0;

   int i = 0; // 1st call;
    int j  = 0; // 2nd call;
    int k = 0; // Visit call;
    int l = 0; // Repo call;
    // Check testing

  *//*  firstcall = 0;
    secondcall = 2;
    Visitcall = 8;
    Repocalls = 0;*//*

  *//*firstcall = 5;
    secondcall = 5;
    Visitcall = 6;
    Repocalls = 3;
    number = 18;*//*

    int callsize = calData.size();
    int listsizee = listData.size();


    if (firstcall >= 20 && secondcall >=10 && Visitcall >=10 && Repocalls >=10){
        i=20;j=10;k=10;l=10;
    }else if (firstcall >= 13 && secondcall >=13&& Visitcall >= 12 && Repocalls >=12){
     i=13;j=13;k=12;l=12;}
    else if (firstcall >= 26 && secondcall >=25 && Visitcall >=0 && Repocalls >=0){
     i=25;j=25;k=0;l=0;
    }else if (firstcall >= 26 && secondcall >=0&& Visitcall >= 25 && Repocalls >=0 ){
        i = 25;j=0;k=25;l=0;
    }else if (firstcall >= 26 && secondcall >=0&& Visitcall >= 0 && Repocalls >=25){
        i =25;j=0;k=0;l=25;
    }else if (firstcall >= 25 && secondcall >=26&& Visitcall >= 0 && Repocalls >=0){
        i = 25;j=25;k=0;l=0;
    }else if (firstcall >= 0 && secondcall >=26&& Visitcall >= 25 && Repocalls >=0 ){
        i =0;j=25;k=25;l=0;
    }else if (firstcall >= 0 && secondcall >=26&& Visitcall >= 0 && Repocalls >=25 ){
        i=0;j=25;k=0;l=25;
    }else if (firstcall >= 25 && secondcall >=0&& Visitcall >= 26 && Repocalls >=0 ){
        i = 25;j=0;k=25;l=0;
    }else if (firstcall >= 0 && secondcall >=25&& Visitcall >= 26 && Repocalls >=0 ){
        i = 0;j=25;k=25;l=0;
    }else if (firstcall >= 0 && secondcall >=0&& Visitcall >= 26 && Repocalls >=26 ){
        i = 0;j=0;k=25;l=25;
    }else if (firstcall >=25 && secondcall >=0&& Visitcall >= 0&& Repocalls >=26 ){
        i = 25;j=0;k=0;l=25;
    }else if (firstcall >= 0 && secondcall >=25&& Visitcall >= 0 && Repocalls >=26 ){
        i = 0;j=25;k=0;l=25;
    }else if (firstcall >= 0 && secondcall >=0&& Visitcall >= 25 && Repocalls >=26 ){
        i = 0;j=0;k=25;l=25;
    }else if (firstcall >= 13 && secondcall >=13&& Visitcall >= 24 && Repocalls >=0 ){
    i =13;j=13;k=24;l=0;
    }else if (firstcall >= 13 && secondcall >=13&& Visitcall >= 0 && Repocalls >=24 ){
        i = 13;j=13;k=0;l=24;
    }else if (firstcall >= 24 && secondcall >=13&& Visitcall >= 13 && Repocalls >=0 ){
        i =24;j= 13;k=13;l=0;
    }else if (firstcall >= 0 && secondcall >=24&& Visitcall >= 13 && Repocalls >=13 ){
        i = 0;j=24;k=13;l=13;
    }else if (firstcall >= 26 && secondcall >=8&& Visitcall >= 8 && Repocalls >=8 ){
        i =26;j=8;k=8;l=8;
    }else if (firstcall >= 8 && secondcall >=26&& Visitcall >=8 && Repocalls >=8 ){
        i = 8;j=26;k=8;l=8;
    }
    else if (firstcall >= 8 && secondcall >=8&& Visitcall >=26 && Repocalls >=8 ){
        i = 8;j=8;k=26;l=8;
    }
    else if (firstcall >= 8 && secondcall >=8&& Visitcall >=8 && Repocalls >=26 ){
        i = 8;j=8;k=8;l=26;
    }else if (firstcall >= 34 && secondcall >=0&& Visitcall >=8 && Repocalls >=8 ){
        i = 34;j=0;k=8;l=8;
    }
    else if (firstcall >= 8 && secondcall >=34&& Visitcall >=0 && Repocalls >=8 ){
        i = 8;j=34;k=0;l=8;
    }
    else if (firstcall >= 8 && secondcall >=8&& Visitcall >=34 && Repocalls >=0 ){
        i = 8;j=8;k=34;l=0;
    }
    else if (firstcall >= 0 && secondcall >=8&& Visitcall >=8 && Repocalls >=34 ){
        i = 0;j=8;k=8;l=34;
    }
    else if (firstcall >= 20 && secondcall >=30&& Visitcall >=0 && Repocalls >=0 ){
        i = 20;j=30;k=0;l=0;
    }
    else if (firstcall >= 30 && secondcall >=20&& Visitcall >=0 && Repocalls >=0 ){
        i = 30;j=20;k=0;l=0;
    }
    else if (firstcall >= 0 && secondcall >=0&& Visitcall >=20 && Repocalls >=30 ){
        i = 0;j=0;k=20;l=30;
    }
    else if (firstcall >= 0 && secondcall >=0&& Visitcall >=30 && Repocalls >=20 ){
        i = 0;j=0;k=30;l=20;
    }
    else if (firstcall >= 0 && secondcall >=20&& Visitcall >=30 && Repocalls >=0 ){
        i = 0;j=20;k=30;l=0;
    }
    else if (firstcall >= 0 && secondcall >=30&& Visitcall >=20 && Repocalls >=0 ){
        i = 0;j=30;k=20;l=0;
    }
    else if (firstcall >= 20 && secondcall >=0&& Visitcall >=0 && Repocalls >=30 ){
        i = 20;j=0;k=0;l=30;
    }
    else if (firstcall >= 30 && secondcall >=0&& Visitcall >=0 && Repocalls >=20 ){
        i = 30;j=0;k=0;l=20;
    }
    else if (firstcall >=41 && secondcall >=10 && Visitcall >=0 && Repocalls >=0 ){
            i=40;j=10;k=0;l=0;
    } else if (firstcall >=10 && secondcall >=41 && Visitcall >=0 && Repocalls >=0 ){
        i=10;j=40;k=0;l=0;
    } else if (firstcall >=0 && secondcall >=0 && Visitcall >=41 && Repocalls >=10 ){
        i=10;j=0;k=40;l=10;
    }else if (firstcall >=10 && secondcall >=0 && Visitcall >=0 && Repocalls >=41){
        i=10;j=40;k=0;l=0;
    }else if (firstcall >= 36&& secondcall >=15 && Visitcall >=0 && Repocalls >=0 ){
        i=36;j=15;k=0;l=0;
    }else if (firstcall >= 15&& secondcall >=36 && Visitcall >=0 && Repocalls >=0 ){
        i=15;j=36;k=0;l=0;
    }else if (firstcall >= 0&& secondcall >=15 && Visitcall >=36 && Repocalls >=0 ){
        i=0;j=15;k=36;l=0;
    }else if (firstcall >= 51&& secondcall >= 0 && Visitcall >= 0 && Repocalls >=0){
        i = 50;j=0;k=0;l=0;
    }else if (firstcall >= 0&& secondcall >= 51 && Visitcall >= 0 && Repocalls >=0){
        i = 0;j=50;k=0;l=0;
    }else if (firstcall >= 0&& secondcall >= 0 && Visitcall >= 51 && Repocalls >=0){
        i = 0;j=0;k=50;l=0;
    }else if (firstcall >= 0&& secondcall >= 0 && Visitcall >= 0 && Repocalls >=51){
        i = 0;j=0;k=0;l=50;
    }else if (firstcall >= 20&& secondcall >= 20 && Visitcall >= 0 && Repocalls >=20){

        i= 20; j= 15;k=0;l=15;
    }else if (firstcall >= 0&& secondcall >= 20 && Visitcall >= 20 && Repocalls >=20){
        i= 0;j=20;k=15;l=15;
    }else if (firstcall >= 20&& secondcall >= 0 && Visitcall >= 20 && Repocalls >=20){
        i=20;j=0;k=15;l=15;
    }else if (firstcall >= 20&& secondcall >= 20 && Visitcall >= 20 && Repocalls >=0){
        i=20;j=15;k=15;l=0;
    }else if (firstcall >= 17&& secondcall >= 17 && Visitcall >= 17 && Repocalls >=0){
        i = 17;j=17;k=16;l=0;
    }else if (firstcall >= 0&& secondcall >= 17 && Visitcall >= 17 && Repocalls >=17){
        i=0;j=17;k=16;l=17;
    }else if (firstcall >= 17&& secondcall >= 17 && Visitcall >= 0 && Repocalls >=17){
        i=17;j=17;k=0;l=16;
    }else if (firstcall >= 17&& secondcall >= 0 && Visitcall >= 17 && Repocalls >=17){
        i=17;j=0;k=16;l=17;
    }



*//*



if (firstcall >= 5 && secondcall >=3 && Visitcall >= 1 && Repocalls >=1){
    i =5;j = 3;l = 1;k= 1;num = i+j+k+l;
}else if ( firstcall <=0 && secondcall >= 8 && Visitcall >=1 && Repocalls >=1){
    i=0;j = 8;l= 1;k = 1;
}else if (firstcall <=5 && secondcall <=3 && Visitcall<=1&& Repocalls<=2 ){
    i = 4;j= 2;k = 1;l =2;
}else if (firstcall >= 10 && secondcall >= 2&& Visitcall>=1&& Repocalls>=1){
    i=6;j=2;k= 1;l =1;
}else if (firstcall>= 10 && secondcall<= 0&& Visitcall>=1 && Repocalls >=1){
    i=8;k=1;j=0;l=1;
}else if (firstcall>=6 && secondcall>= 3&& Visitcall<=0 && Repocalls >=1){
    i=6;j=3;k=0;l=1;
}else if (firstcall >=7 && secondcall>=2&&Visitcall>=1&&Repocalls<=0){
    i=7;j=2;k=1;l=0;
}else if (firstcall >= 5 && secondcall >=2&&Visitcall>=1 &&Repocalls>=1){
    i = 5;j=3;k=1;l=1;
}else if (firstcall<=0 && secondcall >=3 && Visitcall>=5&&Repocalls>=1){
    i =0;j=3;k=5;l=1;
}
else if (firstcall<=0 && secondcall >=3 && Visitcall>=1&&Repocalls>=5){
 i = 0;j=3;k=1;l=5;
}
else if (firstcall >=10&& secondcall<=0&&Visitcall<=0&&Repocalls<=0){
    i = 10;j=0;k=0;l=0;
} else if (firstcall <=0&& secondcall>=10&&Visitcall<=0&&Repocalls<=0){
    i = 0;j=10;k=0;l=0;
} else if (firstcall <=0&& secondcall<=0&&Visitcall>=10&&Repocalls<=0){
    i = 0;j=0;k=10;l=0;
} else if (firstcall <=0&& secondcall<=0&&Visitcall<=0&&Repocalls>=10){
    i = 0;j=0;k=0;l=10;
}else if (firstcall>=8&& secondcall>=2&& Visitcall<=0&& Repocalls<=0){
    i=8;j=2;k=0;l=0;
}
else if (firstcall<=0&&secondcall<=0&&Visitcall<=8&& Repocalls<=2){
    i =0;j=0;k=8;l=2;
}

else if (firstcall<=0&&secondcall<=0&&Visitcall<=2&& Repocalls<=8){
    i =0;j=0;k=2;l=8;
}
else if (firstcall>=8&& secondcall<=0&&Visitcall>=2&&Repocalls>=0){
    i =8;j=0;k=2;l=0;
}else if (firstcall>=8&& secondcall<=0&&Visitcall<=0&&Repocalls>=2){
    i =8;j=0;k=0;l=2;
}else if (firstcall>=2&&secondcall>=8&&Visitcall<=0&&Repocalls<=0){
    i =2;j=8;k=0;l=0;
}else if (firstcall<=0&& secondcall>=8&& Visitcall>=2&&Repocalls<=0){
    i=0;j=8;k=2;l=0;
}else if (firstcall<=0&& secondcall>=8&& Visitcall<=0&&Repocalls>=2){
    i=0;j=8;k=0;l=2;
}else if (firstcall>=2&& secondcall<=0&& Visitcall>=8&&Repocalls<=0){
    i=2;j=0;k=8;l=0;
}else if (firstcall<=0&& secondcall>=2&& Visitcall>=8&&Repocalls<=0){
    i=0;j=2;k=8;l=0;
} else if (firstcall<=0&& secondcall<=0&& Visitcall>=8&&Repocalls>=2){
    i=0;j=0;k=8;l=2;
} else if (firstcall>=2&& secondcall<=0&& Visitcall<=0&&Repocalls>=8){
    i=2;j=0;k=0;l=8;
} else if (firstcall<=0&& secondcall>=2&& Visitcall<=0&&Repocalls>=8){
    i=0;j=2;k=0;l=8;
}
else if (firstcall<=0&& secondcall<=0&& Visitcall>=2&&Repocalls>=8){
    i=0;j=0;k=2;l=8;
}else if (firstcall>=8 && secondcall>= 1&& Visitcall<=0 && Repocalls >=1){
    i=8;j=1;k=0;l=1;
}else if (firstcall>=4 && secondcall >=3 && Visitcall >=3 && Repocalls<=0){
    i=4;j=3;k=3;l=0;
}else if (firstcall<=0 && secondcall>=4 && Visitcall>=3 && Repocalls >=3){
    i=0;j=4;k=3;l=3;
}else if (firstcall>=4 && secondcall<=0 && Visitcall >=3 && Repocalls >=3){
i=4;j=0;k=3;l=3;
}else if (firstcall>=4 && secondcall>=3 && Visitcall<=0 && Repocalls>=3){
    i=4;j=3;k=0;l=3;
}
else if (firstcall>=11 && secondcall>=1 && Visitcall<=0 && Repocalls<=0){
    i=9;j=1;k=0;l=0;
}else if (firstcall>=11 && secondcall<=0 && Visitcall>=1 && Repocalls<=0){
    i=9;j=0;k=1;l=0;
}else if (firstcall>=11 && secondcall<=0 && Visitcall<=0 && Repocalls>=1){
    i=9;j=0;k=0;l=1;
}

*//*






    if (number > 51) {


        while (data.moveToNext()) {


            HashMap<String, String> item = new HashMap<String, String>();

            //((String.valueOf(firstcall) != null && firstcall >= 8) && (listData.size() < i))

//5
            if ((String.valueOf(firstcall) != null && firstcall >= 3) && (listData.size() < i)) {
                if ((listData.size() < i) && (data.getString(3).equalsIgnoreCase("1st Call") || listData.size() > i)) {
                    Log.d("Item", String.valueOf(item.size()));
                    item.put(listParameters[0], data.getString(0));
                    item.put(listParameters[1], data.getString(1));
                    item.put(listParameters[2], data.getString(2));
                    item.put(listParameters[3], data.getString(3)); // 1st Call
                    calData.add(item);
                    listData.add(item);

                } else {
                    item.put(listParameters[0], data.getString(0));
                    item.put(listParameters[1], data.getString(1));
                    item.put(listParameters[2], data.getString(2));
                    item.put(listParameters[3], data.getString(3)); // 1st Call
                    listData.add(item);
                }
//8
            } else if ((String.valueOf(secondcall) != null && secondcall >= 1) && (calData.size()) < i+j) {

                if ((listData.size()+1 > firstcall) && (data.getString(3).equalsIgnoreCase("2nd Call")) && (calData.size() < i+j)) {
                    Log.d("Item", String.valueOf(item.size()));
                    item.put(listParameters[0], data.getString(0));
                    item.put(listParameters[1], data.getString(1));
                    item.put(listParameters[2], data.getString(2));
                    item.put(listParameters[3], data.getString(3)); // 2nd Call
                    calData.add(item);
                    listData.add(item);

                } else {
                    item.put(listParameters[0], data.getString(0));
                    item.put(listParameters[1], data.getString(1));
                    item.put(listParameters[2], data.getString(2));
                    item.put(listParameters[3], data.getString(3)); // 2nd Call
                    listData.add(item);
                }
//9
            } else if ((String.valueOf(Repocalls) != null && Repocalls >= 1) && (calData.size()) < i+j+l) {
                if (listData.size()+1 > firstcall + secondcall && (data.getString(3).equalsIgnoreCase("Repo")) && (calData.size() < i+j+l)) {

                    item.put(listParameters[0], data.getString(0));
                    item.put(listParameters[1], data.getString(1));
                    item.put(listParameters[2], data.getString(2));
                    item.put(listParameters[3], data.getString(3)); // Repo Call
                    calData.add(item);
                    listData.add(item);

                } else {
                    item.put(listParameters[0], data.getString(0));
                    item.put(listParameters[1], data.getString(1));
                    item.put(listParameters[2], data.getString(2));
                    item.put(listParameters[3], data.getString(3)); // Repo Call
                    listData.add(item);

                }
                //10
            } else if ((String.valueOf(Visitcall) != null && Visitcall >= 1) && (calData.size()) < i+j+k+l) {
                if (listData.size()+1 < total_count && (data.getString(3).equalsIgnoreCase("Visit")) && (calData.size() < i+j+k+l)) {

                    item.put(listParameters[0], data.getString(0));
                    item.put(listParameters[1], data.getString(1));
                    item.put(listParameters[2], data.getString(2));
                    item.put(listParameters[3], data.getString(3)); // Visit Call
                    calData.add(item);
                    listData.add(item);

                } else {
                    item.put(listParameters[0], data.getString(0));
                    item.put(listParameters[1], data.getString(1));
                    item.put(listParameters[2], data.getString(2));
                    item.put(listParameters[3], data.getString(3)); // Visit Call
                    listData.add(item);

                }
            }


        }





        Log.d("LOGGGGGGGGDATTTTTTTT",calData+"AA"+listData+"CACA"+callCount);



        Log.d("CA****** Data********", callCount.toString());



    } else {

        while (data.moveToNext()) {
            HashMap<String, String> item = new HashMap<String, String>();
            item.put(listParameters[0], data.getString(0));
            item.put(listParameters[1], data.getString(1));
            item.put(listParameters[2], data.getString(2));
            item.put(listParameters[3], data.getString(3));
            listData.add(item);
            calData.add(item);
        }
        Log.d("LIst Data ", listData.toString());

    }
    Log.d("Mioddle@@LOGGGG",calData.toString());

}else {
    while (data.moveToNext()) {
        HashMap<String, String> item = new HashMap<String, String>();
        item.put(listParameters[0], data.getString(0));
        item.put(listParameters[1], data.getString(1));
        item.put(listParameters[2], data.getString(2));
        item.put(listParameters[3], data.getString(3));
        listData.add(item);
        calData.add(item);
    }

}


        this.close();


        Log.d("LISTTTTTTTTTTTt",listData.toString());

        Log.d("LOGGGGGGGGDATTTTTTTT",calData.toString());
        //return  listData;
        return calData;
    }*/

    private class DatabaseOpenHelper extends SQLiteOpenHelper {
        public DatabaseOpenHelper(Context context, String name, CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            String createQuery = "CREATE TABLE IF NOT EXISTS tasks" + "(_id INTEGER PRIMARY KEY, task_id TEXT, call_count TEXT, owner_name TEXT, owner_msisdn TEXT, ticket_created BOOLEAN"
                    + ", total_paid TEXT, product_name TEXT, days_past_due TEXT, ticket_id TEXT, call_complete BOOLEAN, next_payment_date TEXT, call_duration TEXT"
                    + ", account_number TEXT, date_of_latest_payment_utc TEXT, ticket_type TEXT, requester_id TEXT, task_complete BOOLEAN, ticket_solved BOOLEAN);";
            db.execSQL(createQuery);

          /*  String createQuery = "CREATE TABLE tasks" + "(_id INTEGER PRIMARY KEY, task_id TEXT, call_count TEXT, owner_name TEXT, owner_msisdn TEXT, ticket_created BOOLEAN"
                    + ", total_paid TEXT, product_name TEXT, days_past_due TEXT, ticket_id TEXT, call_complete BOOLEAN, next_payment_date TEXT, call_duration TEXT"
                    + ", account_number TEXT, date_of_latest_payment_utc TEXT, ticket_type TEXT, requester_id TEXT, task_complete BOOLEAN, ticket_solved BOOLEAN,ticket_reason TEXT);";
            db.execSQL(createQuery);

          */

              // table for Checking OTP
            String createQuery2 = "CREATE TABLE IF NOT EXISTS OtpCheck"+ "(_id INTEGER PRIMARY KEY, agent_name TEXT, phone_number TEXT, check_otp INTEGER DEFAULT 0);";
            db.execSQL(createQuery2);

// table for logs;
            String createQuery3 = "CREATE TABLE IF NOT EXISTS logs"+"(_id INTEGER PRIMARY KEY, eo_name TEXT, activity_page TEXT, action TEXT, httpCall TEXT, elaspse_time TEXT, timeStamp TEXT, country TEXT);";
      db.execSQL(createQuery3);

        }



        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
           /* db.execSQL("DROP TABLE IF EXISTS tasks");
            db.execSQL("DROP TABLE IF EXISTS OtpCheck");
            db.execSQL("DROP TABLE IF EXISTS logs");
            onCreate(db);*/

            onCreate(db);
        }



    }

    public String getCompletedTickets() {
        String completedTicketIDs = "";
        this.open();
        Cursor data = this.database.query("tasks", new String[]{"ticket_id"}, "task_complete=1 AND ticket_solved=0", null, null, null, null);
        while (data.moveToNext()) {
            completedTicketIDs = completedTicketIDs + data.getString(0) + ",";
        }
        this.close();
        if (!completedTicketIDs.equals("")) {
            completedTicketIDs = completedTicketIDs.substring(0, completedTicketIDs.length() - 1);
        }
        return completedTicketIDs;
    }

    public Cursor getCompletedTicketsInformation() {
        // getCompletedticket information
        return this.database.query("tasks", new String[]{"requester_id,next_payment_date,ticket_type,task_id, call_duration"}, "task_complete=1 AND ticket_created=0 AND ticket_type LIKE '%Call'", null, null, null, null);
    }

    public void updateTicketCreated(String taskID, ContentValues ticketCreated) {
        // ticket created by database
        database.update("tasks", ticketCreated, "task_id=?", new String[]{taskID});
    }

    public String getLogs(String eo_name){

        String logsData = "";

        Cursor cursor =  this.database.query("logs", new String[]{"eo_name","activity_page","action","httpCall","elaspse_time","timeStamp","country"},null,null,null,null,null);

        while (cursor.moveToNext()) {
            logsData = logsData + cursor.getString(0)+"," +cursor.getString(1)+"," +cursor.getString(2)+"," +cursor.getString(3)+"," +cursor.getString(4)+"," +cursor.getString(5)+"," +cursor.getString(6)+"&";
        }
        this.close();
        if (!logsData.equals("")) {
            logsData = logsData.substring(0, logsData.length() - 1);
        }

       // return this.database.query("logs", null, "task_id=?", new String[]{taskID}, null, null, null);

       // return this.database.query("logs",null,"eo_name=?",new String[]{eo_name},null,null,null);

        System.out.println("*****"+logsData.toString());

          return logsData;

    }

    public Cursor getCallCount() {

        return this.database.query("tasks", new String[]{"call_count"}, null, null, null, null, null);
    }

    public Cursor getCount(){
     /*   ContentValues Cv = new ContentValues();
        Cv.get("ticket_id");
        for (int i = 0; i < ticketIDs.length; i++) {
            database.update("tasks", ticketSolved, "ticket_id=?", new String[]{ticketIDs[i]});
        }*/
        return this.database.query("tasks",new String[]{"ticket_type"}, null, null, null,null,null);
    }

    public void updateTicketSolved(String[] ticketIDs) {
        ContentValues ticketSolved = new ContentValues();
        ticketSolved.put("ticket_solved", true);
        this.open();
        for (int i = 0; i < ticketIDs.length; i++) {
            database.update("tasks", ticketSolved, "ticket_id=?", new String[]{ticketIDs[i]});
        }
        this.close();
    }

    public Cursor getarea() {

        return this.database.query("tasks", new String[]{"product_name"}, null, null, null, null, null);
    }

}