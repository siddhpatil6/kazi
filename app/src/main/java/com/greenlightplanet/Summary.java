package com.greenlightplanet;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;


import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.greenlightplanet.kazi.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

/**
 * Created by prateek on 12-02-2018.
 */

public class Summary extends Activity   implements NavigationView.OnNavigationItemSelectedListener{

    private static final String TAG = Summary.class.getSimpleName();

    private SharedPreferences savedPreferences;
    private SharedPreferences.Editor preferencesEditor;
    DrawerLayout drawer;
    int scoreweek;
    public String  call_count = "";
    public String firstcall = "";
    public String seccall = "";
    public String visitcall = "";
    public String repocall = "";
    public String taskCount = "task";
    public String callCount,DataCall = "";
    double final_commision_tier;
    int agent_checkOTP = 0;
    public TextView txt_status , txtagentname;
    private ProgressDialog progressDialog;
    String agentName,country,curent_week,agentScores,current_month,cmsd,week_to_date,commision_tier,this_week_collection,disabled_unit_count,total_unpaid_unit,eo_phone_Number,current_area,total_possible_incenitive,count_u_under_repayment,count_unit_lmsd,sum_balance_to_collect,count_unit_repayment;
    private HashMap<String,String> summaryscore;
double final_value;
    Locale myLocale;
    double final_Score;
    double moneyValue;
    String str_moneyValue,str_total_possible_incenitive=null;
    double third_day_score,six_day_score,one_day_score,two_day_score,forth_day_score,fivth_day_score,seven_day_score;
    double commion_tier,formatter_incentive_value;
    Float comml;
    int finaltier;
    String currency;
    Context mContext;
    HashMap<String, String> Summary_data = new HashMap<>();
    TextView txt_curreny_month,txt_current_week,txt_last_month_sd,txt_week_todate_score,txt_commision_tier,txt_this_week_collection,txt_disabled_unit_count,txt_total_unpaid_unit,txt_total_possible_inc,txt_total_possible_inc2;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        mContext = Summary.this;



        setContentView(R.layout.summary);
        txt_current_week = (TextView) findViewById(R.id.agent_nw_sale_this_week);
        txt_curreny_month = (TextView) findViewById(R.id.agent_nw_sale_this_month);
        txt_last_month_sd = (TextView) findViewById(R.id.agent_nw_sale_this_last_month);
        txt_current_week = (TextView) findViewById(R.id.agent_nw_sale_this_week);
        txt_week_todate_score = (TextView) findViewById(R.id.agent_nw_inc_current_score);
        txt_commision_tier = (TextView) findViewById(R.id.agent_nw_inc_commision_per);
        txt_this_week_collection = (TextView) findViewById(R.id.agent_nw_week_collection);
        txt_disabled_unit_count = (TextView) findViewById(R.id.agent_nw_dis_unit_count);
        txt_total_unpaid_unit = (TextView) findViewById(R.id.agent_nw_total_unpaid_unit);
      //  txt_total_possible_inc =(TextView) findViewById(R.id.agent_nw_total_possible_inc);
        txt_total_possible_inc2 = (TextView) findViewById(R.id.agent_nw_total_possible_inc_2);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        progressDialog = new ProgressDialog(Summary.this);
        progressDialog.setTitle(getString(R.string.progress_bar_title));
        progressDialog.setMessage(getString(R.string.progress_bar_message));
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);


        navigationView = (NavigationView) findViewById(R.id.nav_view);
//    View headerView = LayoutInflater.from(this).inflate(R.layout.nav_header_main, navigationView, false);
        navigationView.setNavigationItemSelectedListener(this);
        //  navigationView.addHeaderView(headerView);
        View header=navigationView.getHeaderView(0);
        txtagentname = (TextView) header.findViewById(R.id.txt_agent_name);








        savedPreferences = getSharedPreferences("preferences", MODE_PRIVATE); // loads the preferences of the user
        agentName = savedPreferences.getString("agent", "None");
        country = savedPreferences.getString("agentCountry","None");
        agentScores = savedPreferences.getString("finalScore","None");


      /*  Gson gson = new Gson();
        String json = savedPreferences.getString("finalScore","");
       // java.lang.reflect.Type type = new TypeToken<HashMap<String ,String>>(){}.getType();
        java.lang.reflect.Type type = new TypeToken<HashMap<Integer,HashMap>>(){}.getType();
        HashMap<Integer,HashMap> obj = gson.fromJson(json, type);*/
if (agentScores != null) {

    JsonObject jsonObject = new JsonParser().parse(agentScores).getAsJsonObject();
     third_day_score = Double.parseDouble(String.valueOf(jsonObject.get("third_day_score").getAsDouble()));
    six_day_score = Double.parseDouble(String.valueOf(jsonObject.get("six_day_score").getAsDouble()));
    one_day_score = Double.parseDouble(String.valueOf(jsonObject.get("one_day_score").getAsDouble()));
     two_day_score = Double.parseDouble(String.valueOf(jsonObject.get("two_day_score").getAsDouble()));
     forth_day_score = Double.parseDouble(String.valueOf(jsonObject.get("forth_day_score").getAsDouble()));
     fivth_day_score = Double.parseDouble(String.valueOf(jsonObject.get("fivth_day_score").getAsDouble()));
     seven_day_score = Double.parseDouble(String.valueOf(jsonObject.get("seven_day_score").getAsDouble()));

}


        txtagentname.setText(agentName.toString());

        if (new GetData().isNetworkAvailable(Summary.this)) {


            /*long startTime = System.currentTimeMillis();

            String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());*/


//            new summaryScore().execute();

            LoadingDialog.showLoadingDialog(mContext, "Please wait...");
            String tablecode = "2004";
            new GetData().firstCallqueryFilter(mContext, tablecode, agentName, new VolleyCallback() {
                @Override
                public void onSuccess(String result) {


                String name =  agentName;
                    try {

                        JSONArray agentSummaryData = new JSONArray(result);




                        if (!(agentSummaryData == null)) {
                            for (int i = 0; i < agentSummaryData.length(); i++) {
                                JSONObject agentName = (JSONObject) agentSummaryData.get(i);
                                if (agentName.get("accounts.agent").equals(name)) {
                                    cmsd = agentName.getString("accounts.count_units_mtd");
                                    count_unit_repayment = agentName.get("accounts.count_units_under_repayment").toString();
                                    curent_week = agentName.getString("accounts.current_week_sales");
                                    this_week_collection = agentName.getString("payments.current_week_collections");
                                    count_unit_lmsd = agentName.getString("accounts.count_units_lmsd_depricated");
                                    sum_balance_to_collect = agentName.getString("accounts.sum_balance_to_collect");
                                    disabled_unit_count = agentName.getString("accounts.count_disabled_over_3_days");
                                    total_possible_incenitive = agentName.getString("payments.current_week_collections");
                                    current_area = agentName.getString("angaza_users_facts.current_area");
                                    eo_phone_Number = agentName.getString("accounts.agent_phone_number");

                                }

                            }

                         //   Log.d("Value", sum_balance_to_collect + " " + cmsd);

                            unpaidIncentive(); //check the data


                            if (total_possible_incenitive.length() >= 4) {

                                DecimalFormatter(total_possible_incenitive);
                            } else {
                                str_total_possible_incenitive = total_possible_incenitive;
                            }


//                            };

               /* String text = Double.toString(Math.abs(moneyValue));

                int integerPlaces = text.indexOf('.');
                int decimalPlaces = text.length() - integerPlaces - 1;*/
/*
if (integerPlaces == 4){
    DecimalFormat decimalFormat = new DecimalFormat("#,###.00");
   // moneyValue = Double.parseDouble(String.valueOf(Double.parseDouble(decimalFormat.format(moneyValue))));
    moneyValue = Double.parseDouble(decimalFormat.format(moneyValue));

}*/


                            if (country.equalsIgnoreCase("Kenya")) {

                                currency = "KES";
                            } else if (country.equalsIgnoreCase("Uganda")) {

                                currency = "UGX";
                            } else if (country.equalsIgnoreCase("Nigeria")) {

                                currency = "NGN";
                            } else if (country.equalsIgnoreCase("Tanzania")) {

                                currency = "TZS";
                            } else if (country.equalsIgnoreCase("Myanmar (Burma)")) {

                                currency = "MMK";
                            } else if (country.equalsIgnoreCase("India")) {
                                currency = "INR";
                            }


                            txt_current_week.setText(curent_week);
                            txt_curreny_month.setText(cmsd);
                            txt_last_month_sd.setText(count_unit_lmsd);
                            txt_week_todate_score.setText(String.valueOf(final_value));
                            txt_commision_tier.setText(finaltier + " %");
                            txt_this_week_collection.setText(str_total_possible_incenitive + " " + currency);// tottal possible incentive
                            txt_disabled_unit_count.setText(disabled_unit_count);
                            txt_total_unpaid_unit.setText(count_unit_repayment);
//                txt_total_possible_inc.setText(count_unit_repayment);
                            txt_total_possible_inc2.setText(str_moneyValue + " " + currency);



                            LoadingDialog.cancelLoading();

//                                progressDialog.dismiss();


                        }
                    } catch (NumberFormatException ex){

                        createErrorDialogBoxInternetConnection("Something went wrong please try again");
                        progressDialog.dismiss();

                    } catch (Exception e){
                        e.printStackTrace();
                        createErrorDialogBoxInternetConnection("Something went wrong please try again");
                        progressDialog.dismiss();
                    }
                }

            });
        }
            else{

            createErrorDialogBoxInternetConnection(getString(R.string.no_internet_connection));
        }


        FinalDayScore();

        NumberFormat nf = NumberFormat.getNumberInstance(Locale.US);
        DecimalFormat formatter = (DecimalFormat)nf;
        formatter.applyPattern("###.#");

       // NumberFormat df = new DecimalFormat("###.#");



if (scoreweek == 1){

    final_value = one_day_score;

   final_value = Double.parseDouble(formatter.format(final_value));

}else if (scoreweek == 2){

    final_value = two_day_score;
   //final_value= Double.parseDouble(df.format(final_value));
   // String fString = formatter.format(final_value);
final_value = Double.parseDouble(formatter.format(final_value));

}else if (scoreweek ==3){
    final_value = third_day_score;
    final_value = Double.parseDouble(formatter.format(final_value));
}else if (scoreweek == 4){
    final_value = forth_day_score;
   final_value = Double.parseDouble(formatter.format(final_value));
    }else if (scoreweek == 5){
    final_value = fivth_day_score;
  final_value = Double.parseDouble(formatter.format(final_value));
}else if (scoreweek ==6){
    final_value = six_day_score;
 final_value= Double.parseDouble(formatter.format(final_value));
}else if (scoreweek ==7 || scoreweek == 0){
    final_value = seven_day_score;
   final_value = Double.parseDouble(formatter.format(final_value));
}



    CommisionTier();





        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        // setSupportActionBar(toolbar);

        toolbar = (Toolbar) findViewById(R.id.toolbar);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close){

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        }; // Drawer Toggle Object Made
        drawer.setDrawerListener(toggle);
        toggle.syncState();


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (drawer.isDrawerOpen(Gravity.LEFT)) {
                    drawer.closeDrawer(Gravity.LEFT);
                } else {
                    drawer.openDrawer(Gravity.LEFT);
                }
            }
        });




    }


    private void unpaidIncentive() {

        DecimalFormat decimalFormat = new DecimalFormat("#,###");
        DecimalFormat decimalFormat1 = new DecimalFormat("##,###");
        DecimalFormat decimalFormat2 = new DecimalFormat("###,###");
        //DecimalFormat decimalFormat3 = new DecimalFormat("##,##,###");
        DecimalFormat decimalFormat3 = new DecimalFormat("#,###,###");
        DecimalFormat decimalFormat4 = new DecimalFormat("##,###,###");


        try {
          //  Double.parseDouble(df.format(final_value));
       int countpay = Integer.parseInt(sum_balance_to_collect);
          moneyValue   =   Double.parseDouble(String.valueOf(countpay * finaltier))/100;
            String text = Double.toString(Math.abs(moneyValue));
            int integerPlaces = text.indexOf('.');
            int decimalPlaces = text.length() - integerPlaces - 1;

if (integerPlaces == 4) {
 str_moneyValue = decimalFormat.format(moneyValue).toString();
}
else if (integerPlaces == 5){
    str_moneyValue = decimalFormat1.format(moneyValue).toString();

}else if (integerPlaces == 6){

    str_moneyValue = decimalFormat2.format(moneyValue).toString();
}else if (integerPlaces == 7){

    str_moneyValue = decimalFormat3.format(moneyValue).toString();
}else if (integerPlaces == 8){
    str_moneyValue = decimalFormat4.format(moneyValue).toString();
}
else if (integerPlaces==3){
    str_moneyValue = String.valueOf(moneyValue).toString();

}else if (integerPlaces ==2){
    str_moneyValue = String.valueOf(moneyValue).toString();
}else if (integerPlaces == 1){
    str_moneyValue = String.valueOf(moneyValue).toString();

}


        } catch (NumberFormatException ex){
            ex.printStackTrace();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    private void CommisionTier() {



try {

    if (final_value >= 1 && final_value <= 50) {

        finaltier = 6; // old one is 4
// do action
    } else if (final_value >= 50 && final_value <= 60) {

        finaltier = 8; // old one is 6
    } else if (final_value >= 60 && final_value <=100) {

        finaltier = 10; // old one is 8
    }
}catch (NumberFormatException e){
    e.printStackTrace();

}catch (NullPointerException ex){
    ex.printStackTrace();
}

    }

    private int FinalDayScore() {

       int wk = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);

        System.out.println("Week of day :" + wk);
    scoreweek = wk -1;

   return wk;

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            exitByBackKey();

            //moveTaskToBack(false);

            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    protected void exitByBackKey() {

        AlertDialog alertbox = new AlertDialog.Builder(this)
                .setMessage(Summary.this.getString(R.string.back_btn_close))
                .setPositiveButton(Summary.this.getString(R.string.txt_yes), new DialogInterface.OnClickListener() {

                    // do something when the button is clicked
                    public void onClick(DialogInterface arg0, int arg1) {

                        finish();
                        //close();


                    }
                })
                .setNegativeButton(Summary.this.getString(R.string.txt_no), new DialogInterface.OnClickListener() {

                    // do something when the button is clicked
                    public void onClick(DialogInterface arg0, int arg1) {

                        finish();
                        startActivity(getIntent());
                    }
                })
                .show();

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {


        int id = item.getItemId();

        if (id == R.id.nav_task) {
            // Toast.makeText(TaskList.this,"TASK ",Toast.LENGTH_SHORT).show();
            TaskCountDialog();
            // Handle the camera action
        } else if (id == R.id.nav_incentive) {
            // Toast.makeText(TaskList.this,"nav_incentive ",Toast.LENGTH_SHORT).show();
            if (new GetData().isNetworkAvailable(Summary.this)) {
                Intent i = new Intent(Summary.this, Incentives.class);
                i.putExtra("current_area",current_area);
                i.putExtra("eo_phone_Number",eo_phone_Number);
                // i.putExtra("Agentname",agentName);
                startActivity(i);
                this.finish();
            }else {
                createErrorDialogBoxInternetConnection(getString(R.string.no_internet_connection));
            }

        }else if (id == R.id.nav_sum_task){
            Intent i = new Intent(Summary.this,TaskList.class);
            startActivity(i);
            this.finish();
        }
        else if (id == R.id.nav_training) {
            if (new GetData().isNetworkAvailable(Summary.this)) {
            Intent i = new Intent(Summary.this,attendance_module.class);
            i.putExtra("current_area",current_area);
            i.putExtra("country",country);
            startActivity(i);
            this.finish();
            }else {
                createErrorDialogBoxInternetConnection(getString(R.string.no_internet_connection));
            }

        } else if (id == R.id.nav_lagout) {
            //Toast.makeText(TaskList.this,"nav_lagout ",Toast.LENGTH_SHORT).show();
            DiallogLogout();
        }else if (id == R.id.nav_faq){
            // Toast.makeText(TaskList.this,"nav_faq ",Toast.LENGTH_SHORT).show();
            Intent i = new Intent(Summary.this,Faq.class);
            startActivity(i);
            this.finish();
        }else if (id == R.id.nav_update){
            //Toast.makeText(TaskList.this,"Update is on Progress ",Toast.LENGTH_SHORT).show();
            Update();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;


    }

    public void Update() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Summary.this);
        alertDialogBuilder.setIcon(R.drawable.alert_yellow);
        alertDialogBuilder.setTitle(" Update ");
        alertDialogBuilder.setMessage("Please check on google store for new update  ");
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                if (new GetData().isNetworkAvailable(Summary.this)) {


                    final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                    }
                }else {
                    createErrorDialogBoxInternetConnection(getString(R.string.no_internet_connection));

                }
            }
        });
        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel(); // dialog box disappears if user does not confirm task is complete
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void DiallogLogout() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Summary.this);
        alertDialogBuilder.setIcon(R.drawable.alert_yellow);
        alertDialogBuilder.setTitle(R.string.dilaog_logout);
        alertDialogBuilder.setMessage(R.string.dilaog_logout_msg);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                SharedPreferences.Editor editor = savedPreferences.edit();
                editor.clear();
                editor.commit();


                Intent i = new Intent(Summary.this, AgentSelection.class);
                i.putExtra("Login","Languagecheck");
                startActivity(i);
                finish();

            }
        });
        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel(); // dialog box disappears if user does not confirm task is complete
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    };

    public void TaskCountDialog(){

        String version ="";
        String MessageTxt = "";

        callCount = "0";
        //Log.d("InDialofWe;","Welcome"+callCount);
        try {

            PackageManager manager = Summary.this.getPackageManager();
            PackageInfo info = manager.getPackageInfo(
                    Summary.this.getPackageName(), 0);
            version = info.versionName;

            TaskListDatabaseConnector taskListDatabaseConnector = new TaskListDatabaseConnector(Summary.this);
            taskListDatabaseConnector.open();
            Cursor callData = taskListDatabaseConnector.getCallCount();
            //   Cursor callData = taskListDatabaseConnector.getCountTasktype(agentName);

            while (callData.moveToNext()) {
                callCount = callData.getString(0);
            }

            if (callCount.length() > 1) {
                String call[] = callCount.split(",");
                call_count = call[0];
                firstcall = call[1];
                seccall = call[2];
                visitcall = call[3];
                repocall = call[4];
                taskListDatabaseConnector.close();

                MessageTxt = "Hey : " + agentName + "\r\n" + Summary.this.getString(R.string.dilaog_you_have) + "\r\n  "+"  1st Calls   :   "+ firstcall + "\r\n  "+ "  2nd Calls  :   "+ seccall+"\r\n  "+"  Visit           :   "+ visitcall+"\r\n  "+"  Repo          :   "+repocall + "\r\n"+ Summary.this.getString(R.string.dilaog_task_worry)+ "\r\n"+Summary.this.getString(R.string.dialog_complete_it) + "\r\n"+ Summary.this.getString(R.string.dilaog_refresh_task)+"\r\n"+ Summary.this.getString(R.string.txt_app_version) +" "+ version;

            } while (callData.moveToNext()) {
                callCount = callData.getString(0);
            }
            if (callCount.length() > 1) {
                String call[] = callCount.split(",");
                call_count = call[0];
                firstcall = call[1];
                seccall = call[2];
                visitcall = call[3];
                repocall = call[4];
                taskListDatabaseConnector.close();

                MessageTxt = "Hey : " + agentName + "\r\n" + Summary.this.getString(R.string.dilaog_you_have) + "\r\n  "+"  1st Calls   :   "+ firstcall + "\r\n  "+ "  2nd Calls  :   "+ seccall+"\r\n  "+"  Visit           :   "+ visitcall+"\r\n  "+"  Repo          :   "+repocall + "\r\n"+ Summary.this.getString(R.string.dilaog_task_worry)+ "\r\n"+Summary.this.getString(R.string.dialog_complete_it) + "\r\n"+ Summary.this.getString(R.string.dilaog_refresh_task)+"\r\n"+ Summary.this.getString(R.string.txt_app_version) +" "+ version;

            }
            else{
                call_count = "0";
                firstcall = "0";
                seccall = "0";
                visitcall = "0";
                repocall = "0";

                MessageTxt = "Hey : " + agentName + "\r\n" +Summary.this.getString(R.string.dilaog_you_have) +Summary.this.getString(R.string.txt_no_call) + "\r\n"+ Summary.this.getString(R.string.dilaog_task_worry)+ "\r\n"+Summary.this.getString(R.string.dialog_complete_it) + "\r\n"+ Summary.this.getString(R.string.dilaog_refresh_task)+"\r\n"+ Summary.this.getString(R.string.txt_app_version) + version;

            }
            //new TaskListDatabaseConnector(TaskList.this).getCallCount(callCount);
            //  new TaskListDatabaseConnector(TaskList.this).Call(callCount);
            //new GetData().SavingCount(callCount);
       /* if (callCount.equals(null)){
            callCount = "0";
        }*/
        } catch (Exception e) {
            e.printStackTrace();
        }

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Summary.this);
        alertDialogBuilder.setIcon(R.drawable.ic_assignment_turned_in_white_48dp);
        alertDialogBuilder.setTitle(" Task information ");


        alertDialogBuilder.setMessage(MessageTxt);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // check testing
                TaskListDatabaseConnector taskListDatabaseConnector = new TaskListDatabaseConnector(Summary.this);
                taskListDatabaseConnector.open();

              //  Cursor dataLogs = taskListDatabaseConnector.getLogs(agentName);
         String LogsData =  taskListDatabaseConnector.getLogs(agentName);



                String[] separatedLogs = LogsData.split("&");

            //    Log.d("LogsSeperated","LOgs"+separatedLogs[0]);














               // int deletelogs = taskListDatabaseConnector.deleteLogs();


              //  Log.d("Logs","LOgs"+dataLogs);
                //System.out.println(dataLogs);
/*


                while (dataLogs.moveToNext()) {
                    DataCall = dataLogs.getString(0);
                    DataCall = dataLogs.getString(1);
                    DataCall = dataLogs.getString(2);
                    DataCall = dataLogs.getString(3);
                    DataCall = dataLogs.getString(4);
                    DataCall = dataLogs.getString(5);
                    DataCall = dataLogs.getString(6);
                    DataCall = dataLogs.getString(7);
                  //  DataCall = dataLogs.getString(8);
                }
                if (DataCall.length() > 0) {
                    String call[] = DataCall.split(",");
                   String eo_name = call[0];
                   Log.d("Logs Data",eo_name);
                    String activity_page = call[1];
                   String action = call[2];
                    String httpCall = call[3];
                    String elaspe_time = call[4];
                    String timeStamp = call[5];
                    String country = call[6];
                    taskListDatabaseConnector.close();


                }else{

                }
*/


                taskListDatabaseConnector.close();


                dialog.cancel();
            }
        });
   /*     alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel(); // dialog box disappears if user does not confirm task is complete
            }
        });*/
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


    private class summaryScore extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {


            if (new GetData().isNetworkAvailable(Summary.this)) {

                try {

                       String tablecode = "2004";
                    summaryscore = new GetData().getSummaryscore(agentName, tablecode);

                   // summaryscore = new GetData().firstCallqueryFilter(mCon)



                 //   Log.d("Scorecummary",summaryscore.toString());




                } catch (Exception e) {
                }

            }else {
                createErrorDialogBoxInternetConnection(getString(R.string.no_internet_connection));

            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            String txt,key = null,value=null;
            String home,pro = null,boom = null,homeradio = null;
            try {

                int size = summaryscore.size();

                    for (int i = 0; i < summaryscore.size(); i++) {
                        JSONObject jobj = new JSONObject(summaryscore);

                        cmsd = jobj.get("accounts.w_c_u_mtd").toString();
                        count_unit_repayment = jobj.get("accounts.count_units_under_repayment").toString();
                        curent_week = jobj.get("accounts.cws").toString();
                        this_week_collection = jobj.get("payments.current_week_collections").toString();
                        count_unit_lmsd = jobj.get("accounts.w_count_units_lmsd_depricated").toString();
                        sum_balance_to_collect = jobj.get("accounts.sum_balance_to_collect").toString();
                        disabled_unit_count = jobj.get("accounts.count_disabled_over_3_days").toString();
                        total_possible_incenitive = jobj.get("payments.current_week_collections").toString();
                        current_area = jobj.get("angaza_users_facts.current_area").toString();
                        eo_phone_Number = jobj.get("accounts.agent_phone_number").toString();

                    }

//                    Log.d("Value", sum_balance_to_collect + " " + cmsd);

                    unpaidIncentive(); //check the data


                    if (total_possible_incenitive.length() >= 4) {

                        DecimalFormatter(total_possible_incenitive);
                    } else {
                        str_total_possible_incenitive = total_possible_incenitive;
                    }

               /* String text = Double.toString(Math.abs(moneyValue));

                int integerPlaces = text.indexOf('.');
                int decimalPlaces = text.length() - integerPlaces - 1;*/
/*
if (integerPlaces == 4){
    DecimalFormat decimalFormat = new DecimalFormat("#,###.00");
   // moneyValue = Double.parseDouble(String.valueOf(Double.parseDouble(decimalFormat.format(moneyValue))));
    moneyValue = Double.parseDouble(decimalFormat.format(moneyValue));

}*/


if (country.equalsIgnoreCase("Kenya")){

     currency = "KES";
}else if (country.equalsIgnoreCase("Uganda")){

     currency = "UGX";
}else if (country.equalsIgnoreCase("Nigeria")){

     currency = "NGN";
}else if (country.equalsIgnoreCase("Tanzania")){

     currency = "TZS";
}else if (country.equalsIgnoreCase("Myanmar (Burma)")){

    currency = "MMK";
}else if (country.equalsIgnoreCase("India")){
    currency = "INR";
}




                txt_current_week.setText(curent_week);
                txt_curreny_month.setText(cmsd);
                txt_last_month_sd.setText(count_unit_lmsd);
                txt_week_todate_score.setText(String.valueOf(final_value));
                txt_commision_tier.setText(finaltier+" %");
                txt_this_week_collection.setText(str_total_possible_incenitive+" "+currency);// tottal possible incentive
                txt_disabled_unit_count.setText(disabled_unit_count);
                txt_total_unpaid_unit.setText(count_unit_repayment);
//                txt_total_possible_inc.setText(count_unit_repayment);
                txt_total_possible_inc2.setText(str_moneyValue+" "+ currency);




          //      Log.d("summaryScore@@@!!",summaryscore.toString()+current_area);


                progressDialog.dismiss();

            } catch (NumberFormatException ex){

                createErrorDialogBoxInternetConnection("Something went wrong please try again");
                progressDialog.dismiss();

            } catch (Exception e){
                e.printStackTrace();
                createErrorDialogBoxInternetConnection("Something went wrong please try again");
                progressDialog.dismiss();
            }

        }
    }

    private String DecimalFormatter(String total_possible_incenitive) {


        try {


            DecimalFormat decimalFormat = new DecimalFormat("#,###");
            DecimalFormat decimalFormat1 = new DecimalFormat("##,###");
            DecimalFormat decimalFormat2 = new DecimalFormat("###,###");
            DecimalFormat decimalFormat3 = new DecimalFormat("##,##,###");

            String text = Double.toString(Math.abs(Double.parseDouble(total_possible_incenitive)));
            int integerPlaces = text.indexOf('.');


            if (integerPlaces == 4) {
                str_total_possible_incenitive = decimalFormat.format(Double.parseDouble(total_possible_incenitive)).toString();
            }
            else if (integerPlaces == 5){
                str_total_possible_incenitive = decimalFormat1.format(Double.parseDouble(total_possible_incenitive)).toString();

            }else if (integerPlaces == 6){

                str_total_possible_incenitive = decimalFormat2.format(Double.parseDouble(total_possible_incenitive)).toString();
            }else if (integerPlaces == 7){

                str_total_possible_incenitive = decimalFormat3.format(Double.parseDouble(total_possible_incenitive)).toString();
            }




        }catch (NumberFormatException ec){

            ec.printStackTrace();
        }

return str_total_possible_incenitive;

    }


    public void createErrorDialogBoxInternetConnection(String message) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Summary.this);
        alertDialogBuilder.setTitle(getString(R.string.error_dialog_box_header));
        alertDialogBuilder.setMessage(message);


        alertDialogBuilder.setPositiveButton(" OK ", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {

               // finish();

            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setCancelable(false);

        alertDialog.show();


    }
}
