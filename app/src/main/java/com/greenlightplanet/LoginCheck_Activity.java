package com.greenlightplanet;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.Preference;
import android.support.annotation.Nullable;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.greenlightplanet.kazi.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


/**
 * Created by prateek on 28-03-2018.
 */

public class LoginCheck_Activity extends  Activity {
        private ProgressDialog progressDialog;

    private static final String TAG = LoginCheck_Activity.class.getSimpleName();

    LinearLayout ll_password,ll_LOgin,ll_setup_login,ll_updatepassword,ll_passwordd;
    EditText ed_txtmobile,ed_txtpassword,ed_correct_password,passwordcheck,ed_pass1d,ed_pass2d;

    String txtLoginCheck = "None";

    Button btn_login,btncheck,eo_confirmation_button,eo_forgetbutton,btn_update;
    private HashMap<String,String> login_Check = null;
    TextView tv_txtlogin,tv_signup,tv_code,tv_code_l,loginMcheck;
    String Mnumber,country,confirmPassword,Agent_Name = null;
    Preference preference;
    String update_password1,update_password2;
    Context mContext;
    private SharedPreferences savedPreferences;
    private SharedPreferences.Editor preferencesEditor;
    String data_name,data_password,data_mobileNumber = null;
    CheckBox checkBox_pass,login_showpd,create_pdck;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_check);
        mContext = LoginCheck_Activity.this;

        progressDialog = new ProgressDialog(LoginCheck_Activity.this);
        progressDialog.setTitle(getString(R.string.progress_bar_title));
        progressDialog.setMessage(getString(R.string.progress_bar_message));
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        preference = new Preference(mContext);

        Intent intent = getIntent();
        country = intent.getStringExtra("country");

        ed_txtmobile = (EditText) findViewById(R.id.ed_mobile);
        ed_txtpassword = (EditText) findViewById(R.id.ed_pass1);
        ed_correct_password = (EditText) findViewById(R.id.ed_pass2);
        tv_txtlogin = (TextView) findViewById(R.id.tv);
        tv_signup = (TextView) findViewById(R.id.textview);
        tv_code = (TextView) findViewById(R.id.tv_code);
        tv_code_l = (TextView) findViewById(R.id.tv_code_l);
        checkBox_pass = (CheckBox) findViewById(R.id.cbShowPwd);
        login_showpd = (CheckBox) findViewById(R.id.login_showpd);
        create_pdck = (CheckBox) findViewById(R.id.create_pdck);

        btn_login = (Button)findViewById(R.id.login_button);
        btncheck = (Button) findViewById(R.id.checklogin_button);
        ll_password = (LinearLayout) findViewById(R.id.ll_password);
        ll_passwordd = (LinearLayout)findViewById(R.id.ll_passwordd);
        ll_updatepassword = (LinearLayout) findViewById(R.id.ll_updatepassword);
        btn_update = (Button) findViewById(R.id.update);
     ed_pass1d = (EditText) findViewById(R.id.ed_pass1d);
      ed_pass2d = (EditText) findViewById(R.id.ed_pass2d);
        ll_setup_login = (LinearLayout) findViewById(R.id.ll_login);
        ll_LOgin = (LinearLayout) findViewById(R.id.ll_LOgin);
        passwordcheck = (EditText) findViewById(R.id.passwordcheck);
        loginMcheck = (TextView) findViewById(R.id.loginMcheck);
        eo_forgetbutton = (Button) findViewById(R.id.eo_cancelbutton);
        eo_confirmation_button =(Button) findViewById(R.id.eo_confirmation_button);
        if (country.equals("Kenya")) {
            tv_code.setText("+254");
            tv_code_l.setText("+254");
        } else if (country.equals("Nigeria")) {
            tv_code.setText("+234");
            tv_code_l.setText("+234");
        } else if (country.equals("Myanmar (Burma)")) {
            tv_code.setText("+95");
            tv_code_l.setText("+95");
        } else if (country.equals("Uganda")) {
            tv_code.setText("+256");
            tv_code_l.setText("+256");
        } else if (country.equals("Tanzania")) {
            tv_code.setText("+255");
            tv_code_l.setText("+255");
        } else if (country.equals("India")){
            tv_code.setText("+91");
            tv_code_l.setText("+91");
        }


        checkBox_pass.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // checkbox status is changed from uncheck to checked.
                if (!isChecked) {
                    // show password
                    ed_txtpassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    ed_correct_password.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else {
                    // hide password

                    ed_txtpassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    ed_correct_password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());

                }
            }
        });

        login_showpd.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // checkbox status is changed from uncheck to checked.
                if (!isChecked) {
                    // show password
                    passwordcheck.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    } else {
                    // hide password

                    passwordcheck.setTransformationMethod(HideReturnsTransformationMethod.getInstance());

                }
            }
        });


        create_pdck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // checkbox status is changed from uncheck to checked.
                if (!isChecked) {
                    // show password
                    ed_pass1d.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    ed_pass2d.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else {
                    // hide password
                    ed_pass1d.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    ed_pass2d.setTransformationMethod(HideReturnsTransformationMethod.getInstance());

                }
            }
        });


        eo_forgetbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                openDialog();



            }
        });


        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
     update_password1= ed_pass1d.getText().toString();
     update_password2 = ed_pass2d.getText().toString();

                if (update_password1.toString().length() < 1) {
                    Toast.makeText(getApplicationContext(), "Enter password ", Toast.LENGTH_LONG).show();
                }else if (update_password1.contains("%") || update_password2.contains("%")){

                    Toast.makeText(getApplicationContext(), "Sorry % is not allowed", Toast.LENGTH_LONG).show();
                }
                else if (update_password2.toString().length() < 1) {
                    Toast.makeText(getApplicationContext(), "Enter confirm Password", Toast.LENGTH_LONG).show();
                }

          else if (update_password1.equalsIgnoreCase(update_password2)) {
               String confirmpass = update_password2;

               if (Connectivity.isConnected(mContext)) {

                   /*long startTime = System.currentTimeMillis();

                   String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
*/
                   //Mnumber = Mnumber.replaceAll("[+]","");
                   fetchInternetforgetpass(Mnumber, confirmpass);


                //   new GetData().DataBaseurl_login(mContext);

                  /* long elapsedTime = System.currentTimeMillis() - startTime;
                   System.out.println("Total elapsed http request/response time in milliseconds: " + elapsedTime+" Incentive ");
                   TaskListDatabaseConnector taskListDatabaseConnector = new TaskListDatabaseConnector(LoginCheck_Activity.this);
                   taskListDatabaseConnector.insertLogs(Mnumber,TAG,"ForgetPassword","http://13.126.146.126/Sunking_EB/kazi_forget.php"+"?mobileNumber=",String.valueOf(elapsedTime),currentDateTimeString,country);

                   taskListDatabaseConnector.close();
*/

               }
           }else{
               createErrorDialogBoxInternetConnection(getString(R.string.no_internet_connection));

           }

            }
        });
        btncheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                txtLoginCheck = "LOGIN";

                Mnumber = ed_txtmobile.getText().toString();

                loginMcheck.setText(Mnumber);

                if (Mnumber.length()>=8) {

                    if (country.equals("Kenya")) {
                        //  Log.d("Update View","Update");
                        Mnumber = "+254" + Mnumber;
                        tv_code.setText("+254");


                    } else if (country.equals("Nigeria")) {

                        Mnumber = "+234" + Mnumber;
                        tv_code.setText("+234");


                    } else if (country.equals("Myanmar (Burma)")) {

                        Mnumber = "+95" + Mnumber;
                        tv_code.setText("+95");

                    } else if (country.equals("Uganda")) {

                        Mnumber = "+256" + Mnumber;
                        tv_code.setText("+256");
                    } else if (country.equals("Tanzania")) {

                        Mnumber = "+255" + Mnumber;
                        tv_code.setText("+255");

                    } else if (country.equals("India")) {

                        Mnumber = "+91" + Mnumber;

                        tv_code.setText("+91");
                    }

                    if (Connectivity.isConnected(mContext)) {
                        checkMnumber(Mnumber);
                    }else{
                        createErrorDialogBoxInternetConnection(getString(R.string.no_internet_connection));
                    }

                }else{

                    Toast.makeText(getApplicationContext(), "Please check the Mobile Number!!", Toast.LENGTH_LONG).show();

                }
               // new login_check().execute();

            }
        });



        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              String firstpasword =   ed_txtpassword.getText().toString();
                String secoundpasword = ed_correct_password.getText().toString();



              //  final Pattern USER_PASSWORD_PATTERN = Pattern.compile("^[a-zA-Z0-9@.#$%^&*_&\\\\]+$");


                //String digits="0,1,2,3,4,5,6,7,8,9,*@#$%_-\\^.&<qwertzuiopasdfghjklyxcvbnmABCDEFGHIJKLMNOPQRSTUVWXYZ";
                //System.out.println(digits.matches("^[-a-zA-Z0-9@.#$%^&*_&,<\\\\]+$"));

                if (firstpasword.toString().equalsIgnoreCase(secoundpasword.toString())) {


                    confirmPassword = firstpasword;


                   // new SendRequest().execute();


                    if (Mnumber.toString().length() < 1) {
                        Toast.makeText(getApplicationContext(), "Enter User number", Toast.LENGTH_LONG).show();
                    }else if (firstpasword.contains("%") || secoundpasword.contains("%")|| confirmPassword.contains("%")){

                        Toast.makeText(getApplicationContext(), "% is not allowed", Toast.LENGTH_LONG).show();
                    }

                    else if (ed_correct_password.getText().toString().length() < 1) {
                        Toast.makeText(getApplicationContext(), "Enter Password", Toast.LENGTH_LONG).show();
                    } else {

                        if (Connectivity.isConnected(mContext)) {

                            // Retrofit testing
                          //  getdata(Agent_Name.toString(),confirmPassword,Mnumber,country);


                          //  new SendLoginData().execute();
                            //new SendRequest(Agent_Name.toString(), confirmPassword, Mnumber, country).execute();
 //fetchInternet(Agent_Name.toString(), confirmPassword, Mnumber, country);


                           /* long startTime = System.currentTimeMillis();

                            String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
*/
                           fetchingdata(Agent_Name.toString(), confirmPassword, Mnumber, country);


                           /* long elapsedTime = System.currentTimeMillis() - startTime;
                            System.out.println("Total elapsed http request/response time in milliseconds: " + elapsedTime);
                            TaskListDatabaseConnector taskListDatabaseConnector = new TaskListDatabaseConnector(LoginCheck_Activity.this);
                            taskListDatabaseConnector.insertLogs(Mnumber,TAG,"Registering...","http://13.126.146.126/Sunking_EB/register.php"+"?name=",String.valueOf(elapsedTime),currentDateTimeString,country);

                            taskListDatabaseConnector.close();*/

                         //  volleyStringRequst(Agent_Name.toString(), confirmPassword, Mnumber, country);
                        }
                            else
                            createErrorDialogBoxInternetConnection(getString(R.string.no_internet_connection));

                    }


                  //  Toast.makeText(LoginCheck_Activity.this, "your password is correct!!", Toast.LENGTH_LONG).show();

                }else{

                    ed_txtpassword.setText("");
                    ed_correct_password.setText("");
                    Toast.makeText(LoginCheck_Activity.this, "Password doesn't match!!", Toast.LENGTH_LONG).show();

                }

            }
        });

        eo_confirmation_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              // String  User_login =  loginMcheck.getText().toString();
               String User_password =  passwordcheck.getText().toString();


// checking for the login process number and password is entered on db


                        if (User_password.length()>0) {


                            if (Connectivity.isConnected(mContext)) {
                               /* long startTime = System.currentTimeMillis();

                                String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
*/

                                fetchInternet(Mnumber, User_password);


                                /*long elapsedTime = System.currentTimeMillis() - startTime;
                                System.out.println("Total elapsed http request/response time in milliseconds: " + elapsedTime);
                                TaskListDatabaseConnector taskListDatabaseConnector = new TaskListDatabaseConnector(LoginCheck_Activity.this);
                                taskListDatabaseConnector.insertLogs(Mnumber,TAG,"LoginCheck...","http://13.126.146.126/Sunking_EB/kazi_login_check.php"+"?mobileNumber=",String.valueOf(elapsedTime),currentDateTimeString,country);

                                taskListDatabaseConnector.close();*/

                            }
                            else{
                                createErrorDialogBoxInternetConnection(getString(R.string.no_internet_connection));

                            }


                        }else{

                            Toast.makeText(LoginCheck_Activity.this,"Please check your credentials",Toast.LENGTH_LONG).show();

                        }



            }
        });


    }

    private void openDialog(){


        LayoutInflater inflater = LayoutInflater.from(LoginCheck_Activity.this);
        View subView = inflater.inflate(R.layout.forget_layout, null);
        final EditText dialog_ed_mobile = (EditText)subView.findViewById(R.id.dialog_ed_mobile);
        final EditText ed_pass1d = (EditText)subView.findViewById(R.id.ed_pass1d);
        final EditText ed_pass2d = (EditText)subView.findViewById(R.id.ed_pass2d);
        final TextView tv_code_d = (TextView) subView.findViewById(R.id.tv_code_d);


        Drawable drawable = getResources().getDrawable(R.mipmap.ic_launcher);
        //subImageView.setImageDrawable(drawable);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Forget Password");
        builder.setMessage("Update your password!!");
        builder.setView(subView);
        AlertDialog alertDialog = builder.create();

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
              //  textInfo.setText(subEditText.getText().toString());

         String  dialog_mnumber = dialog_ed_mobile.getText().toString();


                if (country.equals("Kenya")) {
                    //  Log.d("Update View","Update");
                    dialog_mnumber = "+254"+dialog_mnumber;

                } else if (country.equals("Nigeria")) {

                    dialog_mnumber = "+234"+dialog_mnumber;

                } else if (country.equals("Myanmar (Burma)")) {

                    dialog_mnumber = "+95"+dialog_mnumber;

                } else if (country.equals("Uganda")) {

                    dialog_mnumber = "+256"+dialog_mnumber;
                } else if (country.equals("Tanzania")) {

                    dialog_mnumber = "+255"+dialog_mnumber;
                } else if (country.equals("India")){

                    dialog_mnumber = "+91"+dialog_mnumber;
                }


                if (Connectivity.isConnected(mContext)) {




                   /* long startTime = System.currentTimeMillis();

                    String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
*/


                    txtLoginCheck = "FORPASS";
                  String tableCode = "2239";
                   firstCallquery(tableCode);
                  //  new login_checkdialog(dialog_mnumber).execute();


                   /* long elapsedTime = System.currentTimeMillis() - startTime;
                    System.out.println("Total elapsed http request/response time in milliseconds: " + elapsedTime);
                    TaskListDatabaseConnector taskListDatabaseConnector = new TaskListDatabaseConnector(LoginCheck_Activity.this);
                  //  taskListDatabaseConnector.insertLogs(Mnumber,TAG,"LoginCheck...","http://13.126.146.126/Sunking_EB/kazi_login_check.php"+"?mobileNumber=",String.valueOf(elapsedTime),currentDateTimeString,country);
                    taskListDatabaseConnector.insertLogs(Mnumber,TAG,"LoginCheck","https://greenlightplanet.looker.com:19999/api/3.0/looks/2239",String.valueOf(elapsedTime),currentDateTimeString,"-");

                    taskListDatabaseConnector.close();
*/








                }





                else{
                    createErrorDialogBoxInternetConnection(getString(R.string.no_internet_connection));

                }


            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(LoginCheck_Activity.this, "Cancel", Toast.LENGTH_LONG).show();
            }
        });

        builder.show();
    }


    public void createErrorDialogBoxInternetConnection(String message) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(LoginCheck_Activity.this);
        alertDialogBuilder.setTitle(getString(R.string.error_dialog_box_header));
        alertDialogBuilder.setMessage(message);


        alertDialogBuilder.setPositiveButton(" OK ", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {
                Toast.makeText(LoginCheck_Activity.this,getString(R.string.no_internet_try_again),Toast.LENGTH_SHORT).show();
                finish();
//    dialog.dismiss();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setCancelable(false);

        alertDialog.show();


    }



    public class login_check extends AsyncTask<Void, Void, Void> {




        @Override
        protected void onPreExecute() {
            progressDialog.show();



        }


        @Override
        protected Void doInBackground(Void... voids) {

            String tablecode = "2239"; // testing for area code

            try {
                login_Check = new GetData().getLogincheck(Mnumber, tablecode);




            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            String user_area = null;
            try {


                if (login_Check.size() > 0) {

                    btncheck.setVisibility(View.GONE);
                    btn_login.setVisibility(View.VISIBLE);

                    Agent_Name = login_Check.get("accounts.agent_username");


                    Agent_Name = Agent_Name.replaceAll(" ", "_").toString();


                    Toast.makeText(LoginCheck_Activity.this,"Please confirm your password!!",Toast.LENGTH_LONG).show();
                    ll_password.setVisibility(View.VISIBLE);
                    ed_txtmobile.setEnabled(false);

//                    ll_LOgin.setVisibility(View.VISIBLE);
  //                  ll_setup_login.setVisibility(View.GONE);
    //                tv_txtlogin.setText("Registered mobile number and password");
      //              tv_signup.setText("Login");




                    progressDialog.dismiss();

/*
                    ed_txtpassword.setVisibility(View.VISIBLE);
                    ed_correct_password.setVisibility(View.VISIBLE);
*/




                } else {


                    Toast.makeText(LoginCheck_Activity.this,"Please check your registered mobile number!!",Toast.LENGTH_LONG).show();

                    progressDialog.dismiss();
                }

            }catch (Exception ex){
                ex.printStackTrace();
            }
        }


    }









   /* public class login_checkdialog extends AsyncTask<Void, Void, Void> {


        public login_checkdialog(String Mnumber) {



        }

        @Override
        protected void onPreExecute() {
            progressDialog.show();



        }


        @Override
        protected Void doInBackground(Void... voids) {

            String tablecode = "2239"; // testing for area code

            try {
                login_Check = new GetData().getLogincheck(Mnumber, tablecode);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            String user_area = null;
            try {


                if (login_Check.size() > 0) {

                    btncheck.setVisibility(View.GONE);
                    btn_login.setVisibility(View.VISIBLE);

                    Agent_Name = login_Check.get("accounts.agent_username");


                    Agent_Name = Agent_Name.replaceAll(" ", "_").toString();


                    Toast.makeText(LoginCheck_Activity.this,"Please confirm your password!!",Toast.LENGTH_LONG).show();


                    ll_LOgin.setVisibility(View.GONE);
                    ll_passwordd.setVisibility(View.VISIBLE);

                    ll_updatepassword.setVisibility(View.VISIBLE);


                    //ll_password.setVisibility(View.VISIBLE);
                    //
                    //ed_txtmobile.setEnabled(false);




//                    ll_LOgin.setVisibility(View.VISIBLE);
                    //                  ll_setup_login.setVisibility(View.GONE);
                    //                tv_txtlogin.setText("Registered mobile number and password");
                    //              tv_signup.setText("Login");




                    progressDialog.dismiss();

*//*
                    ed_txtpassword.setVisibility(View.VISIBLE);
                    ed_correct_password.setVisibility(View.VISIBLE);
*//*




                } else {


                    Toast.makeText(LoginCheck_Activity.this,"Please check your registered mobile number!!",Toast.LENGTH_LONG).show();

                    progressDialog.dismiss();
                }

            }catch (Exception ex){
                ex.printStackTrace();
            }
        }


    }*/


  /*  private void openDialogPassword(final String dialogMnumber){
        LayoutInflater inflater = LayoutInflater.from(LoginCheck_Activity.this);
        View subView = inflater.inflate(R.layout.forget_layout, null);
        final EditText dialog_ed_mobile = (EditText)subView.findViewById(R.id.dialog_ed_mobile);
        final  TextView dialog_Mn = (TextView) subView.findViewById(R.id.dialog_Mn);
        dialog_ed_mobile.setVisibility(View.VISIBLE);
final  LinearLayout ll_password = (LinearLayout) subView.findViewById(R.id.ll_password);
        final EditText ed_pass1d = (EditText)subView.findViewById(R.id.ed_pass1d);
        final EditText ed_pass2d = (EditText)subView.findViewById(R.id.ed_pass2d);
dialog_ed_mobile.setVisibility(View.GONE);
        dialog_Mn.setText(dialogMnumber);


        Drawable drawable = getResources().getDrawable(R.mipmap.ic_launcher);
        //subImageView.setImageDrawable(drawable);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Update Password");
        builder.setMessage("Update your password!!!");
        builder.setView(subView);
        AlertDialog alertDialog = builder.create();

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //  textInfo.setText(subEditText.getText().toString());


         String password1 = ed_pass1d.getText().toString();

         String password2 = ed_pass2d.getText().toString();



         if (password1.equalsIgnoreCase(password2)){

             String confirmpass = password2;

             fetchInternetforgetpass(dialogMnumber,confirmpass);
         }



            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(LoginCheck_Activity.this, "Cancel", Toast.LENGTH_LONG).show();
            }
        });

        builder.show();
    }*/

/*
    public void volleyStringRequst(String name,String password,String Mnumber,String country){

        progressDialog.show();

        String url = "http://13.126.146.126/Sunking_EB/Kazi_login.php/?name="+name+"&password="+password+"&mobileNumber="+Mnumber +"&country="+country;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String jsonStr) {
                        progressDialog.hide();
                        if (jsonStr.equalsIgnoreCase("Success")){
                            Toast.makeText(LoginCheck_Activity.this,"Successfully Registered!!",Toast.LENGTH_LONG).show();

                            Intent i = new Intent(LoginCheck_Activity.this, TaskList.class);
                            finish();
                            startActivity(i);
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.hide();
                // Error handling
                Toast.makeText(LoginCheck_Activity.this,"Something went wrong!!"+error,Toast.LENGTH_LONG).show();

                System.out.println("Something went wrong!");
                error.printStackTrace();

            }
        });
// Add the request to the queue
        Volley.newRequestQueue(this).add(stringRequest);


    }
*/








/*    public class SendRequest extends AsyncTask<String, Void, String> {

        public SendRequest(String name,String password,String mNo,String country) {


        }

        protected void onPreExecute(){}

        protected String doInBackground(String... arg0) {

            try{
                //Change your web app deployed URL or or u can use this for attributes (name, country)
                // URL url = new URL("https://script.google.com/macros/s/AKfycbz0keSwr2EKCPVdYUT8TjYY_5JgjoPNZlLC3yhfbssIysWn2p1g/exec");
                //   URL url = new URL("https://script.google.com/a/greenlightplanet.com/macros/s/AKfycbz2lPzyAhNdBb69v56Ga3Vswu__PWJPHDtf3MZfFPzxWQ44SO5T/exec?id=1RFx_PJbkvIN6HoCTQh-n0b_BHhx1yhohppe3ZfqqyHM&sheet/edit#gid=1287831504=Kenya");

                //   URL url = new URL("https://script.google.com/macros/s/AKfycbxOLElujQcy1-ZUer1KgEvK16gkTLUqYftApjNCM_IRTL3HSuDk/exec?id=1pPvypKyq6TWp5w2cqkbb5XMV6GaxBhjSq_ojs016RIM&sheet=Kenya");

                // URL url = new URL("https://script.google.com/macros/s/AKfycbxMhe7OP4LT4UF775G-vS7SyvWrur9A53I-Za2BZuZ-AkT4rUE/exec?");
                //URL url = new URL("https://script.google.com/macros/s/AKfycbxMhe7OP4LT4UF775G-vS7SyvWrur9A53I-Za2BZuZ-AkT4rUE/exec?&sheet=Uganda");

                URL url = new URL("https://script.google.com/macros/s/AKfycbxMhe7OP4LT4UF775G-vS7SyvWrur9A53I-Za2BZuZ-AkT4rUE/exec?&sheet=Uganda");


                JSONObject postDataParams = new JSONObject();

                //int i;
                //for(i=1;i<=70;i++)

                *//*SimpleDateFormat curFormater = new SimpleDateFormat("dd/MM/yyyy");
                Date dateObj = curFormater.parse(dateStr);
                SimpleDateFormat postFormater = new SimpleDateFormat("MMMM dd, yyyy");

                String newDateStr = postFormater.format(dateObj);*//*

                String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());



                //    String usn = Integer.toString(i);

                String id= "1pPvypKyq6TWp5w2cqkbb5XMV6GaxBhjSq_ojs016RIM";

                postDataParams.put("name",mEmailView.getText().toString());
                postDataParams.put("password",mPasswordView.getText().toString());
                postDataParams.put("country","Kenya");
                postDataParams.put("id",id);


                Log.e("params",postDataParams.toString());

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000 *//* milliseconds *//*);
                conn.setConnectTimeout(15000 *//* milliseconds *//*);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(postDataParams));

                writer.flush();
                writer.close();
                os.close();

                int responseCode=conn.getResponseCode();

                if (responseCode == HttpsURLConnection.HTTP_OK) {

                    BufferedReader in=new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    StringBuffer sb = new StringBuffer("");
                    String line="";

                    while((line = in.readLine()) != null) {

                        sb.append(line);
                        break;
                    }

                    in.close();
                    return sb.toString();

                }
                else {
                    return new String("false : "+responseCode);
                }
            }
            catch(Exception e){
                return new String("Exception: " + e.getMessage());
            }
        }

        @Override
        protected void onPostExecute(String result) {
     *//*       Toast.makeText(getApplicationContext(), result.toString(),
                    Toast.LENGTH_LONG).show();*//*

        }
    }*/






    public  void fetchingdata(String name,String password, String mnumber,String country){

        progressDialog.show();

// Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(this);
       // String url ="http://www.google.com";

        String TxtCountry = country;


        mnumber = mnumber.replaceAll("[+]","");

        if (TxtCountry.equalsIgnoreCase("Myanmar (Burma)")){

            TxtCountry = "Myanmar(Burma)";

        }

        String url ="http://13.126.146.126/Sunking_EB/register.php"+"?name="+name+"&password="+password+"&mobileNumber="+mnumber+"&country="+TxtCountry;


      //  String url ="http://13.126.146.126/Sunking_EB/register.php"+"?name="+name+"&password="+password+"&mobileNumber="+mnumber+"&country="+country;


        //String url = "http://13.126.146.126/Sunking_EB/Kazi_login.php/?name=prateekchaurasia&password=passcheck&mobileNumber=968992783&country=india";

//        String url = "http://13.126.146.126/Sunking_EB/Kazi_login.php"+"?name="+name+"&password="+password+"&mobileNumber="+mnumber+"&country="India";

// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        // mTextView.setText("Response is: "+ response.substring(0,500));
                 //       Log.d("Response",response.toString());

                        if (response.toString().equalsIgnoreCase("Already registered")){

                            Toast.makeText(LoginCheck_Activity.this,"Sorry!! "+response.toString(),Toast.LENGTH_LONG).show();
                            ll_LOgin.setVisibility(View.VISIBLE);
                            ll_setup_login.setVisibility(View.GONE);
                            tv_txtlogin.setText("Registered mobile number and password");
                            tv_signup.setText("Login");

                        }


if (response.toString().equalsIgnoreCase("Error")){

    Toast.makeText(LoginCheck_Activity.this,"Somethingwent wrong!!",Toast.LENGTH_LONG).show();
}

if (response.equalsIgnoreCase("Success")) {

    ll_LOgin.setVisibility(View.VISIBLE);
    ll_setup_login.setVisibility(View.GONE);
    tv_txtlogin.setText("Registered mobile number and password");
    tv_signup.setText("Login");

    Toast.makeText(LoginCheck_Activity.this,"Successfully Logged in!!",Toast.LENGTH_LONG).show();

}else{
    Toast.makeText(LoginCheck_Activity.this,"Somethingwent wrong!!",Toast.LENGTH_LONG).show();

}


/*

                        Intent i = new Intent(LoginCheck_Activity.this, TaskList.class);
                        finish();
                        startActivity(i);

*/

                        progressDialog.hide();
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

       progressDialog.dismiss();
            }
        });

// Add the request to the RequestQueue.
        queue.add(stringRequest);



    }

    public void fetchInternet(String Mnumber,String password) {
        progressDialog.show();



        Mnumber = Mnumber.replaceAll("[+]","");

        String url = "http://13.126.146.126/Sunking_EB/kazi_login_check.php"+"?mobileNumber="+Mnumber+"&password="+password;

     //   String url = "http://13.126.146.126/Sunking_EB/Kazi_login.php"+"?name="+name+"&password="+password+"&mobileNumber="+Mnumber+"&country="+country;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,

                new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                progressDialog.hide();




              //  Log.d("Response check_LOginn",s.toString());
if (s.length()>=3) {


    try {

        JSONArray jsonArray = new JSONArray(s);
        for (int i = 0; i < jsonArray.length(); i++) {

            JSONObject jsonObject = jsonArray.getJSONObject(i);
             data_name = jsonObject.getString("name");
             data_password = jsonObject.getString("password");
             data_mobileNumber = jsonObject.getString("mobileNumber");


        }

      //  Log.d("Data url",data_name+data_password+data_mobileNumber);
        Toast.makeText(LoginCheck_Activity.this, "Successfully Logged in!!", Toast.LENGTH_LONG).show();

        progressDialog.dismiss();
      //  new AgentSelection().displayName(data_name);

        Intent i = new Intent(LoginCheck_Activity.this,AgentSelection.class);
        i.putExtra("Login",data_name);
        i.putExtra("country",country);


        /*preferencesEditor = savedPreferences.edit();
        preferencesEditor.putString("mobileNumber", data_mobileNumber);
        preferencesEditor.apply();*/

        startActivity(i);
        finish();

     //  AgentSelection agentSelection = new AgentSelection.area_filter(data_name);


        //String mobilen = jsonObject.getString();
    } catch (JSONException e) {
        e.printStackTrace();
    }
}else{
    Toast.makeText(LoginCheck_Activity.this,"Please check your credentials",Toast.LENGTH_LONG).show();
}

            }
        },

                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        Toast.makeText(LoginCheck_Activity.this,"Something went wrong!!"+volleyError,Toast.LENGTH_LONG).show();
                        System.out.println("Something went wrong!");
                        volleyError.printStackTrace();
                        progressDialog.dismiss();

                        createErrorDialogBoxInternetConnection(getString(R.string.no_internet_connection));

                    }
                });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }


    public  void checkMnumber(String mnumber){

//        progressDialog.show();

    //    LoadingDialog.showLoadingDialog(mContext,"Please wait....");

// Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(this);
        // String url ="http://www.google.com";

        mnumber = mnumber.replaceAll("[+]","");


     //   Log.d("MND",mnumber);

        String url ="http://13.126.146.126/Sunking_EB/Mnumber_check.php"+"?mobileNumber="+mnumber;



        //String url = "http://13.126.146.126/Sunking_EB/Kazi_login.php/?name=prateekchaurasia&password=passcheck&mobileNumber=968992783&country=india";

//        String url = "http://13.126.146.126/Sunking_EB/Kazi_login.php"+"?name="+name+"&password="+password+"&mobileNumber="+mnumber+"&country="India";

// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        // mTextView.setText("Response is: "+ response.substring(0,500));
                       // Log.d("Response",response.toString());

                        if (response.toString().equalsIgnoreCase("Already registered")){

                            Toast.makeText(LoginCheck_Activity.this,"Sorry!! "+response.toString(),Toast.LENGTH_LONG).show();
                            ll_LOgin.setVisibility(View.VISIBLE);
                            ll_setup_login.setVisibility(View.GONE);
                            tv_txtlogin.setText("Registered mobile number and password");
                            tv_signup.setText("Login");
//                            progressDialog.dismiss();


                      //      LoadingDialog.cancelLoading();



                        }

                        if (response.toString().equalsIgnoreCase("Not registered")){

                            Toast.makeText(LoginCheck_Activity.this,"Sorry!! "+response.toString(),Toast.LENGTH_LONG).show();

                        /*    long startTime = System.currentTimeMillis();

                            String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());*/
                            //new login_check()
                           //new login_check().execute();
                           // volleyLogin("2239",Mnumber);



                            String tableCode = "2239";


                            firstCallquery(tableCode);



                            /*



                            long elapsedTime = System.currentTimeMillis() - startTime;
                            System.out.println("Total elapsed http request/response time in milliseconds: " + elapsedTime+" Incentive ");
                            TaskListDatabaseConnector taskListDatabaseConnector = new TaskListDatabaseConnector(LoginCheck_Activity.this);
                            taskListDatabaseConnector.insertLogs(Mnumber,TAG,"LoginCheck","https://greenlightplanet.looker.com:19999/api/3.0/looks/2239",String.valueOf(elapsedTime),currentDateTimeString,"-");

                            taskListDatabaseConnector.close();
*/

                        }

/*
                        if (response.toString().equalsIgnoreCase("Error")){

                            Toast.makeText(LoginCheck_Activity.this,"Somethingwent wrong!!",Toast.LENGTH_LONG).show();
                        }
else{
                            Toast.makeText(LoginCheck_Activity.this,"Somethingwent wrong!!",Toast.LENGTH_LONG).show();

                        }*/

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

           //     Log.d("Response",volleyError.toString());
                Toast.makeText(LoginCheck_Activity.this,"Somethingwent wrong! try again!",Toast.LENGTH_LONG).show();
                progressDialog.dismiss();

            }
        });

// Add the request to the RequestQueue.
        queue.add(stringRequest);



    }





    public void fetchInternetforgetpass(String Mnumber,String password) {
        progressDialog.show();



        Mnumber = Mnumber.replaceAll("[+]","");

        String url = "http://13.126.146.126/Sunking_EB/kazi_forget.php"+"?mobileNumber="+Mnumber+"&password="+password;

        //   String url = "http://13.126.146.126/Sunking_EB/Kazi_login.php"+"?name="+name+"&password="+password+"&mobileNumber="+Mnumber+"&country="+country;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,

                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        progressDialog.hide();

if (s.equalsIgnoreCase("Success")) {


    ll_LOgin.setVisibility(View.VISIBLE);
    ll_setup_login.setVisibility(View.GONE);
    ll_passwordd.setVisibility(View.GONE);
    tv_txtlogin.setText("Registered mobile number and password");
    tv_signup.setText("Login");


    Toast.makeText(LoginCheck_Activity.this,"Please login with new credentials",Toast.LENGTH_LONG).show();

    /*Intent i = new Intent(LoginCheck_Activity.this, AgentSelection.class);
    i.putExtra("Login", data_name);
    startActivity(i);
    finish();*/
}

if (s.equalsIgnoreCase("Not registered")){

                            Toast.makeText(LoginCheck_Activity.this,"Please check your credentials",Toast.LENGTH_LONG).show();
                        }

                    }
                },

                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        Toast.makeText(LoginCheck_Activity.this,"Something went wrong!!"+volleyError,Toast.LENGTH_LONG).show();
                        System.out.println("Something went wrong!");
                        volleyError.printStackTrace();
                        progressDialog.dismiss();
                    }
                });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }


    public String getPostDataString(JSONObject params) throws Exception {

        StringBuilder result = new StringBuilder();
        boolean first = true;

        Iterator<String> itr = params.keys();

        while(itr.hasNext()){

            String key= itr.next();
            Object value = params.get(key);

            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));

        }
        return result.toString();
    }


    private static final int PERMISSION_REQUEST_CODE = 200;


    public void volleyLogin(String tableCode, final String mnumber) {
        progressDialog.show();


        final String Table_Code = tableCode;

        String url = "https://greenlightplanet.looker.com:19999/api/3.0/login";
        //   String url = "http://13.126.146.126/Sunking_EB/Kazi_login.php"+"?name="+name+"&password="+password+"&mobileNumber="+Mnumber+"&country="+country;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                 //       Log.d("Response",s);

                        progressDialog.dismiss();

                        try {
                            JSONObject js = new JSONObject(s);
                            String access_Token = js.getString("access_token");
                            String token_type = js.getString("token_type");
                            String expires_in = js.getString("expires_in");


                         //   Log.d("RequestResss",access_Token+","+token_type+","+expires_in);


                            if (!(js == null)) {
                                //getTabledata(access_Token, tableCode,mnumber);

                                getTabledata(access_Token,Table_Code,mnumber);


                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Toast.makeText(LoginCheck_Activity.this,"Something went wrong!!"+volleyError,Toast.LENGTH_LONG).show();
                        System.out.println("Something went wrong!");
                        volleyError.printStackTrace();
                        progressDialog.dismiss();
                        createErrorDialogBoxInternetConnection(getString(R.string.no_internet_connection));
                    }
                }){
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("client_id", "QY6DQzQT6NQM3ngbTRxY");
                params.put("client_secret", "PMb9rtjBzrdYgyvqQhfhTjrB");
                return params;
            }
        }  ;
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = NetworkCallSingleton.getInstance(mContext).getRequestQ();
        requestQueue.add(stringRequest);
        // requestQueue.add(nw.MVolleycall(mContext));
        //     NetworkCallSingleton.getInstance().MVolleycall(mContext);
        // NetworkCallSingleton.getInstance().addToRequestQueue(stringRequest);
    }

    public void getTabledata(String accessToken, final String tableCode,String mnumber){



        final String token = accessToken;


        // Log.d(accessToken,tableCode+"@@"+token);

        progressDialog.show();
        String url = "https://greenlightplanet.looker.com:19999/api/3.0/looks/" + tableCode + "/run/json";
        //   String url = "http://13.126.146.126/Sunking_EB/Kazi_login.php"+"?name="+name+"&password="+password+"&mobileNumber="+Mnumber+"&country="+country;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                     //   Log.d("Response",s);




                        progressDialog.dismiss();



                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Toast.makeText(LoginCheck_Activity.this,"Something went wrong!!"+volleyError,Toast.LENGTH_LONG).show();
                        System.out.println("Something went wrong!");
                        volleyError.printStackTrace();
                        progressDialog.dismiss();
                        createErrorDialogBoxInternetConnection(getString(R.string.no_internet_connection));
                    }
                }){
            @Override
            public Map<String, String> getHeaders()
            //   protected Map<String, String> getParams()throws com.android.volley.AuthFailureError
            {
                Map<String, String  >  headers = new HashMap<String, String>();
                headers.put("Authorization","Bearer " +token);
                return headers;

            }

        }  ;
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = NetworkCallSingleton.getInstance(mContext).getRequestQ();
        requestQueue.add(stringRequest);
        // requestQueue.add(nw.MVolleycall(mContext));
        //     NetworkCallSingleton.getInstance().MVolleycall(mContext);





    }



    // login process Specified query


    public String firstCallquery(final String tableCode) {
        //progressDialog.show();

       LoadingDialog.showLoadingDialog(mContext,"Please wait...");

        String url = "https://greenlightplanet.looker.com:19999/api/3.0/login";
        //   String url = "http://13.126.146.126/Sunking_EB/Kazi_login.php"+"?name="+name+"&password="+password+"&mobileNumber="+Mnumber+"&country="+country;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                    //    Log.d("Response",s);


                  //  LoadingDialog.cancelLoading();
                       // progressDialog.dismiss();

                        try {
                            JSONObject js = new JSONObject(s);
                            String access_Token = js.getString("access_token");
                            String token_type = js.getString("token_type");
                            String expires_in = js.getString("expires_in");


                     //       Log.d("RequestResss",access_Token+","+token_type+","+expires_in);


                          // String  tableCode = "2239";

                            if (!(js == null)) {
                            //    getTabledata(access_Token, tableCode);
                                secoundcall(access_Token,tableCode);

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Toast.makeText(LoginCheck_Activity.this,"Something went wrong!!"+volleyError,Toast.LENGTH_LONG).show();
                        System.out.println("Something went wrong!");
                        volleyError.printStackTrace();
                    //    LoadingDialog.cancelLoading();
                       // progressDialog.dismiss();
                        createErrorDialogBoxInternetConnection(getString(R.string.no_internet_connection));
                    }
                }){
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("client_id", "QY6DQzQT6NQM3ngbTRxY");
                params.put("client_secret", "PMb9rtjBzrdYgyvqQhfhTjrB");
                return params;
            }
        }  ;
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = NetworkCallSingleton.getInstance(mContext).getRequestQ();
        requestQueue.add(stringRequest);
        // requestQueue.add(nw.MVolleycall(mContext));
        //     NetworkCallSingleton.getInstance().MVolleycall(mContext);
        // NetworkCallSingleton.getInstance().addToRequestQueue(stringRequest);
        return url;
    }


    public void secoundcall(String accessToken, final String tableCode){


//     LoadingDialog.showLoadingDialog(mContext,"Please wait...");
        final String token = accessToken;

      String id_query;
        // Log.d(accessToken,tableCode+"@@"+token);

//        progressDialog.show();
        String url = "https://greenlightplanet.looker.com:19999/api/3.0/looks/"+tableCode;
        //   String url = "http://13.126.146.126/Sunking_EB/Kazi_login.php"+"?name="+name+"&password="+password+"&mobileNumber="+Mnumber+"&country="+country;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                    //    Log.d("Response",s);

                     //   LoadingDialog.cancelLoading();
                        int query_id = 0;
                        ArrayList<Integer> query_list = new ArrayList<Integer>();

                        try {
                          //  JSONArray jsonArray= new JSONArray(s);
                            JSONObject jsonObject = new JSONObject(s);

                            for (int i = 0; i < jsonObject.length(); i++) {
                                //JSONObject jsonObject = jsonObject.getJSONObject(i);

                                query_id = (int)jsonObject.get("query_id");
                              query_list.add(query_id);

                              if (query_list.size() > 0){

                                  break;
                              }
                            }

                       //     Log.d("query_ID", String.valueOf(query_id));

                            if (query_list.size() > 0) {
                               // thirdCallquery1(tableCode, query_id, Mnumber);

                                volleyR(query_id,Mnumber);

                            }else{

                                Toast.makeText(LoginCheck_Activity.this,"Something went wrong!!",Toast.LENGTH_LONG).show();


                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                     //   progressDialog.dismiss();



                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Toast.makeText(LoginCheck_Activity.this,"Something went wrong!!"+volleyError,Toast.LENGTH_LONG).show();
                        System.out.println("Something went wrong!");
                        volleyError.printStackTrace();
                  //   LoadingDialog.cancelLoading();
                        createErrorDialogBoxInternetConnection(getString(R.string.no_internet_connection));
                    }
                }){
            @Override
            public Map<String, String> getHeaders()
            //   protected Map<String, String> getParams()throws com.android.volley.AuthFailureError
            {
                Map<String, String  >  headers = new HashMap<String, String>();
                headers.put("Authorization","Bearer " +token);
                return headers;

            }

        }  ;
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = NetworkCallSingleton.getInstance(mContext).getRequestQ();
        requestQueue.add(stringRequest);
        // requestQueue.add(nw.MVolleycall(mContext));
        //     NetworkCallSingleton.getInstance().MVolleycall(mContext);





    }





    public void thirdCallquery1(final String accessToken, final int query_id, final String Mnumber){


      //  LoadingDialog.showLoadingDialog(mContext,"Please wait...");

        final JSONObject filter_update = new JSONObject();



        // Log.d(accessToken,tableCode+"@@"+token);

//        progressDialog.show();
        String url = "https://greenlightplanet.looker.com:19999/api/3.0/queries/" + query_id;
        //   String url = "http://13.126.146.126/Sunking_EB/Kazi_login.php"+"?name="+name+"&password="+password+"&mobileNumber="+Mnumber+"&country="+country;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                //        Log.d("ResponseThird Filterqq",s);



                 //       LoadingDialog.cancelLoading();

                        try {

                            JSONObject finalFilter = new JSONObject();
                            JSONObject jsonObject = new JSONObject(s);
                            for (int i = 0; i < jsonObject.length(); i++) {

                          finalFilter = new JSONObject(String.valueOf(s));


                                String filters = (String) finalFilter.getString("filters");

                                finalFilter.put("client_id", "");
                                JSONObject jsonObject1 = new JSONObject(filters);
                                JSONObject filter_update = jsonObject1.put("angaza_users.agent_phone_number",Mnumber);

                                finalFilter.put("filters",filter_update);
                            }

                     QueryID(finalFilter);


                  //          Log.d("UpdatedFilter",finalFilter.toString());


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


  //                      progressDialog.dismiss();


                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Toast.makeText(LoginCheck_Activity.this,"Something went wrong!!"+volleyError,Toast.LENGTH_LONG).show();
                        System.out.println("Something went wrong!");
                        volleyError.printStackTrace();
                    //    LoadingDialog.cancelLoading();
    //                    progressDialog.dismiss();
                        createErrorDialogBoxInternetConnection(getString(R.string.no_internet_connection));
                    }
                }){


            @Override
            public Map<String, String> getHeaders()
            //   protected Map<String, String> getParams()throws com.android.volley.AuthFailureError
            {
                Map<String, String  >  headers = new HashMap<String, String>();
                headers.put("Authorization","Bearer " +accessToken);
                return headers;

            }

        }  ;
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = NetworkCallSingleton.getInstance(mContext).getRequestQ();
        requestQueue.add(stringRequest);
        // requestQueue.add(nw.MVolleycall(mContext));
        //     NetworkCallSingleton.getInstance().MVolleycall(mContext);





    }



    public void forthCallquery1(final String accessToken, final JSONObject queress){


        // Log.d(accessToken,tableCode+"@@"+token);

      //  progressDialog.show();
        final String requestBody,FinalID;

        requestBody = queress.toString();

        String url = "https://greenlightplanet.looker.com:19999/api/3.0/queries";
        //   String url = "http://13.126.146.126/Sunking_EB/Kazi_login.php"+"?name="+name+"&password="+password+"&mobileNumber="+Mnumber+"&country="+country;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
             //           Log.d("ForthQQQ",s);



                        int final_id = 0;
                        ArrayList<Integer> finalq_list = new ArrayList<Integer>();

                        try {
                            //  JSONArray jsonArray= new JSONArray(s);
                            JSONObject jsonObject = new JSONObject(s);

                            for (int i = 0; i < jsonObject.length(); i++) {
                                //JSONObject jsonObject = jsonObject.getJSONObject(i);

                                final_id = (int)jsonObject.get("id");
                                finalq_list.add(final_id);

                                if (finalq_list.size() > 0){

                                    break;
                                }
                            }

                    //        Log.d("final_id", String.valueOf(final_id));

                            if (finalq_list.size() > 0) {
                                // thirdCallquery1(tableCode, query_id, Mnumber);

                                FinalQuery_login(final_id);

                            }else{

                                Toast.makeText(LoginCheck_Activity.this,"Something went wrong!!",Toast.LENGTH_LONG).show();


                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }





        //                progressDialog.dismiss();


                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Toast.makeText(LoginCheck_Activity.this,"Something went wrong!!"+volleyError,Toast.LENGTH_LONG).show();
                        System.out.println("Something went wrong!");
                        volleyError.printStackTrace();
          //              progressDialog.dismiss();
                        createErrorDialogBoxInternetConnection(getString(R.string.no_internet_connection));
                    }
                }){

            @Override
            public Map<String, String> getHeaders()
            //   protected Map<String, String> getParams()throws com.android.volley.AuthFailureError
            {
                Map<String,String>  headers = new HashMap<String, String>();
                headers.put("Authorization","Bearer " +accessToken);
                headers.put("Content-Type","application/json");
               // headers.put("Query",String.valueOf(queress));
          //      Log.d("STRRRRRRRRRRRR@@@",String.valueOf(queress));
                return headers;
            }

            @Override
            public String getBodyContentType() {
                return String.format("application/json: charset=utf-8");
            }

                    @Override
                    public byte[] getBody() throws AuthFailureError {

                        try {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s",
                                    requestBody, "utf-8");
                            return null;
                        }
                    }


        }
        ;
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = NetworkCallSingleton.getInstance(mContext).getRequestQ();
        requestQueue.add(stringRequest);
        // requestQueue.add(nw.MVolleycall(mContext));
        //     NetworkCallSingleton.getInstance().MVolleycall(mContext);





    }






    public void volleyR(final int query_id, final String Mnumber) {
    //    progressDialog.show();

     //   LoadingDialog.showLoadingDialog(mContext,"Please wait...");
        String url = "https://greenlightplanet.looker.com:19999/api/3.0/login";
        //   String url = "http://13.126.146.126/Sunking_EB/Kazi_login.php"+"?name="+name+"&password="+password+"&mobileNumber="+Mnumber+"&country="+country;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
              //          Log.d("Response",s);

            //            LoadingDialog.cancelLoading();


                        try {
                            JSONObject js = new JSONObject(s);
                            String access_Token = js.getString("access_token");
                            String token_type = js.getString("token_type");
                            String expires_in = js.getString("expires_in");


                   //         Log.d("RequestResss",access_Token+","+token_type+","+expires_in);

                           String tableCode = "2239";
                            if (!(js == null)) {
                               // getTabledata(access_Token, tableCode);

                                thirdCallquery1(access_Token,query_id,Mnumber);


                               // Log.d("MNAAAAAAAAAAAAa",Mnumber);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Toast.makeText(LoginCheck_Activity.this,"Something went wrong!!"+volleyError,Toast.LENGTH_LONG).show();
                        System.out.println("Something went wrong!");
                        volleyError.printStackTrace();
                    //    LoadingDialog.cancelLoading();
                  //      progressDialog.dismiss();
                        createErrorDialogBoxInternetConnection(getString(R.string.no_internet_connection));
                    }
                }){
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("client_id", "QY6DQzQT6NQM3ngbTRxY");
                params.put("client_secret", "PMb9rtjBzrdYgyvqQhfhTjrB");
                return params;
            }
        }  ;
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = NetworkCallSingleton.getInstance(mContext).getRequestQ();
        requestQueue.add(stringRequest);
        // requestQueue.add(nw.MVolleycall(mContext));
        //     NetworkCallSingleton.getInstance().MVolleycall(mContext);
        // NetworkCallSingleton.getInstance().addToRequestQueue(stringRequest);
    }




    public void FinalQuery_login(final int query_ID) {
       // progressDialog.show();
        String url = "https://greenlightplanet.looker.com:19999/api/3.0/login";
        //   String url = "http://13.126.146.126/Sunking_EB/Kazi_login.php"+"?name="+name+"&password="+password+"&mobileNumber="+Mnumber+"&country="+country;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
            //            Log.d("Response",s);

         //               progressDialog.dismiss();

                        try {
                            JSONObject js = new JSONObject(s);
                            String access_Token = js.getString("access_token");
                            String token_type = js.getString("token_type");
                            String expires_in = js.getString("expires_in");


                    //        Log.d("RequestResss",access_Token+","+token_type+","+expires_in);


                            if (!(js == null)) {


                  finalquery1ogin(access_Token,query_ID);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Toast.makeText(LoginCheck_Activity.this,"Something went wrong!!"+volleyError,Toast.LENGTH_LONG).show();
                        System.out.println("Something went wrong!");
                        volleyError.printStackTrace();
                   //     LoadingDialog.cancelLoading();
                    //    progressDialog.dismiss();
                        createErrorDialogBoxInternetConnection(getString(R.string.no_internet_connection));
                    }
                }){
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("client_id", "QY6DQzQT6NQM3ngbTRxY");
                params.put("client_secret", "PMb9rtjBzrdYgyvqQhfhTjrB");
                return params;
            }
        }  ;
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = NetworkCallSingleton.getInstance(mContext).getRequestQ();
        requestQueue.add(stringRequest);
        // requestQueue.add(nw.MVolleycall(mContext));
        //     NetworkCallSingleton.getInstance().MVolleycall(mContext);
        // NetworkCallSingleton.getInstance().addToRequestQueue(stringRequest);
    }




    public void finalquery1ogin(final String accessToken,int final_Id){


        // Log.d(accessToken,tableCode+"@@"+token);

      //  progressDialog.show();





        String url = "https://greenlightplanet.looker.com:19999/api/3.0/queries/" + final_Id + "/run/json";
        //   String url = "http://13.126.146.126/Sunking_EB/Kazi_login.php"+"?name="+name+"&password="+password+"&mobileNumber="+Mnumber+"&country="+country;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
            //            Log.d("Final data",s);

                      LoadingDialog.cancelLoading();
try{
                        JSONArray jsonArray = new JSONArray(s);
    HashMap<String, String> login = new HashMap<String,String>();

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject agentName = (JSONObject) jsonArray.get(i);
                            if (agentName.get("angaza_users.agent_phone_number").toString().equals(Mnumber)) {
                                login.put("accounts.agent_username", agentName.get("accounts.agent").toString());
                                login.put("accounts.country", agentName.get("accounts.country").toString());
                            }
                       // Log.d("MNUNBER",Mnumber);
                        }

                   //     Log.d("LOGINNNNNNNNN",login.toString());

                        if (login.size() > 0) {

if (txtLoginCheck.equalsIgnoreCase("FORPASS")) {
    btncheck.setVisibility(View.GONE);
    btn_login.setVisibility(View.VISIBLE);

    Agent_Name = login.get("accounts.agent_username");


    Agent_Name = Agent_Name.replaceAll(" ", "_").toString();


    Toast.makeText(LoginCheck_Activity.this,"Please confirm your password!!",Toast.LENGTH_LONG).show();


    ll_LOgin.setVisibility(View.GONE);
    ll_passwordd.setVisibility(View.VISIBLE);

    ll_updatepassword.setVisibility(View.VISIBLE);


  //  Log.d("VISSSSSSSSSSSSSSSSSS", login.toString());

}else if (txtLoginCheck.equalsIgnoreCase("LOGIN")){

    btncheck.setVisibility(View.GONE);
    btn_login.setVisibility(View.VISIBLE);

    Agent_Name = login.get("accounts.agent_username");


    Agent_Name = Agent_Name.replaceAll(" ", "_").toString();


    Toast.makeText(LoginCheck_Activity.this,"Please confirm your password!!",Toast.LENGTH_LONG).show();
    ll_password.setVisibility(View.VISIBLE);
    ed_txtmobile.setEnabled(false);


}
      //                      LoadingDialog.cancelLoading();

                        }
                            else {

                                Toast.makeText(LoginCheck_Activity.this,"Please check your registered mobile number!!",Toast.LENGTH_LONG).show();

        //                        LoadingDialog.cancelLoading();
        //                        progressDialog.dismiss();
                            }

          //                  LoadingDialog.cancelLoading();
                   //     progressDialog.dismiss();
}catch (Exception e){
    e.printStackTrace();

}
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Toast.makeText(LoginCheck_Activity.this,"Something went wrong!!"+volleyError,Toast.LENGTH_LONG).show();
                        System.out.println("Something went wrong!");
                        volleyError.printStackTrace();
            //            LoadingDialog.cancelLoading();
                       // progressDialog.dismiss();
                        createErrorDialogBoxInternetConnection(getString(R.string.no_internet_connection));
                    }
                }){


            @Override
            public Map<String, String> getHeaders()
            //   protected Map<String, String> getParams()throws com.android.volley.AuthFailureError
            {
                Map<String, String  >  headers = new HashMap<String, String>();
                headers.put("Authorization","Bearer " +accessToken);
                return headers;

            }

        }
                ;
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = NetworkCallSingleton.getInstance(mContext).getRequestQ();
        requestQueue.add(stringRequest);
        // requestQueue.add(nw.MVolleycall(mContext));
        //     NetworkCallSingleton.getInstance().MVolleycall(mContext);





    }


    public void QueryID(final JSONObject queress) {
       // progressDialog.show();

      //  LoadingDialog.showLoadingDialog(mContext,"PLease wait...");
        String url = "https://greenlightplanet.looker.com:19999/api/3.0/login";
        //   String url = "http://13.126.146.126/Sunking_EB/Kazi_login.php"+"?name="+name+"&password="+password+"&mobileNumber="+Mnumber+"&country="+country;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
         //               Log.d("Response",s);
                  //      LoadingDialog.cancelLoading();

         //               progressDialog.dismiss();

                        try {
                            JSONObject js = new JSONObject(s);
                            String access_Token = js.getString("access_token");
                            String token_type = js.getString("token_type");
                            String expires_in = js.getString("expires_in");



                            if (!(js == null)) {


                                forthCallquery1(access_Token,queress);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Toast.makeText(LoginCheck_Activity.this,"Something went wrong!!"+volleyError,Toast.LENGTH_LONG).show();
                        System.out.println("Something went wrong!");
                        volleyError.printStackTrace();
//                        progressDialog.dismiss();
                        createErrorDialogBoxInternetConnection(getString(R.string.no_internet_connection));
                    }
                }){
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("client_id", "QY6DQzQT6NQM3ngbTRxY");
                params.put("client_secret", "PMb9rtjBzrdYgyvqQhfhTjrB");
                return params;
            }
        }  ;
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = NetworkCallSingleton.getInstance(mContext).getRequestQ();
        requestQueue.add(stringRequest);
        // requestQueue.add(nw.MVolleycall(mContext));
        //     NetworkCallSingleton.getInstance().MVolleycall(mContext);
        // NetworkCallSingleton.getInstance().addToRequestQueue(stringRequest);
    }



    private class SendLoginData extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {

            try {
                new SendSms().sendSMS("pap","1","sd","a","aa","aa","heloo");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            return null;
        }
    }
}
