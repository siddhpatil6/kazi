package com.greenlightplanet;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.menu.ListMenuItemView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.greenlightplanet.kazi.R;

public class Faq extends Activity {
    private static final String TAG = Faq.class.getSimpleName();


ImageView btn_plus_wht_kazi, btn_neg_wht_kazi,btn_plus_signin,btn_neg_signin , btn_plus_gmail,btn_neg_gmail,
        btn_plus_otp,btn_neg_otp,btnplus_coutry_name,btnneg_country_name,btn_plus_proposed_dte,btn_neg_proposed_dte,
        btn_plus_icon,btn_neg_icon,btn_plus_refresh,btn_neg_refresh,btn_plus_incentive,btn_neg_incentive;


    LinearLayout ll_txt_kazi,ll_txt_signin,ll_txt_gmail,ll_txt_otp,ll_txt_country_name,ll_txt_proposed_dte,ll_txt_icon,ll_txt_refresh,final_incentive;

    Button back_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq);

        btn_plus_wht_kazi = (ImageView) findViewById(R.id.imageView2);
        btn_neg_wht_kazi = (ImageView) findViewById(R.id.imageView3);
        btn_neg_signin = (ImageView) findViewById(R.id.imageView_sign);
        btn_plus_signin = (ImageView) findViewById(R.id.im_sign);
        btn_neg_gmail = (ImageView) findViewById(R.id.im_neg_gmail);
        btn_plus_gmail = (ImageView) findViewById(R.id.im_plus_gmail);
        btn_neg_otp = (ImageView) findViewById(R.id.im_neg_otp);
        btn_plus_otp = (ImageView) findViewById(R.id.im_plus_otp);
        btnplus_coutry_name = (ImageView) findViewById(R.id.im_plus_country_name);
        btnneg_country_name = (ImageView) findViewById(R.id.im_neg_country_name);
        btn_neg_proposed_dte = (ImageView) findViewById(R.id.im_neg_prposed_dte);
        btn_plus_proposed_dte = (ImageView) findViewById(R.id.im_plus_prposed_dte);
        btn_plus_incentive = (ImageView) findViewById(R.id.implus_incentive);
        btn_neg_incentive = (ImageView) findViewById(R.id.imageviewnega_incentive);


        btn_plus_icon = (ImageView) findViewById(R.id.im_plus_icon);
        btn_plus_refresh = (ImageView) findViewById(R.id.im_plus_refresh);
        btn_neg_icon = (ImageView) findViewById(R.id.im_neg_icon);
        btn_neg_refresh = (ImageView) findViewById(R.id.im_neg_refresh);






        ll_txt_kazi = (LinearLayout) findViewById(R.id.ll_txt_kazi);
        ll_txt_signin = (LinearLayout) findViewById(R.id.ll_sign_kazi);
        ll_txt_gmail = (LinearLayout) findViewById(R.id.ll_gmail_kazi);
        ll_txt_otp = (LinearLayout) findViewById(R.id.ll_otp_kazi);
        ll_txt_country_name  = (LinearLayout) findViewById(R.id.ll_country_name);
        ll_txt_proposed_dte = (LinearLayout) findViewById(R.id.ll_txt_prposed_dte);
        ll_txt_icon = (LinearLayout) findViewById(R.id.ll_txt_icon);
        ll_txt_refresh = (LinearLayout) findViewById(R.id.ll_txt_refresh);
        final_incentive  = (LinearLayout) findViewById(R.id.ll_txt_final_incentive);

        back_btn  =(Button) findViewById(R.id.faq_view_back_button);


        btn_plus_wht_kazi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                ll_txt_kazi.setVisibility(View.VISIBLE);
                btn_plus_wht_kazi.setVisibility(View.GONE);
                btn_neg_wht_kazi.setVisibility(View.VISIBLE);

            }
        });

        btn_neg_wht_kazi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                btn_neg_wht_kazi.setVisibility(View.GONE);
                btn_plus_wht_kazi.setVisibility(View.VISIBLE);
                ll_txt_kazi.setVisibility(View.GONE);

            }
        });

        btn_plus_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ll_txt_signin.setVisibility(View.VISIBLE);
                btn_plus_signin.setVisibility(View.GONE);
                btn_neg_signin.setVisibility(View.VISIBLE);
            }
        });


        btn_neg_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btn_neg_signin.setVisibility(View.GONE);
                btn_plus_signin.setVisibility(View.VISIBLE);
                ll_txt_signin.setVisibility(View.GONE);
            }
        });


        btn_plus_gmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ll_txt_gmail.setVisibility(View.VISIBLE);
                btn_plus_gmail.setVisibility(View.GONE);
                btn_neg_gmail.setVisibility(View.VISIBLE);
            }
        });


        btn_neg_gmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btn_neg_gmail.setVisibility(View.GONE);
                btn_plus_gmail.setVisibility(View.VISIBLE);
                ll_txt_gmail.setVisibility(View.GONE);
            }
        });

    btn_neg_otp.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            ll_txt_otp.setVisibility(View.GONE);
            btn_neg_otp.setVisibility(View.GONE);
            btn_plus_otp.setVisibility(View.VISIBLE);
        }
    });
    btn_plus_otp.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            ll_txt_otp.setVisibility(View.VISIBLE);
            btn_neg_otp.setVisibility(View.VISIBLE);
            btn_plus_otp.setVisibility(View.GONE);
        }
    });


        btnplus_coutry_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ll_txt_country_name.setVisibility(View.VISIBLE);
                btnneg_country_name.setVisibility(View.VISIBLE);
                btnplus_coutry_name.setVisibility(View.GONE);
            }
        });

        btnneg_country_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ll_txt_country_name.setVisibility(View.GONE);
                btnneg_country_name.setVisibility(View.GONE);
                btnplus_coutry_name.setVisibility(View.VISIBLE);
            }
        });


        btn_plus_proposed_dte.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ll_txt_proposed_dte.setVisibility(View.VISIBLE);
                btn_plus_proposed_dte.setVisibility(View.GONE);
                btn_neg_proposed_dte.setVisibility(View.VISIBLE);
            }
        });

        btn_neg_proposed_dte.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ll_txt_proposed_dte.setVisibility(View.GONE);
                btn_neg_proposed_dte.setVisibility(View.GONE);
                btn_plus_proposed_dte.setVisibility(View.VISIBLE);
            }
        });

        btn_plus_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ll_txt_icon.setVisibility(View.VISIBLE);
                btn_neg_icon.setVisibility(View.VISIBLE);
                btn_plus_icon.setVisibility(View.GONE);
            }
        });
        btn_neg_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btn_neg_icon.setVisibility(View.GONE);
                btn_plus_icon.setVisibility(View.VISIBLE);
                ll_txt_icon.setVisibility(View.GONE);
            }
        });

        btn_plus_refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ll_txt_refresh.setVisibility(View.VISIBLE);
                btn_plus_refresh.setVisibility(View.GONE);
                btn_neg_refresh.setVisibility(View.VISIBLE);
            }
        });

        btn_neg_refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ll_txt_refresh.setVisibility(View.GONE);
                btn_plus_refresh.setVisibility(View.VISIBLE);
                btn_neg_refresh.setVisibility(View.GONE);
            }
        });

        btn_plus_incentive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final_incentive.setVisibility(View.VISIBLE);
                btn_plus_incentive.setVisibility(View.GONE);
                btn_neg_incentive.setVisibility(View.VISIBLE);
            }
        });

        btn_neg_incentive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final_incentive.setVisibility(View.GONE);
                btn_neg_incentive.setVisibility(View.GONE);
                btn_plus_incentive.setVisibility(View.VISIBLE);
            }
        });


back_btn.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        Intent i = new Intent(Faq.this,TaskList.class);
        startActivity(i);
        finish();
    }
});




    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(Faq.this,TaskList.class);
        startActivity(i);
        this.finish();
    }
}
