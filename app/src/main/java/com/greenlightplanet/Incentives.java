package com.greenlightplanet;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;

import android.util.LayoutDirection;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.greenlightplanet.kazi.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;

public class Incentives extends Activity {

    private static final String TAG = Incentives.class.getSimpleName();
    public HashMap<String, String> incentive_list = new HashMap<String, String>();
    public HashMap<String, String> collection_data = new HashMap<String, String>();

    String agentName,agentNumber;
    String tablecode = "";
    Context mContext;
    TextView  tv_lastWeek;
    private ProgressDialog progressDialog;
    private SharedPreferences savedPreferences;
    private SharedPreferences.Editor preferencesEditor;
    LinearLayout ll_incentive, ll_prdct_1, ll_prdct_2, ll_prdct_3, ll_prdct_4, ll_prdct_5, ll_prdct_6, ll_prdct_7, ll_prdct_8, ll_prdct_9, ll_prdct_10,ll_prdct_11,ll_prdct_12,ll_prduct_13, ll_coll_score, ll_coll_commission, ll_coll_this_week_collection, ll_coll_pretax, ll_coll_posttax, ll_collection_total_inc,collection_inc;
    Button btn_back, btn_incentive_comp;
    TextView txt_pre_incentive, txt_post_incentive, txt_final_incentive, incentive_agent_txt, txt_country, txt_area, txt_number, txt_incentive_week, txt_currency, txt_boom, txt_home, txt_homradio, txt_pro, txt_home_120, txt_home_120_radio, txt_home_60, txt_home_60_radio, txt_home_60_stove, txt_home_40_z,txt_400TV,txt_home_120_radio_stove,txt_home_60_radio_stove, tv_score, tv_commission, tv_week_collection, tv_collection_pretax, tv_collection_post, tv_total_incentive;
    String check_new_incentive, country, user_country, user_area, agent_phone_no, user_currency, pre_tax_inc, post_tax_inc, final_tax_inc, incentive_week, prdct_boom, prdct_home, prdct_home_radio, prdct_pro, prdct_home_120, prdct_home_120_radio, prdct_home_60, prdct_home_60_radio, prdct_home_60_stove, prdct_home_40_z,prdct_400,prdct_home_120_radio_stove,prdct_home_60_radio_stove,dec_final_tax_inc,currency ;
    private String current_Area,eo_phone_Number = "None";
    HashMap<String, String> incentive_data = new HashMap<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_incentives);

        mContext = Incentives.this;
        savedPreferences = getSharedPreferences("preferences", MODE_PRIVATE); // loads the preferences of the user
        agentName = savedPreferences.getString("agent", "None");
        country = savedPreferences.getString("agentCountry", "None");
        check_new_incentive = savedPreferences.getString("NewIncentive", "None");
       // agentNumber = savedInstanceState.getString("mobileNumber","None");



        Intent i = getIntent();
       current_Area= i.getStringExtra("current_area");
        eo_phone_Number = i.getStringExtra("eo_phone_Number");


        progressDialog = new ProgressDialog(Incentives.this);
        progressDialog.setTitle(getString(R.string.progress_bar_title));
        progressDialog.setMessage(getString(R.string.progress_bar_message));
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);



        tv_lastWeek = (TextView)findViewById(R.id.sales);
        ll_incentive = (LinearLayout) findViewById(R.id.ll_incentive);
        ll_prdct_1 = (LinearLayout) findViewById(R.id.customer_products); // Boom
        ll_prdct_2 = (LinearLayout) findViewById(R.id.customer_p); // Home
        ll_prdct_3 = (LinearLayout) findViewById(R.id.cus); // Home+radio
        ll_prdct_4 = (LinearLayout) findViewById(R.id.customer_curr); // pro
        ll_prdct_5 = (LinearLayout) findViewById(R.id.customer_prdct); // home+120
        ll_prdct_6 = (LinearLayout) findViewById(R.id.customer_prdct_6); // home+120+radio
        ll_prdct_7 = (LinearLayout) findViewById(R.id.customer_prdct_7); // home 60
        ll_prdct_8 = (LinearLayout) findViewById(R.id.customer_prdct_8);  //  home 60+radio
        ll_prdct_9 = (LinearLayout) findViewById(R.id.customer_prdct_9); // home 60+stove
        ll_prdct_10 = (LinearLayout) findViewById(R.id.customer_prdct_10); //  home 40-z
        ll_prdct_11 = (LinearLayout) findViewById(R.id.customer_prdct_11); // home 400 tv
        ll_prdct_12 = (LinearLayout) findViewById(R.id.customer_prdct_12); // home 120+radio+stove
        ll_prduct_13 = (LinearLayout) findViewById(R.id.customer_prdct_13); // home 60+radio+stove

        ll_coll_score = (LinearLayout) findViewById(R.id.ll_collection_score);
        ll_coll_commission = (LinearLayout) findViewById(R.id.ll_commis_per);
        ll_coll_this_week_collection = (LinearLayout) findViewById(R.id.ll_week_collection);
        ll_coll_pretax = (LinearLayout) findViewById(R.id.ll_collection_pretax);
        ll_coll_posttax = (LinearLayout) findViewById(R.id.ll_collection_posttax);
        ll_collection_total_inc = (LinearLayout) findViewById(R.id.ll_collection_total_inc);
        collection_inc = (LinearLayout) findViewById(R.id.collection_inc);


        incentive_agent_txt = (TextView) findViewById(R.id.incentive_agent);
        txt_final_incentive = (TextView) findViewById(R.id.final_incentive);
        txt_post_incentive = (TextView) findViewById(R.id.post_incentive);
        txt_pre_incentive = (TextView) findViewById(R.id.agent_pre_tax);
        txt_country = (TextView) findViewById(R.id.agent_country);
        txt_area = (TextView) findViewById(R.id.agent_area);
        txt_number = (TextView) findViewById(R.id.agent_number);
        txt_incentive_week = (TextView) findViewById(R.id.agent_incentive_week);
        txt_currency = (TextView) findViewById(R.id.agent_currency);
        txt_boom = (TextView) findViewById(R.id.agent_prod1);
        txt_home = (TextView) findViewById(R.id.agent_prod2);
        txt_homradio = (TextView) findViewById(R.id.agent_prod3);
        txt_pro = (TextView) findViewById(R.id.agent_prod4);
        btn_back = (Button) findViewById(R.id.incentive_back_button);
        btn_incentive_comp = (Button) findViewById(R.id.btn_incentive_comp);
        txt_home_120 = (TextView) findViewById(R.id.agent_prod5);
        txt_home_120_radio = (TextView) findViewById(R.id.agent_prod6);
        txt_home_60 = (TextView) findViewById(R.id.agent_prod7);
        txt_home_60_radio = (TextView) findViewById(R.id.agent_prod8);
        txt_home_60_stove = (TextView) findViewById(R.id.agent_prod9);
        txt_home_40_z = (TextView) findViewById(R.id.agent_prod10);
        txt_400TV = (TextView) findViewById(R.id.agent_prod11);
        txt_home_120_radio_stove = (TextView) findViewById(R.id.agent_prod12);
        txt_home_60_radio_stove = (TextView) findViewById(R.id.agent_prod13);



        tv_collection_post = (TextView) findViewById(R.id.txt_collection_post_tax);
        tv_collection_pretax = (TextView) findViewById(R.id.txt_collection_pretax);
        tv_week_collection = (TextView) findViewById(R.id.txt_week_collection);
        tv_commission = (TextView) findViewById(R.id.txt_commission_per);
        tv_score = (TextView) findViewById(R.id.txt_collection_incentive_score);
        tv_total_incentive = (TextView) findViewById(R.id.txt_collection_total_incen);





      //  new IncentiveAsyntask().execute();



/*
                    long startTime = System.currentTimeMillis();

                    String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
*/

            if (country.equalsIgnoreCase("Kenya")) {
                tablecode = "418";
                //incentive_list = new GetData().getIncentivelist(agentName, tablecode);


                LoadingDialog.showLoadingDialog(mContext,"Loading....");

                new GetData().firstCallqueryFilter(mContext,tablecode,agentName,new VolleyCallback(){


                    @Override
                    public void onSuccess(String result) {
                        incentive_list=    IncentiveData(result,agentName);

                        {

                            String txt, key = null, value = null;
                            String home, pro = null, boom = null, homeradio = null;
                            try {


//            Log.d("Incentive&&&&&&&&", incentive_list.toString());
                                // int size = incentive_list.size();
                                // String Boom = incentive_list.get("Boom");
                                //    size = 0; testing
                                if (incentive_list.size() > 0) {

                                    Iterator myVeryOwnIterator = incentive_list.keySet().iterator();

                                    while (myVeryOwnIterator.hasNext()) {
                                        key = (String) myVeryOwnIterator.next();
                                        value = (String) incentive_list.get(key);


                                        if (key.equalsIgnoreCase("country")) {
                                            user_country = value.toString();
                                        } else if (key.equalsIgnoreCase("user_area")) {
                                            user_area = value.toString();
                                        } else if (key.equalsIgnoreCase("phone_fees_incentive_week")) {
                                            incentive_week = value.toString();
                                        } else if (key.equalsIgnoreCase("Pro")) {
                                            if (value.equals("null")) {
                                                value = "0";
                                            }
                                            prdct_pro = value.toString();
                                        } else if (key.equalsIgnoreCase("Home 120 + Radio")) {
                                            if (value.equals("null")) {
                                                value = "0";
                                            }
                                            prdct_home_120_radio = value.toString();
                                        } else if (key.equalsIgnoreCase("Boom")) {
                                            if (value.equals("null")) {
                                                value = "0";
                                            }
                                            prdct_boom = value.toString();
                                        } else if (key.equalsIgnoreCase("Home 60")) {
                                            if (value.equals("null")) {
                                                value = "0";
                                            }
                                            prdct_home_60 = value.toString();
                                        } else if (key.equalsIgnoreCase("Home 60 + Radio")) {
                                            if (value.equals("null")) {
                                                value = "0";
                                            }
                                            prdct_home_60_radio = value.toString();
                                        } else if (key.equalsIgnoreCase("user_currency")) {
                                            user_currency = value.toString();
                                        } else if (key.equalsIgnoreCase("agent_phone_number")) {
                                            agent_phone_no = value.toString();
                                        } else if (key.equalsIgnoreCase("Home 120")) {
                                            if (value.equals("null")) {
                                                value = "0";
                                            }
                                            prdct_home_120 = value.toString();
                                        } else if (key.equalsIgnoreCase("Home 60 + Stove")) {
                                            if (value.equals("null")) {
                                                value = "0";
                                            }
                                            prdct_home_60_stove = value.toString();
                                        } else if (key.equalsIgnoreCase("Home 40-Z")){
                                            if (value.equals("null")) {
                                                value = "0";
                                            }
                                            prdct_home_40_z = value.toString();
                                        }else if (key.equalsIgnoreCase("Home 60 + Radio + Stove")) {
                                            if (value.equals("null")) {
                                                value = "0";
                                            }
                                            prdct_home_60_radio_stove = value.toString();
                                        }else if (key.equalsIgnoreCase("Home 120 + Radio + Stove")) {
                                            if (value.equals("null")) {
                                                value = "0";
                                            }
                                            prdct_home_120_radio_stove = value.toString();
                                        }else if (key.equalsIgnoreCase("Home 400")) {
                                            if (value.equals("null")) {
                                                value = "0";
                                            }
                                            prdct_400 = value.toString();
                                        }

                                        if (prdct_home_120 == null) {

                                            prdct_home_120 = "0";
                                        }
                                        if (prdct_home_40_z == null){

                                            prdct_home_40_z  = "0";
                                        }
                                        if (prdct_boom == null) {

                                            prdct_boom = "0";
                                        }


                                        else if (key.equalsIgnoreCase("pre_tax_inc")) {
                                            pre_tax_inc = value.toString();
                                        } else if (key.equalsIgnoreCase("post_tax_inc")) {
                                            post_tax_inc = value.toString();
                                        } else if (key.equalsIgnoreCase("final_tax_inc_to_pay")) {
                                            final_tax_inc = value.toString();
                                        }





                                        //    Toast.makeText("Incentice txt", "Key: "+key+" Value: "+value, Toast.LENGTH_LONG).show();
                                    }

                                    ll_incentive.setVisibility(View.VISIBLE);
                                    // incentive_agent_txt.setText("Hi " + agentName + Incentives.this.getString(R.string.txt_incentive_cong) +" "+ incentive_list.get(3));
                                    incentive_agent_txt.setText("Hi " + agentName +" "+ Incentives.this.getString(R.string.txt_incentive_cong) + " " + incentive_week);

                                    post_tax_inc =   DecimatCheck(post_tax_inc);
                                    pre_tax_inc=  DecimatCheck(pre_tax_inc);
                                    // dec_final_tax_inc = final_tax_inc;
                                    dec_final_tax_inc = DecimatCheck(final_tax_inc);



                                    txt_country.setText(country);
                                    txt_area.setText(user_area);
                                    txt_pro.setText(prdct_pro);
                                    txt_boom.setText(prdct_boom);
                                    txt_home_60.setText(prdct_home_60);
                                    txt_home_40_z.setText(prdct_home_40_z);
                                    txt_home.setText(prdct_home);
                                    txt_home_120.setText(prdct_home_120);
                                    txt_home_60_stove.setText(prdct_home_60_stove);
                                    txt_400TV.setText(prdct_400);
                                    txt_home_60_radio_stove.setText(prdct_home_60_radio_stove);
                                    txt_home_120_radio_stove.setText(prdct_home_120_radio_stove);
                                    txt_final_incentive.setText(dec_final_tax_inc);
                                    txt_currency.setText(user_currency);
                                    txt_post_incentive.setText(post_tax_inc);
                                    txt_pre_incentive.setText(pre_tax_inc);
                                    txt_home_120_radio.setText(prdct_home_120_radio);
                                    txt_home_60_radio.setText(prdct_home_60_radio);
                                    txt_homradio.setText(prdct_home_radio);
                                    txt_number.setText(agent_phone_no);
                                    txt_incentive_week.setText(incentive_week);

                                    if (country.equalsIgnoreCase("Kenya")) {

                                        ll_prdct_1.setVisibility(View.VISIBLE);
                                        ll_prdct_5.setVisibility(View.VISIBLE);
                                        ll_prdct_6.setVisibility(View.VISIBLE);
                                        ll_prdct_8.setVisibility(View.VISIBLE);
                                        ll_prdct_9.setVisibility(View.VISIBLE);
                                        ll_prdct_4.setVisibility(View.VISIBLE);
                                        ll_prdct_7.setVisibility(View.VISIBLE);
                                        ll_prdct_11.setVisibility(View.VISIBLE);
                                        ll_prdct_12.setVisibility(View.VISIBLE);
                                        ll_prduct_13.setVisibility(View.VISIBLE);

                                    } else if (country.equalsIgnoreCase("Uganda")) {
                                        ll_prdct_1.setVisibility(View.VISIBLE);
                                        ll_prdct_6.setVisibility(View.VISIBLE);
                                        ll_prdct_7.setVisibility(View.VISIBLE);
                                        ll_prdct_8.setVisibility(View.VISIBLE);
                                        ll_prdct_4.setVisibility(View.VISIBLE);
                                    } else if (country.equalsIgnoreCase("Nigeria")) {
                                        ll_prdct_1.setVisibility(View.VISIBLE);
                                        ll_prdct_6.setVisibility(View.VISIBLE);
                                        ll_prdct_5.setVisibility(View.VISIBLE);
                                        ll_prdct_7.setVisibility(View.VISIBLE);
                                        ll_prdct_8.setVisibility(View.VISIBLE);
                                        ll_prdct_4.setVisibility(View.VISIBLE);
                                    } else if (country.equalsIgnoreCase("Myanmar (Burma)")) {
                                        ll_prdct_1.setVisibility(View.VISIBLE);
                                        ll_prdct_5.setVisibility(View.VISIBLE);
                                        ll_prdct_6.setVisibility(View.VISIBLE);
                                        ll_prdct_4.setVisibility(View.VISIBLE);
                                    } else if (country.equalsIgnoreCase("Tanzania")) {
                                        ll_prdct_1.setVisibility(View.VISIBLE);
                                        ll_prdct_6.setVisibility(View.VISIBLE);
                                        ll_prdct_7.setVisibility(View.VISIBLE);
                                        ll_prdct_8.setVisibility(View.VISIBLE);
                                        ll_prdct_4.setVisibility(View.VISIBLE);
                                        //ll_prdct_10.setVisibility(View.VISIBLE);

                                    } else if (country.equalsIgnoreCase("India")) {
                                        ll_prdct_1.setVisibility(View.VISIBLE);
                                        ll_prdct_5.setVisibility(View.VISIBLE);
                                        ll_prdct_10.setVisibility(View.VISIBLE);
                                    }


                                    if (check_new_incentive.equalsIgnoreCase("Yes")) {

                                        ll_coll_commission.setVisibility(View.VISIBLE);
                                        ll_coll_posttax.setVisibility(View.VISIBLE);
                                        ll_coll_pretax.setVisibility(View.VISIBLE);
                                        ll_coll_score.setVisibility(View.VISIBLE);
                                        ll_coll_this_week_collection.setVisibility(View.VISIBLE);
                                        ll_collection_total_inc.setVisibility(View.VISIBLE);
                                        collection_inc.setVisibility(View.VISIBLE);
                                    }

                                    LoadingDialog.cancelLoading();
                                    //            progressDialog.dismiss();


                                    if (check_new_incentive.equalsIgnoreCase("Yes")) {


                        /*    long startTime = System.currentTimeMillis();

                            String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
*/

                                        LoadingDialog.showLoadingDialog(mContext,"Configuring please wait...");

                                        tablecode = "2025";
                                        new GetData().firstCallqueryFilter(mContext,tablecode,agentName,new VolleyCallback() {
                                                    @Override
                                                    public void onSuccess(String result) {


                                                        collection_data = CollectionData(result,agentName);

                                                        CollectionPostdata(collection_data);


                                                        LoadingDialog.cancelLoading();
                                                    }


                                                });
//                                        new CollectionIncentive().execute();
/*

                            long elapsedTime = System.currentTimeMillis() - startTime;
                            System.out.println("Total elapsed http request/response time in milliseconds: " + elapsedTime+" Incentive ");
                            TaskListDatabaseConnector taskListDatabaseConnector = new TaskListDatabaseConnector(Incentives.this);
                            taskListDatabaseConnector.insertLogs(agentName,TAG,"CollectingIncentive","https://greenlightplanet.looker.com:19999/api/3.0/looks/2025",String.valueOf(elapsedTime),currentDateTimeString,country);

                            taskListDatabaseConnector.close();

*/

                                    }

                                    LoadingDialog.cancelLoading();

                                } else {

                                    LoadingDialog.cancelLoading();
                                   // progressDialog.dismiss();
                                    if (new GetData().isNetworkAvailable(Incentives.this)) {
  //                                      new CollectionIncentive().execute();


                                        LoadingDialog.showLoadingDialog(mContext,"Configuring please wait...");
                                        tablecode = "2025";
                                        new GetData().firstCallqueryFilter(mContext,tablecode,agentName,new VolleyCallback() {
                                            @Override
                                            public void onSuccess(String result) {





                                                collection_data = CollectionData(result,agentName);

                                                CollectionPostdata(collection_data);

                                                LoadingDialog.cancelLoading();
                                            }


                                        });
                                        }else {
                                        LoadingDialog.cancelLoading();
                                        createErrorDialogBoxInternetConnection(getString(R.string.no_internet_connection));
                                    }
                       /* incentive_agent_txt.setText(Incentives.this.getString(R.string.incentive_txt_dear) + " " + agentName + " " + Incentives.this.getString(R.string.incentive_txt));
                        ll_incentive.setVisibility(View.GONE);
                        progressDialog.dismiss();*/

                                }
                            }catch (Exception e) {
                                e.printStackTrace();

                            }
                        }



                    }
                });
            } else if (country.equalsIgnoreCase("Uganda")) {
                tablecode = "421";
                //incentive_list = new GetData().getIncentivelist(agentName, tablecode);

                LoadingDialog.showLoadingDialog(mContext,"Loading....");

                new GetData().firstCallqueryFilter(mContext,tablecode,agentName,new VolleyCallback(){


                    @Override
                    public void onSuccess(String result) {
                        incentive_list=   IncentiveData(result,agentName);
                        {

                            String txt, key = null, value = null;
                            String home, pro = null, boom = null, homeradio = null;
                            try {


//            Log.d("Incentive&&&&&&&&", incentive_list.toString());
                                // int size = incentive_list.size();
                                // String Boom = incentive_list.get("Boom");
                                //    size = 0; testing
                                if (incentive_list.size() > 0) {

                                    Iterator myVeryOwnIterator = incentive_list.keySet().iterator();

                                    while (myVeryOwnIterator.hasNext()) {
                                        key = (String) myVeryOwnIterator.next();
                                        value = (String) incentive_list.get(key);


                                        if (key.equalsIgnoreCase("country")) {
                                            user_country = value.toString();
                                        } else if (key.equalsIgnoreCase("user_area")) {
                                            user_area = value.toString();
                                        } else if (key.equalsIgnoreCase("phone_fees_incentive_week")) {
                                            incentive_week = value.toString();
                                        } else if (key.equalsIgnoreCase("Pro")) {
                                            if (value.equals("null")) {
                                                value = "0";
                                            }
                                            prdct_pro = value.toString();
                                        } else if (key.equalsIgnoreCase("Home 120 + Radio")) {
                                            if (value.equals("null")) {
                                                value = "0";
                                            }
                                            prdct_home_120_radio = value.toString();
                                        } else if (key.equalsIgnoreCase("Boom")) {
                                            if (value.equals("null")) {
                                                value = "0";
                                            }
                                            prdct_boom = value.toString();
                                        } else if (key.equalsIgnoreCase("Home 60")) {
                                            if (value.equals("null")) {
                                                value = "0";
                                            }
                                            prdct_home_60 = value.toString();
                                        } else if (key.equalsIgnoreCase("Home 60 + Radio")) {
                                            if (value.equals("null")) {
                                                value = "0";
                                            }
                                            prdct_home_60_radio = value.toString();
                                        } else if (key.equalsIgnoreCase("user_currency")) {
                                            user_currency = value.toString();
                                        } else if (key.equalsIgnoreCase("agent_phone_number")) {
                                            agent_phone_no = value.toString();
                                        } else if (key.equalsIgnoreCase("Home 120")) {
                                            if (value.equals("null")) {
                                                value = "0";
                                            }
                                            prdct_home_120 = value.toString();
                                        } else if (key.equalsIgnoreCase("Home 60 + Stove")) {
                                            if (value.equals("null")) {
                                                value = "0";
                                            }
                                            prdct_home_60_stove = value.toString();
                                        } else if (key.equalsIgnoreCase("Home 40-Z")){
                                            if (value.equals("null")) {
                                                value = "0";
                                            }
                                            prdct_home_40_z = value.toString();
                                        }else if (key.equalsIgnoreCase("Home 60 + Radio + Stove")) {
                                            if (value.equals("null")) {
                                                value = "0";
                                            }
                                            prdct_home_60_radio_stove = value.toString();
                                        }else if (key.equalsIgnoreCase("Home 120 + Radio + Stove")) {
                                            if (value.equals("null")) {
                                                value = "0";
                                            }
                                            prdct_home_120_radio_stove = value.toString();
                                        }else if (key.equalsIgnoreCase("Home 400")) {
                                            if (value.equals("null")) {
                                                value = "0";
                                            }
                                            prdct_400 = value.toString();
                                        }

                                        if (prdct_home_120 == null) {

                                            prdct_home_120 = "0";
                                        }
                                        if (prdct_home_40_z == null){

                                            prdct_home_40_z  = "0";
                                        }
                                        if (prdct_boom == null) {

                                            prdct_boom = "0";
                                        }


                                        else if (key.equalsIgnoreCase("pre_tax_inc")) {
                                            pre_tax_inc = value.toString();
                                        } else if (key.equalsIgnoreCase("post_tax_inc")) {
                                            post_tax_inc = value.toString();
                                        } else if (key.equalsIgnoreCase("final_tax_inc_to_pay")) {
                                            final_tax_inc = value.toString();
                                        }





                                   //     Log.d("Incentive ttx", "Key: " + key + " Value: " + value);
                                        //    Toast.makeText("Incentice txt", "Key: "+key+" Value: "+value, Toast.LENGTH_LONG).show();
                                    }

                                    ll_incentive.setVisibility(View.VISIBLE);
                                    // incentive_agent_txt.setText("Hi " + agentName + Incentives.this.getString(R.string.txt_incentive_cong) +" "+ incentive_list.get(3));
                                    incentive_agent_txt.setText("Hi " + agentName +" "+ Incentives.this.getString(R.string.txt_incentive_cong) + " " + incentive_week);

                                    post_tax_inc =   DecimatCheck(post_tax_inc);
                                    pre_tax_inc=  DecimatCheck(pre_tax_inc);
                                    // dec_final_tax_inc = final_tax_inc;
                                    dec_final_tax_inc = DecimatCheck(final_tax_inc);



                                    txt_country.setText(country);
                                    txt_area.setText(user_area);
                                    txt_pro.setText(prdct_pro);
                                    txt_boom.setText(prdct_boom);
                                    txt_home_60.setText(prdct_home_60);
                                    txt_home_40_z.setText(prdct_home_40_z);
                                    txt_home.setText(prdct_home);
                                    txt_home_120.setText(prdct_home_120);
                                    txt_home_60_stove.setText(prdct_home_60_stove);
                                    txt_400TV.setText(prdct_400);
                                    txt_home_60_radio_stove.setText(prdct_home_60_radio_stove);
                                    txt_home_120_radio_stove.setText(prdct_home_120_radio_stove);
                                    txt_final_incentive.setText(dec_final_tax_inc);
                                    txt_currency.setText(user_currency);
                                    txt_post_incentive.setText(post_tax_inc);
                                    txt_pre_incentive.setText(pre_tax_inc);
                                    txt_home_120_radio.setText(prdct_home_120_radio);
                                    txt_home_60_radio.setText(prdct_home_60_radio);
                                    txt_homradio.setText(prdct_home_radio);
                                    txt_number.setText(agent_phone_no);
                                    txt_incentive_week.setText(incentive_week);

                                    if (country.equalsIgnoreCase("Kenya")) {

                                        ll_prdct_1.setVisibility(View.VISIBLE);
                                        ll_prdct_5.setVisibility(View.VISIBLE);
                                        ll_prdct_6.setVisibility(View.VISIBLE);
                                        ll_prdct_8.setVisibility(View.VISIBLE);
                                        ll_prdct_9.setVisibility(View.VISIBLE);
                                        ll_prdct_4.setVisibility(View.VISIBLE);
                                        ll_prdct_7.setVisibility(View.VISIBLE);
                                        ll_prdct_11.setVisibility(View.VISIBLE);
                                        ll_prdct_12.setVisibility(View.VISIBLE);
                                        ll_prduct_13.setVisibility(View.VISIBLE);

                                    } else if (country.equalsIgnoreCase("Uganda")) {
                                        ll_prdct_1.setVisibility(View.VISIBLE);
                                        ll_prdct_6.setVisibility(View.VISIBLE);
                                        ll_prdct_7.setVisibility(View.VISIBLE);
                                        ll_prdct_8.setVisibility(View.VISIBLE);
                                        ll_prdct_4.setVisibility(View.VISIBLE);
                                    } else if (country.equalsIgnoreCase("Nigeria")) {
                                        ll_prdct_1.setVisibility(View.VISIBLE);
                                        ll_prdct_6.setVisibility(View.VISIBLE);
                                        ll_prdct_5.setVisibility(View.VISIBLE);
                                        ll_prdct_7.setVisibility(View.VISIBLE);
                                        ll_prdct_8.setVisibility(View.VISIBLE);
                                        ll_prdct_4.setVisibility(View.VISIBLE);
                                    } else if (country.equalsIgnoreCase("Myanmar (Burma)")) {
                                        ll_prdct_1.setVisibility(View.VISIBLE);
                                        ll_prdct_5.setVisibility(View.VISIBLE);
                                        ll_prdct_6.setVisibility(View.VISIBLE);
                                        ll_prdct_4.setVisibility(View.VISIBLE);
                                    } else if (country.equalsIgnoreCase("Tanzania")) {
                                        ll_prdct_1.setVisibility(View.VISIBLE);
                                        ll_prdct_6.setVisibility(View.VISIBLE);
                                        ll_prdct_7.setVisibility(View.VISIBLE);
                                        ll_prdct_8.setVisibility(View.VISIBLE);
                                        ll_prdct_4.setVisibility(View.VISIBLE);
                                        //ll_prdct_10.setVisibility(View.VISIBLE);

                                    } else if (country.equalsIgnoreCase("India")) {
                                        ll_prdct_1.setVisibility(View.VISIBLE);
                                        ll_prdct_5.setVisibility(View.VISIBLE);
                                        ll_prdct_10.setVisibility(View.VISIBLE);
                                    }


                                    if (check_new_incentive.equalsIgnoreCase("Yes")) {

                                        ll_coll_commission.setVisibility(View.VISIBLE);
                                        ll_coll_posttax.setVisibility(View.VISIBLE);
                                        ll_coll_pretax.setVisibility(View.VISIBLE);
                                        ll_coll_score.setVisibility(View.VISIBLE);
                                        ll_coll_this_week_collection.setVisibility(View.VISIBLE);
                                        ll_collection_total_inc.setVisibility(View.VISIBLE);
                                        collection_inc.setVisibility(View.VISIBLE);
                                    }

                                    progressDialog.dismiss();

                                    if (check_new_incentive.equalsIgnoreCase("Yes")) {


                        /*    long startTime = System.currentTimeMillis();

                            String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
*/
                         //               new CollectionIncentive().execute();



                                        tablecode = "2025";
                                        new GetData().firstCallqueryFilter(mContext,tablecode,agentName,new VolleyCallback() {
                                                    @Override
                                                    public void onSuccess(String result) {

                                                        collection_data = CollectionData(result,agentName);

                                                        CollectionPostdata(collection_data);
                                                        LoadingDialog.cancelLoading();

                                                    }


                                                });




                                        /*

                            long elapsedTime = System.currentTimeMillis() - startTime;
                            System.out.println("Total elapsed http request/response time in milliseconds: " + elapsedTime+" Incentive ");
                            TaskListDatabaseConnector taskListDatabaseConnector = new TaskListDatabaseConnector(Incentives.this);
                            taskListDatabaseConnector.insertLogs(agentName,TAG,"CollectingIncentive","https://greenlightplanet.looker.com:19999/api/3.0/looks/2025",String.valueOf(elapsedTime),currentDateTimeString,country);

                            taskListDatabaseConnector.close();

*/

                                    }


                                } else {

                                    progressDialog.dismiss();
                                    if (new GetData().isNetworkAvailable(Incentives.this)) {
                                        //new CollectionIncentive().execute();

                                        tablecode = "2025";
                                        new GetData().firstCallqueryFilter(mContext,tablecode,agentName,new VolleyCallback() {
                                            @Override
                                            public void onSuccess(String result) {

                                                collection_data = CollectionData(result,agentName);
                                                CollectionPostdata(collection_data);

                                                LoadingDialog.cancelLoading();
                                            }


                                        });

                                    }else {
                                        createErrorDialogBoxInternetConnection(getString(R.string.no_internet_connection));
                                    }
                       /* incentive_agent_txt.setText(Incentives.this.getString(R.string.incentive_txt_dear) + " " + agentName + " " + Incentives.this.getString(R.string.incentive_txt));
                        ll_incentive.setVisibility(View.GONE);
                        progressDialog.dismiss();*/

                                }
                            }catch (Exception e) {
                                e.printStackTrace();

                            }
                        }

                    }
                });

            } else if (country.equalsIgnoreCase("Nigeria")) {
                tablecode = "420";
                //incentive_list = new GetData().getIncentivelist(agentName, tablecode);

                LoadingDialog.showLoadingDialog(mContext,"Loading....");

                new GetData().firstCallqueryFilter(mContext,tablecode,agentName,new VolleyCallback(){


                    @Override
                    public void onSuccess(String result) {

                        incentive_list=     IncentiveData(result,agentName);

                        {

                            String txt, key = null, value = null;
                            String home, pro = null, boom = null, homeradio = null;
                            try {


//            Log.d("Incentive&&&&&&&&", incentive_list.toString());
                                // int size = incentive_list.size();
                                // String Boom = incentive_list.get("Boom");
                                //    size = 0; testing
                                if (incentive_list.size() > 0) {

                                    Iterator myVeryOwnIterator = incentive_list.keySet().iterator();

                                    while (myVeryOwnIterator.hasNext()) {
                                        key = (String) myVeryOwnIterator.next();
                                        value = (String) incentive_list.get(key);


                                        if (key.equalsIgnoreCase("country")) {
                                            user_country = value.toString();
                                        } else if (key.equalsIgnoreCase("user_area")) {
                                            user_area = value.toString();
                                        } else if (key.equalsIgnoreCase("phone_fees_incentive_week")) {
                                            incentive_week = value.toString();
                                        } else if (key.equalsIgnoreCase("Pro")) {
                                            if (value.equals("null")) {
                                                value = "0";
                                            }
                                            prdct_pro = value.toString();
                                        } else if (key.equalsIgnoreCase("Home 120 + Radio")) {
                                            if (value.equals("null")) {
                                                value = "0";
                                            }
                                            prdct_home_120_radio = value.toString();
                                        } else if (key.equalsIgnoreCase("Boom")) {
                                            if (value.equals("null")) {
                                                value = "0";
                                            }
                                            prdct_boom = value.toString();
                                        } else if (key.equalsIgnoreCase("Home 60")) {
                                            if (value.equals("null")) {
                                                value = "0";
                                            }
                                            prdct_home_60 = value.toString();
                                        } else if (key.equalsIgnoreCase("Home 60 + Radio")) {
                                            if (value.equals("null")) {
                                                value = "0";
                                            }
                                            prdct_home_60_radio = value.toString();
                                        } else if (key.equalsIgnoreCase("user_currency")) {
                                            user_currency = value.toString();
                                        } else if (key.equalsIgnoreCase("agent_phone_number")) {
                                            agent_phone_no = value.toString();
                                        } else if (key.equalsIgnoreCase("Home 120")) {
                                            if (value.equals("null")) {
                                                value = "0";
                                            }
                                            prdct_home_120 = value.toString();
                                        } else if (key.equalsIgnoreCase("Home 60 + Stove")) {
                                            if (value.equals("null")) {
                                                value = "0";
                                            }
                                            prdct_home_60_stove = value.toString();
                                        } else if (key.equalsIgnoreCase("Home 40-Z")){
                                            if (value.equals("null")) {
                                                value = "0";
                                            }
                                            prdct_home_40_z = value.toString();
                                        }else if (key.equalsIgnoreCase("Home 60 + Radio + Stove")) {
                                            if (value.equals("null")) {
                                                value = "0";
                                            }
                                            prdct_home_60_radio_stove = value.toString();
                                        }else if (key.equalsIgnoreCase("Home 120 + Radio + Stove")) {
                                            if (value.equals("null")) {
                                                value = "0";
                                            }
                                            prdct_home_120_radio_stove = value.toString();
                                        }else if (key.equalsIgnoreCase("Home 400")) {
                                            if (value.equals("null")) {
                                                value = "0";
                                            }
                                            prdct_400 = value.toString();
                                        }

                                        if (prdct_home_120 == null) {

                                            prdct_home_120 = "0";
                                        }
                                        if (prdct_home_40_z == null){

                                            prdct_home_40_z  = "0";
                                        }
                                        if (prdct_boom == null) {

                                            prdct_boom = "0";
                                        }


                                        else if (key.equalsIgnoreCase("pre_tax_inc")) {
                                            pre_tax_inc = value.toString();
                                        } else if (key.equalsIgnoreCase("post_tax_inc")) {
                                            post_tax_inc = value.toString();
                                        } else if (key.equalsIgnoreCase("final_tax_inc_to_pay")) {
                                            final_tax_inc = value.toString();
                                        }





                                //        Log.d("Incentive ttx", "Key: " + key + " Value: " + value);
                                        //    Toast.makeText("Incentice txt", "Key: "+key+" Value: "+value, Toast.LENGTH_LONG).show();
                                    }

                                    ll_incentive.setVisibility(View.VISIBLE);
                                    // incentive_agent_txt.setText("Hi " + agentName + Incentives.this.getString(R.string.txt_incentive_cong) +" "+ incentive_list.get(3));
                                    incentive_agent_txt.setText("Hi " + agentName +" "+ Incentives.this.getString(R.string.txt_incentive_cong) + " " + incentive_week);

                                    post_tax_inc =   DecimatCheck(post_tax_inc);
                                    pre_tax_inc=  DecimatCheck(pre_tax_inc);
                                    // dec_final_tax_inc = final_tax_inc;
                                    dec_final_tax_inc = DecimatCheck(final_tax_inc);



                                    txt_country.setText(country);
                                    txt_area.setText(user_area);
                                    txt_pro.setText(prdct_pro);
                                    txt_boom.setText(prdct_boom);
                                    txt_home_60.setText(prdct_home_60);
                                    txt_home_40_z.setText(prdct_home_40_z);
                                    txt_home.setText(prdct_home);
                                    txt_home_120.setText(prdct_home_120);
                                    txt_home_60_stove.setText(prdct_home_60_stove);
                                    txt_400TV.setText(prdct_400);
                                    txt_home_60_radio_stove.setText(prdct_home_60_radio_stove);
                                    txt_home_120_radio_stove.setText(prdct_home_120_radio_stove);
                                    txt_final_incentive.setText(dec_final_tax_inc);
                                    txt_currency.setText(user_currency);
                                    txt_post_incentive.setText(post_tax_inc);
                                    txt_pre_incentive.setText(pre_tax_inc);
                                    txt_home_120_radio.setText(prdct_home_120_radio);
                                    txt_home_60_radio.setText(prdct_home_60_radio);
                                    txt_homradio.setText(prdct_home_radio);
                                    txt_number.setText(agent_phone_no);
                                    txt_incentive_week.setText(incentive_week);

                                    if (country.equalsIgnoreCase("Kenya")) {

                                        ll_prdct_1.setVisibility(View.VISIBLE);
                                        ll_prdct_5.setVisibility(View.VISIBLE);
                                        ll_prdct_6.setVisibility(View.VISIBLE);
                                        ll_prdct_8.setVisibility(View.VISIBLE);
                                        ll_prdct_9.setVisibility(View.VISIBLE);
                                        ll_prdct_4.setVisibility(View.VISIBLE);
                                        ll_prdct_7.setVisibility(View.VISIBLE);
                                        ll_prdct_11.setVisibility(View.VISIBLE);
                                        ll_prdct_12.setVisibility(View.VISIBLE);
                                        ll_prduct_13.setVisibility(View.VISIBLE);

                                    } else if (country.equalsIgnoreCase("Uganda")) {
                                        ll_prdct_1.setVisibility(View.VISIBLE);
                                        ll_prdct_6.setVisibility(View.VISIBLE);
                                        ll_prdct_7.setVisibility(View.VISIBLE);
                                        ll_prdct_8.setVisibility(View.VISIBLE);
                                        ll_prdct_4.setVisibility(View.VISIBLE);
                                    } else if (country.equalsIgnoreCase("Nigeria")) {
                                        ll_prdct_1.setVisibility(View.VISIBLE);
                                        ll_prdct_6.setVisibility(View.VISIBLE);
                                        ll_prdct_5.setVisibility(View.VISIBLE);
                                        ll_prdct_7.setVisibility(View.VISIBLE);
                                        ll_prdct_8.setVisibility(View.VISIBLE);
                                        ll_prdct_4.setVisibility(View.VISIBLE);
                                    } else if (country.equalsIgnoreCase("Myanmar (Burma)")) {
                                        ll_prdct_1.setVisibility(View.VISIBLE);
                                        ll_prdct_5.setVisibility(View.VISIBLE);
                                        ll_prdct_6.setVisibility(View.VISIBLE);
                                        ll_prdct_4.setVisibility(View.VISIBLE);
                                    } else if (country.equalsIgnoreCase("Tanzania")) {
                                        ll_prdct_1.setVisibility(View.VISIBLE);
                                        ll_prdct_6.setVisibility(View.VISIBLE);
                                        ll_prdct_7.setVisibility(View.VISIBLE);
                                        ll_prdct_8.setVisibility(View.VISIBLE);
                                        ll_prdct_4.setVisibility(View.VISIBLE);
                                        //ll_prdct_10.setVisibility(View.VISIBLE);

                                    } else if (country.equalsIgnoreCase("India")) {
                                        ll_prdct_1.setVisibility(View.VISIBLE);
                                        ll_prdct_5.setVisibility(View.VISIBLE);
                                        ll_prdct_10.setVisibility(View.VISIBLE);
                                    }


                                    if (check_new_incentive.equalsIgnoreCase("Yes")) {

                                        ll_coll_commission.setVisibility(View.VISIBLE);
                                        ll_coll_posttax.setVisibility(View.VISIBLE);
                                        ll_coll_pretax.setVisibility(View.VISIBLE);
                                        ll_coll_score.setVisibility(View.VISIBLE);
                                        ll_coll_this_week_collection.setVisibility(View.VISIBLE);
                                        ll_collection_total_inc.setVisibility(View.VISIBLE);
                                        collection_inc.setVisibility(View.VISIBLE);
                                    }

                                    progressDialog.dismiss();

                                    if (check_new_incentive.equalsIgnoreCase("Yes")) {


                        /*    long startTime = System.currentTimeMillis();

                            String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
*/
                                       // new CollectionIncentive().execute();

                                        tablecode = "2025";
                                        new GetData().firstCallqueryFilter(mContext,tablecode,agentName,new VolleyCallback() {
                                            @Override
                                            public void onSuccess(String result) {

                                                collection_data = CollectionData(result,agentName);
                                                CollectionPostdata(collection_data);

                                                LoadingDialog.cancelLoading();

                                            }


                                        });







                                        /*

                            long elapsedTime = System.currentTimeMillis() - startTime;
                            System.out.println("Total elapsed http request/response time in milliseconds: " + elapsedTime+" Incentive ");
                            TaskListDatabaseConnector taskListDatabaseConnector = new TaskListDatabaseConnector(Incentives.this);
                            taskListDatabaseConnector.insertLogs(agentName,TAG,"CollectingIncentive","https://greenlightplanet.looker.com:19999/api/3.0/looks/2025",String.valueOf(elapsedTime),currentDateTimeString,country);

                            taskListDatabaseConnector.close();

*/

                                    }


                                } else {

                                    progressDialog.dismiss();
                                    if (new GetData().isNetworkAvailable(Incentives.this)) {
                                    //    new CollectionIncentive().execute();

                                        tablecode = "2025";
                                        new GetData().firstCallqueryFilter(mContext,tablecode,agentName,new VolleyCallback() {
                                            @Override
                                            public void onSuccess(String result) {


                                                collection_data = CollectionData(result,agentName);
                                                CollectionPostdata(collection_data);
                                                LoadingDialog.cancelLoading();
                                            }


                                        });


                                    }else {
                                        createErrorDialogBoxInternetConnection(getString(R.string.no_internet_connection));
                                    }
                       /* incentive_agent_txt.setText(Incentives.this.getString(R.string.incentive_txt_dear) + " " + agentName + " " + Incentives.this.getString(R.string.incentive_txt));
                        ll_incentive.setVisibility(View.GONE);
                        progressDialog.dismiss();*/

                                }
                            }catch (Exception e) {
                                e.printStackTrace();

                            }
                        }
                    }
                });

            } else if (country.equalsIgnoreCase("Myanmar (Burma)")) {
                tablecode = "419";
                //incentive_list = new GetData().getIncentivelist(agentName, tablecode);

                LoadingDialog.showLoadingDialog(mContext,"Loading....");

                new GetData().firstCallqueryFilter(mContext,tablecode,agentName,new VolleyCallback(){

                    @Override
                    public void onSuccess(String result) {

                        incentive_list=    IncentiveData(result,agentName);

                        {

                            String txt, key = null, value = null;
                            String home, pro = null, boom = null, homeradio = null;
                            try {


//            Log.d("Incentive&&&&&&&&", incentive_list.toString());
                                // int size = incentive_list.size();
                                // String Boom = incentive_list.get("Boom");
                                //    size = 0; testing
                                if (incentive_list.size() > 0) {

                                    Iterator myVeryOwnIterator = incentive_list.keySet().iterator();

                                    while (myVeryOwnIterator.hasNext()) {
                                        key = (String) myVeryOwnIterator.next();
                                        value = (String) incentive_list.get(key);


                                        if (key.equalsIgnoreCase("country")) {
                                            user_country = value.toString();
                                        } else if (key.equalsIgnoreCase("user_area")) {
                                            user_area = value.toString();
                                        } else if (key.equalsIgnoreCase("phone_fees_incentive_week")) {
                                            incentive_week = value.toString();
                                        } else if (key.equalsIgnoreCase("Pro")) {
                                            if (value.equals("null")) {
                                                value = "0";
                                            }
                                            prdct_pro = value.toString();
                                        } else if (key.equalsIgnoreCase("Home 120 + Radio")) {
                                            if (value.equals("null")) {
                                                value = "0";
                                            }
                                            prdct_home_120_radio = value.toString();
                                        } else if (key.equalsIgnoreCase("Boom")) {
                                            if (value.equals("null")) {
                                                value = "0";
                                            }
                                            prdct_boom = value.toString();
                                        } else if (key.equalsIgnoreCase("Home 60")) {
                                            if (value.equals("null")) {
                                                value = "0";
                                            }
                                            prdct_home_60 = value.toString();
                                        } else if (key.equalsIgnoreCase("Home 60 + Radio")) {
                                            if (value.equals("null")) {
                                                value = "0";
                                            }
                                            prdct_home_60_radio = value.toString();
                                        } else if (key.equalsIgnoreCase("user_currency")) {
                                            user_currency = value.toString();
                                        } else if (key.equalsIgnoreCase("agent_phone_number")) {
                                            agent_phone_no = value.toString();
                                        } else if (key.equalsIgnoreCase("Home 120")) {
                                            if (value.equals("null")) {
                                                value = "0";
                                            }
                                            prdct_home_120 = value.toString();
                                        } else if (key.equalsIgnoreCase("Home 60 + Stove")) {
                                            if (value.equals("null")) {
                                                value = "0";
                                            }
                                            prdct_home_60_stove = value.toString();
                                        } else if (key.equalsIgnoreCase("Home 40-Z")){
                                            if (value.equals("null")) {
                                                value = "0";
                                            }
                                            prdct_home_40_z = value.toString();
                                        }else if (key.equalsIgnoreCase("Home 60 + Radio + Stove")) {
                                            if (value.equals("null")) {
                                                value = "0";
                                            }
                                            prdct_home_60_radio_stove = value.toString();
                                        }else if (key.equalsIgnoreCase("Home 120 + Radio + Stove")) {
                                            if (value.equals("null")) {
                                                value = "0";
                                            }
                                            prdct_home_120_radio_stove = value.toString();
                                        }else if (key.equalsIgnoreCase("Home 400")) {
                                            if (value.equals("null")) {
                                                value = "0";
                                            }
                                            prdct_400 = value.toString();
                                        }

                                        if (prdct_home_120 == null) {

                                            prdct_home_120 = "0";
                                        }
                                        if (prdct_home_40_z == null){

                                            prdct_home_40_z  = "0";
                                        }
                                        if (prdct_boom == null) {

                                            prdct_boom = "0";
                                        }


                                        else if (key.equalsIgnoreCase("pre_tax_inc")) {
                                            pre_tax_inc = value.toString();
                                        } else if (key.equalsIgnoreCase("post_tax_inc")) {
                                            post_tax_inc = value.toString();
                                        } else if (key.equalsIgnoreCase("final_tax_inc_to_pay")) {
                                            final_tax_inc = value.toString();
                                        }





                                //        Log.d("Incentive ttx", "Key: " + key + " Value: " + value);
                                        //    Toast.makeText("Incentice txt", "Key: "+key+" Value: "+value, Toast.LENGTH_LONG).show();
                                    }

                                    ll_incentive.setVisibility(View.VISIBLE);
                                    // incentive_agent_txt.setText("Hi " + agentName + Incentives.this.getString(R.string.txt_incentive_cong) +" "+ incentive_list.get(3));
                                    incentive_agent_txt.setText("Hi " + agentName +" "+ Incentives.this.getString(R.string.txt_incentive_cong) + " " + incentive_week);

                                    post_tax_inc =   DecimatCheck(post_tax_inc);
                                    pre_tax_inc=  DecimatCheck(pre_tax_inc);
                                    // dec_final_tax_inc = final_tax_inc;
                                    dec_final_tax_inc = DecimatCheck(final_tax_inc);



                                    txt_country.setText(country);
                                    txt_area.setText(user_area);
                                    txt_pro.setText(prdct_pro);
                                    txt_boom.setText(prdct_boom);
                                    txt_home_60.setText(prdct_home_60);
                                    txt_home_40_z.setText(prdct_home_40_z);
                                    txt_home.setText(prdct_home);
                                    txt_home_120.setText(prdct_home_120);
                                    txt_home_60_stove.setText(prdct_home_60_stove);
                                    txt_400TV.setText(prdct_400);
                                    txt_home_60_radio_stove.setText(prdct_home_60_radio_stove);
                                    txt_home_120_radio_stove.setText(prdct_home_120_radio_stove);
                                    txt_final_incentive.setText(dec_final_tax_inc);
                                    txt_currency.setText(user_currency);
                                    txt_post_incentive.setText(post_tax_inc);
                                    txt_pre_incentive.setText(pre_tax_inc);
                                    txt_home_120_radio.setText(prdct_home_120_radio);
                                    txt_home_60_radio.setText(prdct_home_60_radio);
                                    txt_homradio.setText(prdct_home_radio);
                                    txt_number.setText(agent_phone_no);
                                    txt_incentive_week.setText(incentive_week);

                                    if (country.equalsIgnoreCase("Kenya")) {

                                        ll_prdct_1.setVisibility(View.VISIBLE);
                                        ll_prdct_5.setVisibility(View.VISIBLE);
                                        ll_prdct_6.setVisibility(View.VISIBLE);
                                        ll_prdct_8.setVisibility(View.VISIBLE);
                                        ll_prdct_9.setVisibility(View.VISIBLE);
                                        ll_prdct_4.setVisibility(View.VISIBLE);
                                        ll_prdct_7.setVisibility(View.VISIBLE);
                                        ll_prdct_11.setVisibility(View.VISIBLE);
                                        ll_prdct_12.setVisibility(View.VISIBLE);
                                        ll_prduct_13.setVisibility(View.VISIBLE);

                                    } else if (country.equalsIgnoreCase("Uganda")) {
                                        ll_prdct_1.setVisibility(View.VISIBLE);
                                        ll_prdct_6.setVisibility(View.VISIBLE);
                                        ll_prdct_7.setVisibility(View.VISIBLE);
                                        ll_prdct_8.setVisibility(View.VISIBLE);
                                        ll_prdct_4.setVisibility(View.VISIBLE);
                                    } else if (country.equalsIgnoreCase("Nigeria")) {
                                        ll_prdct_1.setVisibility(View.VISIBLE);
                                        ll_prdct_6.setVisibility(View.VISIBLE);
                                        ll_prdct_5.setVisibility(View.VISIBLE);
                                        ll_prdct_7.setVisibility(View.VISIBLE);
                                        ll_prdct_8.setVisibility(View.VISIBLE);
                                        ll_prdct_4.setVisibility(View.VISIBLE);
                                    } else if (country.equalsIgnoreCase("Myanmar (Burma)")) {
                                        ll_prdct_1.setVisibility(View.VISIBLE);
                                        ll_prdct_5.setVisibility(View.VISIBLE);
                                        ll_prdct_6.setVisibility(View.VISIBLE);
                                        ll_prdct_4.setVisibility(View.VISIBLE);
                                    } else if (country.equalsIgnoreCase("Tanzania")) {
                                        ll_prdct_1.setVisibility(View.VISIBLE);
                                        ll_prdct_6.setVisibility(View.VISIBLE);
                                        ll_prdct_7.setVisibility(View.VISIBLE);
                                        ll_prdct_8.setVisibility(View.VISIBLE);
                                        ll_prdct_4.setVisibility(View.VISIBLE);
                                        //ll_prdct_10.setVisibility(View.VISIBLE);

                                    } else if (country.equalsIgnoreCase("India")) {
                                        ll_prdct_1.setVisibility(View.VISIBLE);
                                        ll_prdct_5.setVisibility(View.VISIBLE);
                                        ll_prdct_10.setVisibility(View.VISIBLE);
                                    }


                                    if (check_new_incentive.equalsIgnoreCase("Yes")) {

                                        ll_coll_commission.setVisibility(View.VISIBLE);
                                        ll_coll_posttax.setVisibility(View.VISIBLE);
                                        ll_coll_pretax.setVisibility(View.VISIBLE);
                                        ll_coll_score.setVisibility(View.VISIBLE);
                                        ll_coll_this_week_collection.setVisibility(View.VISIBLE);
                                        ll_collection_total_inc.setVisibility(View.VISIBLE);
                                        collection_inc.setVisibility(View.VISIBLE);
                                    }

                                    progressDialog.dismiss();

                                    if (check_new_incentive.equalsIgnoreCase("Yes")) {


                                        tablecode = "2025";
                                        new GetData().firstCallqueryFilter(mContext,tablecode,agentName,new VolleyCallback() {
                                            @Override
                                            public void onSuccess(String result) {

                                                collection_data = CollectionData(result,agentName);
                                                CollectionPostdata(collection_data);
                                                LoadingDialog.cancelLoading();

                                            }


                                        });

                        /*    long startTime = System.currentTimeMillis();

                            String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
*/
//                                        new CollectionIncentive().execute();
/*

                            long elapsedTime = System.currentTimeMillis() - startTime;
                            System.out.println("Total elapsed http request/response time in milliseconds: " + elapsedTime+" Incentive ");
                            TaskListDatabaseConnector taskListDatabaseConnector = new TaskListDatabaseConnector(Incentives.this);
                            taskListDatabaseConnector.insertLogs(agentName,TAG,"CollectingIncentive","https://greenlightplanet.looker.com:19999/api/3.0/looks/2025",String.valueOf(elapsedTime),currentDateTimeString,country);

                            taskListDatabaseConnector.close();

*/

                                    }


                                } else {

                                    progressDialog.dismiss();
                                    if (new GetData().isNetworkAvailable(Incentives.this)) {
                                       // new CollectionIncentive().execute();

                                        tablecode = "2025";
                                        new GetData().firstCallqueryFilter(mContext,tablecode,agentName,new VolleyCallback() {
                                            @Override
                                            public void onSuccess(String result) {

                                                collection_data = CollectionData(result,agentName);
                                                CollectionPostdata(collection_data);
                                                LoadingDialog.cancelLoading();

                                            }


                                        });

                                    }else {
                                        createErrorDialogBoxInternetConnection(getString(R.string.no_internet_connection));
                                    }
                       /* incentive_agent_txt.setText(Incentives.this.getString(R.string.incentive_txt_dear) + " " + agentName + " " + Incentives.this.getString(R.string.incentive_txt));
                        ll_incentive.setVisibility(View.GONE);
                        progressDialog.dismiss();*/

                                }
                            }catch (Exception e) {
                                e.printStackTrace();

                            }
                        }
                    }
                });
            } else if (country.equalsIgnoreCase("Tanzania")) {
                tablecode = "441";
                //incentive_list = new GetData().getIncentivelist(agentName, tablecode);
                LoadingDialog.showLoadingDialog(mContext,"Loading....");

                new GetData().firstCallqueryFilter(mContext,tablecode,agentName,new VolleyCallback(){

                    @Override
                    public void onSuccess(String result) {

                        incentive_list=   IncentiveData(result,agentName);
                        {

                            String txt, key = null, value = null;
                            String home, pro = null, boom = null, homeradio = null;
                            try {


//            Log.d("Incentive&&&&&&&&", incentive_list.toString());
                                // int size = incentive_list.size();
                                // String Boom = incentive_list.get("Boom");
                                //    size = 0; testing
                                if (incentive_list.size() > 0) {

                                    Iterator myVeryOwnIterator = incentive_list.keySet().iterator();

                                    while (myVeryOwnIterator.hasNext()) {
                                        key = (String) myVeryOwnIterator.next();
                                        value = (String) incentive_list.get(key);


                                        if (key.equalsIgnoreCase("country")) {
                                            user_country = value.toString();
                                        } else if (key.equalsIgnoreCase("user_area")) {
                                            user_area = value.toString();
                                        } else if (key.equalsIgnoreCase("phone_fees_incentive_week")) {
                                            incentive_week = value.toString();
                                        } else if (key.equalsIgnoreCase("Pro")) {
                                            if (value.equals("null")) {
                                                value = "0";
                                            }
                                            prdct_pro = value.toString();
                                        } else if (key.equalsIgnoreCase("Home 120 + Radio")) {
                                            if (value.equals("null")) {
                                                value = "0";
                                            }
                                            prdct_home_120_radio = value.toString();
                                        } else if (key.equalsIgnoreCase("Boom")) {
                                            if (value.equals("null")) {
                                                value = "0";
                                            }
                                            prdct_boom = value.toString();
                                        } else if (key.equalsIgnoreCase("Home 60")) {
                                            if (value.equals("null")) {
                                                value = "0";
                                            }
                                            prdct_home_60 = value.toString();
                                        } else if (key.equalsIgnoreCase("Home 60 + Radio")) {
                                            if (value.equals("null")) {
                                                value = "0";
                                            }
                                            prdct_home_60_radio = value.toString();
                                        } else if (key.equalsIgnoreCase("user_currency")) {
                                            user_currency = value.toString();
                                        } else if (key.equalsIgnoreCase("agent_phone_number")) {
                                            agent_phone_no = value.toString();
                                        } else if (key.equalsIgnoreCase("Home 120")) {
                                            if (value.equals("null")) {
                                                value = "0";
                                            }
                                            prdct_home_120 = value.toString();
                                        } else if (key.equalsIgnoreCase("Home 60 + Stove")) {
                                            if (value.equals("null")) {
                                                value = "0";
                                            }
                                            prdct_home_60_stove = value.toString();
                                        } else if (key.equalsIgnoreCase("Home 40-Z")){
                                            if (value.equals("null")) {
                                                value = "0";
                                            }
                                            prdct_home_40_z = value.toString();
                                        }else if (key.equalsIgnoreCase("Home 60 + Radio + Stove")) {
                                            if (value.equals("null")) {
                                                value = "0";
                                            }
                                            prdct_home_60_radio_stove = value.toString();
                                        }else if (key.equalsIgnoreCase("Home 120 + Radio + Stove")) {
                                            if (value.equals("null")) {
                                                value = "0";
                                            }
                                            prdct_home_120_radio_stove = value.toString();
                                        }else if (key.equalsIgnoreCase("Home 400")) {
                                            if (value.equals("null")) {
                                                value = "0";
                                            }
                                            prdct_400 = value.toString();
                                        }

                                        if (prdct_home_120 == null) {

                                            prdct_home_120 = "0";
                                        }
                                        if (prdct_home_40_z == null){

                                            prdct_home_40_z  = "0";
                                        }
                                        if (prdct_boom == null) {

                                            prdct_boom = "0";
                                        }


                                        else if (key.equalsIgnoreCase("pre_tax_inc")) {
                                            pre_tax_inc = value.toString();
                                        } else if (key.equalsIgnoreCase("post_tax_inc")) {
                                            post_tax_inc = value.toString();
                                        } else if (key.equalsIgnoreCase("final_tax_inc_to_pay")) {
                                            final_tax_inc = value.toString();
                                        }





                                   //     Log.d("Incentive ttx", "Key: " + key + " Value: " + value);
                                        //    Toast.makeText("Incentice txt", "Key: "+key+" Value: "+value, Toast.LENGTH_LONG).show();
                                    }

                                    ll_incentive.setVisibility(View.VISIBLE);
                                    // incentive_agent_txt.setText("Hi " + agentName + Incentives.this.getString(R.string.txt_incentive_cong) +" "+ incentive_list.get(3));
                                    incentive_agent_txt.setText("Hi " + agentName +" "+ Incentives.this.getString(R.string.txt_incentive_cong) + " " + incentive_week);

                                    post_tax_inc =   DecimatCheck(post_tax_inc);
                                    pre_tax_inc=  DecimatCheck(pre_tax_inc);
                                    // dec_final_tax_inc = final_tax_inc;
                                    dec_final_tax_inc = DecimatCheck(final_tax_inc);



                                    txt_country.setText(country);
                                    txt_area.setText(user_area);
                                    txt_pro.setText(prdct_pro);
                                    txt_boom.setText(prdct_boom);
                                    txt_home_60.setText(prdct_home_60);
                                    txt_home_40_z.setText(prdct_home_40_z);
                                    txt_home.setText(prdct_home);
                                    txt_home_120.setText(prdct_home_120);
                                    txt_home_60_stove.setText(prdct_home_60_stove);
                                    txt_400TV.setText(prdct_400);
                                    txt_home_60_radio_stove.setText(prdct_home_60_radio_stove);
                                    txt_home_120_radio_stove.setText(prdct_home_120_radio_stove);
                                    txt_final_incentive.setText(dec_final_tax_inc);
                                    txt_currency.setText(user_currency);
                                    txt_post_incentive.setText(post_tax_inc);
                                    txt_pre_incentive.setText(pre_tax_inc);
                                    txt_home_120_radio.setText(prdct_home_120_radio);
                                    txt_home_60_radio.setText(prdct_home_60_radio);
                                    txt_homradio.setText(prdct_home_radio);
                                    txt_number.setText(agent_phone_no);
                                    txt_incentive_week.setText(incentive_week);

                                    if (country.equalsIgnoreCase("Kenya")) {

                                        ll_prdct_1.setVisibility(View.VISIBLE);
                                        ll_prdct_5.setVisibility(View.VISIBLE);
                                        ll_prdct_6.setVisibility(View.VISIBLE);
                                        ll_prdct_8.setVisibility(View.VISIBLE);
                                        ll_prdct_9.setVisibility(View.VISIBLE);
                                        ll_prdct_4.setVisibility(View.VISIBLE);
                                        ll_prdct_7.setVisibility(View.VISIBLE);
                                        ll_prdct_11.setVisibility(View.VISIBLE);
                                        ll_prdct_12.setVisibility(View.VISIBLE);
                                        ll_prduct_13.setVisibility(View.VISIBLE);

                                    } else if (country.equalsIgnoreCase("Uganda")) {
                                        ll_prdct_1.setVisibility(View.VISIBLE);
                                        ll_prdct_6.setVisibility(View.VISIBLE);
                                        ll_prdct_7.setVisibility(View.VISIBLE);
                                        ll_prdct_8.setVisibility(View.VISIBLE);
                                        ll_prdct_4.setVisibility(View.VISIBLE);
                                    } else if (country.equalsIgnoreCase("Nigeria")) {
                                        ll_prdct_1.setVisibility(View.VISIBLE);
                                        ll_prdct_6.setVisibility(View.VISIBLE);
                                        ll_prdct_5.setVisibility(View.VISIBLE);
                                        ll_prdct_7.setVisibility(View.VISIBLE);
                                        ll_prdct_8.setVisibility(View.VISIBLE);
                                        ll_prdct_4.setVisibility(View.VISIBLE);
                                    } else if (country.equalsIgnoreCase("Myanmar (Burma)")) {
                                        ll_prdct_1.setVisibility(View.VISIBLE);
                                        ll_prdct_5.setVisibility(View.VISIBLE);
                                        ll_prdct_6.setVisibility(View.VISIBLE);
                                        ll_prdct_4.setVisibility(View.VISIBLE);
                                    } else if (country.equalsIgnoreCase("Tanzania")) {
                                        ll_prdct_1.setVisibility(View.VISIBLE);
                                        ll_prdct_6.setVisibility(View.VISIBLE);
                                        ll_prdct_7.setVisibility(View.VISIBLE);
                                        ll_prdct_8.setVisibility(View.VISIBLE);
                                        ll_prdct_4.setVisibility(View.VISIBLE);
                                        //ll_prdct_10.setVisibility(View.VISIBLE);

                                    } else if (country.equalsIgnoreCase("India")) {
                                        ll_prdct_1.setVisibility(View.VISIBLE);
                                        ll_prdct_5.setVisibility(View.VISIBLE);
                                        ll_prdct_10.setVisibility(View.VISIBLE);
                                    }


                                    if (check_new_incentive.equalsIgnoreCase("Yes")) {

                                        ll_coll_commission.setVisibility(View.VISIBLE);
                                        ll_coll_posttax.setVisibility(View.VISIBLE);
                                        ll_coll_pretax.setVisibility(View.VISIBLE);
                                        ll_coll_score.setVisibility(View.VISIBLE);
                                        ll_coll_this_week_collection.setVisibility(View.VISIBLE);
                                        ll_collection_total_inc.setVisibility(View.VISIBLE);
                                        collection_inc.setVisibility(View.VISIBLE);
                                    }

                                    progressDialog.dismiss();

                                    if (check_new_incentive.equalsIgnoreCase("Yes")) {


                                        tablecode = "2025";
                                        new GetData().firstCallqueryFilter(mContext,tablecode,agentName,new VolleyCallback() {
                                            @Override
                                            public void onSuccess(String result) {

                                                collection_data = CollectionData(result,agentName);
                                                CollectionPostdata(collection_data);

                                       LoadingDialog.cancelLoading();

                                            }


                                        });

                        /*    long startTime = System.currentTimeMillis();

                            String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
*/
                                       // new CollectionIncentive().execute();
/*

                            long elapsedTime = System.currentTimeMillis() - startTime;
                            System.out.println("Total elapsed http request/response time in milliseconds: " + elapsedTime+" Incentive ");
                            TaskListDatabaseConnector taskListDatabaseConnector = new TaskListDatabaseConnector(Incentives.this);
                            taskListDatabaseConnector.insertLogs(agentName,TAG,"CollectingIncentive","https://greenlightplanet.looker.com:19999/api/3.0/looks/2025",String.valueOf(elapsedTime),currentDateTimeString,country);

                            taskListDatabaseConnector.close();

*/

                                    }


                                } else {

                                    progressDialog.dismiss();
                                    if (new GetData().isNetworkAvailable(Incentives.this)) {
                                       // new CollectionIncentive().execute();


                                        tablecode = "2025";
                                        new GetData().firstCallqueryFilter(mContext,tablecode,agentName,new VolleyCallback() {
                                            @Override
                                            public void onSuccess(String result) {

                                                collection_data = CollectionData(result,agentName);
                                                CollectionPostdata(collection_data);

                                                LoadingDialog.cancelLoading();

                                            }


                                        });

                                    }else {
                                        createErrorDialogBoxInternetConnection(getString(R.string.no_internet_connection));
                                    }


                       /* incentive_agent_txt.setText(Incentives.this.getString(R.string.incentive_txt_dear) + " " + agentName + " " + Incentives.this.getString(R.string.incentive_txt));
                        ll_incentive.setVisibility(View.GONE);
                        progressDialog.dismiss();*/

                                }
                            }catch (Exception e) {
                                e.printStackTrace();

                            }
                        }

                    }
                });
            } else if (country.equalsIgnoreCase("India")) {
                tablecode = "1776";
                //incentive_list = new GetData().getIncentivelist(agentName, tablecode);
                LoadingDialog.showLoadingDialog(mContext,"Loading....");

                new GetData().firstCallqueryFilter(mContext,tablecode,agentName,new VolleyCallback(){


                    @Override
                    public void onSuccess(String result) {
                        incentive_list  =  IncentiveData(result,agentName);
                        {

                            String txt, key = null, value = null;
                            String home, pro = null, boom = null, homeradio = null;
                            try {


//            Log.d("Incentive&&&&&&&&", incentive_list.toString());
                                // int size = incentive_list.size();
                                // String Boom = incentive_list.get("Boom");
                                //    size = 0; testing
                                if (incentive_list.size() > 0) {

                                    Iterator myVeryOwnIterator = incentive_list.keySet().iterator();

                                    while (myVeryOwnIterator.hasNext()) {
                                        key = (String) myVeryOwnIterator.next();
                                        value = (String) incentive_list.get(key);


                                        if (key.equalsIgnoreCase("country")) {
                                            user_country = value.toString();
                                        } else if (key.equalsIgnoreCase("user_area")) {
                                            user_area = value.toString();
                                        } else if (key.equalsIgnoreCase("phone_fees_incentive_week")) {
                                            incentive_week = value.toString();
                                        } else if (key.equalsIgnoreCase("Pro")) {
                                            if (value.equals("null")) {
                                                value = "0";
                                            }
                                            prdct_pro = value.toString();
                                        } else if (key.equalsIgnoreCase("Home 120 + Radio")) {
                                            if (value.equals("null")) {
                                                value = "0";
                                            }
                                            prdct_home_120_radio = value.toString();
                                        } else if (key.equalsIgnoreCase("Boom")) {
                                            if (value.equals("null")) {
                                                value = "0";
                                            }
                                            prdct_boom = value.toString();
                                        } else if (key.equalsIgnoreCase("Home 60")) {
                                            if (value.equals("null")) {
                                                value = "0";
                                            }
                                            prdct_home_60 = value.toString();
                                        } else if (key.equalsIgnoreCase("Home 60 + Radio")) {
                                            if (value.equals("null")) {
                                                value = "0";
                                            }
                                            prdct_home_60_radio = value.toString();
                                        } else if (key.equalsIgnoreCase("user_currency")) {
                                            user_currency = value.toString();
                                        } else if (key.equalsIgnoreCase("agent_phone_number")) {
                                            agent_phone_no = value.toString();
                                        } else if (key.equalsIgnoreCase("Home 120")) {
                                            if (value.equals("null")) {
                                                value = "0";
                                            }
                                            prdct_home_120 = value.toString();
                                        } else if (key.equalsIgnoreCase("Home 60 + Stove")) {
                                            if (value.equals("null")) {
                                                value = "0";
                                            }
                                            prdct_home_60_stove = value.toString();
                                        } else if (key.equalsIgnoreCase("Home 40-Z")){
                                            if (value.equals("null")) {
                                                value = "0";
                                            }
                                            prdct_home_40_z = value.toString();
                                        }else if (key.equalsIgnoreCase("Home 60 + Radio + Stove")) {
                                            if (value.equals("null")) {
                                                value = "0";
                                            }
                                            prdct_home_60_radio_stove = value.toString();
                                        }else if (key.equalsIgnoreCase("Home 120 + Radio + Stove")) {
                                            if (value.equals("null")) {
                                                value = "0";
                                            }
                                            prdct_home_120_radio_stove = value.toString();
                                        }else if (key.equalsIgnoreCase("Home 400")) {
                                            if (value.equals("null")) {
                                                value = "0";
                                            }
                                            prdct_400 = value.toString();
                                        }

                                        if (prdct_home_120 == null) {

                                            prdct_home_120 = "0";
                                        }
                                        if (prdct_home_40_z == null){

                                            prdct_home_40_z  = "0";
                                        }
                                        if (prdct_boom == null) {

                                            prdct_boom = "0";
                                        }


                                        else if (key.equalsIgnoreCase("pre_tax_inc")) {
                                            pre_tax_inc = value.toString();
                                        } else if (key.equalsIgnoreCase("post_tax_inc")) {
                                            post_tax_inc = value.toString();
                                        } else if (key.equalsIgnoreCase("final_tax_inc_to_pay")) {
                                            final_tax_inc = value.toString();
                                        }





                                  //      Log.d("Incentive ttx", "Key: " + key + " Value: " + value);
                                        //    Toast.makeText("Incentice txt", "Key: "+key+" Value: "+value, Toast.LENGTH_LONG).show();
                                    }

                                    ll_incentive.setVisibility(View.VISIBLE);
                                    // incentive_agent_txt.setText("Hi " + agentName + Incentives.this.getString(R.string.txt_incentive_cong) +" "+ incentive_list.get(3));
                                    incentive_agent_txt.setText("Hi " + agentName +" "+ Incentives.this.getString(R.string.txt_incentive_cong) + " " + incentive_week);

                                    post_tax_inc =   DecimatCheck(post_tax_inc);
                                    pre_tax_inc=  DecimatCheck(pre_tax_inc);
                                    // dec_final_tax_inc = final_tax_inc;
                                    dec_final_tax_inc = DecimatCheck(final_tax_inc);



                                    txt_country.setText(country);
                                    txt_area.setText(user_area);
                                    txt_pro.setText(prdct_pro);
                                    txt_boom.setText(prdct_boom);
                                    txt_home_60.setText(prdct_home_60);
                                    txt_home_40_z.setText(prdct_home_40_z);
                                    txt_home.setText(prdct_home);
                                    txt_home_120.setText(prdct_home_120);
                                    txt_home_60_stove.setText(prdct_home_60_stove);
                                    txt_400TV.setText(prdct_400);
                                    txt_home_60_radio_stove.setText(prdct_home_60_radio_stove);
                                    txt_home_120_radio_stove.setText(prdct_home_120_radio_stove);
                                    txt_final_incentive.setText(dec_final_tax_inc);
                                    txt_currency.setText(user_currency);
                                    txt_post_incentive.setText(post_tax_inc);
                                    txt_pre_incentive.setText(pre_tax_inc);
                                    txt_home_120_radio.setText(prdct_home_120_radio);
                                    txt_home_60_radio.setText(prdct_home_60_radio);
                                    txt_homradio.setText(prdct_home_radio);
                                    txt_number.setText(agent_phone_no);
                                    txt_incentive_week.setText(incentive_week);

                                    if (country.equalsIgnoreCase("Kenya")) {

                                        ll_prdct_1.setVisibility(View.VISIBLE);
                                        ll_prdct_5.setVisibility(View.VISIBLE);
                                        ll_prdct_6.setVisibility(View.VISIBLE);
                                        ll_prdct_8.setVisibility(View.VISIBLE);
                                        ll_prdct_9.setVisibility(View.VISIBLE);
                                        ll_prdct_4.setVisibility(View.VISIBLE);
                                        ll_prdct_7.setVisibility(View.VISIBLE);
                                        ll_prdct_11.setVisibility(View.VISIBLE);
                                        ll_prdct_12.setVisibility(View.VISIBLE);
                                        ll_prduct_13.setVisibility(View.VISIBLE);

                                    } else if (country.equalsIgnoreCase("Uganda")) {
                                        ll_prdct_1.setVisibility(View.VISIBLE);
                                        ll_prdct_6.setVisibility(View.VISIBLE);
                                        ll_prdct_7.setVisibility(View.VISIBLE);
                                        ll_prdct_8.setVisibility(View.VISIBLE);
                                        ll_prdct_4.setVisibility(View.VISIBLE);
                                    } else if (country.equalsIgnoreCase("Nigeria")) {
                                        ll_prdct_1.setVisibility(View.VISIBLE);
                                        ll_prdct_6.setVisibility(View.VISIBLE);
                                        ll_prdct_5.setVisibility(View.VISIBLE);
                                        ll_prdct_7.setVisibility(View.VISIBLE);
                                        ll_prdct_8.setVisibility(View.VISIBLE);
                                        ll_prdct_4.setVisibility(View.VISIBLE);
                                    } else if (country.equalsIgnoreCase("Myanmar (Burma)")) {
                                        ll_prdct_1.setVisibility(View.VISIBLE);
                                        ll_prdct_5.setVisibility(View.VISIBLE);
                                        ll_prdct_6.setVisibility(View.VISIBLE);
                                        ll_prdct_4.setVisibility(View.VISIBLE);
                                    } else if (country.equalsIgnoreCase("Tanzania")) {
                                        ll_prdct_1.setVisibility(View.VISIBLE);
                                        ll_prdct_6.setVisibility(View.VISIBLE);
                                        ll_prdct_7.setVisibility(View.VISIBLE);
                                        ll_prdct_8.setVisibility(View.VISIBLE);
                                        ll_prdct_4.setVisibility(View.VISIBLE);
                                        //ll_prdct_10.setVisibility(View.VISIBLE);

                                    } else if (country.equalsIgnoreCase("India")) {
                                        ll_prdct_1.setVisibility(View.VISIBLE);
                                        ll_prdct_5.setVisibility(View.VISIBLE);
                                        ll_prdct_10.setVisibility(View.VISIBLE);
                                    }


                                    if (check_new_incentive.equalsIgnoreCase("Yes")) {

                                        ll_coll_commission.setVisibility(View.VISIBLE);
                                        ll_coll_posttax.setVisibility(View.VISIBLE);
                                        ll_coll_pretax.setVisibility(View.VISIBLE);
                                        ll_coll_score.setVisibility(View.VISIBLE);
                                        ll_coll_this_week_collection.setVisibility(View.VISIBLE);
                                        ll_collection_total_inc.setVisibility(View.VISIBLE);
                                        collection_inc.setVisibility(View.VISIBLE);
                                    }

                                    progressDialog.dismiss();

                                    if (check_new_incentive.equalsIgnoreCase("Yes")) {

                                        tablecode = "2025";
                                        new GetData().firstCallqueryFilter(mContext,tablecode,agentName,new VolleyCallback() {
                                            @Override
                                            public void onSuccess(String result) {


                                                collection_data = CollectionData(result,agentName);
                                                CollectionPostdata(collection_data);
                                                LoadingDialog.cancelLoading();

                                            }


                                        });

                        /*    long startTime = System.currentTimeMillis();

                            String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
*/
                                       // new CollectionIncentive().execute();
/*

                            long elapsedTime = System.currentTimeMillis() - startTime;
                            System.out.println("Total elapsed http request/response time in milliseconds: " + elapsedTime+" Incentive ");
                            TaskListDatabaseConnector taskListDatabaseConnector = new TaskListDatabaseConnector(Incentives.this);
                            taskListDatabaseConnector.insertLogs(agentName,TAG,"CollectingIncentive","https://greenlightplanet.looker.com:19999/api/3.0/looks/2025",String.valueOf(elapsedTime),currentDateTimeString,country);

                            taskListDatabaseConnector.close();

*/

                                    }


                                } else {

                                    progressDialog.dismiss();
                                    if (new GetData().isNetworkAvailable(Incentives.this)) {
//                                        new CollectionIncentive().execute();


                                        tablecode = "2025";
                                        new GetData().firstCallqueryFilter(mContext,tablecode,agentName,new VolleyCallback() {
                                            @Override
                                            public void onSuccess(String result) {

collection_data = CollectionData(result,agentName);

                                                CollectionPostdata(collection_data);

                                                LoadingDialog.cancelLoading();
                                            }


                                        });

                                    }else {
                                        createErrorDialogBoxInternetConnection(getString(R.string.no_internet_connection));
                                    }
                       /* incentive_agent_txt.setText(Incentives.this.getString(R.string.incentive_txt_dear) + " " + agentName + " " + Incentives.this.getString(R.string.incentive_txt));
                        ll_incentive.setVisibility(View.GONE);
                        progressDialog.dismiss();*/

                                }
                            }catch (Exception e) {
                                e.printStackTrace();

                            }
                        }

                    }
                });

            }



/*
                       long elapsedTime = System.currentTimeMillis() - startTime;
                        System.out.println("Total elapsed http request/response time in milliseconds: " + elapsedTime+" Incentive ");
              TaskListDatabaseConnector taskListDatabaseConnector = new TaskListDatabaseConnector(Incentives.this);
                    taskListDatabaseConnector.insertLogs(agentName,TAG,"GetIncentive","https://greenlightplanet.looker.com:19999/api/3.0/looks/"+tablecode,String.valueOf(elapsedTime),currentDateTimeString,country);

                    taskListDatabaseConnector.close();
                    // Log.d("Incentive&&&&&&&&",incentive_list.toString());
*/







        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent openTaskList = new Intent(Incentives.this, TaskList.class);
                startActivity(openTaskList);
                finish();
            }
        });


        btn_incentive_comp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {

                    // Testing the agentlist for message
                    Intent i = new Intent(Incentives.this, MainActivity.class);
                    startActivity(i);


                    // String final_date = incentive_list.get(3);

                    //   new GetData().createNewIncentiveTickets(getApplicationContext(), country+","+agentName+","+final_date);


                    //  btn_incentive_comp.setEnabled(false);

                    Toast.makeText(Incentives.this, "Firstclick", Toast.LENGTH_SHORT).show();
                    btn_incentive_comp.setEnabled(false);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        });

    }

    private void CollectionPostdata(HashMap<String, String> collection_data) {


        {

            // DecimalFormat df = new DecimalFormat("###.#");

            NumberFormat nf = NumberFormat.getNumberInstance(Locale.US);
            DecimalFormat formatter = (DecimalFormat)nf;
            formatter.applyPattern("###.#");

            String final_com_value = null;

            try {


//            Log.d("Incentive&&&&&&&&", incentive_list.toString());
                int size = collection_data.size();


             //   Log.d("SIZEEEEEEEEEE", String.valueOf(collection_data.size()));
                // String Boom = incentive_list.get("Boom");
                //    size = 0; testing
                if (size > 0) {


                    String cc_score = collection_data.get("portfolio_derived.seven_day_average_account_disabled_score");
                    String this_week_cc = collection_data.get("portfolio_derived.total_follow_on_cumulative_paid_last_7_complete_days");
                    String comm_percentage = collection_data.get("portfolio_derived.percent_of_collections_earned");
                    String pre_txinc = collection_data.get("portfolio_derived.pre_tax_total_collections_incentive");
                    String post_txinc = collection_data.get("portfolio_derived.post_tax_total_collections_incentive");





                    double cc_score_final = Double.parseDouble(cc_score);

                    // cc_score_final = Double.parseDouble(df.format(cc_score_final));
                    cc_score_final = Double.parseDouble(formatter.format(cc_score_final));

                    if (final_tax_inc == null){

                        final_tax_inc = "0.0";

                    }

                    int total_inc = (int) (Double.parseDouble(post_txinc) + Double.parseDouble(final_tax_inc));


                    if (comm_percentage.equalsIgnoreCase("0.1")) {
                        final_com_value = "10";
                    } else if (comm_percentage.equalsIgnoreCase("0.08")) {
                        final_com_value = "8";
                    } else if (comm_percentage.equalsIgnoreCase("0.06")) {
                        final_com_value = "6";

                    }



                    this_week_cc = DecimatCheck(this_week_cc);
                    pre_txinc =  DecimatCheck(pre_txinc);
                    post_txinc = DecimatCheck(post_txinc);
                    String total_inc_vslue = DecimatCheck(String.valueOf(total_inc));
                    // total_inc = Double.parseDouble(DecimatCheck(String.valueOf(total_inc)));



                    if (user_area == null ) {
                        user_area = current_Area;
                    }  if (incentive_week == null) {
                        incentive_week ="Last week";
                    }  if (prdct_pro == null) {
                        prdct_pro = "0";

                    }  if (prdct_home_120_radio == null) {

                        prdct_home_120_radio = "0";
                    }  if (prdct_boom == null) {

                        prdct_boom = "0";
                    }  if (prdct_home_60 == null) {

                        prdct_home_60 = "0";
                    }  if (prdct_home_60_radio == null) {

                        prdct_home_60_radio = "0";
                    }  if (user_currency == null) {
                        user_currency = "0";
                    }  if (agent_phone_no == null) {
                        agent_phone_no = eo_phone_Number;
                    }  if (prdct_home_120 == null) {

                        prdct_home_120 = "0";
                    }  if (prdct_home_60_stove == null) {

                        prdct_home_60_stove = "0";
                    }
                    if (prdct_home_40_z == null){

                        prdct_home_40_z  = "0";
                    }
                    if (prdct_home_60_radio_stove == null) {

                        prdct_home_60_radio_stove = "0";
                    } if (prdct_home_120_radio_stove == null) {

                        prdct_home_120_radio_stove = "0";
                    } if (prdct_400 == null) {

                        prdct_400 = "0";
                    }

                    if (pre_tax_inc == null) {
                        pre_tax_inc = "0";
                    }  if (post_tax_inc == null) {
                        post_tax_inc = "0";
                    }  if (final_tax_inc == null) {
                        final_tax_inc ="0";
                    }
                    if (dec_final_tax_inc == null){
                        dec_final_tax_inc = "0";

                    }

                    //  float final_com_per = ((Float.parseFloat(comm_percentage)*100));
///

                    ll_incentive.setVisibility(View.VISIBLE);
                    incentive_agent_txt.setText("Hi " + agentName +" "+ Incentives.this.getString(R.string.txt_incentive_ind));
                    tv_lastWeek.setText(Incentives.this.getString(R.string.inc_txt_last_week));




                    if (country.equalsIgnoreCase("Kenya")) {

                        ll_prdct_1.setVisibility(View.VISIBLE);
                        ll_prdct_5.setVisibility(View.VISIBLE);
                        ll_prdct_6.setVisibility(View.VISIBLE);
                        ll_prdct_8.setVisibility(View.VISIBLE);
                        ll_prdct_9.setVisibility(View.VISIBLE);
                        ll_prdct_4.setVisibility(View.VISIBLE);
                        ll_prdct_7.setVisibility(View.VISIBLE);
                        ll_prdct_11.setVisibility(View.VISIBLE);
                        ll_prdct_12.setVisibility(View.VISIBLE);
                        ll_prduct_13.setVisibility(View.VISIBLE);
                        currency = "KES";

                    } else if (country.equalsIgnoreCase("Uganda")) {
                        ll_prdct_1.setVisibility(View.VISIBLE);
                        ll_prdct_6.setVisibility(View.VISIBLE);
                        ll_prdct_7.setVisibility(View.VISIBLE);
                        ll_prdct_8.setVisibility(View.VISIBLE);
                        ll_prdct_4.setVisibility(View.VISIBLE);

                        currency = "UGX";
                    } else if (country.equalsIgnoreCase("Nigeria")) {
                        ll_prdct_1.setVisibility(View.VISIBLE);
                        ll_prdct_6.setVisibility(View.VISIBLE);
                        ll_prdct_5.setVisibility(View.VISIBLE);
                        ll_prdct_7.setVisibility(View.VISIBLE);
                        ll_prdct_8.setVisibility(View.VISIBLE);
                        ll_prdct_4.setVisibility(View.VISIBLE);
                        currency = "NGN";
                    } else if (country.equalsIgnoreCase("Myanmar (Burma)")) {
                        ll_prdct_1.setVisibility(View.VISIBLE);
                        ll_prdct_5.setVisibility(View.VISIBLE);
                        ll_prdct_6.setVisibility(View.VISIBLE);
                        ll_prdct_4.setVisibility(View.VISIBLE);
                        currency = "MMK";
                    } else if (country.equalsIgnoreCase("Tanzania")) {
                        ll_prdct_1.setVisibility(View.VISIBLE);
                        ll_prdct_6.setVisibility(View.VISIBLE);
                        ll_prdct_7.setVisibility(View.VISIBLE);
                        ll_prdct_8.setVisibility(View.VISIBLE);
                        ll_prdct_4.setVisibility(View.VISIBLE);
                        //ll_prdct_10.setVisibility(View.VISIBLE);
                        currency = "TZS";

                    } else if (country.equalsIgnoreCase("India")) {
                        ll_prdct_1.setVisibility(View.VISIBLE);
                        ll_prdct_5.setVisibility(View.VISIBLE);
                        ll_prdct_10.setVisibility(View.VISIBLE);
                        currency = "INR";
                    }


                    if (check_new_incentive.equalsIgnoreCase("Yes")) {

                        ll_coll_commission.setVisibility(View.VISIBLE);
                        ll_coll_posttax.setVisibility(View.VISIBLE);
                        ll_coll_pretax.setVisibility(View.VISIBLE);
                        ll_coll_score.setVisibility(View.VISIBLE);
                        ll_coll_this_week_collection.setVisibility(View.VISIBLE);
                        ll_collection_total_inc.setVisibility(View.VISIBLE);
                        collection_inc.setVisibility(View.VISIBLE);
                    }



                    txt_country.setText(country);
                    txt_area.setText(user_area);
                    txt_pro.setText(prdct_pro);
                    txt_boom.setText(prdct_boom);
                    txt_home_60.setText(prdct_home_60);
                    txt_home_40_z.setText(prdct_home_40_z);
                    txt_home.setText(prdct_home);
                    txt_home_120.setText(prdct_home_120);
                    txt_home_60_stove.setText(prdct_home_60_stove);
                    txt_400TV.setText(prdct_400);
                    txt_home_60_radio_stove.setText(prdct_home_60_radio_stove);
                    txt_home_120_radio_stove.setText(prdct_home_120_radio_stove);
                    txt_final_incentive.setText(dec_final_tax_inc);
                    txt_currency.setText(currency);
                    txt_post_incentive.setText(post_tax_inc);
                    txt_pre_incentive.setText(pre_tax_inc);
                    txt_home_120_radio.setText(prdct_home_120_radio);
                    txt_home_60_radio.setText(prdct_home_60_radio);
                    txt_homradio.setText(prdct_home_radio);
                    txt_number.setText(agent_phone_no);
                    txt_incentive_week.setText(incentive_week);


                    tv_score.setText(String.valueOf(cc_score_final));
                    tv_week_collection.setText(this_week_cc);
                    tv_commission.setText(final_com_value + " %");
                    tv_collection_pretax.setText(pre_txinc);
                    tv_collection_post.setText(post_txinc);
                    // tv_total_incentive.setText(String.valueOf(total_inc));
                    tv_total_incentive.setText(total_inc_vslue);


                    progressDialog.dismiss();


                }else{

                    incentive_agent_txt.setText(Incentives.this.getString(R.string.incentive_txt_dear) + " " + agentName + " " + Incentives.this.getString(R.string.incentive_txt));
                    ll_incentive.setVisibility(View.GONE);
                    progressDialog.dismiss();


/*


                        ll_incentive.setVisibility(View.VISIBLE);


                        incentive_agent_txt.setText("Hi " + agentName +" "+ Incentives.this.getString(R.string.txt_incentive_cong) + " " + incentive_week);



                        if (country.equalsIgnoreCase("Kenya")) {

                            ll_prdct_1.setVisibility(View.VISIBLE);
                            ll_prdct_5.setVisibility(View.VISIBLE);
                            ll_prdct_6.setVisibility(View.VISIBLE);
                            ll_prdct_8.setVisibility(View.VISIBLE);
                            ll_prdct_9.setVisibility(View.VISIBLE);
                            ll_prdct_4.setVisibility(View.VISIBLE);
                            ll_prdct_7.setVisibility(View.VISIBLE);
                            currency = "KES";

                        } else if (country.equalsIgnoreCase("Uganda")) {
                            ll_prdct_1.setVisibility(View.VISIBLE);
                            ll_prdct_7.setVisibility(View.VISIBLE);
                            ll_prdct_8.setVisibility(View.VISIBLE);
                            ll_prdct_4.setVisibility(View.VISIBLE);

                            currency = "UGX";
                        } else if (country.equalsIgnoreCase("Nigeria")) {
                            ll_prdct_1.setVisibility(View.VISIBLE);
                            ll_prdct_5.setVisibility(View.VISIBLE);
                            ll_prdct_7.setVisibility(View.VISIBLE);
                            ll_prdct_8.setVisibility(View.VISIBLE);
                            ll_prdct_4.setVisibility(View.VISIBLE);
                            currency = "NGN";
                        } else if (country.equalsIgnoreCase("Myanmar (Burma)")) {
                            ll_prdct_1.setVisibility(View.VISIBLE);
                            ll_prdct_5.setVisibility(View.VISIBLE);
                            ll_prdct_6.setVisibility(View.VISIBLE);
                            ll_prdct_4.setVisibility(View.VISIBLE);
                            currency = "MMK";
                        } else if (country.equalsIgnoreCase("Tanzania")) {
                            ll_prdct_1.setVisibility(View.VISIBLE);
                            ll_prdct_7.setVisibility(View.VISIBLE);
                            ll_prdct_8.setVisibility(View.VISIBLE);
                            ll_prdct_4.setVisibility(View.VISIBLE);
                            ll_prdct_10.setVisibility(View.VISIBLE);
                            currency = "TZS";

                        } else if (country.equalsIgnoreCase("India")) {
                            ll_prdct_1.setVisibility(View.VISIBLE);
                            ll_prdct_5.setVisibility(View.VISIBLE);
                            currency = "INR";
                        }


                        if (check_new_incentive.equalsIgnoreCase("Yes")) {

                            ll_coll_commission.setVisibility(View.VISIBLE);
                            ll_coll_posttax.setVisibility(View.VISIBLE);
                            ll_coll_pretax.setVisibility(View.VISIBLE);
                            ll_coll_score.setVisibility(View.VISIBLE);
                            ll_coll_this_week_collection.setVisibility(View.VISIBLE);
                            ll_collection_total_inc.setVisibility(View.VISIBLE);
                            collection_inc.setVisibility(View.VISIBLE);
                        }




                        txt_country.setText(country);
                        txt_area.setText(user_area);
                        txt_pro.setText(prdct_pro);
                        txt_boom.setText(prdct_boom);
                        txt_home_60.setText(prdct_home_60);
                        txt_home_40_z.setText(prdct_home_40_z);
                        txt_home.setText(prdct_home);
                        txt_home_120.setText(prdct_home_120);
                        txt_home_60_stove.setText(prdct_home_60_stove);
                        txt_final_incentive.setText(dec_final_tax_inc);
                        txt_currency.setText(currency);
                        txt_post_incentive.setText(post_tax_inc);
                        txt_pre_incentive.setText(pre_tax_inc);
                        txt_home_120_radio.setText(prdct_home_120_radio);
                        txt_home_60_radio.setText(prdct_home_60_radio);
                        txt_homradio.setText(prdct_home_radio);
                        txt_number.setText(agent_phone_no);
                        txt_incentive_week.setText(incentive_week);



*/


                    progressDialog.dismiss();





                }


            } catch (Exception e) {
                e.printStackTrace();
                progressDialog.dismiss();
                Toast.makeText(Incentives.this, "Something went wrong!! Please try again", Toast.LENGTH_SHORT).show();

            }

        }


    }

    public HashMap<String,String> CollectionData(String result,String Name) {


            HashMap<String, String> collection_data = new HashMap<>();


        try {

            JSONArray agentListData = new JSONArray(result);

            ///    ArrayList<String> agentincentiveList = new ArrayList<String>();


                if (!(agentListData == null)) {
                    for (int i = 0; i < agentListData.length(); i++) {

                        JSONObject agentName = (JSONObject) agentListData.get(i);

                        if (agentName.get("accounts.agent").toString().equals(Name)) {

                            collection_data.put("portfolio_derived.seven_day_average_account_disabled_score", agentName.get("portfolio_derived.seven_day_average_account_disabled_score").toString());

                            collection_data.put("portfolio_derived.total_follow_on_cumulative_paid_last_7_complete_days", agentName.get("portfolio_derived.total_follow_on_cumulative_paid_last_7_complete_days").toString());
                            collection_data.put("portfolio_derived.percent_of_collections_earned", agentName.get("portfolio_derived.percent_of_collections_earned").toString());
                            collection_data.put("portfolio_derived.pre_tax_total_collections_incentive", agentName.get("portfolio_derived.pre_tax_total_collections_incentive").toString());
                            collection_data.put("portfolio_derived.post_tax_total_collections_incentive", agentName.get("portfolio_derived.post_tax_total_collections_incentive").toString());

                        } else {


                        }
                    }
                }
                return collection_data;

            } catch (JSONException ej) {
                ej.printStackTrace();
//                Log.d("JSON Error ", ej.toString());

            } catch (Exception e) {
                e.printStackTrace();

           //     Log.d("Error in List", e.toString());
                return null;
            }
            return collection_data;

        }


    @Override
    public void onBackPressed() {
        Intent i = new Intent(Incentives.this, TaskList.class);
        startActivity(i);
        this.finish();
    }

    private class IncentiveAsyntask extends AsyncTask<Void, Void, Void> {



        @Override
        protected void onPreExecute() {
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {


            if (new GetData().isNetworkAvailable(Incentives.this)) {

                try {
/*
                    long startTime = System.currentTimeMillis();

                    String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
*/

                    if (country.equalsIgnoreCase("Kenya")) {
                        tablecode = "418";
                        //incentive_list = new GetData().getIncentivelist(agentName, tablecode);


                        LoadingDialog.showLoadingDialog(mContext,"Loading....");

                        new GetData().firstCallqueryFilter(mContext,tablecode,agentName,new VolleyCallback(){


                            @Override
                            public void onSuccess(String result) {
                                incentive_list  =       IncentiveData(result,agentName);

                            }
                            });
                        } else if (country.equalsIgnoreCase("Uganda")) {
                        tablecode = "421";
                        //incentive_list = new GetData().getIncentivelist(agentName, tablecode);

                        LoadingDialog.showLoadingDialog(mContext,"Loading....");

                        new GetData().firstCallqueryFilter(mContext,tablecode,agentName,new VolleyCallback(){


                            @Override
                            public void onSuccess(String result) {
                                incentive_list=   IncentiveData(result,agentName);

                            }
                        });

                    } else if (country.equalsIgnoreCase("Nigeria")) {
                        tablecode = "420";
                        //incentive_list = new GetData().getIncentivelist(agentName, tablecode);

                        LoadingDialog.showLoadingDialog(mContext,"Loading....");

                        new GetData().firstCallqueryFilter(mContext,tablecode,agentName,new VolleyCallback(){


                            @Override
                            public void onSuccess(String result) {

                                incentive_list=     IncentiveData(result,agentName);
                            }
                        });

                    } else if (country.equalsIgnoreCase("Myanmar (Burma)")) {
                        tablecode = "419";
                        //incentive_list = new GetData().getIncentivelist(agentName, tablecode);

                        LoadingDialog.showLoadingDialog(mContext,"Loading....");

                        new GetData().firstCallqueryFilter(mContext,tablecode,agentName,new VolleyCallback(){

                            @Override
                            public void onSuccess(String result) {

                                incentive_list=    IncentiveData(result,agentName);
                            }
                        });
                    } else if (country.equalsIgnoreCase("Tanzania")) {
                        tablecode = "441";
                        //incentive_list = new GetData().getIncentivelist(agentName, tablecode);
                        LoadingDialog.showLoadingDialog(mContext,"Loading....");

                        new GetData().firstCallqueryFilter(mContext,tablecode,agentName,new VolleyCallback(){

                            @Override
                            public void onSuccess(String result) {

                                incentive_list=   IncentiveData(result,agentName);

                            }
                        });
                    } else if (country.equalsIgnoreCase("India")) {
                        tablecode = "1776";
                        //incentive_list = new GetData().getIncentivelist(agentName, tablecode);
                        LoadingDialog.showLoadingDialog(mContext,"Loading....");

                        new GetData().firstCallqueryFilter(mContext,tablecode,agentName,new VolleyCallback(){


                            @Override
                            public void onSuccess(String result) {
                                incentive_list  =  IncentiveData(result,agentName);
                            }
                        });

                    }



/*
                       long elapsedTime = System.currentTimeMillis() - startTime;
                        System.out.println("Total elapsed http request/response time in milliseconds: " + elapsedTime+" Incentive ");
              TaskListDatabaseConnector taskListDatabaseConnector = new TaskListDatabaseConnector(Incentives.this);
                    taskListDatabaseConnector.insertLogs(agentName,TAG,"GetIncentive","https://greenlightplanet.looker.com:19999/api/3.0/looks/"+tablecode,String.valueOf(elapsedTime),currentDateTimeString,country);

                    taskListDatabaseConnector.close();
                    // Log.d("Incentive&&&&&&&&",incentive_list.toString());
*/
                } catch (Exception e) {

                }

            } else {
                createErrorDialogBoxInternetConnection(getString(R.string.no_internet_connection));
                // finish();
                //  Toast.makeText(AgentSelection.this,"Please Connect to Internet Connection...",Toast.LENGTH_SHORT);
                //finish();
            }
            return null;
        }


        @Override
        protected void onPostExecute(Void aVoid) {

            String txt, key = null, value = null;
            String home, pro = null, boom = null, homeradio = null;
            try {


//            Log.d("Incentive&&&&&&&&", incentive_list.toString());
            // int size = incentive_list.size();
                // String Boom = incentive_list.get("Boom");
                //    size = 0; testing
                if (incentive_list.size() > 0) {

                    Iterator myVeryOwnIterator = incentive_list.keySet().iterator();

                    while (myVeryOwnIterator.hasNext()) {
                        key = (String) myVeryOwnIterator.next();
                        value = (String) incentive_list.get(key);


                        if (key.equalsIgnoreCase("country")) {
                            user_country = value.toString();
                        } else if (key.equalsIgnoreCase("user_area")) {
                            user_area = value.toString();
                        } else if (key.equalsIgnoreCase("phone_fees_incentive_week")) {
                            incentive_week = value.toString();
                        } else if (key.equalsIgnoreCase("Pro")) {
                            if (value.equals("null")) {
                                value = "0";
                            }
                            prdct_pro = value.toString();
                        } else if (key.equalsIgnoreCase("Home 120 + Radio")) {
                            if (value.equals("null")) {
                                value = "0";
                            }
                            prdct_home_120_radio = value.toString();
                        } else if (key.equalsIgnoreCase("Boom")) {
                            if (value.equals("null")) {
                                value = "0";
                            }
                            prdct_boom = value.toString();
                        } else if (key.equalsIgnoreCase("Home 60")) {
                            if (value.equals("null")) {
                                value = "0";
                            }
                            prdct_home_60 = value.toString();
                        } else if (key.equalsIgnoreCase("Home 60 + Radio")) {
                            if (value.equals("null")) {
                                value = "0";
                            }
                            prdct_home_60_radio = value.toString();
                        } else if (key.equalsIgnoreCase("user_currency")) {
                            user_currency = value.toString();
                        } else if (key.equalsIgnoreCase("agent_phone_number")) {
                            agent_phone_no = value.toString();
                        } else if (key.equalsIgnoreCase("Home 120")) {
                            if (value.equals("null")) {
                                value = "0";
                            }
                            prdct_home_120 = value.toString();
                        } else if (key.equalsIgnoreCase("Home 60 + Stove")) {
                            if (value.equals("null")) {
                                value = "0";
                            }
                            prdct_home_60_stove = value.toString();
                        } else if (key.equalsIgnoreCase("Home 40-Z")){
                            if (value.equals("null")) {
                                value = "0";
                            }
                            prdct_home_40_z = value.toString();
                        }else if (key.equalsIgnoreCase("Home 60 + Radio + Stove")) {
                            if (value.equals("null")) {
                                value = "0";
                            }
                            prdct_home_60_radio_stove = value.toString();
                        }else if (key.equalsIgnoreCase("Home 120 + Radio + Stove")) {
                            if (value.equals("null")) {
                                value = "0";
                            }
                            prdct_home_120_radio_stove = value.toString();
                        }else if (key.equalsIgnoreCase("Home 400")) {
                            if (value.equals("null")) {
                                value = "0";
                            }
                            prdct_400 = value.toString();
                        }

                        if (prdct_home_120 == null) {

                            prdct_home_120 = "0";
                        }
                        if (prdct_home_40_z == null){

                            prdct_home_40_z  = "0";
                        }
                        if (prdct_boom == null) {

                            prdct_boom = "0";
                        }


                        else if (key.equalsIgnoreCase("pre_tax_inc")) {
                            pre_tax_inc = value.toString();
                        } else if (key.equalsIgnoreCase("post_tax_inc")) {
                            post_tax_inc = value.toString();
                        } else if (key.equalsIgnoreCase("final_tax_inc_to_pay")) {
                            final_tax_inc = value.toString();
                        }





                      //  Log.d("Incentive ttx", "Key: " + key + " Value: " + value);
                        //    Toast.makeText("Incentice txt", "Key: "+key+" Value: "+value, Toast.LENGTH_LONG).show();
                    }

                    ll_incentive.setVisibility(View.VISIBLE);
                    // incentive_agent_txt.setText("Hi " + agentName + Incentives.this.getString(R.string.txt_incentive_cong) +" "+ incentive_list.get(3));
                    incentive_agent_txt.setText("Hi " + agentName +" "+ Incentives.this.getString(R.string.txt_incentive_cong) + " " + incentive_week);

          post_tax_inc =   DecimatCheck(post_tax_inc);
          pre_tax_inc=  DecimatCheck(pre_tax_inc);
         // dec_final_tax_inc = final_tax_inc;
                    dec_final_tax_inc = DecimatCheck(final_tax_inc);



                    txt_country.setText(country);
                    txt_area.setText(user_area);
                    txt_pro.setText(prdct_pro);
                    txt_boom.setText(prdct_boom);
                    txt_home_60.setText(prdct_home_60);
                    txt_home_40_z.setText(prdct_home_40_z);
                    txt_home.setText(prdct_home);
                    txt_home_120.setText(prdct_home_120);
                    txt_home_60_stove.setText(prdct_home_60_stove);
                    txt_400TV.setText(prdct_400);
                    txt_home_60_radio_stove.setText(prdct_home_60_radio_stove);
                    txt_home_120_radio_stove.setText(prdct_home_120_radio_stove);
                    txt_final_incentive.setText(dec_final_tax_inc);
                    txt_currency.setText(user_currency);
                    txt_post_incentive.setText(post_tax_inc);
                    txt_pre_incentive.setText(pre_tax_inc);
                    txt_home_120_radio.setText(prdct_home_120_radio);
                    txt_home_60_radio.setText(prdct_home_60_radio);
                    txt_homradio.setText(prdct_home_radio);
                    txt_number.setText(agent_phone_no);
                    txt_incentive_week.setText(incentive_week);

                    if (country.equalsIgnoreCase("Kenya")) {

                        ll_prdct_1.setVisibility(View.VISIBLE);
                        ll_prdct_5.setVisibility(View.VISIBLE);
                        ll_prdct_6.setVisibility(View.VISIBLE);
                        ll_prdct_8.setVisibility(View.VISIBLE);
                        ll_prdct_9.setVisibility(View.VISIBLE);
                        ll_prdct_4.setVisibility(View.VISIBLE);
                        ll_prdct_7.setVisibility(View.VISIBLE);
                        ll_prdct_11.setVisibility(View.VISIBLE);
                        ll_prdct_12.setVisibility(View.VISIBLE);
                        ll_prduct_13.setVisibility(View.VISIBLE);

                    } else if (country.equalsIgnoreCase("Uganda")) {
                        ll_prdct_1.setVisibility(View.VISIBLE);
                        ll_prdct_6.setVisibility(View.VISIBLE);
                        ll_prdct_7.setVisibility(View.VISIBLE);
                        ll_prdct_8.setVisibility(View.VISIBLE);
                        ll_prdct_4.setVisibility(View.VISIBLE);
                    } else if (country.equalsIgnoreCase("Nigeria")) {
                        ll_prdct_1.setVisibility(View.VISIBLE);
                        ll_prdct_6.setVisibility(View.VISIBLE);
                        ll_prdct_5.setVisibility(View.VISIBLE);
                        ll_prdct_7.setVisibility(View.VISIBLE);
                        ll_prdct_8.setVisibility(View.VISIBLE);
                        ll_prdct_4.setVisibility(View.VISIBLE);
                    } else if (country.equalsIgnoreCase("Myanmar (Burma)")) {
                        ll_prdct_1.setVisibility(View.VISIBLE);
                        ll_prdct_5.setVisibility(View.VISIBLE);
                        ll_prdct_6.setVisibility(View.VISIBLE);
                        ll_prdct_4.setVisibility(View.VISIBLE);
                    } else if (country.equalsIgnoreCase("Tanzania")) {
                        ll_prdct_1.setVisibility(View.VISIBLE);
                        ll_prdct_6.setVisibility(View.VISIBLE);
                        ll_prdct_7.setVisibility(View.VISIBLE);
                        ll_prdct_8.setVisibility(View.VISIBLE);
                        ll_prdct_4.setVisibility(View.VISIBLE);
                        //ll_prdct_10.setVisibility(View.VISIBLE);

                    } else if (country.equalsIgnoreCase("India")) {
                        ll_prdct_1.setVisibility(View.VISIBLE);
                        ll_prdct_5.setVisibility(View.VISIBLE);
                        ll_prdct_10.setVisibility(View.VISIBLE);
                    }


                    if (check_new_incentive.equalsIgnoreCase("Yes")) {

                        ll_coll_commission.setVisibility(View.VISIBLE);
                        ll_coll_posttax.setVisibility(View.VISIBLE);
                        ll_coll_pretax.setVisibility(View.VISIBLE);
                        ll_coll_score.setVisibility(View.VISIBLE);
                        ll_coll_this_week_collection.setVisibility(View.VISIBLE);
                        ll_collection_total_inc.setVisibility(View.VISIBLE);
                        collection_inc.setVisibility(View.VISIBLE);
                    }

                   progressDialog.dismiss();

                        if (check_new_incentive.equalsIgnoreCase("Yes")) {


                        /*    long startTime = System.currentTimeMillis();

                            String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
*/
                            new CollectionIncentive().execute();
/*

                            long elapsedTime = System.currentTimeMillis() - startTime;
                            System.out.println("Total elapsed http request/response time in milliseconds: " + elapsedTime+" Incentive ");
                            TaskListDatabaseConnector taskListDatabaseConnector = new TaskListDatabaseConnector(Incentives.this);
                            taskListDatabaseConnector.insertLogs(agentName,TAG,"CollectingIncentive","https://greenlightplanet.looker.com:19999/api/3.0/looks/2025",String.valueOf(elapsedTime),currentDateTimeString,country);

                            taskListDatabaseConnector.close();

*/

                        }


                    } else {

                    progressDialog.dismiss();
                    if (new GetData().isNetworkAvailable(Incentives.this)) {
                        new CollectionIncentive().execute();

                    }else {
                        createErrorDialogBoxInternetConnection(getString(R.string.no_internet_connection));
                    }
                       /* incentive_agent_txt.setText(Incentives.this.getString(R.string.incentive_txt_dear) + " " + agentName + " " + Incentives.this.getString(R.string.incentive_txt));
                        ll_incentive.setVisibility(View.GONE);
                        progressDialog.dismiss();*/

                }
                }catch (Exception e) {
                e.printStackTrace();

            }
            }
        }

    public HashMap<String, String> IncentiveData(String result,String Name) {

        JSONArray agentListData = null;
        try {
            agentListData = new JSONArray(result);


            if (!(agentListData == null)) {
                for (int i = 0; i < agentListData.length(); i++) {

                    JSONObject agentName = (JSONObject) agentListData.get(i);

                    if (agentName.get("accounts.agent").toString().equals(Name)) {
                        incentive_data.put("country", agentName.get("accounts.country").toString());
                        incentive_data.put("user_area", agentName.get("angaza_users_facts.current_area").toString());
                        //      agentincentiveList.add((String) agentName.get("accounts.agent"));
                        //    agentincentiveList.add((String) agentName.get("angaza_users.username"));
                        incentive_data.put("agent_phone_number", agentName.get("angaza_users.agent_phone_number").toString());
                        incentive_data.put("phone_fees_incentive_week", agentName.get("phone_fees.incentive_week").toString());
                        incentive_data.put("pre_tax_inc", agentName.get("phone_fees.pre_tax_total_upfront_incentive").toString());
                        incentive_data.put("post_tax_inc", agentName.get("phone_fees.post_tax_total_upfront_incentive").toString());
                        incentive_data.put("final_tax_inc_to_pay", agentName.get("phone_fees.final_incentive_to_pay").toString());
                        incentive_data.put("user_currency", agentName.get("angaza_users.currency").toString());
                        //agentincentiveList.add((String) agentName.get("accounts.count_units").toString());

                        JSONObject product = agentName.getJSONObject("accounts.count_units");
                        JSONObject productname = product.getJSONObject("accounts.product_name");


                        if (productname.has("Boom")) {
                            incentive_data.put("Boom", productname.getString("Boom").toString());
                        }
                        if (productname.has("Home")) {
                            incentive_data.put("Home", productname.getString("Home").toString());
                        }
                        if (productname.has("Home + Radio")) {
                            incentive_data.put("Home + Radio", productname.getString("Home + Radio").toString());
                        }
                        if (productname.has("Pro")) {
                            incentive_data.put("Pro", productname.getString("Pro").toString());
                        }
                        if (productname.has("Home 120")) {
                            incentive_data.put("Home 120", productname.getString("Home 120").toString());
                        }
                        if (productname.has("Home 120 + Radio")) {
                            incentive_data.put("Home 120 + Radio", productname.getString("Home 120 + Radio").toString());
                        }
                        if (productname.has("Home 60")) {
                            incentive_data.put("Home 60", productname.getString("Home 60"));
                        }
                        if (productname.has("Home 60 + Radio")) {
                            incentive_data.put("Home 60 + Radio", productname.getString("Home 60 + Radio"));
                        }
                        if (productname.has("Home 60 + Stove")) {
                            incentive_data.put("Home 60 + Stove", productname.getString("Home 60 + Stove"));
                        }
                        if (productname.has("Home 40-Z")) {
                            incentive_data.put("Home 40-Z", productname.getString("Home 40-Z").toString());
                        }
                        if (productname.has("Home 60 + Radio + Stove")) {
                            incentive_data.put("Home 60 + Radio + Stove", productname.getString("Home 60 + Radio + Stove").toString());
                        }
                        if (productname.has("Home 120 + Radio + Stove")) {
                            incentive_data.put("Home 120 + Radio Stove", productname.getString("Home 120 + Radio + Stove").toString());
                        }
                        if (productname.has("Home 400")) {
                            incentive_data.put("Home 400", productname.getString("Home 400").toString());
                        }

                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return incentive_data;
    }
    private String DecimatCheck(String Value) {

        DecimalFormat decimalFormat = new DecimalFormat("#,###");
        DecimalFormat decimalFormat1 = new DecimalFormat("##,###");
        DecimalFormat decimalFormat2 = new DecimalFormat("###,###");
        DecimalFormat decimalFormat3 = new DecimalFormat("##,##,###");
        String Txt_Value;

        String text = Double.toString(Math.abs(Double.parseDouble(String.valueOf(Value))));
        int integerPlaces = text.indexOf('.');
        int decimalPlaces = text.length() - integerPlaces - 1;

        try {
            //  Double.parseDouble(df.format(final_value));


            if (integerPlaces == 4) {

                Txt_Value  =    decimalFormat.format(Double.parseDouble(Value)).toString();
                Value = Txt_Value;

            }
            else if (integerPlaces == 5){
                Txt_Value = decimalFormat1.format(Double.parseDouble(Value)).toString();
                Value = Txt_Value;
            }else if (integerPlaces == 6){

                Txt_Value = decimalFormat2.format(Double.parseDouble(Value)).toString();
                Value = Txt_Value;
            }else if (integerPlaces == 7){

                Txt_Value = decimalFormat3.format(Double.parseDouble(Value)).toString();
                Value = Txt_Value;


            }


        } catch (NumberFormatException ex){
            ex.printStackTrace();
        } catch (Exception e){
            e.printStackTrace();
        }


        return  Value;

    }


    private class CollectionIncentive extends AsyncTask<Void, Void, Void> {

            @Override
            protected void onPreExecute() {
                progressDialog.show();
            }

            @Override
            protected Void doInBackground(Void... params) {



                if (new GetData().isNetworkAvailable(Incentives.this)) {

                    try {
                        if (check_new_incentive.equalsIgnoreCase("Yes")) {
                            tablecode = "2025";
                           collection_data = new GetData().getCollectionData(agentName, tablecode);
//ldmcl;mdm


                        }

                        // Log.d("Incentive&&&&&&&&",incentive_list.toString());
                    } catch (Exception e) {

                        progressDialog.dismiss();
                        Toast.makeText(Incentives.this,"Something went wrong!!",Toast.LENGTH_SHORT).show();
                    }

                } else {
                    createErrorDialogBoxInternetConnection(getString(R.string.no_internet_connection));
                    // finish();
                    //  Toast.makeText(AgentSelection.this,"Please Connect to Internet Connection...",Toast.LENGTH_SHORT);
                    //finish();
                }
                return null;
            }


            @Override
            protected void onPostExecute(Void aVoid) {

               // DecimalFormat df = new DecimalFormat("###.#");

                NumberFormat nf = NumberFormat.getNumberInstance(Locale.US);
                DecimalFormat formatter = (DecimalFormat)nf;
                formatter.applyPattern("###.#");

                String final_com_value = null;

                try {


//            Log.d("Incentive&&&&&&&&", incentive_list.toString());
                    int size = collection_data.size();
                    // String Boom = incentive_list.get("Boom");
                    //    size = 0; testing
                    if (size > 0) {


                        String cc_score = collection_data.get("portfolio_derived.seven_day_average_account_disabled_score");
                        String this_week_cc = collection_data.get("portfolio_derived.total_follow_on_cumulative_paid_last_7_complete_days");
                        String comm_percentage = collection_data.get("portfolio_derived.percent_of_collections_earned");
                        String pre_txinc = collection_data.get("portfolio_derived.pre_tax_total_collections_incentive");
                        String post_txinc = collection_data.get("portfolio_derived.post_tax_total_collections_incentive");





                        double cc_score_final = Double.parseDouble(cc_score);

                       // cc_score_final = Double.parseDouble(df.format(cc_score_final));
                        cc_score_final = Double.parseDouble(formatter.format(cc_score_final));

                      if (final_tax_inc == null){

                          final_tax_inc = "0.0";

                      }

                        int total_inc = (int) (Double.parseDouble(post_txinc) + Double.parseDouble(final_tax_inc));


                        if (comm_percentage.equalsIgnoreCase("0.1")) {
                            final_com_value = "10";
                        } else if (comm_percentage.equalsIgnoreCase("0.08")) {
                            final_com_value = "8";
                        } else if (comm_percentage.equalsIgnoreCase("0.06")) {
                            final_com_value = "6";

                        }



                      this_week_cc = DecimatCheck(this_week_cc);
                        pre_txinc =  DecimatCheck(pre_txinc);
                        post_txinc = DecimatCheck(post_txinc);
                       String total_inc_vslue = DecimatCheck(String.valueOf(total_inc));
                       // total_inc = Double.parseDouble(DecimatCheck(String.valueOf(total_inc)));



                        if (user_area == null ) {
                            user_area = current_Area;
                        }  if (incentive_week == null) {
                            incentive_week ="Last week";
                        }  if (prdct_pro == null) {
                            prdct_pro = "0";

                        }  if (prdct_home_120_radio == null) {

                            prdct_home_120_radio = "0";
                        }  if (prdct_boom == null) {

                            prdct_boom = "0";
                        }  if (prdct_home_60 == null) {

                            prdct_home_60 = "0";
                        }  if (prdct_home_60_radio == null) {

                            prdct_home_60_radio = "0";
                        }  if (user_currency == null) {
                            user_currency = "0";
                        }  if (agent_phone_no == null) {
                            agent_phone_no = eo_phone_Number;
                        }  if (prdct_home_120 == null) {

                            prdct_home_120 = "0";
                        }  if (prdct_home_60_stove == null) {

                            prdct_home_60_stove = "0";
                        }
                        if (prdct_home_40_z == null){

                            prdct_home_40_z  = "0";
                        }
                         if (prdct_home_60_radio_stove == null) {

                            prdct_home_60_radio_stove = "0";
                        } if (prdct_home_120_radio_stove == null) {

                            prdct_home_120_radio_stove = "0";
                        } if (prdct_400 == null) {

                            prdct_400 = "0";
                        }

                        if (pre_tax_inc == null) {
                            pre_tax_inc = "0";
                        }  if (post_tax_inc == null) {
                            post_tax_inc = "0";
                        }  if (final_tax_inc == null) {
                            final_tax_inc ="0";
                        }
                        if (dec_final_tax_inc == null){
                            dec_final_tax_inc = "0";

                        }

                        //  float final_com_per = ((Float.parseFloat(comm_percentage)*100));
///

                        ll_incentive.setVisibility(View.VISIBLE);
                        incentive_agent_txt.setText("Hi " + agentName +" "+ Incentives.this.getString(R.string.txt_incentive_ind));
                        tv_lastWeek.setText(Incentives.this.getString(R.string.inc_txt_last_week));




                        if (country.equalsIgnoreCase("Kenya")) {

                            ll_prdct_1.setVisibility(View.VISIBLE);
                            ll_prdct_5.setVisibility(View.VISIBLE);
                            ll_prdct_6.setVisibility(View.VISIBLE);
                            ll_prdct_8.setVisibility(View.VISIBLE);
                            ll_prdct_9.setVisibility(View.VISIBLE);
                            ll_prdct_4.setVisibility(View.VISIBLE);
                            ll_prdct_7.setVisibility(View.VISIBLE);
                            ll_prdct_11.setVisibility(View.VISIBLE);
                            ll_prdct_12.setVisibility(View.VISIBLE);
                            ll_prduct_13.setVisibility(View.VISIBLE);
                            currency = "KES";

                        } else if (country.equalsIgnoreCase("Uganda")) {
                            ll_prdct_1.setVisibility(View.VISIBLE);
                            ll_prdct_6.setVisibility(View.VISIBLE);
                            ll_prdct_7.setVisibility(View.VISIBLE);
                            ll_prdct_8.setVisibility(View.VISIBLE);
                            ll_prdct_4.setVisibility(View.VISIBLE);

                            currency = "UGX";
                        } else if (country.equalsIgnoreCase("Nigeria")) {
                            ll_prdct_1.setVisibility(View.VISIBLE);
                            ll_prdct_6.setVisibility(View.VISIBLE);
                            ll_prdct_5.setVisibility(View.VISIBLE);
                            ll_prdct_7.setVisibility(View.VISIBLE);
                            ll_prdct_8.setVisibility(View.VISIBLE);
                            ll_prdct_4.setVisibility(View.VISIBLE);
                            currency = "NGN";
                        } else if (country.equalsIgnoreCase("Myanmar (Burma)")) {
                            ll_prdct_1.setVisibility(View.VISIBLE);
                            ll_prdct_5.setVisibility(View.VISIBLE);
                            ll_prdct_6.setVisibility(View.VISIBLE);
                            ll_prdct_4.setVisibility(View.VISIBLE);
                            currency = "MMK";
                        } else if (country.equalsIgnoreCase("Tanzania")) {
                            ll_prdct_1.setVisibility(View.VISIBLE);
                            ll_prdct_6.setVisibility(View.VISIBLE);
                            ll_prdct_7.setVisibility(View.VISIBLE);
                            ll_prdct_8.setVisibility(View.VISIBLE);
                            ll_prdct_4.setVisibility(View.VISIBLE);
                            //ll_prdct_10.setVisibility(View.VISIBLE);
                            currency = "TZS";

                        } else if (country.equalsIgnoreCase("India")) {
                            ll_prdct_1.setVisibility(View.VISIBLE);
                            ll_prdct_5.setVisibility(View.VISIBLE);
                            ll_prdct_10.setVisibility(View.VISIBLE);
                            currency = "INR";
                        }


                        if (check_new_incentive.equalsIgnoreCase("Yes")) {

                            ll_coll_commission.setVisibility(View.VISIBLE);
                            ll_coll_posttax.setVisibility(View.VISIBLE);
                            ll_coll_pretax.setVisibility(View.VISIBLE);
                            ll_coll_score.setVisibility(View.VISIBLE);
                            ll_coll_this_week_collection.setVisibility(View.VISIBLE);
                            ll_collection_total_inc.setVisibility(View.VISIBLE);
                            collection_inc.setVisibility(View.VISIBLE);
                        }



                        txt_country.setText(country);
                        txt_area.setText(user_area);
                        txt_pro.setText(prdct_pro);
                        txt_boom.setText(prdct_boom);
                        txt_home_60.setText(prdct_home_60);
                        txt_home_40_z.setText(prdct_home_40_z);
                        txt_home.setText(prdct_home);
                        txt_home_120.setText(prdct_home_120);
                        txt_home_60_stove.setText(prdct_home_60_stove);
                        txt_400TV.setText(prdct_400);
                        txt_home_60_radio_stove.setText(prdct_home_60_radio_stove);
                        txt_home_120_radio_stove.setText(prdct_home_120_radio_stove);
                        txt_final_incentive.setText(dec_final_tax_inc);
                        txt_currency.setText(currency);
                        txt_post_incentive.setText(post_tax_inc);
                        txt_pre_incentive.setText(pre_tax_inc);
                        txt_home_120_radio.setText(prdct_home_120_radio);
                        txt_home_60_radio.setText(prdct_home_60_radio);
                        txt_homradio.setText(prdct_home_radio);
                        txt_number.setText(agent_phone_no);
                        txt_incentive_week.setText(incentive_week);


                        tv_score.setText(String.valueOf(cc_score_final));
                        tv_week_collection.setText(this_week_cc);
                        tv_commission.setText(final_com_value + " %");
                        tv_collection_pretax.setText(pre_txinc);
                        tv_collection_post.setText(post_txinc);
                       // tv_total_incentive.setText(String.valueOf(total_inc));
                        tv_total_incentive.setText(total_inc_vslue);


                        progressDialog.dismiss();


                    }else{

                        incentive_agent_txt.setText(Incentives.this.getString(R.string.incentive_txt_dear) + " " + agentName + " " + Incentives.this.getString(R.string.incentive_txt));
                        ll_incentive.setVisibility(View.GONE);
                        progressDialog.dismiss();


/*


                        ll_incentive.setVisibility(View.VISIBLE);


                        incentive_agent_txt.setText("Hi " + agentName +" "+ Incentives.this.getString(R.string.txt_incentive_cong) + " " + incentive_week);



                        if (country.equalsIgnoreCase("Kenya")) {

                            ll_prdct_1.setVisibility(View.VISIBLE);
                            ll_prdct_5.setVisibility(View.VISIBLE);
                            ll_prdct_6.setVisibility(View.VISIBLE);
                            ll_prdct_8.setVisibility(View.VISIBLE);
                            ll_prdct_9.setVisibility(View.VISIBLE);
                            ll_prdct_4.setVisibility(View.VISIBLE);
                            ll_prdct_7.setVisibility(View.VISIBLE);
                            currency = "KES";

                        } else if (country.equalsIgnoreCase("Uganda")) {
                            ll_prdct_1.setVisibility(View.VISIBLE);
                            ll_prdct_7.setVisibility(View.VISIBLE);
                            ll_prdct_8.setVisibility(View.VISIBLE);
                            ll_prdct_4.setVisibility(View.VISIBLE);

                            currency = "UGX";
                        } else if (country.equalsIgnoreCase("Nigeria")) {
                            ll_prdct_1.setVisibility(View.VISIBLE);
                            ll_prdct_5.setVisibility(View.VISIBLE);
                            ll_prdct_7.setVisibility(View.VISIBLE);
                            ll_prdct_8.setVisibility(View.VISIBLE);
                            ll_prdct_4.setVisibility(View.VISIBLE);
                            currency = "NGN";
                        } else if (country.equalsIgnoreCase("Myanmar (Burma)")) {
                            ll_prdct_1.setVisibility(View.VISIBLE);
                            ll_prdct_5.setVisibility(View.VISIBLE);
                            ll_prdct_6.setVisibility(View.VISIBLE);
                            ll_prdct_4.setVisibility(View.VISIBLE);
                            currency = "MMK";
                        } else if (country.equalsIgnoreCase("Tanzania")) {
                            ll_prdct_1.setVisibility(View.VISIBLE);
                            ll_prdct_7.setVisibility(View.VISIBLE);
                            ll_prdct_8.setVisibility(View.VISIBLE);
                            ll_prdct_4.setVisibility(View.VISIBLE);
                            ll_prdct_10.setVisibility(View.VISIBLE);
                            currency = "TZS";

                        } else if (country.equalsIgnoreCase("India")) {
                            ll_prdct_1.setVisibility(View.VISIBLE);
                            ll_prdct_5.setVisibility(View.VISIBLE);
                            currency = "INR";
                        }


                        if (check_new_incentive.equalsIgnoreCase("Yes")) {

                            ll_coll_commission.setVisibility(View.VISIBLE);
                            ll_coll_posttax.setVisibility(View.VISIBLE);
                            ll_coll_pretax.setVisibility(View.VISIBLE);
                            ll_coll_score.setVisibility(View.VISIBLE);
                            ll_coll_this_week_collection.setVisibility(View.VISIBLE);
                            ll_collection_total_inc.setVisibility(View.VISIBLE);
                            collection_inc.setVisibility(View.VISIBLE);
                        }




                        txt_country.setText(country);
                        txt_area.setText(user_area);
                        txt_pro.setText(prdct_pro);
                        txt_boom.setText(prdct_boom);
                        txt_home_60.setText(prdct_home_60);
                        txt_home_40_z.setText(prdct_home_40_z);
                        txt_home.setText(prdct_home);
                        txt_home_120.setText(prdct_home_120);
                        txt_home_60_stove.setText(prdct_home_60_stove);
                        txt_final_incentive.setText(dec_final_tax_inc);
                        txt_currency.setText(currency);
                        txt_post_incentive.setText(post_tax_inc);
                        txt_pre_incentive.setText(pre_tax_inc);
                        txt_home_120_radio.setText(prdct_home_120_radio);
                        txt_home_60_radio.setText(prdct_home_60_radio);
                        txt_homradio.setText(prdct_home_radio);
                        txt_number.setText(agent_phone_no);
                        txt_incentive_week.setText(incentive_week);



*/


                        progressDialog.dismiss();





                    }


                } catch (Exception e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                    Toast.makeText(Incentives.this, "Something went wrong!! Please try again", Toast.LENGTH_SHORT).show();

                }

            }



            private String DecimatCheck(String Value) {

                DecimalFormat decimalFormat4 = new DecimalFormat("###");
                DecimalFormat decimalFormat = new DecimalFormat("#,###");
                DecimalFormat decimalFormat1 = new DecimalFormat("##,###");
                DecimalFormat decimalFormat2 = new DecimalFormat("###,###");
                DecimalFormat decimalFormat3 = new DecimalFormat("##,##,###");
            String Txt_Value;

                String text = Double.toString(Math.abs(Double.parseDouble(String.valueOf(Value))));
                int integerPlaces = text.indexOf('.');
                int decimalPlaces = text.length() - integerPlaces - 1;

                try {
                    //  Double.parseDouble(df.format(final_value));


                    if (integerPlaces == 4) {

                        Txt_Value  =    decimalFormat.format(Double.parseDouble(Value)).toString();
                        Value = Txt_Value;

                    }
                    else if (integerPlaces == 5){
                        Txt_Value = decimalFormat1.format(Double.parseDouble(Value)).toString();
                        Value = Txt_Value;
                    }else if (integerPlaces == 6){

                        Txt_Value = decimalFormat2.format(Double.parseDouble(Value)).toString();
                        Value = Txt_Value;
                    }else if (integerPlaces == 7){

                        Txt_Value = decimalFormat3.format(Double.parseDouble(Value)).toString();
                        Value = Txt_Value;
                    }else if (integerPlaces ==3){
                        Txt_Value = decimalFormat4.format(Double.parseDouble(Value)).toString();
                        Value = Txt_Value;

                    }else if (integerPlaces == 2){

                        Txt_Value = Value.toString();

                        Value = Txt_Value;
                    }
                    else if (integerPlaces == 1){

                        Txt_Value = Value.toString();

                        Value = Txt_Value;
                    }


                } catch (NumberFormatException ex){
                    ex.printStackTrace();
                } catch (Exception e){
                    e.printStackTrace();
                }


         return  Value;

            }



        }


        public void createErrorDialogBoxInternetConnection(String message) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Incentives.this);
            alertDialogBuilder.setTitle(getString(R.string.error_dialog_box_header));
            alertDialogBuilder.setMessage(message);


            alertDialogBuilder.setPositiveButton(" OK ", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int id) {

                    finish();

                }
            });

            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.setCancelable(false);

            alertDialog.show();


        }

        ;
    }
