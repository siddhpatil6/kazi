package com.greenlightplanet;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;

import retrofit2.Call;


public class LoadingDialog1 {

    static ProgressDialog progressDialog;
    static Context ctx;
    static Call<String> calll;

    public static void showLoadingDialog(Context context, String message) {

        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(message);
        ctx=context;
        progressDialog.setCancelable(true);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {

                //calll.cancel();

            }
        });


        progressDialog.show();

    }

    public static void cancelLoading() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.cancel();

    }

    //create method to get refernece from PNRfragment
    public static void getcall( Call<String> call) {
        calll=call;

    }


}
