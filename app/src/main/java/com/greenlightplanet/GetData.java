package com.greenlightplanet;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.StrictMode;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.security.ProviderInstaller;
import com.google.gson.Gson;
import com.greenlightplanet.kazi.R;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.commons.codec.binary.Base64;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.ConnectionSpec;
import okhttp3.OkHttpClient;
import okhttp3.TlsVersion;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by Prateek on 16/4/2017.
 */
public class GetData {

    HostnameVerifier hostnameVerifier = org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;
    private final String baseDatabaseURL = "https://greenlightplanet.looker.com:19999/api/3.0/";
    private final String baseDatabaseClientID = "QY6DQzQT6NQM3ngbTRxY"; //"583NStjC5y5mvP3n5c3F";
    private final String baseDatabaseClientSecret = "PMb9rtjBzrdYgyvqQhfhTjrB";//"RpgZ2Qdj6RtxsjpY25R5HFv2";
    private String finalZendeskbaseURL, finalzendeskUsername, finalzendeskpass;
    private final String zendeskBaseURL = "https://sunkingpaygo.zendesk.com";  // only for kenya
    private final String zendeskUsername = "ihsaan@greenlightplanet.com";
    private final String zendeskPassword = "pingpong31905.";
    // Uganda
    private final String zendeskBaseURL_ug = "https://sunkingpaygoug.zendesk.com";  // only for uganda
    private final String zendeskUsername_ug = "pierandrea@greenlightplanet.com";
    private final String zendeskPassword_ug = "Easybuy100%";

    String txt_Country, url_check;
    ArrayList arrayList;
    // nigeria
    private final String zendeskBaseURL_ng = "https://sunkingpaygong.zendesk.com"; // for nigeria
    private final String zendeskUsername_ng = "pierandrea@greenlightplanet.com";
    private final String zendeskPassword_ng = "Easybuy100%";

    //Myanmar
    private final String zendeskBaseURL_mg = "https://sunkingpaygomm.zendesk.com";  // only for Myanmar
    private final String zendeskUsername_mg = "pierandrea@greenlightplanet.com";
    private final String zendeskPassword_mg = "Easybuy100%";
// tanzania


    private final String zendeskBaseURL_tz = "https://gcstz.zendesk.com";  // only for tanzia
    private final String zendeskUsername_tz = "pierandrea@greenlightplanet.com";
    private final String zendeskPassword_tz = "GCScs1";


    private final String zendeskBaseURL_ind = "https://greenlightplaneteasybuyindia.zendesk.com";           // Only for India
    private final String zendeskUsername_ind = "varun@greenlightplanet.com";
    private final String zendeskPassword_ind = "zendesk";

    public String appSolver = "Kazi";
    private static final String TAG = "AcGetData ";

    private SharedPreferences.Editor preferencesEditor;

    public static void main(String[] args) {

    }

    public JSONArray retreiveDataFromDataBase(String tableCode) {
        try {


            // getSocketFactory();
            //new  MySSLSocketFactory();

            //  long startTime = System.currentTimeMillis();

            Log.d("Table CodeRetrieve Data", tableCode);
            URL loginURL = new URL(baseDatabaseURL + "login?client_id=" + baseDatabaseClientID + "&client_secret=" + baseDatabaseClientSecret);
            HttpURLConnection loginURLConnection = (HttpURLConnection) loginURL.openConnection();



            // set the connection timeout to 1 seconds and the read timeout to 5 seconds
            loginURLConnection.setConnectTimeout(3000);
            loginURLConnection.setReadTimeout(3000);
            loginURLConnection.setRequestMethod("POST");
          //  getNewHttpClient();
            String unparsedAccessToken = downloadData(loginURLConnection);
            // connection has made


            if (!(unparsedAccessToken == null)) {
                JSONObject accessTokenJSONObject = new JSONObject(unparsedAccessToken);
                String accessToken = accessTokenJSONObject.get("access_token").toString();
                URL tableURL = new URL(baseDatabaseURL + "looks/" + tableCode + "/run/json");

                //  Log.d("TABLE URL CAll", String.valueOf(tableURL));

//Testing the table
                //  https://greenlightplanet.looker.com:19999/api/3.0/queries/models/Data/views/Results/run/json?fields=Results.Score&limit=500&query_timezone=America/Los_Angeles

                // URL tableURL = new URL(baseDatabaseURL +)
                HttpURLConnection tableURLConnection = (HttpURLConnection) tableURL.openConnection();
                tableURLConnection.setRequestProperty("Authorization", "Bearer " + accessToken);

                tableURLConnection.setRequestMethod("POST");
                String unparsedTableData = downloadData(tableURLConnection);
                JSONArray tableData = new JSONArray(unparsedTableData);

           /*     long elapsedTime = System.currentTimeMillis() - startTime;
                System.out.println("@@Total elapsed http request/response time in milliseconds: " + elapsedTime);



                Log.d("@@!!TableNdata","2st query.Elapse time"+elapsedTime);

                Log.d("Table Data", tableData.toString());

           */
                return tableData;

            } else {
                Log.d("GetData", "length is zero");
            }

        } catch (NullPointerException n) {
            n.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG + "Exception", e.toString());


            return null;
        }

        return null;
    }


/////////////////////////////////













    /*

    public JSONArray retreiveDataFromDataBase(String tableCode) {
        try {

            URL tableURL;
            String query_id = null;
            JSONArray tableData = null;
            JSONObject final_list_query= null;
            Log.d("Table CodeRetrieve Data", tableCode);
            URL loginURL = new URL(baseDatabaseURL + "login?client_id=" + baseDatabaseClientID + "&client_secret=" + baseDatabaseClientSecret);
            HttpURLConnection loginURLConnection = (HttpURLConnection) loginURL.openConnection();
            loginURLConnection.setRequestMethod("POST");
            String unparsedAccessToken = downloadData(loginURLConnection);
            // connection has made
            JSONObject accessTokenJSONObject = new JSONObject(unparsedAccessToken);
            String accessToken = accessTokenJSONObject.get("access_token").toString();
            //URL tableURL = new URL(baseDatabaseURL + "looks/" + tableCode + "/run/json");
            if (tableCode.equalsIgnoreCase("399")) {
                url_check = "jsonArray";
                tableURL = new URL(baseDatabaseURL + "looks/" + tableCode + "/run/json");

            } else if (tableCode.equalsIgnoreCase("655")) {
                url_check = "jsonArray";
                tableURL = new URL(baseDatabaseURL + "looks/" + tableCode + "/run/json");

            } else {


                //testing process looks calling 1 step
                url_check = "jsonObject";
                tableURL = new URL("https://greenlightplanet.looker.com:19999/api/3.0/looks/410");

                // tableURL = new URL(baseDatabaseURL + "looks/" + tableCode + "/run/json");

                //tableURL=  new URL("https://greenlightplanet.looker.com:19999/api/3.0/looks/1309?fields=account.area");
                //  tableCode = "410";
                //    tableURL = new URL(baseDatabaseURL + "looks/" + tableCode + "/run/json?fields=accounts.owner_name" + Area_tst);

//     tableURL = new URL(baseDatabaseURL + "queries/" + "/models/" + tableCode + "/views/filters:{field: \"accounts area\",value: \"Kitale\"}/run/json");
//    tableURL = new URL(baseDatabaseURL + "queries/" + "/models/" + tableCode + "/views/tickets_all_countries_eo_ticket/run/json?fields=accounts_account_number,accounts_owner_msisdn,&limit=500&query_timezone=America/Los_Angeles");


                // https://looker.mycompany.com:19999/api/3.0/queries/models/thelook/views/inventory_items/run/json?fields=category.name,inventory_items.days_in_inventory_tier,products.count&f[category.name]=socks&sorts=products.count+desc+0&limit=500&query_timezone=America/Los_Angeles

                //tableURL = new URL("https://greenlightplanet.looker.com:19999/api/3.0/looks/1309/run/json/filters:{field: \"accounts area\",value: \"Kitale\"}");


//    tableURL = new URL("https://greenlightplanet.looker.com:19999/api/3.0/queries/1309/greenlightplanet/views/tickets_all_countries/run/json?fields=accounts.account_number,accounts.agent,accounts.date_of_latest_payment_utc_date,accounts.days_disabled,accounts.group_name,accounts.owner_msisdn,accounts.ower_name,accounts.total_paid,tickets_all_countries.eo_ticket,tickets_all_countries.id,tickets_all_countries.requester_id,tickets_all_countries.status,accounts.area%26f[accounts.area]=-Embu%2C-Bomet%2C-Migori%2C-Nakuru%2C-Machakos%2C-Kitui%26f[tickets_all_countries.eo_ticket]=-%26f[tickets_all_countries.status]=open HTTP/1.1");
            }
            //  Log.d("TABLE URL CAll", String.valueOf(tableURL));

//Testing the table
            //  https://greenlightplanet.looker.com:19999/api/3.0/queries/models/Data/views/Results/run/json?fields=Results.Score&limit=500&query_timezone=America/Los_Angeles

            // URL tableURL = new URL(baseDatabaseURL +)


            HttpURLConnection tableURLConnection = (HttpURLConnection) tableURL.openConnection();
            tableURLConnection.setRequestProperty("Authorization", "Bearer " + accessToken);

            tableURLConnection.setRequestMethod("GET");
            String unparsedTableData = downloadData(tableURLConnection);
            if (url_check.equalsIgnoreCase("jsonArray")) {
                 tableData = new JSONArray(unparsedTableData);

                Log.d("Table Data Array", tableData.toString());

                //return tableData;
            } else if (url_check.equalsIgnoreCase("jsonObject")) {


                JSONObject tableData1 = new JSONObject(unparsedTableData);

                String User_id = tableData1.getString("user_id");
                 query_id = tableData1.getString("query_id");

                Iterator x = tableData1.keys();
                tableData = new JSONArray();

                while (x.hasNext()) {
                    String key = (String) x.next();
                    tableData.put(tableData1.get(key));
                }
                Log.d("Table Data JsonSObject", tableData.toString());

                //  return tableData;
            }


            if (url_check.equalsIgnoreCase("jsonObject")){

                // query(query_id) 2nd step looker
                 final_list_query =  retreiveDataFromDataBase_final_field(tableCode,query_id);

                url_check = "jsonObjectdone";

                Log.d("Create Query",final_list_query.toString());
               // return final_list_query;


       }

       if (url_check.equalsIgnoreCase("jsonObjectdone")){

                // 3rd step Looker
                JSONObject create_query = null;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                    create_query = retreiveDataFromDataBase_run_query(final_list_query);
                }
                Log.d("Create Query",create_query.toString());
url_check = "jsonrun_query";
            }


if (url_check.equalsIgnoreCase("jsonrun_query")){


    // final step run_query looker
   tableData = retreiveDataFromDataBase_run_final_query(final_list_query);

Log.d("JsonArray Data",tableData.toString());

           // return tableData;
        }

        return tableData;


    }catch (Exception e){

    e.printStackTrace();}
    return null;
    }

*/


    public JSONArray retreiveDataFromDataBase_query(String tableCode, String agent_name) {

        String final_id = null;
        JSONArray tableData = null;
        try {

            long startTime = System.currentTimeMillis();

            String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
            URL tableURL;
            String query_id = null;

            JSONObject final_list_query = null;
            Log.d("Table CodeRetrieve Data", tableCode);
            URL loginURL = new URL(baseDatabaseURL + "login?client_id=" + baseDatabaseClientID + "&client_secret=" + baseDatabaseClientSecret);
            HttpURLConnection loginURLConnection = (HttpURLConnection) loginURL.openConnection();


            long elapsedTime = System.currentTimeMillis() - startTime;


            Log.d("@@CallingConnec_Looker", "1st query.Elapse time" + elapsedTime + ",URL" + loginURL);
            // set the connection timeout to 1 seconds and the read timeout to 5 seconds
            loginURLConnection.setConnectTimeout(3000);
            loginURLConnection.setReadTimeout(3000);
            loginURLConnection.setRequestMethod("POST");

            long startTime2 = System.currentTimeMillis();

            String unparsedAccessToken = downloadData(loginURLConnection);


            long elapsedTime2 = System.currentTimeMillis() - startTime2;

            Log.d("@@UnparsedToken", "2st query.Elapse time" + elapsedTime2 + ",URL" + loginURL);

            long startTime3 = System.currentTimeMillis();
            // connection has made
            JSONObject accessTokenJSONObject = new JSONObject(unparsedAccessToken);
            String accessToken = accessTokenJSONObject.get("access_token").toString();


            long elapsedTime3 = System.currentTimeMillis() - startTime3;

            Log.d("@@accessToken", "3rd query.Elapse time" + elapsedTime3 + ",URL" + loginURL);

            //URL tableURL = new URL(baseDatabaseURL + "looks/" + tableCode + "/run/json");
            //testing process looks calling 1 step
            url_check = "jsonObject";
            tableURL = new URL("https://greenlightplanet.looker.com:19999/api/3.0/looks/" + tableCode);


            long startTime4 = System.currentTimeMillis();
            //tableURL = new URL("https://greenlightplanet.looker.com:19999/api/3.0/looks/410");
            HttpURLConnection tableURLConnection = (HttpURLConnection) tableURL.openConnection();
            tableURLConnection.setRequestProperty("Authorization", "Bearer " + accessToken);

            long elapsedTime4 = System.currentTimeMillis() - startTime4;

            Log.d("@@processCreating", "4rd query.Elapse time" + elapsedTime4 + ",URL" + loginURL);

            tableURLConnection.setRequestMethod("GET");
            String unparsedTableData = downloadData(tableURLConnection);
            if (url_check.equalsIgnoreCase("jsonObject")) {


                long startTime5 = System.currentTimeMillis();

                JSONObject tableData1 = new JSONObject(unparsedTableData);

                String User_id = tableData1.getString("user_id");
                query_id = tableData1.getString("query_id");

                Iterator x = tableData1.keys();
                tableData = new JSONArray();

                while (x.hasNext()) {
                    String key = (String) x.next();
                    tableData.put(tableData1.get(key));
                }
                Log.d("Table Data JsonSObject", tableData.toString());

                long elapsedTime5 = System.currentTimeMillis() - startTime5;
                Log.d("@@JsonObjecttoLooker", "5th query.Elapse time" + elapsedTime5 + ",URL" + loginURL);

                //  return tableData;
            }


            if (url_check.equalsIgnoreCase("jsonObject")) {

                // query(query_id) 2nd step looker

                long startTime6 = System.currentTimeMillis();

                if (query_id != null) {
                    final_list_query = retreiveDataFromDataBase_final_field(tableCode, query_id, agent_name);

                    long elapsedTime6 = System.currentTimeMillis() - startTime6;
                    Log.d("@@Creating Query", "6th query.Elapse time" + elapsedTime6 + ",URL" + loginURL);
                } else {

                }


                url_check = "jsonObjectdone";

                Log.d("Create Query", final_list_query.toString());
                // return final_list_query;


                long elapsedTime6 = System.currentTimeMillis() - startTime6;
                Log.d("@@Creating Query", "6th query.Elapse time" + elapsedTime6 + ",URL" + loginURL);

            }

            if (url_check.equalsIgnoreCase("jsonObjectdone")) {

                long startTime7 = System.currentTimeMillis();
                // 3rd step Looker
                JSONObject create_query = null;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                    if (final_list_query.length() > 0) {
                        create_query = retreiveDataFromDataBase_run_query(final_list_query);

                        long elapsedTime7 = System.currentTimeMillis() - startTime7;
                        Log.d("@@JsonRunQuery", "7th query.Elapse time" + elapsedTime7 + ",URL" + loginURL);
                    } else {

                    }
                }

                final_id = create_query.getString("id");
                Log.d("Create Query", create_query.toString());
                url_check = "jsonrun_query";

                long elapsedTime7 = System.currentTimeMillis() - startTime7;
                Log.d("@@jsonRunQuery", "7th query.Elapse time" + elapsedTime7 + ",URL" + url_check);

            }
            long startTime8 = System.currentTimeMillis();

            if (url_check.equalsIgnoreCase("jsonrun_query")) {


                // final step run_query looker
                if (final_id != null) {
                    tableData = retreiveDataFromDataBase_run_final_query(final_id);
                    long elapsedTime8 = System.currentTimeMillis() - startTime8;
                    Log.d("@@finalQuery", "8th query.Elapse time" + elapsedTime8 + ",URL" + url_check);
                } else {

                }
                // Log.d("JsonArray Data",tableData.toString());

                // return tableData;


            }

            //          return tableData;


        } catch (Exception e) {

            e.printStackTrace();


        }

        return tableData;
    }


    public JSONArray retreiveDataFromDataBase_field(String tableCode, String field) {
        try {

            Log.d("Table CodeRetrieve Data", tableCode);
            URL loginURL = new URL(baseDatabaseURL + "login?client_id=" + baseDatabaseClientID + "&client_secret=" + baseDatabaseClientSecret);
            HttpURLConnection loginURLConnection = (HttpURLConnection) loginURL.openConnection();
            loginURLConnection.setRequestMethod("POST");
            String unparsedAccessToken = downloadData(loginURLConnection);
            // connection has made
            JSONObject accessTokenJSONObject = new JSONObject(unparsedAccessToken);
            String accessToken = accessTokenJSONObject.get("access_token").toString();
            //  URL tableURL = new URL(baseDatabaseURL + "looks/" + tableCode + "/run/json");
//https://greenlightplanet.looker.com:19999/api/3.0/queries/models/410/views/Area/run/json
            URL tableURL = new URL(baseDatabaseURL + "queries/" + "/models/" + tableCode + "/views/" + field + "/run/json");
            //  Log.d("TABLE URL CAll", String.valueOf(tableURL));

//Testing the table
            //  https://greenlightplanet.looker.com:19999/api/3.0/queries/models/Data/views/Results/run/json?fields=Results.Score&limit=500&query_timezone=America/Los_Angeles
            //  tableURL = new URL("https://greenlightplanet.looker.com:19999/api/3.0/queries/140148");
            // URL tableURL = new URL(baseDatabaseURL +)
            HttpURLConnection tableURLConnection = (HttpURLConnection) tableURL.openConnection();
            tableURLConnection.setRequestProperty("Authorization", "Bearer " + accessToken);

            tableURLConnection.setRequestMethod("GET");
            String unparsedTableData = downloadData(tableURLConnection);
            JSONArray tableData = new JSONArray(unparsedTableData);

            Log.d("Table Data", tableData.toString());

            return tableData;
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG + "Exception", e.toString());


            return null;
        }


    }


    public JSONObject retreiveDataFromDataBase_final_field(String tableCode, String queryId, String agent_name) {
        JSONObject table = null;
        JSONObject updated_json = null;
        JSONObject filter_update = null;
        JSONObject final_tableData = null;
        try {
            JSONArray tableData = null;
            Log.d("Table CodeRetrieve Data", tableCode);
            URL loginURL = new URL(baseDatabaseURL + "login?client_id=" + baseDatabaseClientID + "&client_secret=" + baseDatabaseClientSecret);
            HttpURLConnection loginURLConnection = (HttpURLConnection) loginURL.openConnection();
            loginURLConnection.setRequestMethod("POST");
            String unparsedAccessToken = downloadData(loginURLConnection);
            // connection has made
            JSONObject accessTokenJSONObject = new JSONObject(unparsedAccessToken);
            String accessToken = accessTokenJSONObject.get("access_token").toString();
            //  URL tableURL = new URL(baseDatabaseURL + "looks/" + tableCode + "/run/json");
//https://greenlightplanet.looker.com:19999/api/3.0/queries/models/410/views/Area/run/json
            //  URL tableURL = new URL(baseDatabaseURL + "queries/" + "/models/" + tableCode + "/views/" + field +"/run/json");
            //  Log.d("TABLE URL CAll", String.valueOf(tableURL));


//Testing the table
            //  https://greenlightplanet.looker.com:19999/api/3.0/queries/models/Data/views/Results/run/json?fields=Results.Score&limit=500&query_timezone=America/Los_Angeles
            URL tableURL = new URL("https://greenlightplanet.looker.com:19999/api/3.0/queries/" + queryId);
            // URL tableURL = new URL(baseDatabaseURL +)
            HttpURLConnection tableURLConnection = (HttpURLConnection) tableURL.openConnection();
            tableURLConnection.setRequestProperty("Authorization", "Bearer " + accessToken);

            tableURLConnection.setRequestMethod("GET");
            String unparsedTableData = downloadData(tableURLConnection);
            //JSONArray tableData = new JSONArray(unparsedTableData);
            table = new JSONObject(unparsedTableData);


            final_tableData = new JSONObject(String.valueOf(table));


            String filters = final_tableData.getString("filters");

            final_tableData.put("client_id", "");

            filter_update = new JSONObject(filters);
            filter_update = filter_update.put("accounts.agent", agent_name);


            final_tableData.put("filters", filter_update);
            // parsingData(table,agent_name);
            //  String User_id_final = table.getString("user_id");
            // String  query_id_final = table.getString("id");

          /*  Iterator x = table.keys();
            tableData = new JSONArray();

            while (x.hasNext()) {
                String key = (String) x.next();
                tableData.put(table.get(key));

                if (key.equals("accounts.agent")){

                    tableData.put(table.get("Michael Owili"));
                }
            }
*/
            Log.d("Table Data", table.toString());


        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG + "Exception", e.toString());


        }
        return final_tableData;
    }


    public JSONObject retreiveDataFromDataBase_final_field_login(String tableCode, String queryId, String agent_Mnumber) {
        JSONObject table = null;
        JSONObject updated_json = null;
        JSONObject filter_update = null;
        JSONObject final_tableData = null;
        try {
            JSONArray tableData = null;
            Log.d("Table CodeRetrieve Data", tableCode);
            URL loginURL = new URL(baseDatabaseURL + "login?client_id=" + baseDatabaseClientID + "&client_secret=" + baseDatabaseClientSecret);
            HttpURLConnection loginURLConnection = (HttpURLConnection) loginURL.openConnection();
            loginURLConnection.setRequestMethod("POST");
            String unparsedAccessToken = downloadData(loginURLConnection);
            // connection has made
            JSONObject accessTokenJSONObject = new JSONObject(unparsedAccessToken);
            String accessToken = accessTokenJSONObject.get("access_token").toString();
            //  URL tableURL = new URL(baseDatabaseURL + "looks/" + tableCode + "/run/json");
//https://greenlightplanet.looker.com:19999/api/3.0/queries/models/410/views/Area/run/json
            //  URL tableURL = new URL(baseDatabaseURL + "queries/" + "/models/" + tableCode + "/views/" + field +"/run/json");
            //  Log.d("TABLE URL CAll", String.valueOf(tableURL));


//Testing the table
            //  https://greenlightplanet.looker.com:19999/api/3.0/queries/models/Data/views/Results/run/json?fields=Results.Score&limit=500&query_timezone=America/Los_Angeles
            URL tableURL = new URL("https://greenlightplanet.looker.com:19999/api/3.0/queries/" + queryId);
            // URL tableURL = new URL(baseDatabaseURL +)
            HttpURLConnection tableURLConnection = (HttpURLConnection) tableURL.openConnection();
            tableURLConnection.setRequestProperty("Authorization", "Bearer " + accessToken);

            tableURLConnection.setRequestMethod("GET");
            String unparsedTableData = downloadData(tableURLConnection);
            //JSONArray tableData = new JSONArray(unparsedTableData);
            table = new JSONObject(unparsedTableData);


            final_tableData = new JSONObject(String.valueOf(table));


            String filters = final_tableData.getString("filters");

            final_tableData.put("client_id", "");

            filter_update = new JSONObject(filters);
            filter_update = filter_update.put("angaza_users.agent_phone_number", agent_Mnumber);


            final_tableData.put("filters", filter_update);
            // parsingData(table,agent_name);
            //  String User_id_final = table.getString("user_id");
            // String  query_id_final = table.getString("id");

          /*  Iterator x = table.keys();
            tableData = new JSONArray();

            while (x.hasNext()) {
                String key = (String) x.next();
                tableData.put(table.get(key));

                if (key.equals("accounts.agent")){

                    tableData.put(table.get("Michael Owili"));
                }
            }
*/
            Log.d("Table Data", table.toString());


        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG + "Exception", e.toString());


        }
        return final_tableData;
    }


    private void parsingData(JSONObject table, String agent_name) throws JSONException {
        HashMap<String, HashMap<String, String>> taskListqueryData = new HashMap<String, HashMap<String, String>>();


        try {
            JSONObject final_tableData = new JSONObject(String.valueOf(table));

            JSONObject updated_json;
            JSONObject filter_update;
            String filters = final_tableData.getString("filters");

            final_tableData.put("client_id", "");

            filter_update = new JSONObject(filters);
            filter_update = filter_update.put("accounts.agent", agent_name);


        } catch (JSONException e) {
            e.printStackTrace();
        }


    }


    public JSONObject retreiveDataFromDataBase_run_query(JSONObject queries) {
        OutputStream os;
        try {
            JSONObject tableData = null;
            Log.d("Table CodeRetrieve Data", queries.toString());
            URL loginURL = new URL(baseDatabaseURL + "login?client_id=" + baseDatabaseClientID + "&client_secret=" + baseDatabaseClientSecret);
            HttpURLConnection loginURLConnection = (HttpURLConnection) loginURL.openConnection();
            loginURLConnection.setRequestMethod("POST");
            //  loginURLConnection.setRequestProperty("Query", String.valueOf(queries));
            String unparsedAccessToken = downloadData(loginURLConnection);
            // connection has made


            JSONObject accessTokenJSONObject = new JSONObject(unparsedAccessToken);
            String accessToken = accessTokenJSONObject.get("access_token").toString();
            //  URL tableURL = new URL(baseDatabaseURL + "looks/" + tableCode + "/run/json");
//https://greenlightplanet.looker.com:19999/api/3.0/queries/models/410/views/Area/run/json
            //  URL tableURL = new URL(baseDatabaseURL + "queries/" + "/models/" + tableCode + "/views/" + field +"/run/json");
            //  Log.d("TABLE URL CAll", String.valueOf(tableURL));

//            loginURLConnection.setRequestProperty("Query", String.valueOf(queries));

            URL tableURL = new URL("https://greenlightplanet.looker.com:19999/api/3.0/queries");
//Testing the table
            //  https://greenlightplanet.looker.com:19999/api/3.0/queries/models/Data/views/Results/run/json?fields=Results.Score&limit=500&query_timezone=America/Los_Angeles

            // URL tableURL = new URL(baseDatabaseURL +)
            HttpURLConnection tableURLConnection = (HttpURLConnection) tableURL.openConnection();
            tableURLConnection.setRequestProperty("Authorization", "Bearer " + accessToken);
            //tableURLConnection.setRequestProperty("Query", String.valueOf(queries));

            tableURLConnection.setRequestMethod("POST");
            tableURLConnection.setRequestProperty("Content-Type", "application/json");
            tableURLConnection.setRequestProperty("Query", String.valueOf(queries));

            os = tableURLConnection.getOutputStream();
            os.write(String.valueOf(queries).getBytes());


            String unparsedTableData = downloadData(tableURLConnection);
            tableData = new JSONObject(unparsedTableData);

            /*os.flush();
            os.close();*/

            //  tableURLConnection.setRequestMethod("GET");
            //   String unparsedTableData = downloadData(tableURLConnection);
            //JSONArray tableData = new JSONArray(unparsedTableData);
            // tableData = new JSONObject(unparsedTableData);

            int responseCode = tableURLConnection.getResponseCode();

            os.flush();
            //          os.close();
            System.out.println("POST Response Code :: " + responseCode);
           /* if (responseCode == HttpURLConnection.HTTP_OK) { //success
                BufferedReader in = new BufferedReader(new InputStreamReader(
                        tableURLConnection.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
//                in.close();

                // print result
                System.out.println("input reader"+response.toString());
            } else {
                System.out.println("POST request not worked");
            }*/


            // String requestedParameter = downloadData(tableURLConnection);

            //JSONObject tableDatatask = new JSONObject(requestedParameter);

            // tableURLConnection.setRequestMethod("GET");
            //String unparsedTableData = downloadData(tableURLConnection);
            //JSONArray tableData = new JSONArray(unparsedTableData);

            int code = tableURLConnection.getResponseCode();
            System.out.println("Response code" + code);

            //JSONObject table = new JSONObject(unparsedTableData);


            //  String User_id_final = table.getString("user_id");
            // String  query_id_final = table.getString("id");
/*

            Iterator x = table.keys();
            tableData = new JSONArray();

            while (x.hasNext()) {
                String key = (String) x.next();
                tableData.put(table.get(key));
            }
*/

            // Log.d("Table Data", table.toString());

            return tableData;
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG + "Exception", e.toString());


            return null;
        }


    }


    public JSONArray retreiveDataFromDataBase_run_final_query(String final_Id) {
        JSONObject table = null;
        JSONArray tableData = null;
        try {

            Log.d("Table CodeRetrieve Data", final_Id.toString());
            URL loginURL = new URL(baseDatabaseURL + "login?client_id=" + baseDatabaseClientID + "&client_secret=" + baseDatabaseClientSecret);
            HttpURLConnection loginURLConnection = (HttpURLConnection) loginURL.openConnection();
            loginURLConnection.setRequestMethod("POST");
            String unparsedAccessToken = downloadData(loginURLConnection);
            // connection has made
            JSONObject accessTokenJSONObject = new JSONObject(unparsedAccessToken);
            String accessToken = accessTokenJSONObject.get("access_token").toString();
            //  URL tableURL = new URL(baseDatabaseURL + "looks/" + tableCode + "/run/json");
//https://greenlightplanet.looker.com:19999/api/3.0/queries/models/410/views/Area/run/json
            //  URL tableURL = new URL(baseDatabaseURL + "queries/" + "/models/" + tableCode + "/views/" + field +"/run/json");
            //  Log.d("TABLE URL CAll", String.valueOf(tableURL));

            //https://greenlightplanet.looker.com:19999/api/3.0/queries/152431/run/json
            //  URL tableURL = new URL("https://greenlightplanet.looker.com:19999/api/3.0/queries/152431/run/json");
            URL tableURL = new URL("https://greenlightplanet.looker.com:19999/api/3.0/queries/" + final_Id + "/run/json");
//Testing the table
            //  https://greenlightplanet.looker.com:19999/api/3.0/queries/models/Data/views/Results/run/json?fields=Results.Score&limit=500&query_timezone=America/Los_Angeles

            // URL tableURL = new URL(baseDatabaseURL +)
            HttpURLConnection tableURLConnection = (HttpURLConnection) tableURL.openConnection();
            tableURLConnection.setRequestProperty("Authorization", "Bearer " + accessToken);

            tableURLConnection.setRequestMethod("GET");
            String unparsedTableData = downloadData(tableURLConnection);
            tableData = new JSONArray(unparsedTableData);
            //  table = new JSONObject(unparsedTableData);


            //  String User_id_final = table.getString("user_id");
            // String  query_id_final = table.getString("id");
/*

            Iterator x = table.keys();
            tableData = new JSONArray();

            while (x.hasNext()) {
                String key = (String) x.next();
                tableData.put(table.get(key));
            }
*/

            // Log.d("Table Data", table.toString());

            return tableData;
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG + "Exception", e.toString());


            return tableData;
        }


    }


    public String downloadData(HttpURLConnection apiConnection) {
        try {

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(apiConnection.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line).append("\n");
            }
            bufferedReader.close();
            return stringBuilder.toString();
        } catch (Exception e) {
            System.out.println(e);
            return null;
        }
    }

    public static void getSocketFactory() {
        try {
            TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }

                @Override
                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }

                @Override
                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }
            }};

            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String arg0, SSLSession arg1) {
                    return true;
                }
            });
        } catch (Exception ignored) {
        }
    }


// top  Agents in Kenya


    // Agentlist for Kenya
   /* public ArrayList<String> getAgentList_sms_Kenya(Context mContext) {
        ArrayList<String> kn_agent_list = new ArrayList<String>();
        try {

            JSONArray keniya_agent_list = retreiveDataFromDataBase("1039");

//            ArrayList keniya_agent_list =  DataBaseurl_login(mContext);





            Log.d("Ken@@@",keniya_agent_list.toString());

            for (int i = 0; i < keniya_agent_list.size(); i++) {
                JSONObject agent_ke_name = (JSONObject) keniya_agent_list.get(i);
                //kn_agent_list.add((String) agent_ke_name.get("accounts.agent") + "-" + agent_ke_name.get("angaza_users.agent_phone_number"));
                 kn_agent_list.add((String) agent_ke_name.get("accounts.country"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return kn_agent_list;
    }*/


    // Agent List for Nigeria

    public ArrayList<String> getAgentList_Nigeria() {
        ArrayList<String> nigeria_agent_list = new ArrayList<String>();
        try {
            // JSONArray ni_agent_list = retreiveDataFromDataBase("404");
            JSONArray ni_agent_list = retreiveDataFromDataBase("687");
            for (int i = 0; i < ni_agent_list.length(); i++) {
                JSONObject agent_mn_name = (JSONObject) ni_agent_list.get(i);
                // nigeria_agent_list.add((String) agent_mn_name.get("accounts.agent"));
                nigeria_agent_list.add((String) agent_mn_name.get("accounts.agent") + "-" + agent_mn_name.get("angaza_users.agent_phone_number"));

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return nigeria_agent_list;
    }


    // Agentlist for Kenya
    public ArrayList<String> getAgentList_Kenya() {
        ArrayList<String> kn_agent_list = new ArrayList<String>();
        try {
            // JSONArray keniya_agent_list = retreiveDataFromDataBase("394");
            JSONArray keniya_agent_list = retreiveDataFromDataBase("655");

            for (int i = 0; i < keniya_agent_list.length(); i++) {
                JSONObject agent_ke_name = (JSONObject) keniya_agent_list.get(i);
                kn_agent_list.add((String) agent_ke_name.get("accounts.agent") + "-" + agent_ke_name.get("angaza_users.agent_phone_number"));
                // kn_agent_list.add((String) agent_ke_name.get("angaza_users.agent_phone_number"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return kn_agent_list;
    }


    // Agentlist for India
    public ArrayList<String> getAgentList_india() {
        ArrayList<String> kn_agent_list = new ArrayList<String>();
        try {
            // JSONArray keniya_agent_list = retreiveDataFromDataBase("394");
            JSONArray keniya_agent_list = retreiveDataFromDataBase("1753");

            for (int i = 0; i < keniya_agent_list.length(); i++) {
                JSONObject agent_ke_name = (JSONObject) keniya_agent_list.get(i);
                kn_agent_list.add((String) agent_ke_name.get("accounts.agent") + "-" + agent_ke_name.get("angaza_users.agent_phone_number"));
                // kn_agent_list.add((String) agent_ke_name.get("angaza_users.agent_phone_number"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return kn_agent_list;
    }


// Agent List for Myanmar

    public ArrayList<String> getAgentList_myanmarB() {
        ArrayList<String> Myanmar_agent_list = new ArrayList<String>();
        try {
            //JSONArray keniya_agent_list = retreiveDataFromDataBase("400");
            JSONArray mynamar_agent_list = retreiveDataFromDataBase("686");

            for (int i = 0; i < mynamar_agent_list.length(); i++) {
                JSONObject agent_mn_name = (JSONObject) mynamar_agent_list.get(i);
                //Myanmar_agent_list.add((String) agent_mn_name.get("accounts.agent"));
                Myanmar_agent_list.add((String) agent_mn_name.get("accounts.agent") + "-" + agent_mn_name.get("angaza_users.agent_phone_number"));

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return Myanmar_agent_list;
    }


// Agent List for Uganda


    public ArrayList<String> getAgentList_uganda() {
        ArrayList<String> Uganda_agent_list = new ArrayList<String>();
        try {
            //  JSONArray ug_agent_list = retreiveDataFromDataBase("403");

            JSONArray ug_agent_list = retreiveDataFromDataBase("688");
            for (int i = 0; i < ug_agent_list.length(); i++) {
                JSONObject agent_ke_name = (JSONObject) ug_agent_list.get(i);
                // Uganda_agent_list.add((String) agent_ke_name.get("accounts.agent"));
                Uganda_agent_list.add((String) agent_ke_name.get("accounts.agent") + "-" + agent_ke_name.get("angaza_users.agent_phone_number"));

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return Uganda_agent_list;
    }

    // Agent List for Tanazia


    public ArrayList<String> getAgentList_tanzania() {
        ArrayList<String> tanzania_agent_list = new ArrayList<String>();
        try {
            //  JSONArray tz_agent_list = retreiveDataFromDataBase("557");
            JSONArray tz_agent_list = retreiveDataFromDataBase("689");
            for (int i = 0; i < tz_agent_list.length(); i++) {
                JSONObject agent_ke_name = (JSONObject) tz_agent_list.get(i);
                // tanzania_agent_list.add((String) agent_ke_name.get("accounts.agent"));
                tanzania_agent_list.add((String) agent_ke_name.get("accounts.agent") + "-" + agent_ke_name.get("angaza_users.agent_phone_number"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return tanzania_agent_list;
    }


    public ArrayList<String> getAgentList() {
        ArrayList<String> agentList = new ArrayList<String>();

        try {
            JSONArray agentListData = retreiveDataFromDataBase("29"); // table code is 29


            //  if(agentList.toString().length()< 1 ) {
            for (int i = 0; i < agentListData.length(); i++) {
                JSONObject agentName = (JSONObject) agentListData.get(i);
                agentList.add((String) agentName.get("accounts.agent"));
                // Log.d("Agent account", agentList.toString());
                // Log.d("Data Response", agentList.get(i));

            }
            return agentList;
/*
            }else {

                agentList.add("Zin Mar Oo");
                agentList.add("c");
                agentList.add("d");
                agentList.add("e");
                agentList.add("f");
return   agentList;
            }*/

        } catch (JSONException ej) {
            ej.printStackTrace();
            Log.d("JSON Error ", ej.toString());

        } catch (Exception e) {
            e.printStackTrace();

            Log.d("Error in List", e.toString());
            return null;
        }
        return null;

    }

    // Country List


    public ArrayList<String> getCountrylist() {

        ArrayList<String> agentList = new ArrayList<String>();
        try {

            JSONArray agentListData = retreiveDataFromDataBase("399"); // table code is 29
           //getdata("399");

            Log.d("AgentList" + TAG, agentList.toString());
            //  if(agentList.toString().length()< 1 ) {
            for (int i = 0; i < agentListData.length(); i++) {
                JSONObject agentName = (JSONObject) agentListData.get(i);
                agentList.add((String) agentName.get("accounts.country"));
                // agentList.add("Please Select the Country...");
                //Log.d("Agent account", agentList.toString());
                //  Log.d("Data Response",agentList.get(i));

            }
            return agentList;
/*
            }else {

                agentList.add("Zin Mar Oo");
                agentList.add("c");
                agentList.add("d");
                agentList.add("e");
                agentList.add("f");
return   agentList;
            }*/

        /*} catch (JSONException ej) {
            ej.printStackTrace();
            Log.d("JSON Error ", ej.toString());
*/
        } catch (Exception e) {
            e.printStackTrace();

            Log.d("Error in List", e.toString());
            return null;
        }
       /* return null;*/

    }


    public static void initializeSSLContext(Context mContext){
        try {
            SSLContext.getInstance("TLSv1.1");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        try {
            ProviderInstaller.installIfNeeded(mContext.getApplicationContext());
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }



    private OkHttpClient getNewHttpClient() {
        OkHttpClient.Builder client = new OkHttpClient.Builder()
                .followRedirects(true)
                .followSslRedirects(true)
                .retryOnConnectionFailure(true)
                .cache(null)
                .connectTimeout(5, TimeUnit.SECONDS)
                .writeTimeout(5, TimeUnit.SECONDS)
                .readTimeout(5, TimeUnit.SECONDS);

        return enableTls12OnPreLollipop(client).build();
    }


   /* private RequestQueue getPinnedRequestQueue(Context context) throws CertificateException, IOException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException {

        CertificateFactory cf = CertificateFactory.getInstance("X.509");

        // Generate the certificate using the certificate file under res/raw/cert.cer
        InputStream caInput = new BufferedInputStream(context.getResources().openRawResource(R.raw.sbi_uat_der));
        final Certificate ca = cf.generateCertificate(caInput);
        caInput.close();

        // Create a KeyStore containing our trusted CAs
        String keyStoreType = KeyStore.getDefaultType();
        KeyStore trusted = KeyStore.getInstance(keyStoreType);
        trusted.load(null, null);
        trusted.setCertificateEntry("ca", ca);

        // Create a TrustManager that trusts the CAs in our KeyStore
        String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
        TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
        tmf.init(trusted);

        // Create an SSLContext that uses our TrustManager
        SSLContext sslContext = SSLContext.getInstance("TLS");
        sslContext.init(null, tmf.getTrustManagers(), null);

        SSLSocketFactory sf = sslContext.getSocketFactory();
        HurlStack hurlStack = new HurlStack(null, sf) {
            @Override
            protected HttpURLConnection createConnection(URL url) throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super.createConnection(url);

                httpsURLConnection.setHostnameVerifier(new HostnameVerifier() {
                    @Override
                    public boolean verify(String hostName, SSLSession sslSession) {
                        String certificateDomainName = ((X509Certificate) ca).getSubjectDN().toString();
                        Log.d(TAG, "Index : " + certificateDomainName.indexOf("CN=") + " Len : " + certificateDomainName.codePointCount(certificateDomainName.indexOf("CN="), certificateDomainName.indexOf(",")));
                        String certificateName = certificateDomainName.substring(certificateDomainName.indexOf("CN="), certificateDomainName.codePointCount(certificateDomainName.indexOf("CN="), certificateDomainName.indexOf(",")));
                        certificateName = certificateName.replace("CN=", "");
                        Log.d(TAG, "hostName : " + hostName + " certificateName : " + certificateName);
                        if (certificateName.isEmpty())
                            return false;
                        return certificateName.equals(hostName);
                    }
                });
                return httpsURLConnection;
            }
        };

        return new Volley().newRequestQueue(context, hurlStack);
    }

*/


    public static OkHttpClient.Builder enableTls12OnPreLollipop(OkHttpClient.Builder client) {
        if (Build.VERSION.SDK_INT >= 16 && Build.VERSION.SDK_INT < 22) {
            try {
                SSLContext sc = SSLContext.getInstance("TLSv1.1");
                sc.init(null, null, null);
                client.sslSocketFactory(new TLS12SocketFactory(sc.getSocketFactory()));

                ConnectionSpec cs = new ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
                        .tlsVersions(TlsVersion.TLS_1_1)
                        .build();

                List<ConnectionSpec> specs = new ArrayList<>();
                specs.add(cs);
                specs.add(ConnectionSpec.COMPATIBLE_TLS);
                specs.add(ConnectionSpec.CLEARTEXT);

                client.connectionSpecs(specs);
            } catch (Exception exc) {
                Log.e("OkHttpTLSCompat", "Error while setting TLS 1.1", exc);
            }
        }

        return client;
    }


    public HashMap<String, String> getLogincheck(String Mnumber, String tableCode) throws JSONException {
        HashMap<String, String> login = new HashMap<>();

        try {
            JSONArray areaListData = retreiveDataFromDataBase_query_LoginProcess(tableCode, Mnumber); //

            if (!(areaListData == null)) {
                for (int i = 0; i < areaListData.length(); i++) {

                    JSONObject agentName = (JSONObject) areaListData.get(i);

                    if (agentName.get("angaza_users.agent_phone_number").toString().equals(Mnumber)) {

                        login.put("accounts.agent_username", agentName.get("accounts.agent").toString());
                        login.put("accounts.country", agentName.get("accounts.country").toString());
                    }
                }
                return login;

            }

        } catch (JSONException ej) {
            ej.printStackTrace();
            Log.d("JSON Error ", ej.toString());

        } catch (Exception e) {
            e.printStackTrace();

            Log.d("Error in List", e.toString());
            return null;
        }
        return null;
    }

    public HashMap<String, String> getarealist(String agent_name, String tableCode) throws JSONException {
        HashMap<String, String> arealist = new HashMap<>();

        try {

            JSONArray areaListData = retreiveDataFromDataBase_query(tableCode, agent_name); // table code is 29


            if (!(areaListData == null)) {
                for (int i = 0; i < areaListData.length(); i++) {

                    JSONObject agentName = (JSONObject) areaListData.get(i);

                    if (agentName.get("accounts.agent").toString().equals(agent_name)) {
                        arealist.put("area", agentName.get("angaza_users_facts.current_area").toString());
                        arealist.put("one_day_score", agentName.get("portfolio_derived.one_day_average_account_disabled_score").toString());
                        arealist.put("two_day_score", agentName.get("portfolio_derived.two_day_average_account_disabled_score").toString());
                        arealist.put("third_day_score", agentName.get("portfolio_derived.three_day_average_account_disabled_score").toString());
                        arealist.put("forth_day_score", agentName.get("portfolio_derived.four_day_average_account_disabled_score").toString());
                        arealist.put("fivth_day_score", agentName.get("portfolio_derived.five_day_average_account_disabled_score").toString());
                        arealist.put("six_day_score", agentName.get("portfolio_derived.six_day_average_account_disabled_score").toString());
                        arealist.put("seven_day_score", agentName.get("portfolio_derived.seven_day_average_account_disabled_score").toString());
                        arealist.put("accounts.country", agentName.get("accounts.country").toString());
                    }
                }
                return arealist;

            }

        } catch (JSONException ej) {
            ej.printStackTrace();
            Log.d("JSON Error ", ej.toString());

        } catch (Exception e) {
            e.printStackTrace();

            Log.d("Error in List", e.toString());
            return null;
        }
        return null;

    }

    public HashMap<String, String> getarealist_task_list(String agent_name, String tableCode) throws JSONException {
        HashMap<String, String> arealist = new HashMap<>();

        try {
            JSONArray areaListData = retreiveDataFromDataBase_query(tableCode, agent_name); // table code is 29

            if (!(areaListData == null)) {
                for (int i = 0; i < areaListData.length(); i++) {

                    JSONObject agentName = (JSONObject) areaListData.get(i);

                    if (agentName.get("accounts.agent").toString().equals(agent_name)) {
                        arealist.put("area", agentName.get("angaza_users_facts.current_area").toString());
                        arealist.put("one_day_score", agentName.get("portfolio_derived.one_day_average_account_disabled_score").toString());
                        arealist.put("two_day_score", agentName.get("portfolio_derived.two_day_average_account_disabled_score").toString());
                        arealist.put("third_day_score", agentName.get("portfolio_derived.three_day_average_account_disabled_score").toString());
                        arealist.put("forth_day_score", agentName.get("portfolio_derived.four_day_average_account_disabled_score").toString());
                        arealist.put("fivth_day_score", agentName.get("portfolio_derived.five_day_average_account_disabled_score").toString());
                        arealist.put("six_day_score", agentName.get("portfolio_derived.six_day_average_account_disabled_score").toString());
                        arealist.put("seven_day_score", agentName.get("portfolio_derived.seven_day_average_account_disabled_score").toString());
                        arealist.put("accounts.country", agentName.get("accounts.country").toString());

                    }
                }
                return arealist;

            }

        } catch (JSONException ej) {
            ej.printStackTrace();
            Log.d("JSON Error ", ej.toString());

        } catch (Exception e) {
            e.printStackTrace();

            Log.d("Error in List", e.toString());

        }
        return arealist;

    }


    public HashMap<String, String> getIncentivelist(String accountName, String tabelCode) {
        HashMap<String, String> incentive_data = new HashMap<>();

        ///    ArrayList<String> agentincentiveList = new ArrayList<String>();
        try {
            JSONArray agentListData = retreiveDataFromDataBase_query(tabelCode, accountName);
            // JSONArray agentListData = retreiveDataFromDataBase(tabelCode);
            // JSONArray array = agentListData.getJSONArray();
            //    Log.d("AgentList" + TAG, agentincentiveList.toString());
            //  if(agentList.toString().length()< 1 ) {

            if (!(agentListData == null)) {
                for (int i = 0; i < agentListData.length(); i++) {

                    JSONObject agentName = (JSONObject) agentListData.get(i);

                    if (agentName.get("accounts.agent").toString().equals(accountName)) {
                        incentive_data.put("country", agentName.get("accounts.country").toString());
                        incentive_data.put("user_area", agentName.get("angaza_users_facts.current_area").toString());
                        //      agentincentiveList.add((String) agentName.get("accounts.agent"));
                        //    agentincentiveList.add((String) agentName.get("angaza_users.username"));
                        incentive_data.put("agent_phone_number", agentName.get("angaza_users.agent_phone_number").toString());
                        incentive_data.put("phone_fees_incentive_week", agentName.get("phone_fees.incentive_week").toString());
                        incentive_data.put("pre_tax_inc", agentName.get("phone_fees.pre_tax_total_upfront_incentive").toString());
                        incentive_data.put("post_tax_inc", agentName.get("phone_fees.post_tax_total_upfront_incentive").toString());
                        incentive_data.put("final_tax_inc_to_pay", agentName.get("phone_fees.final_incentive_to_pay").toString());
                        incentive_data.put("user_currency", agentName.get("angaza_users.currency").toString());
                        //agentincentiveList.add((String) agentName.get("accounts.count_units").toString());

                        JSONObject product = agentName.getJSONObject("accounts.count_units");
                        JSONObject productname = product.getJSONObject("accounts.product_name");


                        if (productname.has("Boom")) {
                            incentive_data.put("Boom", productname.getString("Boom").toString());
                        }
                        if (productname.has("Home")) {
                            incentive_data.put("Home", productname.getString("Home").toString());
                        }
                        if (productname.has("Home + Radio")) {
                            incentive_data.put("Home + Radio", productname.getString("Home + Radio").toString());
                        }
                        if (productname.has("Pro")) {
                            incentive_data.put("Pro", productname.getString("Pro").toString());
                        }
                        if (productname.has("Home 120")) {
                            incentive_data.put("Home 120", productname.getString("Home 120").toString());
                        }
                        if (productname.has("Home 120 + Radio")) {
                            incentive_data.put("Home 120 + Radio", productname.getString("Home 120 + Radio").toString());
                        }
                        if (productname.has("Home 60")) {
                            incentive_data.put("Home 60", productname.getString("Home 60"));
                        }
                        if (productname.has("Home 60 + Radio")) {
                            incentive_data.put("Home 60 + Radio", productname.getString("Home 60 + Radio"));
                        }
                        if (productname.has("Home 60 + Stove")) {
                            incentive_data.put("Home 60 + Stove", productname.getString("Home 60 + Stove"));
                        }
                        if (productname.has("Home 40-Z")) {
                            incentive_data.put("Home 40-Z", productname.getString("Home 40-Z").toString());
                        }
                        if (productname.has("Home 60 + Radio + Stove")) {
                            incentive_data.put("Home 60 + Radio + Stove", productname.getString("Home 60 + Radio + Stove").toString());
                        }
                        if (productname.has("Home 120 + Radio + Stove")) {
                            incentive_data.put("Home 120 + Radio Stove", productname.getString("Home 120 + Radio + Stove").toString());
                        }
                        if (productname.has("Home 400")) {
                            incentive_data.put("Home 400", productname.getString("Home 400").toString());
                        }

                    /*agentincentiveList.add((String) productname.getString("Boom").toString());
                        agentincentiveList.add((String) productname.getString("Home").toString());
                    agentincentiveList.add((String) productname.getString("Home + Radio").toString());
                    agentincentiveList.add((String) productname.getString("Pro").toString());*/


                        //JSONObject obj = new JSONObject( agentName.get("accounts.count_units").toString());
                        //  JSONObject array = obj.getJSONObject("accounts.product_name");

                        // JSONObject user = json.getJSONObject("user");
                        // JSONObject status = user.getJSONObject("status");
                        // int stat1 = status.getInt("stat1");

            /*        for (int i1 = 0; i1 < array.length() ;i++){

                      //  agentincentiveList.add((String) agentName.get("accounts.product_name").toString());
                      //  agentincentiveList.add((String) agentName.get("Home").toString());
                        agentincentiveList.add((String) agentName.get("Home + Radio").toString());
                        agentincentiveList.add((String) agentName.get("Pro").toString());

                    }*/




/*
                    agentincentiveList.add((String) agentName.get("accounts.product_name").toString());
                    agentincentiveList.add((String) agentName.get("Boom").toString());
                    agentincentiveList.add((String) agentName.get("Home").toString());
                    agentincentiveList.add((String) agentName.get("home + Radio").toString());
                    agentincentiveList.add((String) agentName.get("Pro").toString());*/







                 /*   agentincentiveList.add((String) agentName.get("country_facts.tax_applied"));
                    agentincentiveList.add((String) agentName.get("country_facts.tax_threshold").toString());
                    agentincentiveList.add((String) agentName.get("country_facts.tax_rate").toString());*/

                        //   agentincentiveList.add((String) agentName.get("country_facts.phone_repayment_rate").toString());

                    /*agentincentiveList.add((String) agentName.get("phone_fees.first_sale_including_inherited_units_week").toString());
                    agentincentiveList.add((String) agentName.get("country_facts.phone_price").toString());
                    agentincentiveList.add((String) agentName.get("phone_fees.phone_fee_applicable").toString());*/

                        //  {"accounts.product_name":{"Boom":null,"Home":14,"Home + Radio":3,"Pro":1}}


                    } else {


                    }
                    // agentList.add("Please Select the Country...");
                    //Log.d("Agent account", agentList.toString());
                    //  Log.d("Data Response",agentList.get(i));

                }
            }

/*
            }else {

                agentList.add("Zin Mar Oo");
                agentList.add("c");
                agentList.add("d");
                agentList.add("e");
                agentList.add("f");
return   agentList;
            }*/

        } catch (JSONException ej) {
            ej.printStackTrace();
            Log.d("JSON Error ", ej.toString());

        } catch (Exception e) {
            e.printStackTrace();

            Log.d("Error in List", e.toString());
            return null;
        }
        return incentive_data;

    }


    public HashMap<String, String> getCollectionData(String accountName, String tabelCode) {
        HashMap<String, String> collection_data = new HashMap<>();

        ///    ArrayList<String> agentincentiveList = new ArrayList<String>();
        try {
            JSONArray agentListData = retreiveDataFromDataBase_query(tabelCode, accountName);
            // JSONArray agentListData = retreiveDataFromDataBase(tabelCode);
            // JSONArray array = agentListData.getJSONArray();
            //    Log.d("AgentList" + TAG, agentincentiveList.toString());
            //  if(agentList.toString().length()< 1 ) {

            if (!(agentListData == null)) {
                for (int i = 0; i < agentListData.length(); i++) {

                    JSONObject agentName = (JSONObject) agentListData.get(i);

                    if (agentName.get("accounts.agent").toString().equals(accountName)) {

                        collection_data.put("portfolio_derived.seven_day_average_account_disabled_score", agentName.get("portfolio_derived.seven_day_average_account_disabled_score").toString());

                        collection_data.put("portfolio_derived.total_follow_on_cumulative_paid_last_7_complete_days", agentName.get("portfolio_derived.total_follow_on_cumulative_paid_last_7_complete_days").toString());
                        collection_data.put("portfolio_derived.percent_of_collections_earned", agentName.get("portfolio_derived.percent_of_collections_earned").toString());
                        collection_data.put("portfolio_derived.pre_tax_total_collections_incentive", agentName.get("portfolio_derived.pre_tax_total_collections_incentive").toString());
                        collection_data.put("portfolio_derived.post_tax_total_collections_incentive", agentName.get("portfolio_derived.post_tax_total_collections_incentive").toString());

                    } else {


                    }
                }
            }
            return collection_data;

        } catch (JSONException ej) {
            ej.printStackTrace();
            Log.d("JSON Error ", ej.toString());

        } catch (Exception e) {
            e.printStackTrace();

            Log.d("Error in List", e.toString());
            return null;
        }
        return null;

    }


    //// reterive the data for login process


    public JSONArray retreiveDataFromDataBase_query_LoginProcess(String tableCode, String agent_name_Mnumber) {

        String final_id = null;
        JSONArray tableData = null;
        try {

            URL tableURL;
            String query_id = null;

            JSONObject final_list_query = null;
            Log.d("Table CodeRetrieve Data", tableCode);
            URL loginURL = new URL(baseDatabaseURL + "login?client_id=" + baseDatabaseClientID + "&client_secret=" + baseDatabaseClientSecret);
            HttpURLConnection loginURLConnection = (HttpURLConnection) loginURL.openConnection();

            // set the connection timeout to 1 seconds and the read timeout to 5 seconds
            loginURLConnection.setConnectTimeout(3000);
            loginURLConnection.setReadTimeout(3000);
            loginURLConnection.setRequestMethod("POST");
            String unparsedAccessToken = downloadData(loginURLConnection);
            // connection has made
            JSONObject accessTokenJSONObject = new JSONObject(unparsedAccessToken);
            String accessToken = accessTokenJSONObject.get("access_token").toString();
            //URL tableURL = new URL(baseDatabaseURL + "looks/" + tableCode + "/run/json");
            //testing process looks calling 1 step
            url_check = "jsonObject";
            tableURL = new URL("https://greenlightplanet.looker.com:19999/api/3.0/looks/" + tableCode);


            //tableURL = new URL("https://greenlightplanet.looker.com:19999/api/3.0/looks/410");
            HttpURLConnection tableURLConnection = (HttpURLConnection) tableURL.openConnection();
            tableURLConnection.setRequestProperty("Authorization", "Bearer " + accessToken);

            tableURLConnection.setRequestMethod("GET");
            String unparsedTableData = downloadData(tableURLConnection);
            if (url_check.equalsIgnoreCase("jsonObject")) {


                JSONObject tableData1 = new JSONObject(unparsedTableData);

                String User_id = tableData1.getString("user_id");
                query_id = tableData1.getString("query_id");

                Iterator x = tableData1.keys();
                tableData = new JSONArray();

                while (x.hasNext()) {
                    String key = (String) x.next();
                    tableData.put(tableData1.get(key));
                }
                Log.d("Table Data JsonSObject", tableData.toString());

                //  return tableData;
            }


            if (url_check.equalsIgnoreCase("jsonObject")) {

                // query(query_id) 2nd step looker

                if (query_id != null) {
                    final_list_query = retreiveDataFromDataBase_final_field_login(tableCode, query_id, agent_name_Mnumber);
                } else {

                }


                url_check = "jsonObjectdone";

                Log.d("Create Query", final_list_query.toString());
                // return final_list_query;


            }

            if (url_check.equalsIgnoreCase("jsonObjectdone")) {

                // 3rd step Looker
                JSONObject create_query = null;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                    if (final_list_query.length() > 0) {
                        create_query = retreiveDataFromDataBase_run_query(final_list_query);
                    } else {

                    }
                }

                final_id = create_query.getString("id");
                Log.d("Create Query", create_query.toString());
                url_check = "jsonrun_query";
            }


            if (url_check.equalsIgnoreCase("jsonrun_query")) {


                // final step run_query looker
                if (final_id != null) {
                    tableData = retreiveDataFromDataBase_run_final_query(final_id);
                } else {

                }
                // Log.d("JsonArray Data",tableData.toString());

                // return tableData;
            }

            //          return tableData;


        } catch (Exception e) {

            e.printStackTrace();


        }

        return tableData;
    }










/*

    public ArrayList<String> getCountryarealist(String areacode) {
        ArrayList<String> agentList = new ArrayList<String>();

        try {
            JSONArray agentListData = retreiveDataFromDataBase(areacode); // table code is 29


            //  if(agentList.toString().length()< 1 ) {
            for (int i = 0; i < agentListData.length(); i++) {
                JSONObject agentName = (JSONObject) agentListData.get(i);
                agentList.add((String) agentName.get("accounts.area"));
                //agentList.add((String) agentName.get("accounts.country"));
                // agentList.add("Please Select the Country...");
                //Log.d("Agent account", agentList.toString());
                //  Log.d("Data Response",agentList.get(i));

            }
            return agentList;
*/
/*
            }else {

                agentList.add("Zin Mar Oo");
                agentList.add("c");
                agentList.add("d");
                agentList.add("e");
                agentList.add("f");
return   agentList;
            }*//*


        } catch (JSONException ej) {
            ej.printStackTrace();
            Log.d("JSON Error ", ej.toString());

        } catch (Exception e) {
            e.printStackTrace();

            Log.d("Error in List", e.toString());
            return null;
        }
        return null;

    }
*/


    // calling area country fron the selection of the agent hard Coded

    public ArrayList<String> getCountryarea_kenya() {
        ArrayList<String> agentList = new ArrayList<String>();

        try {
            JSONArray agentListData = retreiveDataFromDataBase("487"); // table code is 29


            //  if(agentList.toString().length()< 1 ) {
            for (int i = 0; i < agentListData.length(); i++) {
                JSONObject agentName = (JSONObject) agentListData.get(i);
                agentList.add((String) agentName.get("accounts.area"));
                //agentList.add((String) agentName.get("accounts.country"));
                // agentList.add("Please Select the Country...");
                //Log.d("Agent account", agentList.toString());
                //  Log.d("Data Response",agentList.get(i));

            }
            return agentList;
/*
            }else {

                agentList.add("Zin Mar Oo");
                agentList.add("c");
                agentList.add("d");
                agentList.add("e");
                agentList.add("f");
return   agentList;
            }*/

        } catch (JSONException ej) {
            ej.printStackTrace();
            Log.d("JSON Error ", ej.toString());

        } catch (Exception e) {
            e.printStackTrace();

            Log.d("Error in List", e.toString());
            return null;
        }
        return null;

    }


// Calling fornigeria


    public ArrayList<String> getCountryarea_nigeria() {
        ArrayList<String> agentList = new ArrayList<String>();

        try {
            JSONArray agentListData = retreiveDataFromDataBase("489"); // table code is 29

            Log.d("AgentList" + TAG, agentList.toString());
            //  if(agentList.toString().length()< 1 ) {
            for (int i = 0; i < agentListData.length(); i++) {
                JSONObject agentName = (JSONObject) agentListData.get(i);
                agentList.add((String) agentName.get("accounts.area"));
                //agentList.add((String) agentName.get("accounts.country"));
                // agentList.add("Please Select the Country...");
                //Log.d("Agent account", agentList.toString());
                //  Log.d("Data Response",agentList.get(i));

            }
            return agentList;
/*
            }else {

                agentList.add("Zin Mar Oo");
                agentList.add("c");
                agentList.add("d");
                agentList.add("e");
                agentList.add("f");
return   agentList;
            }*/

        } catch (JSONException ej) {
            ej.printStackTrace();
            Log.d("JSON Error ", ej.toString());

        } catch (Exception e) {
            e.printStackTrace();

            Log.d("Error in List", e.toString());
            return null;
        }
        return null;

    }

// Myanmar


    public ArrayList<String> getCountryarea_myanmar() {
        ArrayList<String> agentList = new ArrayList<String>();

        try {
            JSONArray agentListData = retreiveDataFromDataBase("488"); // table code is 29


            //  if(agentList.toString().length()< 1 ) {
            for (int i = 0; i < agentListData.length(); i++) {
                JSONObject agentName = (JSONObject) agentListData.get(i);
                agentList.add((String) agentName.get("accounts.area"));
                //agentList.add((String) agentName.get("accounts.country"));
                // agentList.add("Please Select the Country...");
                //Log.d("Agent account", agentList.toString());
                //  Log.d("Data Response",agentList.get(i));

            }
            return agentList;
/*
            }else {

                agentList.add("Zin Mar Oo");
                agentList.add("c");
                agentList.add("d");
                agentList.add("e");
                agentList.add("f");
return   agentList;
            }*/

        } catch (JSONException ej) {
            ej.printStackTrace();
            Log.d("JSON Error ", ej.toString());

        } catch (Exception e) {
            e.printStackTrace();

            Log.d("Error in List", e.toString());
            return null;
        }
        return null;

    }

// uganda


    public ArrayList<String> getCountryarea_uganda() {
        ArrayList<String> agentList = new ArrayList<String>();

        try {
            JSONArray agentListData = retreiveDataFromDataBase("490"); // table code is 29

            Log.d("AgentList" + TAG, agentList.toString());
            //  if(agentList.toString().length()< 1 ) {
            for (int i = 0; i < agentListData.length(); i++) {
                JSONObject agentName = (JSONObject) agentListData.get(i);
                agentList.add((String) agentName.get("accounts.area"));
                //agentList.add((String) agentName.get("accounts.country"));
                // agentList.add("Please Select the Country...");
                //Log.d("Agent account", agentList.toString());
                //  Log.d("Data Response",agentList.get(i));

            }
            return agentList;
/*
            }else {

                agentList.add("Zin Mar Oo");
                agentList.add("c");
                agentList.add("d");
                agentList.add("e");
                agentList.add("f");
return   agentList;
            }*/

        } catch (JSONException ej) {
            ej.printStackTrace();
            Log.d("JSON Error ", ej.toString());

        } catch (Exception e) {
            e.printStackTrace();

            Log.d("Error in List", e.toString());
            return null;
        }
        return null;

    }


    public void inputDataIntoDatabase(String tableCode, String agentName, Context context) {
        try {

            HashMap<String, HashMap<String, String>> taskListData = new HashMap<String, HashMap<String, String>>();

            //  JSONArray rawTaskListData =  retreiveDataFromDataBase_field(tableCode,"Kisumu");
            // JSONArray rawTaskListData = retreiveDataFromDataBase(tableCode);
            JSONArray rawTaskListData = retreiveDataFromDataBase_query(tableCode, agentName);

            //int lastInteger = 0; //TODO remove this
            int id_count = 0;

            int id_type_count_1stcall = 0;
            int id_type_count_2ndcall = 0;
            int id_type_count_visit = 0;
            int id_type_count_repo = 0;
            String task = "";

            for (int i = 0; i < rawTaskListData.length(); i++) {


                task = String.valueOf(rawTaskListData.length());


                //   Log.d("Input Data :", taskListData.toString());
                JSONObject rawTaskData = (JSONObject) rawTaskListData.get(i);
                if ((rawTaskData.get("accounts.agent").toString()).equals(agentName)) {
                    HashMap<String, String> taskData = new HashMap<String, String>();
                    taskData.put("owner_name", rawTaskData.get("accounts.owner_name").toString());
                    taskData.put("owner_msisdn", rawTaskData.get("accounts.owner_msisdn").toString());
                    taskData.put("total_paid", rawTaskData.get("accounts.total_paid").toString());
                    taskData.put("product_name", rawTaskData.get("accounts.group_name").toString());
                    taskData.put("days_past_due", rawTaskData.get("accounts.days_disabled").toString());
                    taskData.put("date_of_latest_payment_utc", rawTaskData.get("accounts.date_of_latest_payment_utc_date").toString());
                    taskData.put("account_number", rawTaskData.get("accounts.account_number").toString());
                    id_count = id_count + 1;
                    taskData.put("ticket_id", rawTaskData.get("tickets_all_countries.id").toString());

                    taskData.put("ticket_type", rawTaskData.get("tickets_all_countries.eo_ticket").toString());

                    if (rawTaskData.get("tickets_all_countries.eo_ticket").toString().equalsIgnoreCase("1st Call")) {
                        id_type_count_1stcall = id_type_count_1stcall + 1;

                    }
                    if (rawTaskData.get("tickets_all_countries.eo_ticket").toString().equalsIgnoreCase("2nd Call")) {
                        id_type_count_2ndcall = id_type_count_2ndcall + 1;

                    }
                    if (rawTaskData.get("tickets_all_countries.eo_ticket").toString().equalsIgnoreCase("Visit")) {
                        id_type_count_visit = id_type_count_visit + 1;


                    }
                    if ((rawTaskData.get("tickets_all_countries.eo_ticket").toString().equalsIgnoreCase("Repo"))) {
                        id_type_count_repo = id_type_count_repo + 1;

                    }


                    taskData.put("requester_id", rawTaskData.get("tickets_all_countries.requester_id").toString());
                    // taskData.put("request_country_status",rawTaskData.get("tickets_all_countries.status").toString());
                    taskListData.put(Integer.toString(i), taskData);


                    //lastInteger = i;
                }
            }
            String CallCount = String.valueOf(id_count);
            String id_1stcall = String.valueOf(id_type_count_1stcall);
            String id_2ndcall = String.valueOf(id_type_count_2ndcall);
            String id_visit = String.valueOf(id_type_count_visit);
            String id_repo = String.valueOf(id_type_count_repo);

            //  Log.d("TAsk************", CallCount + ":1st" + id_1stcall + ":2nd" + id_2ndcall + "visit:" + id_visit + "repo:" + id_repo +"TASSSSSS"+task);

            CallCount = CallCount + "," + id_1stcall + "," + id_2ndcall + "," + id_visit + "," + id_repo;


            Log.d("CALLL",id_1stcall);
            TaskListDatabaseConnector taskListDatabaseConnector = new TaskListDatabaseConnector(context);
            taskListDatabaseConnector.insertTaskList(taskListData, CallCount); // connecting with the task list data from the Selection
            return;
        } catch (Exception e) {
            Log.d("Errorz", e.toString());
            return;
        }
    }


    public void areafilter(String tableCode, String agentName, Context context) {
        try {
            HashMap<String, HashMap<String, String>> taskListData = new HashMap<String, HashMap<String, String>>();

            //  JSONArray rawTaskListData =  retreiveDataFromDataBase_field(tableCode,"Kisumu");
            // JSONArray rawTaskListData = retreiveDataFromDataBase(tableCode);
            JSONArray rawTaskListData = retreiveDataFromDataBase_query(tableCode, agentName);

            //int lastInteger = 0; //TODO remove this
            int id_count = 0;

            int id_type_count_1stcall = 0;
            int id_type_count_2ndcall = 0;
            int id_type_count_visit = 0;
            int id_type_count_repo = 0;
            String task = "";

            for (int i = 0; i < rawTaskListData.length(); i++) {


                task = String.valueOf(rawTaskListData.length());


                //   Log.d("Input Data :", taskListData.toString());
                JSONObject rawTaskData = (JSONObject) rawTaskListData.get(i);
                if ((rawTaskData.get("accounts.agent").toString()).equals(agentName)) {
                    HashMap<String, String> taskData = new HashMap<String, String>();
                    taskData.put("owner_name", rawTaskData.get("accounts.owner_name").toString());
                    taskData.put("owner_msisdn", rawTaskData.get("accounts.owner_msisdn").toString());
                    taskData.put("total_paid", rawTaskData.get("accounts.total_paid").toString());
                    taskData.put("product_name", rawTaskData.get("accounts.group_name").toString());
                    taskData.put("days_past_due", rawTaskData.get("accounts.days_disabled").toString());
                    taskData.put("date_of_latest_payment_utc", rawTaskData.get("accounts.date_of_latest_payment_utc_date").toString());
                    taskData.put("account_number", rawTaskData.get("accounts.account_number").toString());
                    id_count = id_count + 1;
                    taskData.put("ticket_id", rawTaskData.get("tickets_all_countries.id").toString());

                    taskData.put("ticket_type", rawTaskData.get("tickets_all_countries.eo_ticket").toString());

                    if (rawTaskData.get("tickets_all_countries.eo_ticket").toString().equalsIgnoreCase("1st Call")) {
                        id_type_count_1stcall = id_type_count_1stcall + 1;

                    }
                    if (rawTaskData.get("tickets_all_countries.eo_ticket").toString().equalsIgnoreCase("2nd Call")) {
                        id_type_count_2ndcall = id_type_count_2ndcall + 1;

                    }
                    if (rawTaskData.get("tickets_all_countries.eo_ticket").toString().equalsIgnoreCase("Visit")) {
                        id_type_count_visit = id_type_count_visit + 1;


                    }
                    if ((rawTaskData.get("tickets_all_countries.eo_ticket").toString().equalsIgnoreCase("Repo"))) {
                        id_type_count_repo = id_type_count_repo + 1;

                    }


                    taskData.put("requester_id", rawTaskData.get("tickets_all_countries.requester_id").toString());
                    // taskData.put("request_country_status",rawTaskData.get("tickets_all_countries.status").toString());
                    taskListData.put(Integer.toString(i), taskData);


                    //lastInteger = i;
                }
            }
            /*
            HashMap<String, String> taskData = new HashMap<String, String>();
            taskData.put("owner_name", "Ihsaan Patel");
            taskData.put("owner_msisdn", "+13614434197");
            taskData.put("total_paid", "1000");
            taskData.put("product_name", "Pro");
            taskData.put("days_past_due", "20");
            taskData.put("date_of_latest_payment_utc", "2016-07-23");
            taskData.put("account_number", "12345678");
            taskData.put("ticket_id", "15575");
            taskData.put("ticket_type", "1st Call");
            taskData.put("requester_id", "2630088985");
            taskListData.put(Integer.toString(lastInteger + 1),taskData);
            */
            String CallCount = String.valueOf(id_count);
            String id_1stcall = String.valueOf(id_type_count_1stcall);
            String id_2ndcall = String.valueOf(id_type_count_2ndcall);
            String id_visit = String.valueOf(id_type_count_visit);
            String id_repo = String.valueOf(id_type_count_repo);

            //  Log.d("TAsk************", CallCount + ":1st" + id_1stcall + ":2nd" + id_2ndcall + "visit:" + id_visit + "repo:" + id_repo +"TASSSSSS"+task);

            CallCount = CallCount + "," + id_1stcall + "," + id_2ndcall + "," + id_visit + "," + id_repo;


            TaskListDatabaseConnector taskListDatabaseConnector = new TaskListDatabaseConnector(context);
            taskListDatabaseConnector.insertTaskList(taskListData, CallCount); // connecting with the task list data from the Selection
            return;
        } catch (Exception e) {
            Log.d("Errorz", e.toString());
            return;
        }
    }


    public boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    ;

    public void updateCompletedTasks(Context context, String country_name) {

        Log.d("Countryyyyyyy", country_name);
// Country based zendeskbaseurl
        countryName(country_name);

        TaskListDatabaseConnector taskListDatabaseConnector = new TaskListDatabaseConnector(context);
        String completedTicketIDs = taskListDatabaseConnector.getCompletedTickets();
        Log.d("CompleteTickets", completedTicketIDs);
        String zendeskURL = finalZendeskbaseURL + "/api/v2/tickets/update_many.json?ids=" + completedTicketIDs;
        // String zendeskURL = zendeskBaseURL + "/api/v2/tickets/update_many.json?ids=" + completedTicketIDs;
        String zendeskAPIData = "{\"ticket\": {\"status\": \"solved\"}}";
        if (!completedTicketIDs.equals("")) {

            sendDataToZendesk(zendeskURL, "PUT", zendeskAPIData, country_name);

            String[] ticketIDs = completedTicketIDs.split(",");
            taskListDatabaseConnector.updateTicketSolved(ticketIDs);
        }
    }

    public void countryName(String country_name) {

        if (country_name.equalsIgnoreCase("Kenya")) {

            finalZendeskbaseURL = zendeskBaseURL;
            finalzendeskpass = zendeskPassword;
            finalzendeskUsername = zendeskUsername;
            Log.d("Countryy *****" + finalZendeskbaseURL, finalzendeskUsername);
        } else if (country_name.equalsIgnoreCase("Nigeria")) {

            finalZendeskbaseURL = zendeskBaseURL_ng;
            finalzendeskUsername = zendeskUsername_ng;
            finalzendeskpass = zendeskPassword_ng;

            Log.d("Countryy *****" + finalZendeskbaseURL, finalzendeskUsername);
        } else if (country_name.equalsIgnoreCase("Myanmar (Burma)")) {

            finalZendeskbaseURL = zendeskBaseURL_mg;
            finalzendeskUsername = zendeskUsername_mg;
            finalzendeskpass = zendeskPassword_mg;
            Log.d("Countryy *****" + finalZendeskbaseURL, finalzendeskUsername);
        } else if (country_name.equalsIgnoreCase("Uganda")) {

            finalZendeskbaseURL = zendeskBaseURL_ug;
            finalzendeskUsername = zendeskUsername_ug;
            finalzendeskpass = zendeskPassword_ug;
            Log.d("Countryy *****" + finalZendeskbaseURL, finalzendeskUsername);
        } else if (country_name.equalsIgnoreCase("Tanzania")) {

            finalZendeskbaseURL = zendeskBaseURL_tz;
            finalzendeskUsername = zendeskUsername_tz;
            finalzendeskpass = zendeskPassword_tz;
            Log.d("Countryy *****" + finalZendeskbaseURL, finalzendeskUsername);
        } else if (country_name.equalsIgnoreCase("India")) {

            finalZendeskbaseURL = zendeskBaseURL_ind;
            finalzendeskUsername = zendeskUsername_ind;
            finalzendeskpass = zendeskPassword_ind;
        }

        Log.d("Country Name&&&&&&&", country_name);
    }

    public void createNewTickets(Context context, String country_name) {


        String zendeskAPIData = null;
        Log.d("Countryyyyyyy", country_name);
// Country based zendeskbaseurl
        countryName(country_name);
        ContentValues ticketCreated = new ContentValues();
        ticketCreated.put("ticket_created", true);
        TaskListDatabaseConnector taskListDatabaseConnector = new TaskListDatabaseConnector(context);
        taskListDatabaseConnector.open();
        Cursor ticketData = taskListDatabaseConnector.getCompletedTicketsInformation();
        // country based url


        while (ticketData.moveToNext()) {
            // String zendeskURL = zendeskBaseURL + "/api/v2/tickets.json";
            String zendeskURL = finalZendeskbaseURL + "/api/v2/tickets.json";
            Log.d("FinbasicUrl", country_name + finalZendeskbaseURL);


            String requester_id = ticketData.getString(0);
            String next_payment_date = ticketData.getString(1);
            String callNumber = ticketData.getString(2).substring(0, 3);
            String ticketTag = callNumber + "_promise_call";
            String ticketName = callNumber + " Promise Call";
            String taskID = ticketData.getString(3);
            String callDuration = ticketData.getString(4);

            Log.d("Task Parameter", requester_id + "::" + next_payment_date + "::" + ticketTag + ":" + callNumber + "::" + ticketName + "::" + taskID + "::" + callDuration + "::" + appSolver);
            //callDuration = "30"; //hardcodded time out

            if (country_name.equalsIgnoreCase("Uganda")) {
                zendeskAPIData = String.format("{\"ticket\": {\"due_at\":\"%s\", \"type\":\"task\", \"submitter_id\":2976016765, \"priority\":\"urgent\", "
                        + "\"ticket_form_id\":103209, \"fields\":[{\"id\":36328065,\"value\":\"%s\"},{\"id\":45477029,\"value\":\"%s\"}, {\"id\":36946165,\"value\":\"%s\"}], \"requester_id\":%s,"
                        + "\"comment\":{\"body\":\"%s - Customer said they would pay before %s\"}}}", next_payment_date, ticketTag, appSolver, callDuration, requester_id, ticketName, next_payment_date);
            }
            // it is an older one
            else if (country_name.equalsIgnoreCase("Kenya")) {
                zendeskAPIData = String.format("{\"ticket\": {\"due_at\":\"%s\", \"type\":\"task\", \"submitter_id\":2976016765, \"priority\":\"urgent\", "
                        + "\"ticket_form_id\":103209, \"fields\":[{\"id\":25900655,\"value\":\"%s\"},{\"id\":114101366772,\"value\":\"%s\"}, {\"id\":30006725,\"value\":\"%s\"}], \"requester_id\":%s,"
                        + "\"comment\":{\"body\":\"%s - Customer said they would pay before %s\"}}}", next_payment_date, ticketTag, appSolver, callDuration, requester_id, ticketName, next_payment_date);
                Log.d("zendesk API: ", country_name + ":" + zendeskAPIData);
            } else if (country_name.equalsIgnoreCase("Nigeria")) {
                zendeskAPIData = String.format("{\"ticket\": {\"due_at\":\"%s\",\"tags\":\"%s\",\"type\":\"task\", \"submitter_id\":2976016765, \"priority\":\"urgent\", "
                        + "\"ticket_form_id\":103209, \"fields\":[{\"id\":38786669,\"value\":\"%s\"},{\"id\":45507165,\"value\":\"%s\"}, {\"id\":38787109,\"value\":\"%s\"}], \"requester_id\":%s,"
                        + "\"comment\":{\"body\":\"%s - Customer said they would pay before %s\"}}}", next_payment_date, ticketTag, ticketTag, appSolver, callDuration, requester_id, ticketName, next_payment_date);
            } else if (country_name.equalsIgnoreCase("Myanmar (Burma)")) {
                zendeskAPIData = String.format("{\"ticket\": {\"due_at\":\"%s\",\"tags\":\"%s\",\"type\":\"task\", \"submitter_id\":2976016765, \"priority\":\"urgent\", "
                        + "\"ticket_form_id\":103209, \"fields\":[{\"id\":34146985,\"value\":\"%s\"}, {\"id\":45507145,\"value\":\"%s\"}, {\"id\":34147125,\"value\":\"%s\"}], \"requester_id\":%s,"
                        + "\"comment\":{\"body\":\"%s - Customer said they would pay before %s\"}}}", next_payment_date, ticketTag, ticketTag, appSolver, callDuration, requester_id, ticketName, next_payment_date);
            } else if (country_name.equalsIgnoreCase("Tanzania")) {
                zendeskAPIData = String.format("{\"ticket\": {\"due_at\":\"%s\",\"tags\":\"%s\",\"type\":\"task\", \"submitter_id\":2976016765, \"priority\":\"urgent\", "
                        + "\"ticket_form_id\":103209, \"fields\":[{\"id\":27101785,\"value\":\"%s\"},{\"id\":333756962535,\"value\":\"%s\"}, {\"id\":23050070,\"value\":\"%s\"}], \"requester_id\":%s,"
                        + "\"comment\":{\"body\":\"%s - Customer said they would pay before %s\"}}}", next_payment_date, ticketTag, ticketTag, appSolver, callDuration, requester_id, ticketName, next_payment_date);
            } else if (country_name.equalsIgnoreCase("India")) {
                zendeskAPIData = String.format("{\"ticket\": {\"due_at\":\"%s\",\"tags\":\"%s\",\"type\":\"task\", \"submitter_id\":2976016765, \"priority\":\"urgent\", "
                        + "\"ticket_form_id\":103209, \"fields\":[{\"id\":38763669,\"value\":\"%s\"},{\"id\":45507069,\"value\":\"%s\"}, {\"id\":38915745,\"value\":\"%s\"}], \"requester_id\":%s,"
                        + "\"comment\":{\"body\":\"%s - Customer said they would pay before %s\"}}}", next_payment_date, ticketTag, ticketTag, appSolver, callDuration, requester_id, ticketName, next_payment_date);
            }


            sendDataToZendesk(zendeskURL, "POST", zendeskAPIData, country_name);
            Log.d("Zendeskdata send", zendeskURL);
            taskListDatabaseConnector.updateTicketCreated(taskID, ticketCreated);
        }
        taskListDatabaseConnector.close();
    }


    public void sendDataToZendesk(String zendeskURLString, String requestMethod, String zendeskAPIData, String country_name) {


        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

// Zendesk Username and password
        countryName(country_name);

        //Log.d("Sendata Strings::", zendeskURLString + ":::" + requestMethod + "::" + zendeskAPIData + "::" + country_name);

//        Log.d("Send Data Zendesk", finalZendeskbaseURL + "::" + finalzendeskUsername +"::" + finalzendeskpass);

        // String authString = zendeskUsername + ":" + zendeskPassword;
        String authString = finalzendeskUsername + ":" + finalzendeskpass;

        // byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
        byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
        String authStringEnc = new String(authEncBytes);
        try {
            URL zendeskURL = new URL(zendeskURLString);
            HttpURLConnection zendeskURLConnection = (HttpURLConnection) zendeskURL.openConnection();
            zendeskURLConnection.setDoOutput(true);
            zendeskURLConnection.setRequestProperty("Authorization", "Basic " + authStringEnc);
            zendeskURLConnection.setRequestProperty("Content-Type", "application/json");
            zendeskURLConnection.setRequestProperty("Accept", "application/json");
            zendeskURLConnection.setRequestMethod(requestMethod);
            zendeskURLConnection.connect();
            OutputStreamWriter os = new OutputStreamWriter(zendeskURLConnection.getOutputStream());
            os.write(zendeskAPIData);
            Log.d("ZendeskApi Data", zendeskAPIData + requestMethod + authStringEnc);
            os.flush();
            os.close();
            Log.d("Zendesk request: code", Integer.toString(zendeskURLConnection.getResponseCode()));
        } catch (EOFException eof) {
            Log.d("Error Send Data", eof.toString());
            return;
        } catch (Exception e) {
            Log.d("Zendesk error: ", e.toString());
            return;
        }

    }


    public String createNewIncentiveTickets(Context context, String country_name) {


        Log.d("Countryyyyyyy", country_name);
        String[] Txt_field = country_name.split(",");

        country_name = Txt_field[0];
        String agent_name = Txt_field[1];
        String final_date = Txt_field[2];


// Country based zendeskbaseurl
        countryName(country_name);

        // Cursor ticketData = taskListDatabaseConnector.getCompletedTicketsInformation();


        try {


            // country based url
            String zendeskURL = finalZendeskbaseURL + "/api/v2/tickets.json";
            String next_payment_date = "";
            String ticketTag = "Final incentive report";
            String callDuration = "";
            String requester_id = "";
            String ticketName = agent_name + " has a problem on his/her Final Incentive ";

/*
    String zendeskAPIData = String.format("{\"ticket\": {\"status\":\"open\",\"type\":\"Incentive\", \"submitter_id\":2976016765, \"priority\":\"urgent\", "
            + "\"ticket_form_id\":103209, \"fields\":[{\"id\":25900655,\"value\":\"%s\"}, {\"id\":30006725,\"value\":\"%s\"}],"
            + "\"comment\":{\"body\":\"%s -  %s\"}}}", ticketTag, callDuration,ticketName, next_payment_date);

*/

            String zendeskAPIData = String.format("{\"ticket\": {\"status\": \"open\",\"type\":\"Incentive\",\"comment\":{\"body\":\"%s - \"}}}", ticketName);
            sendDataToZendesk(zendeskURL, "POST", zendeskAPIData, country_name);
            Log.d("Zendeskdata send", zendeskURL);


        } catch (Exception ex) {
            ex.printStackTrace();
            //   taskListDatabaseConnector.updateTicketCreated(taskID, ticketCreated);
        }
        return final_date;
    }
    // taskListDatabaseConnector.close();


    public HashMap<String, String> getSummaryscore(String accountName, String tabelCode) {
        HashMap<String, String> Summary_data = new HashMap<>();

        ///    ArrayList<String> agentincentiveList = new ArrayList<String>();
        try {
            JSONArray agentSummaryData = retreiveDataFromDataBase_query(tabelCode, accountName);
            // JSONArray agentListData = retreiveDataFromDataBase(tabelCode);
            // JSONArray array = agentListData.getJSONArray();
            //    Log.d("AgentList" + TAG, agentincentiveList.toString());
            //  if(agentList.toString().length()< 1 ) {

            // if (!(agentSummaryData == null)) {

            if (!(agentSummaryData == null)) {
                for (int i = 0; i < agentSummaryData.length(); i++) {

                    JSONObject agentName = (JSONObject) agentSummaryData.get(i);

                    if (agentName.get("accounts.agent").toString().equals(accountName)) {
                        Summary_data.put("accounts.cws", agentName.get("accounts.current_week_sales").toString()); // current week s
                        Summary_data.put("accounts.w_c_u_mtd", agentName.get("accounts.count_units_mtd").toString());
                        Summary_data.put("accounts.w_count_units_lmsd_depricated", agentName.get("accounts.count_units_lmsd_depricated").toString());
                        Summary_data.put("accounts.count_disabled_over_3_days", agentName.get("accounts.count_disabled_over_3_days").toString());
                        Summary_data.put("accounts.count_units_under_repayment", agentName.get("accounts.count_units_under_repayment").toString());
                        Summary_data.put("accounts.sum_balance_to_collect", agentName.get("accounts.sum_balance_to_collect").toString());
                        Summary_data.put("payments.current_week_collections", agentName.get("payments.current_week_collections").toString());
                        Summary_data.put("angaza_users_facts.current_area", agentName.get("angaza_users_facts.current_area").toString());
                        Summary_data.put("accounts.agent_phone_number", agentName.get("accounts.agent_phone_number").toString());


                    } else {


                    }
                    // agentList.add("Please Select the Country...");
                    //Log.d("Agent account", agentList.toString());
                    //  Log.d("Data Response",agentList.get(i));

                }
            }

            Log.d("Summary data@@@", Summary_data.toString());
            return Summary_data;
/*
            }else {

                agentList.add("Zin Mar Oo");
                agentList.add("c");
                agentList.add("d");
                agentList.add("e");
                agentList.add("f");
return   agentList;
            }*/

        } catch (JSONException ej) {
            ej.printStackTrace();
            Log.d("JSON Error ", ej.toString());

        } catch (Exception e) {
            e.printStackTrace();

            Log.d("Error in List", e.toString());
            return null;
        }
        return null;

    }


    public void getdata(String tableCode) {


      //  LoadingDialog1.showLoadingDialog(getapp, "Loading....");
        RetrofitClient.GitApiInterface service = RetrofitClient.Indianrail();

        String body = "api/3.0/login?client_id=" + baseDatabaseClientID + "&client_secret=" + baseDatabaseClientSecret;

        Call<String> call = service.getdata(body);
        LoadingDialog1.getcall(call);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                try {

                    LoadingDialog1.cancelLoading();

                    if (response.body() != null) {

                        Log.d("RESPPNS",response.body());



                    } else {
                      //  Toast.makeText(mContext, "No Response. Please try Again!!!", Toast.LENGTH_LONG).show();

                    }


                } catch (Exception e) {
                    // Toast.makeText(this, GetMethodName() + e.getMessage(), Toast.LENGTH_LONG).show();
               //     Toast.makeText(mContext, "Error. Please try Again!!!", Toast.LENGTH_LONG).show();
                    LoadingDialog1.cancelLoading();

                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                if (t instanceof SocketTimeoutException) {
                    LoadingDialog1.cancelLoading();
             //       Toast.makeText(mContext, "Something went wrong. Please try Again!!!", Toast.LENGTH_LONG).show();
                    //Toast.makeText(mContext, "Socket Timeout", Toast.LENGTH_SHORT).show();
                } else {
                    LoadingDialog1.cancelLoading();
             //       Toast.makeText(mContext, "Something went wrong. Please try Again!!!", Toast.LENGTH_LONG).show();
                    // Toast.makeText(mContext, "Error", Toast.LENGTH_SHORT).show();
                }
            }


        });

    }



    public ArrayList volleyR(final Context mContext, final String tableCode , final CallBackVolley callback) {
      //  progressDialog.show();
        String url = "https://greenlightplanet.looker.com:19999/api/3.0/login";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        Log.d("Response",s);
                        //progressDialog.dismiss();
                        try {
                            JSONObject js = new JSONObject(s);
                            String access_Token = js.getString("access_token");
                            String token_type = js.getString("token_type");
                            String expires_in = js.getString("expires_in");
                            Log.d("RequestResss",access_Token+","+token_type+","+expires_in);
                           // String tableCode = "399";
                            if (!(js == null)) {
                          //    ArrayList arrayList = getTabledata(mContext,access_Token, tableCode);

                                getTabledata(mContext,access_Token, tableCode,new CallBackVolley(){
                                    @Override
                                    public void onSuccess(ArrayList<String>  result){
                                     arrayList=result;

                                        callback.onSuccess(arrayList);
                                    }
                                });
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            LoadingDialog.cancelLoading();
                        }
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                      //  Toast.makeText(AgentSelection.this,"Something went wrong!!"+volleyError,Toast.LENGTH_LONG).show();
                        System.out.println("Something went wrong!");
                        volleyError.printStackTrace();
                       // progressDialog.dismiss();
                        //createErrorDialogBoxInternetConnection(getString(R.string.no_internet_connection));
                    }
                }){
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("client_id", "QY6DQzQT6NQM3ngbTRxY");
                params.put("client_secret", "PMb9rtjBzrdYgyvqQhfhTjrB");
                return params;
            }
        }  ;
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = NetworkCallSingleton.getInstance(mContext).getRequestQ();
        requestQueue.add(stringRequest);
        // requestQueue.add(nw.MVolleycall(mContext));
        //     NetworkCallSingleton.getInstance().MVolleycall(mContext);
        // NetworkCallSingleton.getInstance().addToRequestQueue(stringRequest);

        return null;
    }



    public void getTabledata(Context mContext,String accessToken, String tableCode,final CallBackVolley callback){

        final ArrayList<String> agentList = new ArrayList<String>();

        final String token = accessToken;


        Log.d(accessToken,tableCode+"@@"+token);

       // progressDialog.show();
        String url = "https://greenlightplanet.looker.com:19999/api/3.0/looks/" + tableCode + "/run/json";
        //   String url = "http://13.126.146.126/Sunking_EB/Kazi_login.php"+"?name="+name+"&password="+password+"&mobileNumber="+Mnumber+"&country="+country;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        Log.d("Response",s);

                        try {
                            JSONArray jsonArray= new JSONArray(s);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                String country = (String) jsonObject.get("accounts.country");
                                agentList.add(country);
                            }
                            callback.onSuccess(agentList);

                          /*  country_list = agentList;



                            if (country_list != null) {
                                ArrayAdapter<String> countrySelectionSpinnerAdapter = new ArrayAdapter<String>(AgentSelection.this, android.R.layout.simple_spinner_item, country_list);
                                countrySelectionSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                countrySelectionSpinner.setAdapter(countrySelectionSpinnerAdapter);
                                countrySelectionSpinner.setOnItemSelectedListener(countrySelectionSpinnerListener);
                                //  countrySelectionSpinner.setPrompt("Please Select the Country...");
                                progressDialog.dismiss();
                            } else {
                                createErrorDialogBoxInternetConnection(getString(R.string.no_internet_try_again));
                            }

*/


                            Log.d("Array LIst",agentList.toString());

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                     //   progressDialog.dismiss();



                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                       // Toast.makeText(AgentSelection.this,"Something went wrong!!"+volleyError,Toast.LENGTH_LONG).show();
                        System.out.println("Something went wrong!");
                        volleyError.printStackTrace();
                        LoadingDialog.cancelLoading();
                       // progressDialog.dismiss();
                       // createErrorDialogBoxInternetConnection(getString(R.string.no_internet_connection));
                    }
                }){
            @Override
            public Map<String, String> getHeaders()
            //   protected Map<String, String> getParams()throws com.android.volley.AuthFailureError
            {
                Map<String, String  >  headers = new HashMap<String, String>();
                headers.put("Authorization","Bearer " +token);
                return headers;

            }

        }  ;
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = NetworkCallSingleton.getInstance(mContext).getRequestQ();
        requestQueue.add(stringRequest);
        // requestQueue.add(nw.MVolleycall(mContext));
        //     NetworkCallSingleton.getInstance().MVolleycall(mContext);




    }







    public ArrayList firstCallqueryFilter(final Context mContext, final String tableCode, final String agentName,final VolleyCallback callback1) {

        // progressDialog.show();
        String url = "https://greenlightplanet.looker.com:19999/api/3.0/login";
        //   String url = "http://13.126.146.126/Sunking_EB/Kazi_login.php"+"?name="+name+"&password="+password+"&mobileNumber="+Mnumber+"&country="+country;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        Log.d("Response",s);


                        try {
                            JSONObject js = new JSONObject(s);
                            final String access_Token = js.getString("access_token");
                            String token_type = js.getString("token_type");
                            String expires_in = js.getString("expires_in");


                            Log.d("RequestResss",access_Token+","+token_type+","+expires_in);


                            // String  tableCode = "2239";

                            if (!(js == null)) {
                                //    getTabledata(access_Token, tableCode);
                               // secoundcallFilter(mContext,access_Token,tableCode,agentName);
                                secoundcallFilter(mContext,access_Token, tableCode,agentName,new VolleyCallback(){
                                    @Override
                                    public void onSuccess(String  result){

                                     /*   data=result;*/
                                     int a=1;
                                     a=a+2;
try {


    callback1.onSuccess(result);
}
catch (Exception e){

    e.printStackTrace();
}
                                    }
                                });


                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        System.out.println("Something went wrong!");
                        volleyError.printStackTrace();
                    }
                }){
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("client_id", "QY6DQzQT6NQM3ngbTRxY");
                params.put("client_secret", "PMb9rtjBzrdYgyvqQhfhTjrB");
                return params;
            }
        }  ;
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = NetworkCallSingleton.getInstance(mContext).getRequestQ();
        requestQueue.add(stringRequest);
        // requestQueue.add(nw.MVolleycall(mContext));
        //     NetworkCallSingleton.getInstance().MVolleycall(mContext);
        // NetworkCallSingleton.getInstance().addToRequestQueue(stringRequest);

        return null;
    }


    public void secoundcallFilter(final Context mContext, String accessToken, final String tableCode, final String agentName,final VolleyCallback callback2){



        final String token = accessToken;

        String id_query;
        // Log.d(accessToken,tableCode+"@@"+token);

//        progressDialog.show();
        String url = "https://greenlightplanet.looker.com:19999/api/3.0/looks/"+tableCode;
        //   String url = "http://13.126.146.126/Sunking_EB/Kazi_login.php"+"?name="+name+"&password="+password+"&mobileNumber="+Mnumber+"&country="+country;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        Log.d("Response",s);

                        int query_id = 0;
                        ArrayList<Integer> query_list = new ArrayList<Integer>();

                        try {
                            //  JSONArray jsonArray= new JSONArray(s);
                            JSONObject jsonObject = new JSONObject(s);

                            for (int i = 0; i < jsonObject.length(); i++) {
                                //JSONObject jsonObject = jsonObject.getJSONObject(i);

                                query_id = (int)jsonObject.get("query_id");
                                query_list.add(query_id);

                                if (query_list.size() > 0){

                                    break;
                                }
                            }

                            Log.d("query_ID", String.valueOf(query_id));

                            if (query_list.size() > 0) {
                                // thirdCallquery1(tableCode, query_id, Mnumber);
                                volleyRFilter(mContext, query_id, agentName, new VolleyCallback() {
                                    @Override
                                    public void onSuccess(String result) {


                                        callback2.onSuccess(result);

                                    }
                                });
                            }else{
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        System.out.println("Something went wrong!");
                        volleyError.printStackTrace();
                    }
                }){
            @Override
            public Map<String, String> getHeaders()
            //   protected Map<String, String> getParams()throws com.android.volley.AuthFailureError
            {
                Map<String, String  >  headers = new HashMap<String, String>();
                headers.put("Authorization","Bearer " +token);
                return headers;

            }

        }  ;
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = NetworkCallSingleton.getInstance(mContext).getRequestQ();
        requestQueue.add(stringRequest);
        // requestQueue.add(nw.MVolleycall(mContext));
        //     NetworkCallSingleton.getInstance().MVolleycall(mContext);





    }





    public void thirdCallquery1Filter(final Context mContext, final String accessToken, final int query_id, final String Mnumber,final VolleyCallback callback9){


        final JSONObject filter_update = new JSONObject();



        // Log.d(accessToken,tableCode+"@@"+token);

        //   progressDialog.show();
        String url = "https://greenlightplanet.looker.com:19999/api/3.0/queries/" + query_id;
        //   String url = "http://13.126.146.126/Sunking_EB/Kazi_login.php"+"?name="+name+"&password="+password+"&mobileNumber="+Mnumber+"&country="+country;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        Log.d("ResponseThird Filterqq",s);



                        try {

                            JSONObject finalFilter = new JSONObject();
                            JSONObject jsonObject = new JSONObject(s);
                            for (int i = 0; i < jsonObject.length(); i++) {

                                finalFilter = new JSONObject(String.valueOf(s));


                                String filters = (String) finalFilter.getString("filters");

                                finalFilter.put("client_id", "");
                                JSONObject jsonObject1 = new JSONObject(filters);
                                JSONObject filter_update = jsonObject1.put("accounts.agent",Mnumber);

                                finalFilter.put("filters",filter_update);
                            }

                            QueryIDFilter(mContext, finalFilter, new VolleyCallback() {
                                @Override
                                public void onSuccess(String result) {


                                    callback9.onSuccess(result);


                                }
                            });


                            Log.d("UpdatedFilter",finalFilter.toString());


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        System.out.println("Something went wrong!");
                        volleyError.printStackTrace();
                    }
                }){


            @Override
            public Map<String, String> getHeaders()
            //   protected Map<String, String> getParams()throws com.android.volley.AuthFailureError
            {
                Map<String, String  >  headers = new HashMap<String, String>();
                headers.put("Authorization","Bearer " +accessToken);
                return headers;

            }

        }  ;
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = NetworkCallSingleton.getInstance(mContext).getRequestQ();
        requestQueue.add(stringRequest);
        // requestQueue.add(nw.MVolleycall(mContext));
        //     NetworkCallSingleton.getInstance().MVolleycall(mContext);





    }



    public void forthCallquery1Filter(final Context mContext, final String accessToken, final JSONObject queress,final VolleyCallback callback3){


        // Log.d(accessToken,tableCode+"@@"+token);

//        progressDialog.show();
        final String requestBody,FinalID;

        requestBody = queress.toString();

       String url = "https://greenlightplanet.looker.com:19999/api/3.0/queries";
        //   String url = "http://13.126.146.126/Sunking_EB/Kazi_login.php"+"?name="+name+"&password="+password+"&mobileNumber="+Mnumber+"&country="+country;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        Log.d("ForthQQQ",s);


                        int final_id = 0;
                        ArrayList<Integer> finalq_list = new ArrayList<Integer>();

                        try {
                            //  JSONArray jsonArray= new JSONArray(s);
                            JSONObject jsonObject = new JSONObject(s);

                            for (int i = 0; i < jsonObject.length(); i++) {
                                //JSONObject jsonObject = jsonObject.getJSONObject(i);

                                final_id = (int)jsonObject.get("id");
                                finalq_list.add(final_id);

                                if (finalq_list.size() > 0){

                                    break;
                                }
                            }

                            Log.d("final_id", String.valueOf(final_id));

                            if (finalq_list.size() > 0) {
                                // thirdCallquery1(tableCode, query_id, Mnumber);

                               // FinalQuery_loginFilter(mContext,final_id,);


                                FinalQuery_loginFilter(mContext,final_id,new VolleyCallback(){
                                    @Override
                                    public void onSuccess(String  result){


                                        callback3.onSuccess(result);
                                    }
                                });


                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        System.out.println("Something went wrong!");
                        volleyError.printStackTrace();
                    }
                }){

            @Override
            public Map<String, String> getHeaders()
            //   protected Map<String, String> getParams()throws com.android.volley.AuthFailureError
            {
                Map<String,String>  headers = new HashMap<String, String>();
                headers.put("Authorization","Bearer " +accessToken);
                headers.put("Content-Type","application/json");
                // headers.put("Query",String.valueOf(queress));
                Log.d("STRRRRRRRRRRRR@@@",String.valueOf(queress));
                return headers;
            }

            @Override
            public String getBodyContentType() {
                return String.format("application/json: charset=utf-8");
            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s",
                            requestBody, "utf-8");
                    return null;
                }
            }


        }
                ;
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = NetworkCallSingleton.getInstance(mContext).getRequestQ();
        requestQueue.add(stringRequest);
        // requestQueue.add(nw.MVolleycall(mContext));
        //     NetworkCallSingleton.getInstance().MVolleycall(mContext);





    }


    public void volleyRFilter(final Context mContext, final int query_id, final String agentName,final VolleyCallback callback8) {
        //   progressDialog.show();
        String url = "https://greenlightplanet.looker.com:19999/api/3.0/login";
        //   String url = "http://13.126.146.126/Sunking_EB/Kazi_login.php"+"?name="+name+"&password="+password+"&mobileNumber="+Mnumber+"&country="+country;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        Log.d("Response",s);

                       // progressDialog.dismiss();

                        try {
                            JSONObject js = new JSONObject(s);
                            String access_Token = js.getString("access_token");
                            String token_type = js.getString("token_type");
                            String expires_in = js.getString("expires_in");


                            Log.d("RequestResss",access_Token+","+token_type+","+expires_in);

                            // String tableCode = "2239";
                            if (!(js == null)) {
                                // getTabledata(access_Token, tableCode);

                                thirdCallquery1Filter(mContext, access_Token, query_id, agentName, new VolleyCallback() {
                                    @Override
                                    public void onSuccess(String result) {


                                        callback8.onSuccess(result);


                                    }
                                });
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        System.out.println("Something went wrong!");
                        volleyError.printStackTrace();
                     }
                }){
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("client_id", "QY6DQzQT6NQM3ngbTRxY");
                params.put("client_secret", "PMb9rtjBzrdYgyvqQhfhTjrB");
                return params;
            }
        }  ;
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = NetworkCallSingleton.getInstance(mContext).getRequestQ();
        requestQueue.add(stringRequest);
        // requestQueue.add(nw.MVolleycall(mContext));
        //     NetworkCallSingleton.getInstance().MVolleycall(mContext);
        // NetworkCallSingleton.getInstance().addToRequestQueue(stringRequest);
    }




    public void FinalQuery_loginFilter(final Context mContext, final int query_ID, final VolleyCallback callback4) {
        //      progressDialog.show();
        String url = "https://greenlightplanet.looker.com:19999/api/3.0/login";
        //   String url = "http://13.126.146.126/Sunking_EB/Kazi_login.php"+"?name="+name+"&password="+password+"&mobileNumber="+Mnumber+"&country="+country;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        Log.d("Response",s);


                        try {JSONObject js = new JSONObject(s);
                            String access_Token = js.getString("access_token");
                            String token_type = js.getString("token_type");
                            String expires_in = js.getString("expires_in");


                            Log.d("RequestResss",access_Token+","+token_type+","+expires_in);


                            if (!(js == null)) {





                                finalquery1oginFilter(mContext, access_Token, query_ID, new VolleyCallback() {
                                    @Override
                                    public void onSuccess(String result) {



                                        callback4.onSuccess(result);
                                    }
                                });
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                         System.out.println("Something went wrong!");
                        volleyError.printStackTrace();
                        }
                }){
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("client_id", "QY6DQzQT6NQM3ngbTRxY");
                params.put("client_secret", "PMb9rtjBzrdYgyvqQhfhTjrB");
                return params;
            }
        }  ;
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = NetworkCallSingleton.getInstance(mContext).getRequestQ();
        requestQueue.add(stringRequest);
        // requestQueue.add(nw.MVolleycall(mContext));
        //     NetworkCallSingleton.getInstance().MVolleycall(mContext);
        // NetworkCallSingleton.getInstance().addToRequestQueue(stringRequest);
    }




    public void finalquery1oginFilter(Context mContext,final String accessToken,int final_Id,final VolleyCallback callback5){
        String final_score;

        // Log.d(accessToken,tableCode+"@@"+token);


//data=new ArrayList<String>();

        String data;

        String url = "https://greenlightplanet.looker.com:19999/api/3.0/queries/" + final_Id + "/run/json";
        //   String url = "http://13.126.146.126/Sunking_EB/Kazi_login.php"+"?name="+name+"&password="+password+"&mobileNumber="+Mnumber+"&country="+country;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        Log.d("Final data",s);


try{

   // data.add(s);
    callback5.onSuccess(s);

}catch (Exception e){
   // Toast.makeText(mContext,"Something went wrong!!"+e,Toast.LENGTH_LONG).show();Toast.makeText(mContext.this,"Something went wrong!!"+volleyError,Toast.LENGTH_LONG).show();
e.printStackTrace();
}
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        System.out.println("Something went wrong!");
                        volleyError.printStackTrace();

                    }
                }){


            @Override
            public Map<String, String> getHeaders()
            //   protected Map<String, String> getParams()throws com.android.volley.AuthFailureError
            {
                Map<String, String  >  headers = new HashMap<String, String>();
                headers.put("Authorization","Bearer " +accessToken);
                return headers;

            }

        }
                ;
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = NetworkCallSingleton.getInstance(mContext).getRequestQ();
        requestQueue.add(stringRequest);
        // requestQueue.add(nw.MVolleycall(mContext));
        //     NetworkCallSingleton.getInstance().MVolleycall(mContext);





    }


    public void QueryIDFilter(final Context mContext, final JSONObject queress,final VolleyCallback callback0) {
        String url = "https://greenlightplanet.looker.com:19999/api/3.0/login";
        //   String url = "http://13.126.146.126/Sunking_EB/Kazi_login.php"+"?name="+name+"&password="+password+"&mobileNumber="+Mnumber+"&country="+country;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        Log.d("Response",s);


                        try {
                            JSONObject js = new JSONObject(s);
                            String access_Token = js.getString("access_token");
                            String token_type = js.getString("token_type");
                            String expires_in = js.getString("expires_in");


                            Log.d("RequestResss",access_Token+","+token_type+","+expires_in);


                            if (!(js == null)) {


                                forthCallquery1Filter(mContext, access_Token, queress, new VolleyCallback() {
                                    @Override
                                    public void onSuccess(String result) {


                                        callback0.onSuccess(result);

                                    }
                                });
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        System.out.println("Something went wrong!");
                        volleyError.printStackTrace();
                    }
                }){
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("client_id", "QY6DQzQT6NQM3ngbTRxY");
                params.put("client_secret", "PMb9rtjBzrdYgyvqQhfhTjrB");
                return params;
            }
        }  ;
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = NetworkCallSingleton.getInstance(mContext).getRequestQ();
        requestQueue.add(stringRequest);
        // requestQueue.add(nw.MVolleycall(mContext));
        //     NetworkCallSingleton.getInstance().MVolleycall(mContext);
        // NetworkCallSingleton.getInstance().addToRequestQueue(stringRequest);
    }





    ArrayList<String> data;



}





