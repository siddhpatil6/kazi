package com.greenlightplanet;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.greenlightplanet.kazi.R;



/**
 * Created by Prateek on 6/4/2017.
 */
public class CustomerView extends Activity {


    private static final String TAG = CustomerView.class.getSimpleName();
    private String taskIDNumber,country_name;
    private Button customerViewBackButton;
    private TextView customerViewCustomerAccount;
    private TextView customerViewTaskType;
    private TextView customerViewCustomerName;
    private TextView customerViewCustomerPhone;
    private TextView customerViewCustomerLight;
    private TextView customerViewCustomerAmountPaid;
    private TextView customerViewCustomerLatestPayment;
    private TextView customerViewCustomerDaysPastDue;
    public TextView customerviewTxtCurrencyName;
    public TextView customerheadername;

    private String customerAccount;
    private String taskType;
    private String customerName;
    private String customerPhone;
    private String customerLight;
    private String customerAmountPaid;
    private String customerLatestPayment;
    private String customerDaysPastDue;
    private String currencytxt;

    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.customer_view);

        Intent taskID = getIntent();
        taskIDNumber = taskID.getStringExtra("task id");
        country_name= taskID.getStringExtra("Country_name");
        System.out.println(taskIDNumber+":"+country_name);

        customerViewBackButton = (Button) findViewById(R.id.customer_view_back_button);
        customerViewBackButton.setOnClickListener(customerViewBackButtonListener);

        customerviewTxtCurrencyName = (TextView) findViewById(R.id.customer_view_currency);
        customerViewCustomerAccount = (TextView) findViewById(R.id.customer_view_customer_account);
        customerViewTaskType = (TextView) findViewById(R.id.customer_view_task_type);
        customerViewCustomerName = (TextView) findViewById(R.id.customer_view_customer_name);
        customerViewCustomerPhone = (TextView) findViewById(R.id.customer_view_phone_number);
        customerViewCustomerLight = (TextView) findViewById(R.id.customer_view_light_name);
        customerViewCustomerAmountPaid = (TextView) findViewById(R.id.customer_view_amount_paid);
        customerViewCustomerLatestPayment = (TextView) findViewById(R.id.customer_view_latest_payment_date);
        customerViewCustomerDaysPastDue = (TextView) findViewById(R.id.customer_view_days_past_due);
        customerheadername = (TextView)findViewById(R.id.customer_view_header_text);

try {
    TaskList.fa.finish();
}catch (Exception e){
    e.printStackTrace();
}


        /*Settings.System.putInt(this.getContentResolver(),Settings.System.SCREEN_BRIGHTNESS, 70);

        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.screenBrightness =0.2f;// 100 / 100.0f;
        getWindow().setAttributes(lp);
*/


        TaskListDatabaseConnector taskListDatabaseConnector = new TaskListDatabaseConnector(getApplicationContext());
        taskListDatabaseConnector.open();
        Cursor customerData = taskListDatabaseConnector.getCustomerData(taskIDNumber);
        System.out.println(customerData);
        while (customerData.moveToNext()) {
            customerAccount = customerData.getString(customerData.getColumnIndex("account_number"));
            taskType = customerData.getString(customerData.getColumnIndex("ticket_type"));
            customerName = customerData.getString(customerData.getColumnIndex("owner_name"));
            customerPhone = customerData.getString(customerData.getColumnIndex("owner_msisdn"));
            customerLight = customerData.getString(customerData.getColumnIndex("product_name"));
            customerAmountPaid = customerData.getString(customerData.getColumnIndex("total_paid"));
            customerLatestPayment = customerData.getString(customerData.getColumnIndex("date_of_latest_payment_utc"));
            customerDaysPastDue = customerData.getString(customerData.getColumnIndex("days_past_due"));
        }
        taskListDatabaseConnector.close();


        if (customerName == null || customerName.isEmpty()) {
            customerName = getString(R.string.unknown_name);
        }

       /* if (customerLight.substring(0,3).equals("Pro")) {
            customerLight = "Pro";
        } else if (customerLight.substring(0,3).contains("Home ")){
            customerLight = "Home";
        }

*/

        if (customerLight.contains("Pro Easy Buy") || customerLight.contains("PRO Easy Buy")) {
            customerLight = "Pro";
        } else if (customerLight.contains("SHS 60 Easy Buy ")){
            customerLight = "SHS 60";
        }
        else if (customerLight.contains("Radio SHS 60 Easy Buy ")){
            customerLight = "SHS 60 Radio";
        }
        else if (customerLight.contains("Radio Stove SHS 60 Easy Buy  ")){
            customerLight = "SHS 60 Radio Stove ";
        }
        else if (customerLight.contains("Boom Easy Buy ")|| customerLight.contains("BOOM Easy Buy")){
            customerLight = "Boom";
        }
        else if (customerLight.contains("SHS 120 Easy Buy ")){
            customerLight = "SHS 120";
        }
        else if (customerLight.contains("Radio SHS 120 Easy Buy  ")){
            customerLight = "SHS 120 Radio";
        }
        else if (customerLight.contains("Radio Stove SHS 120 Easy Buy  ")){
            customerLight = "SHS 120 Radio Stove ";
        }
/*
        else if (customerLight.substring(0,3).contains("Radio Stove SHS 120 Easy Buy  ")){
            customerLight = "SHS 120 Radio Stove ";
        }
*/



        if (country_name.equalsIgnoreCase("Uganda")){
    currencytxt = "UGX";

}else if (country_name.equalsIgnoreCase("Kenya")){
    currencytxt = "KES";


}else if (country_name.equalsIgnoreCase("Nigeria")){
    currencytxt = "NGN";


}else if (country_name.equalsIgnoreCase("Myanmar (Burma)")){

    currencytxt = "MMK";

}else if (country_name.equalsIgnoreCase("Tanzania")){

    currencytxt = "TZS";
}else if (country_name.equalsIgnoreCase("India")){

            currencytxt = "INR";
        }


        customerDaysPastDue = customerDaysPastDue.replace("-","");

        customerViewCustomerAccount.setText(customerAccount);
        customerViewTaskType.setText(taskType);
        customerViewCustomerName.setText(customerName);
        customerViewCustomerPhone.setText(customerPhone);
        customerViewCustomerLight.setText(customerLight);
        customerviewTxtCurrencyName.setText(currencytxt);
        customerViewCustomerAmountPaid.setText(customerAmountPaid.toString());
        customerViewCustomerLatestPayment.setText(customerLatestPayment);
        customerViewCustomerDaysPastDue.setText(customerDaysPastDue.toString());
        customerheadername.setText(customerName);
    }

    private View.OnClickListener customerViewBackButtonListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            Intent openTaskList = new Intent (CustomerView.this, TaskList.class);
            startActivity(openTaskList);
            finish();
        }
    };
    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent openTaskList = new Intent (CustomerView.this, TaskList.class);
        startActivity(openTaskList);
        finish();



    }


}
