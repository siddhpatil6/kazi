package com.greenlightplanet;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.greenlightplanet.kazi.R;


public class SplashActivity extends Activity {
    ImageView imageView;
    Context mContext;


    private static int SPLASH_TIME_OUT = 3000;


    @Override
    public void onPause() {
        super.onPause();
        overridePendingTransition(0, 0);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
       // setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        mContext = SplashActivity.this;
        imageView = (ImageView) findViewById(R.id.logo2);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity

               /* if (bb.length() > 1 && cc.length() > 1 && aa.length() > 1) {
                    testhttp(sp.getString("username", ""), sp.getString("userpassword", ""), sp.getString("propertycode", ""));

                } else*/ {
                    Intent i = new Intent(mContext, LanguageSelection.class);
                    startActivity(i);
                    finish();
                }
            }
        }, SPLASH_TIME_OUT);

    }


 /*   private void testhttp(final String aa, final String bb, final String cc) {
        RestClient.GitApiInterface service = RestClient.getClient();

        Call<String> call = service.getdata(Constants.control + "/resavenueLogin?user=" + aa + "&pass=" + bb + "&regCode=" + cc);

        LoadingDialog.showLoadingDialog(SplashActivity.this, "Loading...");

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Response<String> response) {
                LoadingDialog.cancelLoading();

                try {

                    if (response.body().contains("invalid credentials")) {
                        Toast toast = Toast.makeText(SplashActivity.this,"Invalid Property code or username or password.\n\nPlease try Again...", Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();

                       // Toast.makeText(SplashActivity.this, "Invalid username or password.\n\n           Please try Again...", Toast.LENGTH_LONG).show();
                        Intent i = new Intent(mContext, LoginActivity.class);
                        startActivity(i);
                        finish();
                    } else {
                        SharedPreferences.Editor e = sp.edit();
                        e.putString("username", "");
                        e.putString("userpassword", "");
                        e.putString("propertycode", "");
                        e.commit();
                        SharedPreferences.Editor e1 = sp.edit();
                        e1.putString("username", aa);
                        e1.putString("userpassword", bb);
                        e1.putString("propertycode", cc);
                        Set<String> bodyClick = sp.getStringSet("click", new HashSet<String>());
                        bodyClick.add(aa);
                        e1.putStringSet("click", bodyClick);

                        e1.commit();
                        Intent i = new Intent(SplashActivity.this, MainActivity.class);
                        startActivity(i);
                        finish();
                    }
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();

                }

            }

            @Override
            public void onFailure(Throwable t) {
                LoadingDialog.cancelLoading();
                Toast.makeText(getApplicationContext(), t.toString(), Toast.LENGTH_LONG).show();
            }
        });
    }*/
}
