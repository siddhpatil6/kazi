package com.greenlightplanet;

import android.Manifest;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.TaskStackBuilder;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;

import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import com.google.gson.Gson;
import com.greenlightplanet.kazi.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import static android.content.ClipData.*;
import static com.greenlightplanet.kazi.R.id.nav_sum_task;
import static java.lang.String.valueOf;


/**
 * Created by Prateek on 6/2/2017.
 */
public class TaskList extends Activity
        implements NavigationView.OnNavigationItemSelectedListener {



    private SharedPreferences savedPreferences; // preferences object that stores the agent's name and the date of last update
    private String agentName; // used to filter the tasks specific to the agent from the Looker database
    private LinearLayout taskListHeaderBar; // changes color to pink if user has not refreshed the app in the past day
    private Button taskListRefreshButton, btn_logout;
    private DateFormat dateFormat; // formatter used for determining when the user has last refreshed the app
    private String dateLastUpdated; // date at which the user last refreshed the app
    private ListView taskListItem; // object that holds the list of the user's various tasks
    private String[] from = new String[]{"customerAccount", "taskID", "callComplete", "ticketType"}; //column headings for the listview to be matched with the relevant xml objects
    private int[] to = new int[]{R.id.task_list_customer_account, R.id.task_list_task_id, R.id.task_list_call_complete, R.id.task_list_task_type}; //xml items in the listview to match with the column headings
    private List<HashMap<String, String>> fillList; //object that holds data which fills the listview
    private ProgressDialog progressDialog; // progress dialog box for when the app is connecting to the internet
    private TextView noTasksTextBox, txt_agent_name; // textview that is displayed when the user has no tasks to complete and username too
    private DatePickerDialog datePicker; // dialog box which allows the agent to select the date the customer promised to pay
    private String activeTaskID; // id of the task being updated by the date picker
    public TextView txt_status , txtagentname;
    private String broadcastreciverlapstime = null;
    private String txt_country, OTP,check_new_incentive;
    private static final String TAG = "TaskList";
    private String intentpass = "Tasklist";
    private long daysDifferential = 1;
    public boolean refreshupdatecheck = false;
    NavigationView navigationView;
    Context mContext;
    HashMap<String, HashMap<String, String>> taskListData = new HashMap<String, HashMap<String, String>>();
    public String callCount = "";
    public String  call_count = "";
    public String firstcall = "";
    public String seccall = "";
    public String visitcall = "";
    public String repocall = "";
    public String taskCount = "task";
    public String callarea = "";
    public String agent_area_check,agentScores = null; // this is agent agentScore
    boolean previouslyStarted = false;
    SharedPreferences prefs;
    public static Activity fa;
    private NotificationManager myNotificationManager;
    private int notificationIdOne = 111;
    String[] PERMISSIONS = {Manifest.permission.ACCESS_NETWORK_STATE, Manifest.permission.CALL_PHONE, Manifest.permission.INTERNET, Manifest.permission.WRITE_EXTERNAL_STORAGE};
LinearLayout task_nw;
    public static final int PERMISSION_ALL = 1;
    public int id=111;
    private int numMessagesOne = 0;
    private Toolbar mToolbar;
    String latestVersion;
    Integer Latestversion,presentVersion = null;
    Button btn_back,dialogButton;



    DrawerLayout drawer;

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
  //  private GoogleApiClient client;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

       // Log.d("ONCreate", TAG);
         mContext = TaskList.this;


        savedPreferences = getSharedPreferences("preferences", MODE_PRIVATE); // loads the preferences of the user
        agentName = savedPreferences.getString("agent", "None"); // loads the user's agent name, if the user has not selected an agent name (this is not possible), None is passed
        txt_country = savedPreferences.getString("agentCountry", "None"); // loads user country name for the selection of zendesk completion of the task
        OTP = savedPreferences.getString("OTP","None"); // loads user for OTP registeration
        check_new_incentive = savedPreferences.getString("NewIncentive","None");
        agentScores = savedPreferences.getString("finalScore","None");
        agent_area_check = savedPreferences.getString("NewIncentive","None");
        //  taskListHeaderBar = (LinearLayout) findViewById(R.id.task_list_header_bar);



        Intent intent = getIntent();
        broadcastreciverlapstime = intent.getStringExtra("BroadCast");

        if (check_new_incentive.equals("Yes")){

            setContentView(R.layout.sum_tasks_list);
        }else{

            setContentView(R.layout.task_list);

            navigationView = (NavigationView) findViewById(R.id.nav_view);
            navigationView.setNavigationItemSelectedListener(TaskList.this);

        /*    View header=navigationView.getHeaderView(0);
            txtagentname = (TextView) header.findViewById(R.id.txt_agent_name);*/
        }


        // creates the progress dialog item for use by the activity but does NOT display it
        progressDialog = new ProgressDialog(TaskList.this);
        progressDialog.setTitle(getString(R.string.progress_bar_title));
        progressDialog.setMessage(getString(R.string.progress_bar_message));
        progressDialog.setCancelable(false); // user cannot cancel the progress dialog
        progressDialog.setIndeterminate(true); // sets the progress dialog to a spinning cirlce




        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        // setSupportActionBar(toolbar);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        //TextView tv_title = (TextView) findViewById(R.id.toolbar_title);
       task_nw = (LinearLayout) findViewById(R.id.task_nw);
        btn_back = (Button) findViewById(R.id.task_view_back_button);
         dialogButton = (Button) findViewById(R.id.btnSubmit);




        if (check_new_incentive.equalsIgnoreCase("No")) {
            TextView tv_title = (TextView) findViewById(R.id.toolbar_title);
            drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

             tv_title.setText("Tasks");

            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

                @Override
                public void onDrawerOpened(View drawerView) {
                    super.onDrawerOpened(drawerView);
                }

                @Override
                public void onDrawerClosed(View drawerView) {
                    super.onDrawerClosed(drawerView);
                }
            }; // Drawer Toggle Object Made
            drawer.setDrawerListener(toggle);
            toggle.syncState();


            toolbar.setNavigationOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (drawer.isDrawerOpen(Gravity.LEFT)) {
                        drawer.closeDrawer(Gravity.LEFT);
                    } else {
                        drawer.openDrawer(Gravity.LEFT);
                    }
                }
            });
        }

        if (check_new_incentive.equals("Yes")){

           // toolbar.setVisibility(View.INVISIBLE);
            task_nw.setVisibility(View.VISIBLE);
            btn_back.setVisibility(View.VISIBLE);
        }
        else{
           /* toolbar.setVisibility(View.VISIBLE);
            task_nw.setVisibility(View.INVISIBLE);
            btn_back.setVisibility(View.INVISIBLE);*/

        }


     //   DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
       /* ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();*/






        savedPreferences = getSharedPreferences("preferences", MODE_PRIVATE); // loads the preferences of the user
        agentName = savedPreferences.getString("agent", "None"); // loads the user's agent name, if the user has not selected an agent name (this is not possible), None is passed
        txt_country = savedPreferences.getString("agentCountry", "None"); // loads user country name for the selection of zendesk completion of the task
        OTP = savedPreferences.getString("OTP","None"); // loads user for OTP registeration
        //  taskListHeaderBar = (LinearLayout) findViewById(R.id.task_list_header_bar);

        // taskListRefreshButton = (Button) findViewById(R.id.task_list_refresh_button);
        // taskListRefreshButton.setOnClickListener(taskListRefreshButtonListener);
        // btn_logout = (Button) findViewById(R.id.btn_logout);
        txt_status = (TextView) findViewById(R.id.task_list_header_text);
        txt_agent_name = (TextView) findViewById(R.id.agent_name);
        noTasksTextBox = (TextView) findViewById(R.id.no_tasks_to_complete);
       // btn_back = (Button) findViewById(R.id.task_view_back_button);

        //   btn_logout.setOnClickListener(btnlogout);
        taskListItem = (ListView) findViewById(R.id.task_list_items);
       // NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);


      //  NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        try {




/*
    NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
    navigationView.setNavigationItemSelectedListener(this);
    View header=navigationView.getHeaderView(0);
*//*View view=navigationView.inflateHeaderView(R.layout.nav_header_main);*//*
    name = (TextView)header.findViewById(R.id.username);
    email = (TextView)header.findViewById(R.id.email);
    name.setText(personName);
    email.setText(personEmail);*/

/*
    NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_view);
    View headerView = LayoutInflater.from(this).inflate(R.layout.header, navigationView, false);
    navigationView.addHeaderView(headerView);
    TextView Name = (TextView) headerView.findViewById(R.id.name);
    Title.setText("Myname");
    TextView Number = (TextView) headerView.findViewById(R.id.number);
    Email.setText("+1234567890")


        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_view);
            View headerView = LayoutInflater.from(this).inflate(R.layout.header, navigationView, false);
            navigationView.addHeaderView(headerView);
            TextView Name = (TextView) headerView.findViewById(R.id.name);
            Title.setText("Myname");
            TextView Number = (TextView) headerView.findViewById(R.id.number);
            Email.setText("+1234567890");


    ;*/



         //   navigationView = (NavigationView) findViewById(R.id.nav_view);
//    View headerView = LayoutInflater.from(this).inflate(R.layout.nav_header_main, navigationView, false);
          //  navigationView.setNavigationItemSelectedListener(TaskList.this);
            //  navigationView.addHeaderView(headerView);
            View header=navigationView.getHeaderView(0);
            txtagentname = (TextView) header.findViewById(R.id.txt_agent_name);


            txtagentname.setText(agentName.toString());

        }catch (Exception e){
            e.printStackTrace();
     LoadingDialog.cancelLoading();
     Toast.makeText(mContext,"Something went wrong!!",Toast.LENGTH_SHORT).show();
        }



        // checking for the OTP if he is not registered with his mobile
        //if (!"success".equals(statusCheck))

        // OTP = "None"; //testing for non register eo's
        if (!"Done".equals(OTP)){

            SharedPreferences.Editor editor = savedPreferences.edit();
            editor.clear();
            editor.commit();


            Intent i = new Intent(TaskList.this, AgentSelection.class);
            startActivity(i);
            finish();


        }


     /*   View navHeaderView = navigationView.inflateHeaderView(R.layout.nav_header_main);
        txtagentname= (TextView) navHeaderView.findViewById(R.id.txt_agent_name);
        txtagentname.setText(agentName.toString());*/

        dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        dateLastUpdated = savedPreferences.getString("last update", "None"); // loads the date the app was last updated, if the user has never updated the app, None is passed
        // dateLastUpdated = savedPreferences.getString("last update", "1");

       /* Intent intent = getIntent();
        intentpass = intent.getStringExtra("BroadCast");


*/


        fa = this;


        try {
            if (!hasPermissions(this, PERMISSIONS)) {
                ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
            }
        } catch (Exception e) {
            createErrorDialogBox("Technical Error...");
        }



        // checks if the user needs to refresh the app (the defined time threshold has been crossed), and sets the header bar color accordingly
        if (refreshNeeded(dateLastUpdated)) {
//            taskListHeaderBar.setBackgroundColor(getResources().getColor(R.color.colorAccent)); //
            txt_agent_name.setText("Hi :" + agentName);
            noTasksTextBox.setText(R.string.dialog_updating_task);
            txt_status.setText(R.string.dialog_task_update);
//            txtagentname.setText(agentName);


            //  Toast.makeText(TaskList.this,"DateLastDate"+dateLastUpdated+"::"+daysDifferential,Toast.LENGTH_SHORT).show();  Toast.makeText(TaskList.this,"DateLastDate"+dateLastUpdated+"::"+daysDifferential,Toast.LENGTH_SHORT).show();

            //FoRfresh();
        } else {

            //  noTasksTextBox.setText("Please Wait... "+"\r\n "+ " Kindly refresh your task");
            // taskListHeaderBar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        }

        NotificationManager myNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // remove the notification with the specific id
        myNotificationManager.cancel(id);

if (broadcastreciverlapstime !=null) {

    if (agent_area_check.equals("Yes")) {

        if (broadcastreciverlapstime.equalsIgnoreCase("BroadcastUpdate")) {

         //   displayNotificationOne(agentScores);

        }
    }
}
        // Log.d("IntentPass",intentpass);

      /*  if (intentpass != null) {

            if (intentpass.equalsIgnoreCase("BroadcastUpdate")) {
           //     displayNotificationOne();
                if (new GetData().isNetworkAvailable(TaskList.this)) {
             //       displayNotificationOne();
                    refreshtasklist();

                }

            } else {
                new UpdateView().execute();
            }

        }
*/

/*
        mToolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        drawerFragment = (FragmentDrawer)
                getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), mToolbar);
        drawerFragment.setDrawerListener(TaskList.this);

*/

        prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        final boolean previouslyStarted = prefs.getBoolean(getString(R.string.pref_previously_started), true);
        if (previouslyStarted) {
            SharedPreferences.Editor edit = prefs.edit();
            edit.putBoolean(getString(R.string.pref_previously_started), Boolean.TRUE);
            edit.commit();
            //Toast.makeText(TaskList.this,"Welcome to Kazi",Toast.LENGTH_SHORT).show();
            WelcomeDilaog();


        }

       /* String[] PERMISSIONS = {Manifest.permission.ACCESS_NETWORK_STATE, Manifest.permission.CALL_PHONE, Manifest.permission.INTERNET, Manifest.permission.READ_CALL_LOG, Manifest.permission.WRITE_EXTERNAL_STORAGE};

        try {
            if (!hasPermissions(this, PERMISSIONS)) {
                ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
            }
        } catch (Exception e) {
            createErrorDialogBox("Technical Error...");
        }*/


        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
       // client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();

        //displayNotificationOne(callCount);

      //  if (new GetData().isNetworkAvailable(TaskList.this)) {
        if (Connectivity.isConnected(mContext)) {
            PackageInfo pInfo = null;
            try {
                pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }


            AlertDialog alertDialog = new AlertDialog.Builder(TaskList.this).create();

            String version = pInfo.versionName;

            presentVersion = Integer.parseInt(version.toString());


            //VersionChecker versionChecker = new VersionChecker(TaskList.this);
            // latestVersion = versionChecker.execute().get();

            progressDialog.show();
            String url = "http://13.126.146.126/Sunking_EB/versionCheck.php";

            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,

                    new com.android.volley.Response.Listener<String>() {
                        @Override
                        public void onResponse(String respVersion) {
                            progressDialog.hide();

                        //    Log.d("Response Version", respVersion.toString().replace("\\", "").substring(1));

                            try {

                                JSONObject jsonObject = new JSONObject(respVersion.toString().replace("\\", "").substring(1));

                              //  Log.d("Versionnn", jsonObject.getString("ver"));
                                latestVersion = jsonObject.getString("ver");

                            //    Log.d("LatestVersionnn", latestVersion);

                                Latestversion = Integer.parseInt(latestVersion.toString());

                                if (Latestversion > presentVersion) {

                                    UpdateNewVersion(presentVersion, Latestversion);


                                }


                            } catch (Exception e) {
                                e.printStackTrace();
                            //    Log.d("Errror", e.toString());


                            }
                          //  Log.d("LatestVersionnn!!@@@", Latestversion.toString());


                        }

                    },

                    new com.android.volley.Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {

                            System.out.println("Something went wrong!");
                            volleyError.printStackTrace();
                            progressDialog.dismiss();


                        }
                    });

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);


        }


        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(TaskList.this,Summary.class);
                startActivity(i);
                finish();
            }
        });

    }





    public void UpdateNewVersion(int preVersion, int LatestVersion){



            if (LatestVersion > preVersion) {
                ///////////////Update Dialog//////////////


                //oh yeah we do need an upgrade, let the user know send an alert message
                final AlertDialog.Builder builder = new AlertDialog.Builder(TaskList.this);

                builder.setCancelable(false);
//                        builder.setMessage("There is newer version of this application available, click OK to upgrade now?")
                builder.setMessage("Kazi app just got SMARTER. A new version is available.")
                        .setPositiveButton("Download Now", new DialogInterface.OnClickListener() {
                            //if the user agrees to upgrade
                            public void onClick(DialogInterface dialog, int id) {
                                // new IMEI_Check_Task(ERecharge_submenu.this, IMEI_Handler).execute(sharedPrefs.getString("MDN", null), tm.getDeviceId(), "S");
                                try {
                                    Intent viewIntent =
                                            new Intent("android.intent.action.VIEW",
                                                    Uri.parse("https://play.google.com/store/apps/details?id=com.greenlightplanet.kazi&hl=en"));
                                    startActivity(viewIntent);
                                } catch (Exception e) {
                                    Toast.makeText(getApplicationContext(), "Unable to Connect Try Again...",
                                            Toast.LENGTH_LONG).show();
                                    e.printStackTrace();
                                }
                            }
                        })
                        .setNegativeButton("Remind Later", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                            }
                        });
                //show the alert message
                builder.create().show();
            }

}



    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        getMenuInflater().inflate(R.menu.main, menu);

  /*      if (check_new_incentive.equalsIgnoreCase("NO")) {

            menu.findItem (R.id.nav_sum_task).getSubMenu ().setGroupVisible (R.id.nav_sum_task, false);
        }*/


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {



        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_task) {
            // Toast.makeText(TaskList.this,"TASK ",Toast.LENGTH_SHORT).show();
            TaskCountDialog();
            // Handle the camera action
        } else if (id == R.id.nav_incentive) {
            // Toast.makeText(TaskList.this,"nav_incentive ",Toast.LENGTH_SHORT).show();

            if (new GetData().isNetworkAvailable(TaskList.this)) {
                Intent i = new Intent(TaskList.this, Incentives.class);
                // i.putExtra("Agentname",agentName);
                startActivity(i);
                this.finish();
            }else {
                createErrorDialogBoxInternetConnection(getString(R.string.no_internet_connection));
            }

        } else if (id == R.id.nav_training) {
            //  Toast.makeText(TaskList.this,"nav_training ",Toast.LENGTH_SHORT).show();

            TaskListDatabaseConnector taskListDatabaseConnector = new TaskListDatabaseConnector(TaskList.this);
            taskListDatabaseConnector.open();
            Cursor call = taskListDatabaseConnector.getarea();


            while (call.moveToNext()) {
                callarea = call.getString(0);
            }
            taskListDatabaseConnector.close();

            if (new GetData().isNetworkAvailable(TaskList.this)) {
                Intent i = new Intent(TaskList.this,attendance_module.class);


                // i.putExtra("current_area",);
                //  i.putExtra("current_area",current_area);

                i.putExtra("current_area",callarea);
                i.putExtra("country",txt_country);
                startActivity(i);
                this.finish();
            }else {
                createErrorDialogBoxInternetConnection(getString(R.string.no_internet_connection));
            }


        } else if (id == R.id.nav_lagout) {
            //Toast.makeText(TaskList.this,"nav_lagout ",Toast.LENGTH_SHORT).show();
            DiallogLogout();
        }else if (id == R.id.nav_faq){
            // Toast.makeText(TaskList.this,"nav_faq ",Toast.LENGTH_SHORT).show();
            Intent i = new Intent(TaskList.this,Faq.class);
            startActivity(i);
            this.finish();
        }else if (id == R.id.nav_update){
            //Toast.makeText(TaskList.this,"Update is on Progress ",Toast.LENGTH_SHORT).show();
            Update();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }


    public void displayNotificationOne(String score) {



      //  Log.d("Notification Agentscore",score);

      /*  TaskListDatabaseConnector taskListDatabaseConnector = new TaskListDatabaseConnector(TaskList.this);
        taskListDatabaseConnector.open();
        Cursor taskcount = taskListDatabaseConnector.getCallCount();

        while (taskcount.moveToNext()) {
           taskCount = taskcount.getString(0);
        }
        taskListDatabaseConnector.close();*/


        // Invoking the default notification service
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);


        mBuilder.setContentTitle(" Kazi ");/*
        mBuilder.setContentText(R.string.dilaog_you_have + taskCount + R.string.dilaog_task+R.string.dialog_complete_it);
        mBuilder.setTicker("Hi "+agentName+","+R.string.notification_help);*/
        mBuilder.setContentText("Yesterday's score was 62. Week to date score is 56");
        mBuilder.setTicker("Hi "+agentName+","+R.string.notification_help);
        mBuilder.setSmallIcon(R.mipmap.ic_launcher);

        // Increase notification number every time a new notification arrives
        mBuilder.setNumber(++numMessagesOne);

        // Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(this, TaskList.class);
        resultIntent.putExtra("notificationId", notificationIdOne);

        //This ensures that navigating backward from the Activity leads out of the app to Home page
        TaskStackBuilder stackBuilder = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            stackBuilder = TaskStackBuilder.create(this);
        }
        // Adds the back stack for the Intent
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            stackBuilder.addParentStack(TaskList.class);
        }

        // Adds the Intent that starts the Activity to the top of the stack
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            stackBuilder.addNextIntent(resultIntent);
        }
        PendingIntent resultPendingIntent = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
            resultPendingIntent = stackBuilder.getPendingIntent(
                    0,
                    PendingIntent.FLAG_CANCEL_CURRENT //can only be used once
            );
        }
        // start the activity when the user clicks the notification text
        mBuilder.setContentIntent(resultPendingIntent);

        myNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // pass the Notification object to the system
        myNotificationManager.notify(notificationIdOne, mBuilder.build());



    }


    protected void onResume() {
        super.onResume();


        // refreshtasklist(); // resfreshing the list as well as showing the list f
        if (refreshNeeded(dateLastUpdated)) {
            //   taskListHeaderBar.setBackgroundColor(getResources().getColor(R.color.colorAccent)); //
            //Toast.makeText(TaskList.this,"DateLastDateOnresume"+dateLastUpdated+"::"+daysDifferential,Toast.LENGTH_SHORT).show();
            //FoRfresh();
        } else {

            // taskListHeaderBar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            //  Toast.makeText(TaskList.this,"DaRESUMEEEE"+dateLastUpdated+"::"+daysDifferential,Toast.LENGTH_SHORT).show();
        }
        if (daysDifferential >= 1) {
            if (refreshupdatecheck) {

            } else {

                FoRfresh();
               /* if (new GetData().isNetworkAvailable(TaskList.this)) {
                    FoRfresh();
                    //    displayNotificationOne();
                }*//* else {
                    createErrorDialogBox(getString(R.string.check_refreshtask));
                    new UpdateView().execute();
              // createErrorInternet();
                }*/
            }
            // DiallogLogout();
        } else {
            new UpdateView().execute(); // populates the task list
         //   Log.d("OnREsume", TAG);
        }


    }


    public void refreshtasklist() {


      //  Log.d("TaskList", "RefreshlistFromService");

        //Toast.makeText(TaskList.this, "Refresshlist" + intentpass, Toast.LENGTH_LONG).show();

        if (new GetData().isNetworkAvailable(TaskList.this)) { //check for internet connection
           // new GetTaskData().execute(); // download looker data and repopulate task list



             GetTaskDatacall();








            // update the date that the task list was last refreshed to the current date
            SharedPreferences.Editor preferencesEditor = savedPreferences.edit();
            preferencesEditor.putString("last update", dateFormat.format(Calendar.getInstance().getTime()));
            preferencesEditor.apply();

            //    taskListHeaderBar.setBackgroundColor(getResources().getColor(R.color.colorPrimary)); //change the color of the task bar to indicate the task list has been refreshed
        } else {
            createErrorDialogBox(getString(R.string.no_internet_connection)); // inform the user that there is no internet connection, and so the update cannot occur
            //  new UpdateView().execute();
        }
    }

    public void GetTaskDatacall() {




        if (txt_country.equalsIgnoreCase("Kenya")){

            LoadingDialog.showLoadingDialog(mContext,"Loading....");
            String tableCode = "1880";

         //   Log.d("agentNAme",agentName);
            new GetData().firstCallqueryFilter(mContext, tableCode, agentName, new VolleyCallback() {
                @Override
                public void onSuccess(String result) {

                    //Toast.makeText(TaskList.this,result.toString(),Toast.LENGTH_SHORT).show();

                //    Log.d("TASK Data",result);
                    InseringTaskData(result);

                    new UpdateView().execute();

                    LoadingDialog.cancelLoading();

                }
            });



            //new GetData().inputDataIntoDatabase(TableCode,agentName,getApplicationContext());
        }else if (txt_country.equalsIgnoreCase("Nigeria")){

            // new GetData().inputDataIntoDatabase(TableCode,agentName,getApplicationContext()); // task

            LoadingDialog.showLoadingDialog(mContext,"Loading....");
            String tableCode = "1883";

            new GetData().firstCallqueryFilter(mContext, tableCode, agentName, new VolleyCallback() {
                @Override
                public void onSuccess(String result) {

                    {

                     //   Toast.makeText(TaskList.this,result.toString(),Toast.LENGTH_SHORT).show();

                 //       Log.d("TASK Data",result);




                        InseringTaskData(result);
                        new UpdateView().execute();

                        LoadingDialog.cancelLoading();

                    }

                }
            });




        }else if (txt_country.equalsIgnoreCase("Myanmar (Burma)")){

            //new GetData().inputDataIntoDatabase(TableCode,agentName,getApplicationContext()); //  task

            LoadingDialog.showLoadingDialog(mContext,"Loading....");
            String tableCode = "1884";

            new GetData().firstCallqueryFilter(mContext, tableCode, agentName, new VolleyCallback() {
                @Override
                public void onSuccess(String result) {

                  //  Toast.makeText(TaskList.this,result.toString(),Toast.LENGTH_SHORT).show();





                    InseringTaskData(result);

                    new UpdateView().execute();
                    LoadingDialog.cancelLoading();

                }
            });





        }else if (txt_country.equalsIgnoreCase("Uganda")){
            //   TableCode ="1882";
            //new GetData().inputDataIntoDatabase(TableCode,agentName,getApplicationContext()); // taz task



            LoadingDialog.showLoadingDialog(mContext,"Loading....");
            String tableCode = "1882";



            new GetData().firstCallqueryFilter(mContext, tableCode, agentName, new VolleyCallback() {
                @Override
                public void onSuccess(String result) {

                  //  Toast.makeText(TaskList.this,result.toString(),Toast.LENGTH_SHORT).show();





                    InseringTaskData(result);



                    LoadingDialog.cancelLoading();

                    new UpdateView().execute();

                }
            });







        }else if (txt_country.equalsIgnoreCase("Tanzania")){
            //TableCode = "1881";
            // new GetData().inputDataIntoDatabase(TableCode,agentName,getApplicationContext()); // taz task


            LoadingDialog.showLoadingDialog(mContext,"Loading....");
            String tableCode = "1881";


            new GetData().firstCallqueryFilter(mContext, tableCode, agentName, new VolleyCallback() {
                @Override
                public void onSuccess(String result) {

                  //  Toast.makeText(TaskList.this,result.toString(),Toast.LENGTH_SHORT).show();





                    InseringTaskData(result);

                    LoadingDialog.cancelLoading();
                    new UpdateView().execute();
                }
            });



        } else if (txt_country.equalsIgnoreCase("India")){

            //new GetData().inputDataIntoDatabase(TableCode,agentName,getApplicationContext());



            LoadingDialog.showLoadingDialog(mContext,"Loading....");
            String tableCode = "1885";


            new GetData().firstCallqueryFilter(mContext, tableCode, agentName, new VolleyCallback() {
                @Override
                public void onSuccess(String result) {

                 //   Toast.makeText(TaskList.this,result.toString(),Toast.LENGTH_SHORT).show();





                    InseringTaskData(result);


                    LoadingDialog.cancelLoading();
                    new UpdateView().execute();
                }
            });





        }


    }


    private void InseringTaskData(String result) {

        HashMap<String, HashMap<String, String>> taskListData = new HashMap<String, HashMap<String, String>>();




              try {


            JSONArray rawTaskListData = new JSONArray(result);

            int id_count = 0;

            int id_type_count_1stcall = 0;
            int id_type_count_2ndcall = 0;
            int id_type_count_visit = 0;
            int id_type_count_repo = 0;
            String task = "";

            for (int i = 0; i < rawTaskListData.length(); i++) {

                task = String.valueOf(rawTaskListData.length());


                //   Log.d("Input Data :", taskListData.toString());
                JSONObject rawTaskData = (JSONObject) rawTaskListData.get(i);
                if ((rawTaskData.get("accounts.agent").toString()).equals(agentName)) {
                    HashMap<String, String> taskData = new HashMap<String, String>();
                    taskData.put("owner_name", rawTaskData.get("accounts.owner_name").toString());
                    taskData.put("owner_msisdn", rawTaskData.get("accounts.owner_msisdn").toString());
                    taskData.put("total_paid", rawTaskData.get("accounts.total_paid").toString());
                    taskData.put("product_name", rawTaskData.get("accounts.group_name").toString());
                    taskData.put("days_past_due", rawTaskData.get("accounts.days_disabled").toString());
                    taskData.put("date_of_latest_payment_utc", rawTaskData.get("accounts.date_of_latest_payment_utc_date").toString());
                    taskData.put("account_number", rawTaskData.get("accounts.account_number").toString());
                    id_count = id_count + 1;
                    taskData.put("ticket_id", rawTaskData.get("tickets_all_countries.id").toString());

                    taskData.put("ticket_type", rawTaskData.get("tickets_all_countries.eo_ticket").toString());

                    if (rawTaskData.get("tickets_all_countries.eo_ticket").toString().equalsIgnoreCase("1st Call")) {
                        id_type_count_1stcall = id_type_count_1stcall + 1;

                    }
                    if (rawTaskData.get("tickets_all_countries.eo_ticket").toString().equalsIgnoreCase("2nd Call")) {
                        id_type_count_2ndcall = id_type_count_2ndcall + 1;

                    }
                    if (rawTaskData.get("tickets_all_countries.eo_ticket").toString().equalsIgnoreCase("Visit")) {
                        id_type_count_visit = id_type_count_visit + 1;


                    }
                    if ((rawTaskData.get("tickets_all_countries.eo_ticket").toString().equalsIgnoreCase("Repo"))) {
                        id_type_count_repo = id_type_count_repo + 1;

                    }


                    taskData.put("requester_id", rawTaskData.get("tickets_all_countries.requester_id").toString());
                    // taskData.put("request_country_status",rawTaskData.get("tickets_all_countries.status").toString());
                    taskListData.put(Integer.toString(i), taskData);



                    int  lastInteger = i;
                }

            }

            String CallCount = String.valueOf(id_count);
            String id_1stcall = String.valueOf(id_type_count_1stcall);
            String id_2ndcall = String.valueOf(id_type_count_2ndcall);
            String id_visit = String.valueOf(id_type_count_visit);
            String id_repo = String.valueOf(id_type_count_repo);

            //  Log.d("TAsk************", CallCount + ":1st" + id_1stcall + ":2nd" + id_2ndcall + "visit:" + id_visit + "repo:" + id_repo +"TASSSSSS"+task);

            CallCount = CallCount + "," + id_1stcall + "," + id_2ndcall + "," + id_visit + "," + id_repo;


            TaskListDatabaseConnector taskListDatabaseConnector = new TaskListDatabaseConnector(mContext);
            taskListDatabaseConnector.insertTaskList(taskListData, CallCount); // connecting with the task list data from the Selection




               /* String CallCount = String.valueOf(id_count);
                String id_1stcall = String.valueOf(id_type_count_1stcall);
                String id_2ndcall = String.valueOf(id_type_count_2ndcall);
                String id_visit = String.valueOf(id_type_count_visit);
                String id_repo = String.valueOf(id_type_count_repo);

                //  Log.d("TAsk************", CallCount + ":1st" + id_1stcall + ":2nd" + id_2ndcall + "visit:" + id_visit + "repo:" + id_repo +"TASSSSSS"+task);

                CallCount = CallCount + "," + id_1stcall + "," + id_2ndcall + "," + id_visit + "," + id_repo;


                TaskListDatabaseConnector taskListDatabaseConnector = new TaskListDatabaseConnector(mContext);
                taskListDatabaseConnector.insertTaskList(taskListData, CallCount); // connecting with the task list data from the Selection
*/


            return;




        } catch (JSONException ex){


        } catch (Exception e){

        }
    }

/*
    public String getCallCount() {
        return callCount;
    }

    public void setCallCount(String callCount) {
        this.callCount = callCount;
    }*/

    // When the user clicks the refresh button, if a valid internet connection exists, the task data is downloaded from looker and the task list repopulated
   /* private View.OnClickListener taskListRefreshButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (new GetData().isNetworkAvailable(TaskList.this)) { //check for internet connection
                new GetTaskData().execute(); // download looker data and repopulate task list

                // update the date that the task list was last refreshed to the current date
                SharedPreferences.Editor preferencesEditor = savedPreferences.edit();
                preferencesEditor.putString("last update", dateFormat.format(Calendar.getInstance().getTime()));
                preferencesEditor.apply();

                taskListHeaderBar.setBackgroundColor(getResources().getColor(R.color.colorPrimary)); //change the color of the task bar to indicate the task list has been refreshed
            } else {
                createErrorDialogBox(getString(R.string.no_internet_connection)); // inform the user that there is no internet connection, and so the update cannot occur
            }
        }
    };
*/
    private View.OnClickListener btnlogout = new View.OnClickListener() {
        @Override
        public void onClick(View v) {


            DiallogLogout();


        }
    };

    // creates a dialog box to inform the user that an error has occured
    // @param message: error message displayed to the user
    private void createErrorDialogBox(String message) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(TaskList.this);
        alertDialogBuilder.setTitle(getString(R.string.error_dialog_box_header));
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setPositiveButton(getString(R.string.acknowledgement_button), null); // user can only click ok on the error dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();
    }


    // creates a dialog box to inform the user that an error has occured
    // @param message: error message displayed to the user
    private void createErrorInternet() {
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(TaskList.this);
        alertDialogBuilder.setIcon(R.drawable.alert_yellow);
        alertDialogBuilder.setTitle(R.string.dilaog_refresh);
        alertDialogBuilder.setMessage(R.string.dilag_make_sure);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton(" OK ", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {

                new UpdateView().execute();


            }
        });
   /*     alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel(); // dialog box disappears if user does not confirm task is complete
            }
        });*/
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }






    // creates a welcome dialog for our broadcast fot next day at 6am

    public void WelcomeDilaog() {


        //Log.d("InDialofWe;","Welcome"+callCount);
        try {


            TaskListDatabaseConnector taskListDatabaseConnector = new TaskListDatabaseConnector(TaskList.this);
            taskListDatabaseConnector.open();
            Cursor callData = taskListDatabaseConnector.getCallCount();

            while (callData.moveToNext()) {
                callCount = callData.getString(0);
            }
            taskListDatabaseConnector.close();
            //new TaskListDatabaseConnector(TaskList.this).getCallCount(callCount);
            //  new TaskListDatabaseConnector(TaskList.this).Call(callCount);
            //new GetData().SavingCount(callCount);
       /* if (callCount.equals(null)){
            callCount = "0";
        }*/
        } catch (Exception e) {
            e.printStackTrace();
        }

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(TaskList.this);
        alertDialogBuilder.setIcon(R.drawable.ic_assignment_turned_in_white_48dp);
        alertDialogBuilder.setTitle(R.string.dialog_welcome);
        // alertDialogBuilder.setMessage("Hey :" + agentName + "\r\n" + "You have " + callCount + " Task ,Please complete your task as soon as possible ");
        alertDialogBuilder.setMessage("Hey :" + agentName + "\r\n" + TaskList.this.getString(R.string.dilaog_compelte_your_task));
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {


                // alling the broadcaste from this method
                autoRefresh();
                SharedPreferences.Editor edit = prefs.edit();
                edit.putBoolean(getString(R.string.pref_previously_started), Boolean.FALSE);
                edit.commit();
              //  Log.d("Auoto Refresh Start", "Auto Fers");
                //Toast.makeText(TaskList.this, "Welcome CallBroadcast Start From here", Toast.LENGTH_SHORT).show();
                //   refreshtasklist();

            }
        });
   /*     alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel(); // dialog box disappears if user does not confirm task is complete
            }
        });*/
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


    // creates a dialog box that asks the user to confirm a task is complete
    // @param taskID: id of the task the user has marked complete
    public void createConfirmTaskCompleteDialogBox(final String taskID) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(TaskList.this);
        alertDialogBuilder.setTitle(getString(R.string.complete_tasks_dialog_box_header));
        alertDialogBuilder.setMessage(getString(R.string.complete_tasks_confirmation_message));
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                new SetTaskComplete().execute(taskID); // if the user confirms that the task is complete, it is marked complete in the database
            }
        });
        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel(); // dialog box disappears if user does not confirm task is complete
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    ;


    public void autoRefresh() {

        //  int i = Integer.parseInt(text.getText().toString());

        Intent intent = new Intent(this, MyBroadcastReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(
                this.getApplicationContext(), 234324243, intent, 0);


        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        Calendar firingCal = Calendar.getInstance();
        Calendar currentCal = Calendar.getInstance();
        // firingCal.set(Calendar.HOUR_OF_DAY, 06);
        firingCal.set(Calendar.HOUR, 6); // At the hour you wanna fire
        firingCal.set(Calendar.MINUTE, 0); // Particular minute
        firingCal.set(Calendar.SECOND, 0); // particular second
        firingCal.set(Calendar.AM_PM, Calendar.AM);

        long intendedTime = firingCal.getTimeInMillis();


        long currentTime = currentCal.getTimeInMillis();

        if (intendedTime >= currentTime) {
            // you can add buffer time too here to ignore some small differences in milliseconds
            // set from today
            alarmManager.setRepeating(AlarmManager.RTC, intendedTime, AlarmManager.INTERVAL_DAY, pendingIntent);
        } else {
            // set from next day
            // you might consider using calendar.add() for adding one day to the current day
            firingCal.add(Calendar.DAY_OF_MONTH, 1);
            intendedTime = firingCal.getTimeInMillis();

            alarmManager.setRepeating(AlarmManager.RTC, intendedTime, AlarmManager.INTERVAL_DAY, pendingIntent);
        }







     /*   Intent intent = new Intent(this, MyBroadcastReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(
                this.getApplicationContext(), 234324243, intent, 0);


        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
		*//*alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis()
                + (i * 1000), (i * 1000), pendingIntent);*//*
        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis()
                + (intendedTime * 1000), 8000, pendingIntent);*/

/*
        Toast.makeText(this, "Starting alarm in " + intendedTime + " seconds",
                Toast.LENGTH_LONG).show();*/
    }

    public void DiallogLogout() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(TaskList.this);
        alertDialogBuilder.setIcon(R.drawable.alert_yellow);
        alertDialogBuilder.setTitle(R.string.dilaog_logout);
        alertDialogBuilder.setMessage(R.string.dilaog_logout_msg);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                SharedPreferences.Editor editor = savedPreferences.edit();
                editor.clear();
                editor.commit();


                Intent i = new Intent(TaskList.this, AgentSelection.class);
                i.putExtra("Login","Languagecheck");
                startActivity(i);
               // finish();

            }
        });
        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel(); // dialog box disappears if user does not confirm task is complete
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    };



    public void Update() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(TaskList.this);
        alertDialogBuilder.setIcon(R.drawable.alert_yellow);
        alertDialogBuilder.setTitle(" Update ");
        alertDialogBuilder.setMessage("Please check on google store for new update  ");
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                if (new GetData().isNetworkAvailable(TaskList.this)) {


                    final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                    }
                }else {
                    createErrorDialogBoxInternetConnection(getString(R.string.no_internet_connection));

                }
            }
        });
        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel(); // dialog box disappears if user does not confirm task is complete
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void createErrorDialogBoxInternetConnection(String message) {
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(TaskList.this);
        alertDialogBuilder.setTitle(getString(R.string.error_dialog_box_header));
        alertDialogBuilder.setMessage(message);


        alertDialogBuilder.setPositiveButton(" OK ", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {

              dialog.dismiss();

            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setCancelable(false);

        alertDialog.show();


    }

    ;


    public void TaskCountDialog(){

        String version ="";
        String MessageTxt = "";

        //Log.d("InDialofWe;","Welcome"+callCount);
        try {

            PackageManager manager = TaskList.this.getPackageManager();
            PackageInfo info = manager.getPackageInfo(
                    TaskList.this.getPackageName(), 0);
            version = info.versionName;

            TaskListDatabaseConnector taskListDatabaseConnector = new TaskListDatabaseConnector(TaskList.this);
            taskListDatabaseConnector.open();
            Cursor callData = taskListDatabaseConnector.getCallCount();
            //   Cursor callData = taskListDatabaseConnector.getCountTasktype(agentName);

            while (callData.moveToNext()) {
                callCount = callData.getString(0);
            }
if (callCount.length() > 0) {
    String call[] = callCount.split(",");
    call_count = call[0];
    firstcall = call[1];
    seccall = call[2];
    visitcall = call[3];
    repocall = call[4];
    taskListDatabaseConnector.close();

    MessageTxt = "Hey : " + agentName + "\r\n" + TaskList.this.getString(R.string.dilaog_you_have) + "\r\n  "+"  1st Calls   :   "+ firstcall + "\r\n  "+ "  2nd Calls  :   "+ seccall+"\r\n  "+"  Visit           :   "+ visitcall+"\r\n  "+"  Repo          :   "+repocall + "\r\n"+ TaskList.this.getString(R.string.dilaog_task_worry)+ "\r\n"+TaskList.this.getString(R.string.dialog_complete_it) + "\r\n"+ TaskList.this.getString(R.string.dilaog_refresh_task)+"\r\n"+ TaskList.this.getString(R.string.txt_app_version) +" "+ version;

}
else{
    call_count = "0";
    firstcall = "0";
    seccall = "0";
    visitcall = "0";
    repocall = "0";

    MessageTxt = "Hey : " + agentName + "\r\n" +TaskList.this.getString(R.string.dilaog_you_have) +TaskList.this.getString(R.string.txt_no_call) + "\r\n"+ TaskList.this.getString(R.string.dilaog_task_worry)+ "\r\n"+TaskList.this.getString(R.string.dialog_complete_it) + "\r\n"+ TaskList.this.getString(R.string.dilaog_refresh_task)+"\r\n"+ TaskList.this.getString(R.string.txt_app_version) + version;

}
            //new TaskListDatabaseConnector(TaskList.this).getCallCount(callCount);
            //  new TaskListDatabaseConnector(TaskList.this).Call(callCount);
            //new GetData().SavingCount(callCount);
       /* if (callCount.equals(null)){
            callCount = "0";
        }*/
        } catch (Exception e) {
            e.printStackTrace();
        }

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(TaskList.this);
        alertDialogBuilder.setIcon(R.drawable.ic_assignment_turned_in_white_48dp);
        alertDialogBuilder.setTitle(" Task information ");


        alertDialogBuilder.setMessage(MessageTxt);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {


                dialog.cancel();
            }
        });
   /*     alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel(); // dialog box disappears if user does not confirm task is complete
            }
        });*/
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }




    // calculates the difference between the current time and the date the user last successfully synced the app and checks that against predetermined update threshold
    // @param lastUpdated: date the user last successfully synced the app
    // @param currentDate: the current date
    // @return: boolean on whether or not the difference between the last updated date and the current date is greater than one day
    private boolean needToUpdate(Date lastUpdated, Date currentDate) {
        long diffInMillies = currentDate.getTime() - lastUpdated.getTime(); // calculates difference between dates in milliseconds
        daysDifferential = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS); // converts milliseconds to days
        // FoRfresh(); // calling refresh forcefullu


        //FoRfresh();
        //Toast.makeText(TaskList.this, "Last " + daysDifferential+" days had ago you didn't updated your tasklist ??", Toast.LENGTH_LONG).show();
        //Toast.makeText(TaskList.this, "Please connected with internet and Update your Tasklist", Toast.LENGTH_LONG).show();

        // Toast.makeText(TaskList.this,"DaysDifferentials New One "+daysDifferential,Toast.LENGTH_LONG).show();

        return daysDifferential > 1;
    }


    // refresh for autoRefresh claculating the days if is 2 days then showing this dialog
    public void FoRfresh() {
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(TaskList.this);
        alertDialogBuilder.setIcon(R.drawable.alert_yellow);
        alertDialogBuilder.setTitle(R.string.dilaog_refresh);
        alertDialogBuilder.setMessage(R.string.dilag_make_sure);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton(" OK ", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {

                if (new GetData().isNetworkAvailable(TaskList.this)) {
                    refreshtasklist();
                    refreshupdatecheck = true;
                    try {


                        TaskListDatabaseConnector taskListDatabaseConnector = new TaskListDatabaseConnector(TaskList.this);
                        taskListDatabaseConnector.open();
                        Cursor callData = taskListDatabaseConnector.getCallCount();

                        while (callData.moveToNext()) {
                            callCount = callData.getString(0);
                        }
                        taskListDatabaseConnector.close();

                        int num = (int) Long.parseLong(callCount);
                        if (num >= 1) {
//                            displayNotificationOne(callCount);
                        }



                    } catch (NumberFormatException ex){

                    } catch (Exception e){

                    }
                } else {
                    createErrorDialogBox(getString(R.string.no_internet_connection));

                    new UpdateView().execute();
                }


             /*   if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    alertDialogBuilder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialogInterface) {
                            Toast.makeText(TaskList.this, "Dissmissssssssssssssssss Dialog", Toast.LENGTH_SHORT).show();
                        }
                    });
                }*/
                 /*finish();
                startActivity(getIntent());*/


            }
        });
   /*     alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel(); // dialog box disappears if user does not confirm task is complete
            }
        });*/
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    // calculates whether or not the user must update the app
    // @param date: date the user last successfully synced the app
    // @return: boolean whether or not the user must sync the app (true = yes)
    private boolean refreshNeeded(String date) {
        //     WelcomeDilaog();
        try {
            //if user has never synced the app then it does not need to be updated //TODO: should this be the case?

         /*   Date lastUpdate = dateFormat.parse(date);
            Log.d("Checking the date first",lastUpdate.toString());*/

            if (date.equals("None")) {
                return false;
            }

            Date lastUpdate = dateFormat.parse(date);
         //   Log.d("LastDate update", lastUpdate.toString());
            return needToUpdate(lastUpdate, Calendar.getInstance().getTime());


        } catch (ParseException e) {
            e.printStackTrace();

            return true;
        }
    }

    private boolean autoRefresh(String Code) {

        try {

            if (
                    Code.equals("null")) {
                return false;
            }
            // refreshtasklist();

        } catch (Exception e) {

        }

        return true;
    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
  /*  public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("TaskList Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }
*/
    /*@Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }*/



   /* @Override
    public void onDrawerItemSelected(View view, int position) {

    }
*/

    // updates any previously unsynced completed tasks to zendesk and downloads task list from Looker
    public class GetTaskData extends AsyncTask<Void, Void, Void> {


        @Override
        protected void onPreExecute() {
            progressDialog.show();
          /*  finish();
            startActivity(getIntent());*/
        } // show the progress dialog while this method runs


        @Override
        protected Void doInBackground(Void... voids) {
            new GetData().updateCompletedTasks(getApplicationContext(), txt_country); // mark completed tasks as solved in Zendesk

            new GetData().createNewTickets(getApplicationContext(), txt_country); // create promise to pay tickets in Zendesk
         //  new GetData().inputDataIntoDatabase("410", agentName, getApplicationContext()); // download updated task list from Looker
            if (txt_country.equalsIgnoreCase("Kenya")){
               // new GetData().inputDataIntoDatabase("1309",agentName,getApplicationContext());
                new GetData().inputDataIntoDatabase("1880",agentName,getApplicationContext());
            }else if (txt_country.equalsIgnoreCase("Nigeria")){
                new GetData().inputDataIntoDatabase("1883",agentName,getApplicationContext()); // task
            }else if (txt_country.equalsIgnoreCase("Myanmar (Burma)")){
                new GetData().inputDataIntoDatabase("1884",agentName,getApplicationContext()); //  task
            }else if (txt_country.equalsIgnoreCase("Uganda")){
                new GetData().inputDataIntoDatabase("1882",agentName,getApplicationContext()); // taz task
            }else if (txt_country.equalsIgnoreCase("Tanzania")){
                new GetData().inputDataIntoDatabase("1881",agentName,getApplicationContext()); // taz task
            } else if (txt_country.equalsIgnoreCase("India")){
                new GetData().inputDataIntoDatabase("1885",agentName,getApplicationContext());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            progressDialog.dismiss(); // close the progress dialog box
            new UpdateView().execute(); // populates the task list
        }

    }

    // populates the task list the user sees
    private class UpdateView extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            progressDialog.show();
        } // show the progress dialog while this method runs

        @Override
        protected Void doInBackground(Void... voids) {
            TaskListDatabaseConnector taskListDatabaseConnector = new TaskListDatabaseConnector(getApplicationContext()); // create connector to the local task list database
            fillList = taskListDatabaseConnector.getTaskListData(from, getApplicationContext()); // retrieve all uncompleted tasks and relevant information from the local database


            return null;
        }

        @Override
        protected void onPostExecute(Void v) {


            CustomListAdapter taskListAdapter = new CustomListAdapter(TaskList.this, fillList, R.layout.task_list_item, from, to); // create adapter to map information to xml objects

            taskListItem.setAdapter(taskListAdapter); // apply adapter to the list that the user views
            taskListAdapter.notifyDataSetChanged();

            if (taskListItem.getAdapter().getCount() < 1) { // if the user has no tasks to complete, then display the no tasks to complete text box
                noTasksTextBox.setVisibility(View.VISIBLE);
                txt_agent_name.setText("Hi " + agentName);
                taskListItem.setVisibility(View.GONE);

            } else { // user has tasks to complete, display the task list
                noTasksTextBox.setVisibility(View.GONE);
                taskListItem.setVisibility(View.VISIBLE);
                txt_agent_name.setText("Hi " + agentName);

            }
            progressDialog.dismiss(); // close the progress dialog box
        }

    }

    // updates completed task to zendesk
    // @String: id of the task that has been completed
    private class SetTaskComplete extends AsyncTask<String, Void, Void> {

        @Override
        protected void onPreExecute() {
            progressDialog.show();
        } // show the progress dialog while this method runs

        @Override
        protected Void doInBackground(String... taskID) {
            TaskListDatabaseConnector taskListDatabaseConnector = new TaskListDatabaseConnector(getApplicationContext()); // create connector to the local task list database
            taskListDatabaseConnector.updateTaskComplete(taskID[0]); // updates the task as complete in the local database
            if (new GetData().isNetworkAvailable(TaskList.this)) { // checks if the user is connected to the internet
                new GetData().updateCompletedTasks(TaskList.this, txt_country); // mark completed tasks as solved in Zendesk

                new GetData().createNewTickets(TaskList.this, txt_country); // create promise to pay tickets in Zendesk

            }
            return null;
        } // end method doInBackground

        @Override
        protected void onPostExecute(Void v) {
            progressDialog.dismiss(); // close the progress dialog box
            new UpdateView().execute(); // populates the task list
        }

    }

    // populates the task list that the user views
    private class CustomListAdapter extends SimpleAdapter {
        final String taskTypeValue="";
        int num = 1;
        Button taskListCallButton,taskListCheckButton,taskListSelectTimeButton;

        public CustomListAdapter(Context context, List<? extends Map<String, ?>> data, int resource, String[] from, int[] to) {

            super(context, data, resource, from, to);
        }

      /*  @Override
        public int getCount() {

String task_count;
          *//*  if(num*10 > arrayList.size()){
                return arrayList.size();
            }else{
                return num*10;
            }*//*


            TaskListDatabaseConnector taskListDatabaseConnector = new TaskListDatabaseConnector(TaskList.this);
            taskListDatabaseConnector.open();
            Cursor callData = taskListDatabaseConnector.getCallCount();
            //   Cursor callData = taskListDatabaseConnector.getCountTasktype(agentName);

            while (callData.moveToNext()) {
                callCount = callData.getString(0);
            }

            String call[] =  callCount.split(",");
            call_count = call[0];
            firstcall = call[1];
            seccall = call[2];
            visitcall = call[3];
            repocall = call[4];
            taskListDatabaseConnector.close();



          Log.d("CHeckk######",call_count);


            if (10 >= call_count.length()){

                if ((10 >= firstcall.length())&& (5 >= seccall.length())) {

                    num = 5;
                    Log.d("CallYYYYYY", String.valueOf(num));


                }

            }


            return num;
        }*/

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = super.getView(position, convertView, parent);



            //   TextView cutomerName = (TextView) view.findViewById(R.id.task_customer_name);
            TextView customerAccount = (TextView) view.findViewById(R.id.task_list_customer_account); // text box displaying customer account number
            final TextView taskID = (TextView) view.findViewById(R.id.task_list_task_id); // text box displaying id of the task which is hidden from the user but necessary for the button methods
            TextView taskType = (TextView) view.findViewById(R.id.task_list_task_type); // text box displaying the type of task to the user
            final TextView callCompleteMarker = (TextView) view.findViewById(R.id.task_list_call_complete); // text box displaying whether or not the call is complete, hidden from the user but necessary to determine whether to display phone or calendar icon
            // TextView customer_name = (TextView)view.findViewById(R.id.task_list_customer_name);  // displaying the customer name
            taskListCallButton = (Button) view.findViewById(R.id.task_list_call_button); // phone button which the user can click to call the customer
            taskListCheckButton = (Button) view.findViewById(R.id.task_list_check_button); // check button which the user can click to mark a visit as complete
            taskListSelectTimeButton = (Button) view.findViewById(R.id.task_list_select_time_button); // calendar button which the user can click to select the date the customer promised to pay

              // testing call
            // callCompleteMarker.setText("1");

             final String callComplete = callCompleteMarker.getText().toString(); // call complete value // Final Value is prateek
            callCompleteMarker.setVisibility(View.GONE); // hides call complete text box
            final String taskTypeValue = taskType.getText().toString(); // task type value
              // Log.d("TaskValue@@@@@@@@@####",taskTypeValue+"::"+callComplete);



           // callComplete = "1"; // Testing Call Complete = 1

            if (taskTypeValue.contains("Call")) { // if the task type is a call
                taskListCheckButton.setVisibility(View.GONE); // hide the check box button // GONE "Prateek "
                if (callComplete.equals("1")) { // if the call has been completed
                    taskListCallButton.setVisibility(View.GONE); // hide the phone button
                    taskListSelectTimeButton.setVisibility(View.VISIBLE); // display the calendar button
                } else { // if the call has not been completed
                    taskListSelectTimeButton.setVisibility(View.GONE); // hide the calendar button  Visible "Prateek"
                    taskListCallButton.setVisibility(View.VISIBLE); // display the phone button
                }
            } else { // if the task type is a visit
                taskListCallButton.setVisibility(View.GONE); // hide the phone button
                taskListSelectTimeButton.setVisibility(View.GONE); // hide the calendar button
                taskListCheckButton.setVisibility(View.VISIBLE); // display the check button
            }

            final String taskIDNumber = taskID.getText().toString(); // task id value
            taskID.setVisibility(View.GONE); // hide the task id text box

            // when the user clicks on the account number, the relevant customer view is displayed
            customerAccount.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    Intent openCustomerView = new Intent(TaskList.this, CustomerView.class);
                    openCustomerView.putExtra("task id", taskIDNumber);
                    openCustomerView.putExtra("Country_name", txt_country);
                    startActivity(openCustomerView);
                    finish();
                }
            });

            // when the user clicks the phone button, the user is prompted to call the relevant customer phone number
            taskListCallButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    TaskListDatabaseConnector taskListDatabaseConnector = new TaskListDatabaseConnector(getApplicationContext());
                    String customerPhoneNumber = taskListDatabaseConnector.getPhoneNumber(taskIDNumber);
                    Intent callCustomer = new Intent(Intent.ACTION_CALL);
                    callCustomer.setData(Uri.parse("tel:" + customerPhoneNumber));//callCustomer.setData(Uri.parse("tel:" + "9699964367"));
                    if (ActivityCompat.checkSelfPermission(TaskList.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    startActivity(callCustomer);
                  //  Log.d("Check ",callComplete);
                    if (taskTypeValue.contains("Call")){ // if the task type is a call
                        taskListCheckButton.setVisibility(View.GONE); // hide the check box button // GONE "Prateek "
                        if (callComplete.equals("1")) { // if the call has been completed
                            taskListCallButton.setVisibility(View.GONE); // hide the phone button
                            taskListSelectTimeButton.setVisibility(View.VISIBLE); // display the calendar button
                        } else { // if the call has not been completed
                            taskListSelectTimeButton.setVisibility(View.GONE); // hide the calendar button  Visible "Prateek"
                            taskListCallButton.setVisibility(View.VISIBLE); // display the phone button
                        }
                    }else { // if the task type is a visit
                        taskListCallButton.setVisibility(View.GONE); // hide the phone button
                        taskListSelectTimeButton.setVisibility(View.GONE); // hide the calendar button
                        taskListCheckButton.setVisibility(View.VISIBLE); // display the check button
                    }

                }


            });

            // when the user clicks the check button, the confirm task complete dialog box appears
            taskListCheckButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    createConfirmTaskCompleteDialogBox(taskIDNumber);
                }

            });

            // when the user clicks the calendar button, the date dialog box appears
            taskListSelectTimeButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    activeTaskID = taskIDNumber; // set the activeTaskID (needed after date is selected) to the relevant task

                    // customer feedback!!
                  //  showDialogFeedback();

                    // get the current date and set the minimum date of the date dialog box
                    Calendar calendar = Calendar.getInstance();
                    int year = calendar.get(Calendar.YEAR);
                    int month = calendar.get(Calendar.MONTH);
                    int day = calendar.get(Calendar.DAY_OF_MONTH);
                    datePicker = new DatePickerDialog(TaskList.this, chooseDateListener, year, month, day);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        // set the max date of the date dialog box depending on the type of call
                        int maxDateDays = 0;
                        if (taskTypeValue.contains("1st")) {
                            maxDateDays = 14;
                        } else {
                            maxDateDays = 10;
                        }
                        calendar.add(Calendar.DATE, maxDateDays);

                        // create date dialog box
                        datePicker.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                        datePicker.getDatePicker().setMaxDate(calendar.getTimeInMillis());



                    }



                    showDialog(1);
                }
            });
            return view;
        }
    }

    // creates date dialog box
    @Override
    protected Dialog onCreateDialog(int id) {
        return datePicker;
    }

    private String paymentDate;
    // when the user selects the date the customer promised to pay, the task is marked complete and the date is saved in the local database
    DatePickerDialog.OnDateSetListener chooseDateListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // add one day to the promise to pay date since the call center will call the day after the customer promised to pay
            Calendar calendar = Calendar.getInstance();
            calendar.set(year, monthOfYear, dayOfMonth);
            calendar.add(Calendar.DATE, 1);

            // format the promise to pay date for the local database
            SimpleDateFormat nextPaymentDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            paymentDate = nextPaymentDateFormat.format(calendar.getTime());



          //  showDialogFeedback();



            // mark task complete and save to local database
            TaskListDatabaseConnector taskListDatabaseConnector = new TaskListDatabaseConnector(getApplicationContext());
            taskListDatabaseConnector.updateNextPaymentDue(activeTaskID, paymentDate);
            new SetTaskComplete().execute(activeTaskID);
        }



    };

    private void showDialogFeedback() {


        final Dialog dialog = new Dialog(TaskList.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // tell the Dialog to use the dialog.xml as it's layout description
        dialog.setContentView(R.layout.custom_dialog_box);

			/*
			 * Display display = ((Activity)ctx).
			 * getWindowManager().getDefaultDisplay(); Point size = new Point();
			 * display.getSize(size);
			 */
        int width = 0;
        int height = 0;
        dialog.setCancelable(false);

        Point size = new Point();
        WindowManager w = ((Activity) TaskList.this)
                .getWindowManager();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            w.getDefaultDisplay().getSize(size);
            width = size.x;
            height = size.y;
        } else {
            Display d = w.getDefaultDisplay();
            width = d.getWidth();
            height = d.getHeight();
            ;
        }

        final Spinner sp_feedback = (Spinner) dialog.findViewById(R.id.customer_feedbackspinner);
        final  EditText ed_feedback = (EditText) dialog.findViewById(R.id.tv);

        final TextView txt = (TextView) dialog.findViewById(R.id.tv1);

        Button diaCheckButton = (Button) dialog.findViewById(R.id.btnSubmit);

     //   Button dialogButton = (Button) findViewById(R.id.btnSubmit);
        //  Button cancelButton = (Button) dialog.findViewById(R.id.btncancel);
        dialog.show();
        // (width/2)+((width/2)/2)
        // dialog.getWindow().setLayout((width/2)+((width/2)/2), height/2);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.WHITE));
        dialog.getWindow().setLayout((width / 2) + (width / 1) / 2,
                LinearLayout.LayoutParams.WRAP_CONTENT);



     //   Log.d("After customer feedback","Feeeback calendar");


        diaCheckButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (sp_feedback.getSelectedItem().equals("Select Customer feedback")){



                    txt.setText(R.string.error_invalid_password);
                    txt.setTextColor(getResources().getColor(R.color.colorRed));


                } else if (sp_feedback.getSelectedItem().equals("Others")){

                    ed_feedback.setVisibility(View.VISIBLE);


                    String txt_feedback =  ed_feedback.getText().toString();


                    if (!txt_feedback.equals(null) && txt_feedback.length()>=8){

                        if (!paymentDate.equals(null) && paymentDate.length()>=0) {

                      /*  TaskListDatabaseConnector taskListDatabaseConnector = new TaskListDatabaseConnector(getApplicationContext());
                        taskListDatabaseConnector.updateNextPaymentDue(activeTaskID, paymentDate+"reason:- "+txt_feedback);
                        new SetTaskComplete().execute(activeTaskID);*/
                            dialog.dismiss();

                        }else{

                            txt.setText(R.string.customer_fb);
                            txt.setTextColor(getResources().getColor(R.color.colorRed));

                        }

                    }else{

                        txt.setText(R.string.customer_fb);
                        txt.setTextColor(getResources().getColor(R.color.colorRed));

                    }


                }else if (!sp_feedback.getSelectedItem().equals(null)){



                    if (!paymentDate.equals(null) && paymentDate.length()>=0) {

                      /*  TaskListDatabaseConnector taskListDatabaseConnector = new TaskListDatabaseConnector(getApplicationContext());
                        taskListDatabaseConnector.updateNextPaymentDue(activeTaskID, paymentDate+"reason:- "+txt_feedback);
                        new SetTaskComplete().execute(activeTaskID);*/
                        dialog.dismiss();

                    }else{

                        txt.setText(R.string.custpmer_fb_not);
                        txt.setTextColor(getResources().getColor(R.color.colorRed));

                    }



                  /*  TaskListDatabaseConnector taskListDatabaseConnector = new TaskListDatabaseConnector(getApplicationContext());
                    taskListDatabaseConnector.updateNextPaymentDue(activeTaskID, paymentDate+"reason:- "+sp_feedback.getSelectedItem().toString());
                    new SetTaskComplete().execute(activeTaskID);*/
                    dialog.dismiss();
                }




            }
        });





    }

    public Button getDialogButton() {
        return dialogButton;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            exitByBackKey();

            //moveTaskToBack(false);

            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    protected void exitByBackKey() {

        AlertDialog alertbox = new AlertDialog.Builder(this)
                .setMessage(TaskList.this.getString(R.string.back_btn_close))
                .setPositiveButton(TaskList.this.getString(R.string.txt_yes), new DialogInterface.OnClickListener() {

                    // do something when the button is clicked
                    public void onClick(DialogInterface arg0, int arg1) {

                        finish();
                        //close();


                    }
                })
                .setNegativeButton(TaskList.this.getString(R.string.txt_no), new DialogInterface.OnClickListener() {

                    // do something when the button is clicked
                    public void onClick(DialogInterface arg0, int arg1) {

                        finish();
                        startActivity(getIntent());
                    }
                })
                .show();

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull final String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_ALL) {
            //check if all permissions are granted
            boolean allgranted = false;
            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                    allgranted = true;
                } else {
                    allgranted = false;
                    break;
                }
            }
            if (allgranted) {
                //Toast.makeText(getBaseContext()," you have  got All Permissions", Toast.LENGTH_LONG).show();
            } else if (ActivityCompat.shouldShowRequestPermissionRationale(TaskList.this, permissions[0])
                    || ActivityCompat.shouldShowRequestPermissionRationale(TaskList.this, permissions[1])
                    || ActivityCompat.shouldShowRequestPermissionRationale(TaskList.this, permissions[2])
                    || ActivityCompat.shouldShowRequestPermissionRationale(TaskList.this, permissions[3])
                    ) {

                android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(TaskList.this);
                builder.setTitle("Need Multiple Permissions");
                builder.setMessage("This app needs CallLog , Setting amd Storage permissions.");
                builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        ActivityCompat.requestPermissions(TaskList.this, permissions, PERMISSION_ALL);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Toast.makeText(getBaseContext(), " you have not given any Permissions ", Toast.LENGTH_LONG).show();
                    }
                });
                builder.show();
            } else {
                // Toast.makeText(getBaseContext(), "Unable to get Permission", Toast.LENGTH_LONG).show();
            }
        }
    }

}