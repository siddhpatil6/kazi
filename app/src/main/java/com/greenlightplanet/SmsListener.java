package com.greenlightplanet;

/**
 * Created by prateek on 27-07-2017.
 */

public interface SmsListener {

    public void messageReceived(String messageText);

}
