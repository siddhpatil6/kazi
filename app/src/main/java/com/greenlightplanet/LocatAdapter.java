package com.greenlightplanet;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.greenlightplanet.kazi.R;

import java.util.List;

public class LocatAdapter extends RecyclerView.Adapter<LocatAdapter.MyViewHolder> {


    private List<Locat> LocatList;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, year, genre;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            genre = (TextView) view.findViewById(R.id.genre);
            year = (TextView) view.findViewById(R.id.year);
        }
    }


    public LocatAdapter(List<Locat> LocatList) {
        this.LocatList = LocatList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.movie_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Locat locat = LocatList.get(position);
        holder.title.setText(locat.getTitle());
        holder.genre.setText(locat.getGenre());
        holder.year.setText(locat.getYear());
    }

    @Override
    public int getItemCount() {
        return LocatList.size();
    }
}
