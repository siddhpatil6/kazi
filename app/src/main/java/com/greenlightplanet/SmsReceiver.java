package com.greenlightplanet;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.telephony.SmsMessage;

/**
 * Created by prateek on 27-07-2017.
 */

public class SmsReceiver extends BroadcastReceiver {

    private static SmsListener mListener;

    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle data  = intent.getExtras();
try {
    Object[] pdus = (Object[]) data.get("pdus");

    for (int i = 0; i < pdus.length; i++) {
        SmsMessage smsMessage = SmsMessage.createFromPdu((byte[]) pdus[i]);

        String sender = smsMessage.getDisplayOriginatingAddress();
        //You must check here if the sender is your provider and not another one with same text.

        String messageBody = smsMessage.getMessageBody();
        int msg = messageBody.length();
        //Pass on the text to our listener.


        mListener.messageReceived(messageBody);

    }
}catch (Exception e){
    System.out.println("Exception Sms!"+e);
}
    }

    public static void bindListener(SmsListener listener) {
        mListener = listener;
    }
}
