package com.greenlightplanet;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.greenlightplanet.kazi.R;

import java.util.Locale;

public class LanguageSelection extends Activity {

    private static final String TAG = LanguageSelection.class.getSimpleName();
    private Spinner countrySelectionSpinner,languageSelectionSpinner;
    LinearLayout LL_language,LL_app_logo;
    Locale myLocale;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_language_selection);


        LL_app_logo = (LinearLayout) findViewById(R.id.app_logo);
        LL_language = (LinearLayout) findViewById(R.id.ll_spinner_language);
        languageSelectionSpinner = (Spinner) findViewById(R.id.spinner_language);



        LL_language.setVisibility(View.VISIBLE);
        languageSelectionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {

                if (pos == 1) {

                    Toast.makeText(parent.getContext(),
                            "You have selected English", Toast.LENGTH_SHORT)
                            .show();
                    setLocale("en");

                } else if (pos == 6) {

                    Toast.makeText(parent.getContext(),
                            "You have selected Kenya-Swahili", Toast.LENGTH_SHORT)
                            .show();
                    setLocale("sw");

                } else if (pos == 7) {

                    Toast.makeText(parent.getContext(),
                            "You have selected Burmese", Toast.LENGTH_SHORT)
                            .show();

                    setLocale("my");
                }
                else if (pos == 3) {

                    Toast.makeText(parent.getContext(),
                            "You have selected Hausa", Toast.LENGTH_SHORT)
                            .show();
                    setLocale("ha");

                }
                else if (pos==4){
                    Toast.makeText(parent.getContext(),
                            "You have selected Hindi", Toast.LENGTH_SHORT)
                            .show();
                    setLocale("hi");
                }
                else if (pos == 5){
                    Toast.makeText(parent.getContext(),"Please choose another language work is in progress!!",Toast.LENGTH_LONG).show();
                  //  setLocale("ig");
                }else if (pos == 8){
                    Toast.makeText(parent.getContext(),
                            "You have selected Tanzania-Swahili", Toast.LENGTH_SHORT)
                            .show();
                    setLocale("tn");

                }else if (pos==2){

                    Toast.makeText(parent.getContext(), "You have selected French", Toast.LENGTH_SHORT).show();
                    setLocale("fr");
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });






    }


    public void setLocale(String lang) {

        myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);

        Intent refresh = new Intent(this,AgentSelection.class);
        refresh.putExtra("Login","Languagecheck");
        startActivity(refresh);



      /*  TaskListDatabaseConnector taskListDatabaseConnector = new TaskListDatabaseConnector(LanguageSelection.this);
        taskListDatabaseConnector.deleteLogs();
        Log.d("Logs Deleted","Deleted...****");*/
        this.finish();
   /*     LL_language.setVisibility(View.GONE);
        new country_update().execute();
        LL_country.setVisibility(View.VISIBLE);
*/


    /*    Intent refresh = new Intent(this, AgentSelection.class);
        startActivity(refresh);*/
    }

}
