package com.greenlightplanet;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.greenlightplanet.kazi.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;


/**
 * Created by Prateek  on 6/2/2017.
 */
public class AgentSelection extends Activity implements  InterfaceCalling {


    private final String baseDatabaseURL = "https://greenlightplanet.looker.com:19999/api/3.0/";
    private final String baseDatabaseClientID = "QY6DQzQT6NQM3ngbTRxY"; //"583NStjC5y5mvP3n5c3F";
    private final String baseDatabaseClientSecret = "PMb9rtjBzrdYgyvqQhfhTjrB";//"RpgZ2Qdj6RtxsjpY25R5HFv2";
    private static final String TAG = AgentSelection.class.getSimpleName();
    private AutoCompleteTextView agentSelectionSpinner;
    private Spinner countrySelectionSpinner,languageSelectionSpinner; //
    private TextView agentSelectionSpinnerPrompt, country_selection_txt;
    private Button confirmationButton,btn_resend_otp;
    private SharedPreferences savedPreferences;
    private ArrayList<String> agentList, agent_txt_list;
    private ArrayList<String> country_list;
    ArrayList<String> spinnerArrayList, spinnerArraylistPhone;
    String[] splitedValue;
    String selection=  null;
    LayoutInflater inflater;
    EditText ed_OTP_txt;
    private String agentName, agent_country,agent_phonenumber,agent_area_check,incentiveDone;
    private SharedPreferences.Editor preferencesEditor;
    private ProgressDialog progressDialog;
    HashMap<String, String> arealist = new HashMap<>();
    private EditText passCodeInputField,login_mNumber;
    private Button passCodeSubmitButton, btn_country_OK,btn_login_check,btn_logincancel;
    LinearLayout LL_app_logo, LL_country, LL_passcode, LL_selctName, LL_name_otp, LL_btn_submit, LL_btn_confirm,LL_language,ll_logincheck;
    String[] PERMISSIONS = {Manifest.permission.ACCESS_NETWORK_STATE, Manifest.permission.CALL_PHONE, Manifest.permission.INTERNET, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_CALL_LOG,Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION};
    CheckBox checkBox_pass;
    private String txt_Country,mobileNumber;
    private final int SPLASH_DISPLAY_LENGTH = 1500;
    public static final int PERMISSION_ALL = 1;
    public TextView txt_info;
    char[] otp;

    String s ,txt_phone;
    String agent_number = null;
    int agent_checkOTP = 0;
    String user_country;
    Locale myLocale;
    HashMap<String, String> Agent_score = null;
    private HashMap<String,String>  area_list = null;
    String login = "l";
    Context mContext;
    String new_incentive = null;
    //String agent_score = null;
    HashMap<String,String> agent_score;


    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    //private GoogleApiClient client;
    private int count;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.agent_selection);
        mContext = AgentSelection.this;

        progressDialog = new ProgressDialog(AgentSelection.this);
        progressDialog.setTitle(getString(R.string.progress_bar_title));
        progressDialog.setMessage(getString(R.string.progress_bar_message));
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);

        savedPreferences = getSharedPreferences("preferences", MODE_PRIVATE);
        agentName = savedPreferences.getString("agent", "None");
        agent_phonenumber = savedPreferences.getString("agent_phone","None");
        agent_area_check = savedPreferences.getString("NewIncentive","None");
        incentiveDone = savedPreferences.getString("incentiveDone","None");
        //   mobileNumber = savedPreferences.getString("mobileNumber","None");

        Intent i = getIntent();
        login = i.getStringExtra("Login") ;
        txt_Country = i.getStringExtra("country");
        // mobileNumber = i.getStringExtra("mobileNumber");

       /* getSocketFactory();
        initializeSSLContext(mContext);

       new TlsSniSocketFactory();*/

        // try {
        //  new TLSSocketFactory();
//            initializeSSLContext(mContext);

        /*
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }*/


        if (!login.toString().equalsIgnoreCase("Languagecheck")){
            login = login.replaceAll("_"," ").toString();

           // Log.d("NameInterface",login.toString());
            preferencesEditor = savedPreferences.edit();
            preferencesEditor.putString("agent", login);
            preferencesEditor.commit();
            preferencesEditor.apply();


          /* long startTime = System.currentTimeMillis();

           String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());*/


         // jsdkdjsa
           //new area_filter(login).execute();


            firstCallqueryFilter("2402",login);
          //  new GetData().DataBaseurl_login()

            //  Log.d("Agent_checking area","areaaa");
         /*  long elapsedTime = System.currentTimeMillis() - startTime;
           System.out.println("Total elapsed http request/response time in milliseconds: " + elapsedTime+" Incentive ");
           TaskListDatabaseConnector taskListDatabaseConnector = new TaskListDatabaseConnector(AgentSelection.this);
           taskListDatabaseConnector.insertLogs(agentName,TAG,"AreaFilter_Collectingscore","https://greenlightplanet.looker.com:19999/api/3.0/looks/2402",String.valueOf(elapsedTime),currentDateTimeString,agent_country);

           taskListDatabaseConnector.close();*/




        }


        inflater = (LayoutInflater) AgentSelection.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
/*

        LL_app_logo = (LinearLayout) findViewById(R.id.app_logo);
        LL_country = (LinearLayout) findViewById(R.id.ll_spinner_country);
        LL_passcode = (LinearLayout) findViewById(R.id.ll_spinner);

*/

        LL_app_logo = (LinearLayout) findViewById(R.id.app_logo);
        LL_name_otp = (LinearLayout) findViewById(R.id.ll_otp);
        LL_country = (LinearLayout) findViewById(R.id.ll_spinner_country);
        LL_passcode = (LinearLayout) findViewById(R.id.ll_edit_txt);
        LL_selctName = (LinearLayout) findViewById(R.id.ll_spinner);
        LL_language = (LinearLayout) findViewById(R.id.ll_spinner_language);
        //ll_logincheck = (LinearLayout) findViewById(R.id.ll_loginCheck);



        checkBox_pass = (CheckBox) findViewById(R.id.cbShowPwd);


        confirmationButton = (Button) findViewById(R.id.agent_selection_confirmation_button);
        btn_country_OK = (Button) findViewById(R.id.country_submit_button);
        ed_OTP_txt = (EditText) findViewById(R.id.otp_ed);






        btn_resend_otp = (Button) findViewById(R.id.agent_otp_button);

        passCodeInputField = (EditText) findViewById(R.id.passcode_input_field);
        passCodeSubmitButton = (Button) findViewById(R.id.passcode_submit_button);

        //  app_logo = (Button) findViewById(R.id.app_logo);

        btn_country_OK.setOnClickListener(countryOkbuttonListner);
        /*passCodeInputField.setVisibility((View.GONE));
        passCodeSubmitButton.setVisibility(View.GONE);*/
        txt_info = (TextView) findViewById(R.id.phone_info);
        final TextInputLayout passcode_txt_view = (TextInputLayout) findViewById(R.id.input_text);
        agentSelectionSpinner = (AutoCompleteTextView) findViewById(R.id.agent_selection_spinner);
        countrySelectionSpinner = (Spinner) findViewById(R.id.country_selection_spinner);
        country_selection_txt = (TextView) findViewById(R.id.agent_selection_prompt_country);
        agentSelectionSpinnerPrompt = (TextView) findViewById(R.id.agent_selection_prompt);
        languageSelectionSpinner = (Spinner) findViewById(R.id.spinner_language);
        passcode_txt_view.setHint("Enter the Passcode ");

        try {
            if (!hasPermissions(this, PERMISSIONS)) {
                ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
            }
        } catch (Exception e) {
            createErrorDialogBox(getString(R.string.txt_technical_error));
        }


        //  incentiveDone = "None";

        if (!agentName.equals("None") && incentiveDone.equals("None")) {


            if (new GetData().isNetworkAvailable(AgentSelection.this)) {
                try {

                  /*  TaskListDatabaseConnector taskListDatabaseConnector = new TaskListDatabaseConnector(AgentSelection.this);
                  String tc = String.valueOf(taskListDatabaseConnector.getOtpCheck(agentName));

*/
               /*     long startTime = System.currentTimeMillis();

                    String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());*/


                    LoadingDialog.showLoadingDialog(mContext,"Loading....");
                    String tableCode = "2402";
                    new GetData().firstCallqueryFilter(mContext,tableCode,agentName,new VolleyCallback(){

                        @Override
                        public void onSuccess(String  result){

                            Log.d("Result",result.toString());
                            String final_score;


                            try {

                                JSONArray jsonArray = new JSONArray(result);
                                if (!(jsonArray == null)) {
                                    for (int i = 0; i < jsonArray.length(); i++) {

                                        JSONObject agentNameJ = (JSONObject) jsonArray.get(i);


                                        if (agentNameJ.get("accounts.agent").toString().equals(agentName)) {
                                            arealist.put("area", agentNameJ.get("angaza_users_facts.current_area").toString());
                                            arealist.put("one_day_score", agentNameJ.get("portfolio_derived.one_day_average_account_disabled_score").toString());
                                            arealist.put("two_day_score", agentNameJ.get("portfolio_derived.two_day_average_account_disabled_score").toString());
                                            arealist.put("third_day_score", agentNameJ.get("portfolio_derived.three_day_average_account_disabled_score").toString());
                                            arealist.put("forth_day_score", agentNameJ.get("portfolio_derived.four_day_average_account_disabled_score").toString());
                                            arealist.put("fivth_day_score", agentNameJ.get("portfolio_derived.five_day_average_account_disabled_score").toString());
                                            arealist.put("six_day_score", agentNameJ.get("portfolio_derived.six_day_average_account_disabled_score").toString());
                                            arealist.put("seven_day_score", agentNameJ.get("portfolio_derived.seven_day_average_account_disabled_score").toString());
                                            arealist.put("accounts.country", agentNameJ.get("accounts.country").toString());
                                        }
                                    }
                                }


                                {
                                    String txt,key = null,value=null;
                                    String user_area = null;

                                    try {


                                        if (arealist.size() > 0) {


                                            String one_day = arealist.get("one_day_score");
                                            String two_day = arealist.get("two_day_score");
                                            String third_day = arealist.get("third_day_score");
                                            String forth_day = arealist.get("forth_day_score");
                                            String five_day = arealist.get("fivth_day_score");
                                            String six_day = arealist.get("six_day_score");
                                            String seven_day = arealist.get("seven_day_score");
                                            user_country = arealist.get("accounts.country");
                                            txt_Country = user_country;

                                            final_score = one_day + two_day + third_day + forth_day + five_day + six_day + seven_day;



                                            // preferencesEditor.putString("agent", agentName);
                                            preferencesEditor = savedPreferences.edit();
                                            preferencesEditor.putString("agent", agentName);
                                            preferencesEditor.putString("agentCountry", txt_Country);
                                            preferencesEditor.putString("OTP", "Done");
                                            preferencesEditor.putString("incentiveDone", "Done");
                                            preferencesEditor.apply();
                                            progressDialog.dismiss();

/*
                                            final GoToTaskList goToTaskList = new GoToTaskList("Yes", arealist);
                                            goToTaskList.execute();*/


LoadingDialog.cancelLoading();

                                            CallGoToTaskList("Yes",arealist);


                                          /*  new Handler().postDelayed(new Runnable() {

                                                @Override
                                                public void run() {


                                                 //   LoadingDialog.showLoadingDialog(mContext,"Please wait...");
                                                    CallGoToTaskList("Yes",arealist);
                                                }
                                            },10000 );*/

                                          //  CallGoToTaskList("Yes",arealist);

                                        } else {

                                            arealist.put("No", "NO");
                                            preferencesEditor = savedPreferences.edit();
                                            preferencesEditor.putString("agent", agentName);
                                            preferencesEditor.putString("agentCountry", txt_Country);
                                            preferencesEditor.putString("incentiveDone", "Done");
                                            preferencesEditor.apply();

                                            LoadingDialog.cancelLoading();

                                         //   LoadingDialog.showLoadingDialog(mContext,"Please wait...");
                                            CallGoToTaskList("No",arealist);

                                        /*    new Handler().postDelayed(new Runnable() {

                                                @Override
                                                public void run() {

                                                    LoadingDialog.showLoadingDialog(mContext,"Please wait...");
                                                    CallGoToTaskList("No",arealist);
                                                }
                                            },10000 );

*/
/*
                                            final GoToTaskList goToTaskList = new GoToTaskList("No", arealist);
                                            goToTaskList.execute();
*/
                                        }
                                    }catch (Exception ex){
                                        ex.printStackTrace();
                                    }
                                }
                            }catch (Exception e){
                                e.printStackTrace();

                            }
                            LoadingDialog.cancelLoading();
                        }
                    });







                    // new area_filter_task_list().execute();

                    //  Log.d("Agent_checking area","areaaa");
             /*       long elapsedTime = System.currentTimeMillis() - startTime;
                    System.out.println("Total elapsed http request/response time in milliseconds: " + elapsedTime+" Incentive ");
                    TaskListDatabaseConnector taskListDatabaseConnector = new TaskListDatabaseConnector(AgentSelection.this);
                    taskListDatabaseConnector.insertLogs(agentName,TAG,"checkingArea_CollectingScore","https://greenlightplanet.looker.com:19999/api/3.0/looks/2402",String.valueOf(elapsedTime),currentDateTimeString,agent_country);

                    taskListDatabaseConnector.close();*/


                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {

                createErrorDialogBoxInternetConnection(getString(R.string.no_internet_connection));
            }
        }else if (!agentName.equals("None")){


            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    Intent openTaskList = new Intent(AgentSelection.this, TaskList.class);
                    //openTaskList.putExtra("country_name",txt_Country);
                    startActivity(openTaskList);
                    AgentSelection.this.finish();
                }
            }, SPLASH_DISPLAY_LENGTH);



        }else{
            if (new GetData().isNetworkAvailable(AgentSelection.this)) {

                try {
// calling country name from looker



                    if (login.toString().equalsIgnoreCase("Languagecheck")){



                        //  long startTime = System.currentTimeMillis();

//                    String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());

                     //   volleyR();


                        LoadingDialog.showLoadingDialog(mContext,"Loading....");
                        String tableCode = "399";
                        new GetData().volleyR(mContext,tableCode,new CallBackVolley(){

                            @Override
                            public void onSuccess(ArrayList<String>  result){

                                country_list = result;

                                if (country_list != null) {
                                    ArrayAdapter<String> countrySelectionSpinnerAdapter = new ArrayAdapter<String>(AgentSelection.this, android.R.layout.simple_spinner_item, country_list);
                                    countrySelectionSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                    countrySelectionSpinner.setAdapter(countrySelectionSpinnerAdapter);
                                    countrySelectionSpinner.setOnItemSelectedListener(countrySelectionSpinnerListener);
                                    //  countrySelectionSpinner.setPrompt("Please Select the Country...");

                                    LoadingDialog.cancelLoading();
                                } else {
                                    createErrorDialogBoxInternetConnection(getString(R.string.no_internet_try_again));
                                }


                            }
                        });




                        // new country_update().execute();
                        LL_country.setVisibility(View.VISIBLE);

                        //  Log.d("Agent_checking area","areaaa");
  /*                  long elapsedTime = System.currentTimeMillis() - startTime;
                    System.out.println("Total elapsed http request/response time in milliseconds: " + elapsedTime+" Incentive ");
                    TaskListDatabaseConnector taskListDatabaseConnector = new TaskListDatabaseConnector(AgentSelection.this);
                    taskListDatabaseConnector.insertLogs(agentName,TAG,"countryNames","https://greenlightplanet.looker.com:19999/api/3.0/looks/399",String.valueOf(elapsedTime),currentDateTimeString,agent_country);

                    taskListDatabaseConnector.close();
*/



                    }

                    // LL_country.setVisibility(View.VISIBLE);

                    //   new country_update().execute();
                    //   LL_passcode.setVisibility(View.VISIBLE);
                } catch (Exception e) {
                    createErrorDialogBox(getString(R.string.txt_technical_error));
                }

            } else {
                createErrorDialogBoxInternetConnection(getString(R.string.no_internet_connection));

            }
        }


    }



    private void CallGoToTaskList(String new_incentive_user,HashMap<String,String> user_score) {


        new_incentive = new_incentive_user;
        agent_score = user_score;

        agentName = savedPreferences.getString("agent", "None");

        String TableCode = null;
        if (txt_Country.equalsIgnoreCase("Kenya")){
            // new GetData().inputDataIntoDatabase("1309",agentName,getApplicationContext());
            TableCode = "1880";

            LoadingDialog.showLoadingDialog(mContext,"Please wait...");

            String tableCode = "1880";

            new GetData().firstCallqueryFilter(mContext, tableCode, agentName, new VolleyCallback() {
                @Override
                public void onSuccess(String result) {

                 //   Toast.makeText(AgentSelection.this,result.toString(),Toast.LENGTH_SHORT).show();



                    InseringTaskData(result);



                        preferencesEditor = savedPreferences.edit();
                        //  preferencesEditor.putString("agent", agentName);
                        //  preferencesEditor.putString("agentCountry", txt_Country);
                        preferencesEditor.putString("OTP","Done");
                        preferencesEditor.putString("NewIncentive",new_incentive);
                        Gson gson = new Gson();
                        String json = gson.toJson(agent_score);
                        preferencesEditor.putString("finalScore",json);
                        preferencesEditor.apply();
                        progressDialog.dismiss();
                        if (new_incentive.equalsIgnoreCase("Yes")){
                            LoadingDialog.showLoadingDialog(mContext,"Please wait....");
                            Intent openTaskList = new Intent(AgentSelection.this, Summary.class);
                            startActivity(openTaskList);

                            LoadingDialog.cancelLoading();
                            AgentSelection.this.finish();
                        }else{
                            LoadingDialog.showLoadingDialog(mContext,"Please wait...");
                            Intent openTaskList = new Intent(AgentSelection.this, TaskList.class);
                            startActivity(openTaskList);
                            LoadingDialog.cancelLoading();
                            AgentSelection.this.finish();
                        }


LoadingDialog.cancelLoading();

                }
            });



                //new GetData().inputDataIntoDatabase(TableCode,agentName,getApplicationContext());
        }else if (txt_Country.equalsIgnoreCase("Nigeria")){

           // new GetData().inputDataIntoDatabase(TableCode,agentName,getApplicationContext()); // task

            LoadingDialog.showLoadingDialog(mContext,"Please wait...");
            String tableCode = "1883";

            new GetData().firstCallqueryFilter(mContext, tableCode, agentName, new VolleyCallback() {
                @Override
                public void onSuccess(String result) {

                    {

                       // Toast.makeText(AgentSelection.this,result.toString(),Toast.LENGTH_SHORT).show();





                        InseringTaskData(result);



                        preferencesEditor = savedPreferences.edit();
                        //  preferencesEditor.putString("agent", agentName);
                        //  preferencesEditor.putString("agentCountry", txt_Country);
                        preferencesEditor.putString("OTP","Done");
                        preferencesEditor.putString("NewIncentive",new_incentive);
                        Gson gson = new Gson();
                        String json = gson.toJson(agent_score);
                        preferencesEditor.putString("finalScore",json);
                        preferencesEditor.apply();
                        progressDialog.dismiss();
                        if (new_incentive.equalsIgnoreCase("Yes")){
                            LoadingDialog.showLoadingDialog(mContext,"Please wait....");
                            Intent openTaskList = new Intent(AgentSelection.this, Summary.class);
                            startActivity(openTaskList);

                            LoadingDialog.cancelLoading();
                            AgentSelection.this.finish();
                        }else{
                            LoadingDialog.showLoadingDialog(mContext,"Please wait...");
                            Intent openTaskList = new Intent(AgentSelection.this, TaskList.class);
                            startActivity(openTaskList);
                            LoadingDialog.cancelLoading();
                            AgentSelection.this.finish();
                        }


                        LoadingDialog.cancelLoading();

                    }

                }
            });




        }else if (txt_Country.equalsIgnoreCase("Myanmar (Burma)")){
           // TableCode = "1884";
            //new GetData().inputDataIntoDatabase(TableCode,agentName,getApplicationContext()); //  task

            LoadingDialog.showLoadingDialog(mContext,"Loading....");
            String tableCode = "1884";

            new GetData().firstCallqueryFilter(mContext, tableCode, agentName, new VolleyCallback() {
                @Override
                public void onSuccess(String result) {

                //    Toast.makeText(AgentSelection.this,result.toString(),Toast.LENGTH_SHORT).show();





                    InseringTaskData(result);



                    preferencesEditor = savedPreferences.edit();
                    //  preferencesEditor.putString("agent", agentName);
                    //  preferencesEditor.putString("agentCountry", txt_Country);
                    preferencesEditor.putString("OTP","Done");
                    preferencesEditor.putString("NewIncentive",new_incentive);
                    Gson gson = new Gson();
                    String json = gson.toJson(agent_score);
                    preferencesEditor.putString("finalScore",json);
                    preferencesEditor.apply();
                    progressDialog.dismiss();
                    if (new_incentive.equalsIgnoreCase("Yes")){
                        LoadingDialog.showLoadingDialog(mContext,"Please wait....");
                        Intent openTaskList = new Intent(AgentSelection.this, Summary.class);
                        startActivity(openTaskList);

                        LoadingDialog.cancelLoading();
                        AgentSelection.this.finish();
                    }else{
                        LoadingDialog.showLoadingDialog(mContext,"Please wait...");
                        Intent openTaskList = new Intent(AgentSelection.this, TaskList.class);
                        startActivity(openTaskList);
                        LoadingDialog.cancelLoading();
                        AgentSelection.this.finish();
                    }


                    LoadingDialog.cancelLoading();

                }
            });





        }else if (txt_Country.equalsIgnoreCase("Uganda")){
         //   TableCode ="1882";
            //new GetData().inputDataIntoDatabase(TableCode,agentName,getApplicationContext()); // taz task



            LoadingDialog.showLoadingDialog(mContext,"Loading....");
            String tableCode = "1882";



            new GetData().firstCallqueryFilter(mContext, tableCode, agentName, new VolleyCallback() {
                @Override
                public void onSuccess(String result) {

                  //  Toast.makeText(AgentSelection.this,result.toString(),Toast.LENGTH_SHORT).show();




                    InseringTaskData(result);



                    preferencesEditor = savedPreferences.edit();
                    //  preferencesEditor.putString("agent", agentName);
                    //  preferencesEditor.putString("agentCountry", txt_Country);
                    preferencesEditor.putString("OTP","Done");
                    preferencesEditor.putString("NewIncentive",new_incentive);
                    Gson gson = new Gson();
                    String json = gson.toJson(agent_score);
                    preferencesEditor.putString("finalScore",json);
                    preferencesEditor.apply();
                    progressDialog.dismiss();
                    if (new_incentive.equalsIgnoreCase("Yes")){
                        LoadingDialog.showLoadingDialog(mContext,"Please wait....");
                        Intent openTaskList = new Intent(AgentSelection.this, Summary.class);
                        startActivity(openTaskList);

                        LoadingDialog.cancelLoading();
                        AgentSelection.this.finish();
                    }else{
                        LoadingDialog.showLoadingDialog(mContext,"Please wait...");
                        Intent openTaskList = new Intent(AgentSelection.this, TaskList.class);
                        startActivity(openTaskList);
                        LoadingDialog.cancelLoading();
                        AgentSelection.this.finish();
                    }


                    LoadingDialog.cancelLoading();

                }
            });







        }else if (txt_Country.equalsIgnoreCase("Tanzania")){
            //TableCode = "1881";
           // new GetData().inputDataIntoDatabase(TableCode,agentName,getApplicationContext()); // taz task


            LoadingDialog.showLoadingDialog(mContext,"Loading....");
            String tableCode = "1881";

            new GetData().firstCallqueryFilter(mContext, tableCode, agentName, new VolleyCallback() {
                @Override
                public void onSuccess(String result) {

                 //   Toast.makeText(AgentSelection.this,result.toString(),Toast.LENGTH_SHORT).show();





                    InseringTaskData(result);



                    preferencesEditor = savedPreferences.edit();
                    //  preferencesEditor.putString("agent", agentName);
                    //  preferencesEditor.putString("agentCountry", txt_Country);
                    preferencesEditor.putString("OTP","Done");
                    preferencesEditor.putString("NewIncentive",new_incentive);
                    Gson gson = new Gson();
                    String json = gson.toJson(agent_score);
                    preferencesEditor.putString("finalScore",json);
                    preferencesEditor.apply();
                    progressDialog.dismiss();
                    if (new_incentive.equalsIgnoreCase("Yes")){
                        LoadingDialog.showLoadingDialog(mContext,"Please wait....");
                        Intent openTaskList = new Intent(AgentSelection.this, Summary.class);
                        startActivity(openTaskList);

                        LoadingDialog.cancelLoading();
                        AgentSelection.this.finish();
                    }else{
                        LoadingDialog.showLoadingDialog(mContext,"Please wait...");
                        Intent openTaskList = new Intent(AgentSelection.this, TaskList.class);
                        startActivity(openTaskList);
                        LoadingDialog.cancelLoading();
                        AgentSelection.this.finish();
                    }


                    LoadingDialog.cancelLoading();

                }
            });



        } else if (txt_Country.equalsIgnoreCase("India")){
//            TableCode = "1885";
            //new GetData().inputDataIntoDatabase(TableCode,agentName,getApplicationContext());



            LoadingDialog.showLoadingDialog(mContext,"Loading....");
            String tableCode = "1885";

            new GetData().firstCallqueryFilter(mContext, tableCode, agentName, new VolleyCallback() {
                @Override
                public void onSuccess(String result) {

                  //  Toast.makeText(AgentSelection.this,result.toString(),Toast.LENGTH_SHORT).show();




                    InseringTaskData(result);



                    preferencesEditor = savedPreferences.edit();
                    //  preferencesEditor.putString("agent", agentName);
                    //  preferencesEditor.putString("agentCountry", txt_Country);
                    preferencesEditor.putString("OTP","Done");
                    preferencesEditor.putString("NewIncentive",new_incentive);
                    Gson gson = new Gson();
                    String json = gson.toJson(agent_score);
                    preferencesEditor.putString("finalScore",json);
                    preferencesEditor.apply();
                    progressDialog.dismiss();
                    if (new_incentive.equalsIgnoreCase("Yes")){
                        LoadingDialog.showLoadingDialog(mContext,"Please wait....");
                        Intent openTaskList = new Intent(AgentSelection.this, Summary.class);
                        startActivity(openTaskList);

                        LoadingDialog.cancelLoading();
                        AgentSelection.this.finish();
                    }else{
                        LoadingDialog.showLoadingDialog(mContext,"Please wait...");
                        Intent openTaskList = new Intent(AgentSelection.this, TaskList.class);
                        startActivity(openTaskList);
                        LoadingDialog.cancelLoading();
                        AgentSelection.this.finish();
                    }


                    LoadingDialog.cancelLoading();

                }
            });





        }





    }

    private void InseringTaskData(String result) {

        HashMap<String, HashMap<String, String>> taskListData = new HashMap<String, HashMap<String, String>>();


        try {


            JSONArray rawTaskListData = new JSONArray(result);

            int id_count = 0;

            int id_type_count_1stcall = 0;
            int id_type_count_2ndcall = 0;
            int id_type_count_visit = 0;
            int id_type_count_repo = 0;
            String task = "";

            for (int i = 0; i < rawTaskListData.length(); i++) {

               task = String.valueOf(rawTaskListData.length());


                //   Log.d("Input Data :", taskListData.toString());
                JSONObject rawTaskData = (JSONObject) rawTaskListData.get(i);
                if ((rawTaskData.get("accounts.agent").toString()).equals(agentName)) {
                    HashMap<String, String> taskData = new HashMap<String, String>();

                    taskData.put("owner_name", rawTaskData.get("accounts.owner_name").toString());
                    taskData.put("owner_msisdn", rawTaskData.get("accounts.owner_msisdn").toString());
                    taskData.put("total_paid", rawTaskData.get("accounts.total_paid").toString());
                    taskData.put("product_name", rawTaskData.get("accounts.group_name").toString());
                    taskData.put("days_past_due", rawTaskData.get("accounts.days_disabled").toString());
                    taskData.put("date_of_latest_payment_utc", rawTaskData.get("accounts.date_of_latest_payment_utc_date").toString());
                    taskData.put("account_number", rawTaskData.get("accounts.account_number").toString());
                    id_count = id_count + 1;
                    taskData.put("ticket_id", rawTaskData.get("tickets_all_countries.id").toString());

                    taskData.put("ticket_type", rawTaskData.get("tickets_all_countries.eo_ticket").toString());

                    if (rawTaskData.get("tickets_all_countries.eo_ticket").toString().equalsIgnoreCase("1st Call")) {
                        id_type_count_1stcall = id_type_count_1stcall + 1;

                    }
                    if (rawTaskData.get("tickets_all_countries.eo_ticket").toString().equalsIgnoreCase("2nd Call")) {
                        id_type_count_2ndcall = id_type_count_2ndcall + 1;

                    }
                    if (rawTaskData.get("tickets_all_countries.eo_ticket").toString().equalsIgnoreCase("Visit")) {
                        id_type_count_visit = id_type_count_visit + 1;


                    }
                    if ((rawTaskData.get("tickets_all_countries.eo_ticket").toString().equalsIgnoreCase("Repo"))) {
                        id_type_count_repo = id_type_count_repo + 1;

                    }


                    taskData.put("requester_id", rawTaskData.get("tickets_all_countries.requester_id").toString());
                    // taskData.put("request_country_status",rawTaskData.get("tickets_all_countries.status").toString());
                    taskListData.put(Integer.toString(i), taskData);



                   int  lastInteger = i;
                }

            }

                String CallCount = String.valueOf(id_count);
                String id_1stcall = String.valueOf(id_type_count_1stcall);
                String id_2ndcall = String.valueOf(id_type_count_2ndcall);
                String id_visit = String.valueOf(id_type_count_visit);
                String id_repo = String.valueOf(id_type_count_repo);

                //  Log.d("TAsk************", CallCount + ":1st" + id_1stcall + ":2nd" + id_2ndcall + "visit:" + id_visit + "repo:" + id_repo +"TASSSSSS"+task);

                CallCount = CallCount + "," + id_1stcall + "," + id_2ndcall + "," + id_visit + "," + id_repo;


                TaskListDatabaseConnector taskListDatabaseConnector = new TaskListDatabaseConnector(mContext);
                taskListDatabaseConnector.insertTaskList(taskListData, CallCount); // connecting with the task list data from the Selection




               /* String CallCount = String.valueOf(id_count);
                String id_1stcall = String.valueOf(id_type_count_1stcall);
                String id_2ndcall = String.valueOf(id_type_count_2ndcall);
                String id_visit = String.valueOf(id_type_count_visit);
                String id_repo = String.valueOf(id_type_count_repo);

                //  Log.d("TAsk************", CallCount + ":1st" + id_1stcall + ":2nd" + id_2ndcall + "visit:" + id_visit + "repo:" + id_repo +"TASSSSSS"+task);

                CallCount = CallCount + "," + id_1stcall + "," + id_2ndcall + "," + id_visit + "," + id_repo;


                TaskListDatabaseConnector taskListDatabaseConnector = new TaskListDatabaseConnector(mContext);
                taskListDatabaseConnector.insertTaskList(taskListData, CallCount); // connecting with the task list data from the Selection
*/


                return;




        } catch (JSONException ex){


        } catch (Exception e){

        }
    }



    public void setLocale(String lang) {

        myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
        Intent refresh = new Intent(this, AgentSelection.class);
        startActivity(refresh);
    }


    public static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }




  /*  @Override
    protected void onResume() {
        //progressDialog.show();

        Toast.makeText(AgentSelection.this,"On REsume", Toast.LENGTH_SHORT);
    }*/


    View.OnClickListener countryOkbuttonListner = new View.OnClickListener() {
        @Override
        public void onClick(View v) {


            if (new GetData().isNetworkAvailable(AgentSelection.this)) {
                //   txt_Country = countrySelectionSpinner.getSelectedItem().toString();
                if (countrySelectionSpinner != null && countrySelectionSpinner.getSelectedItem() != null) {
                    txt_Country = (String) countrySelectionSpinner.getSelectedItem();

                    //  new UpdateView().execute();

                    Intent i = new Intent(AgentSelection.this,LoginCheck_Activity.class);
                    i.putExtra("country",txt_Country);
                    //      preferencesEditor.putString("agentCountry", txt_Country);
                    startActivity(i);
                    finish();
/*

                passCodeInputField.setVisibility(View.VISIBLE);
                passCodeSubmitButton.setVisibility(View.VISIBLE);
                countrySelectionSpinner.setVisibility(View.INVISIBLE);
                country_selection_txt.setVisibility(View.INVISIBLE);
                btn_country_OK.setVisibility(View.INVISIBLE);

                // agentSelectionSpinnerPrompt.setText(getString(R.string.enter_passcode));
*/


                } else {
                    Toast.makeText(AgentSelection.this,R.string.txt_select_country,
                            Toast.LENGTH_LONG).show();

                }
          /*  if (new GetData().isNetworkAvailable(AgentSelection.this)) {


            } else {
                createErrorDialogBox(getString(R.string.no_internet_connection));
                //  Toast.makeText(AgentSelection.this,"Please Connect to Internet Connection...",Toast.LENGTH_SHORT);
                //finish();
            }
*/

            } else {
                createErrorDialogBox(AgentSelection.this.getString(R.string.no_internet_connection));
            }

        }
    };




    //displays the confirmation button once the agent has selected a name
    private AdapterView.OnItemSelectedListener countrySelectionSpinnerListener = new AdapterView.OnItemSelectedListener() {

        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            //confirmationButton.setVisibility(View.VISIBLE);
            // Toast.makeText(AgentSelection.this, "Selected Item"+view.toString(), Toast.LENGTH_SHORT).show();
        }


        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };



    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
/*    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("AgentSelection Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }*/

   /* @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }
*/
  /*  @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }

*/

    @Override
    public void displayName(String name) {

        name = name.replaceAll("_"," ").toString();
       // Log.d("NameInterface",name.toString());
      //  new area_filter(name).execute();

        firstCallqueryFilter("2402",name);

    }


    public class area_filter extends AsyncTask<Void, Void, Void> {
        String Agentname;
        String final_score;




        public area_filter(String agentName) {
            Agentname = agentName;
            // agentName = agentName;

        }

        @Override
        protected void onPreExecute() {
            progressDialog.show();
        }


        @Override
        protected Void doInBackground(Void... voids) {
            try {
                // String tablecode = "1976";
                //String tablecode = "2006";
                String tablecode = "2402"; // testing for area code

                area_list = new GetData().getarealist(Agentname, tablecode);
                //  new GetData().inputDataIntoDatabase("1976",agentName,getApplicationContext());

            } catch (Exception en) {
                createErrorDialogBox(getString(R.string.txt_technical_error));

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            String txt,key = null,value=null;
            String user_area = null;

            try {


                if (area_list.size() > 0) {


                    String one_day = area_list.get("one_day_score");
                    String two_day = area_list.get("two_day_score");
                    String third_day = area_list.get("third_day_score");
                    String forth_day = area_list.get("forth_day_score");
                    String five_day = area_list.get("fivth_day_score");
                    String six_day = area_list.get("six_day_score");
                    String seven_day = area_list.get("seven_day_score");
                    user_country = area_list.get("accounts.country");
                    txt_Country = user_country;

                    final_score = one_day + two_day + third_day + forth_day + five_day + six_day + seven_day;



                    // preferencesEditor.putString("agent", agentName);
                    preferencesEditor = savedPreferences.edit();
                    preferencesEditor.putString("agent", Agentname);
                    preferencesEditor.putString("agentCountry", txt_Country);
                    preferencesEditor.putString("OTP", "Done");
                    preferencesEditor.putString("incentiveDone", "Done");
                    preferencesEditor.apply();
                    progressDialog.dismiss();


                    final GoToTaskList goToTaskList = new GoToTaskList("Yes", area_list);
                    goToTaskList.execute();

                } else {

                    area_list.put("No", "NO");
                    preferencesEditor = savedPreferences.edit();
                    preferencesEditor.putString("agent", Agentname);
                    preferencesEditor.putString("agentCountry", txt_Country);
                    preferencesEditor.putString("incentiveDone", "Done");
                    preferencesEditor.apply();

                    final GoToTaskList goToTaskList = new GoToTaskList("No", area_list);
                    goToTaskList.execute();

                }

            }catch (Exception ex){
                ex.printStackTrace();
            }
        }

    }


    private class GoToTaskList extends AsyncTask<Void, Void, Void> {
        String new_incentive = null;
        //String agent_score = null;
        HashMap<String,String> agent_score;


        public GoToTaskList(String new_incentive_user,HashMap<String,String> user_score) {
            ///Log.d("Agent_score",user_score.toString());
            new_incentive = new_incentive_user;
            agent_score = user_score;
            //  Agent_score.put("agent_score",user_score.toString());

        }

        @Override
        protected void onPreExecute() {
            progressDialog.show();
        }


        @Override
        protected Void doInBackground(Void... voids) {
            try {
                //   long startTime = System.currentTimeMillis();

                // String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
                agentName = savedPreferences.getString("agent", "None");

                String TableCode = null;
                if (txt_Country.equalsIgnoreCase("Kenya")){
                    // new GetData().inputDataIntoDatabase("1309",agentName,getApplicationContext());
                    TableCode = "1880";
                    new GetData().inputDataIntoDatabase(TableCode,agentName,getApplicationContext());
                }else if (txt_Country.equalsIgnoreCase("Nigeria")){
                    TableCode = "1883";
                    new GetData().inputDataIntoDatabase(TableCode,agentName,getApplicationContext()); // task
                }else if (txt_Country.equalsIgnoreCase("Myanmar (Burma)")){
                    TableCode = "1884";
                    new GetData().inputDataIntoDatabase(TableCode,agentName,getApplicationContext()); //  task
                }else if (txt_Country.equalsIgnoreCase("Uganda")){
                    TableCode ="1882";
                    new GetData().inputDataIntoDatabase(TableCode,agentName,getApplicationContext()); // taz task
                }else if (txt_Country.equalsIgnoreCase("Tanzania")){
                    TableCode = "1881";
                    new GetData().inputDataIntoDatabase(TableCode,agentName,getApplicationContext()); // taz task
                } else if (txt_Country.equalsIgnoreCase("India")){
                    TableCode = "1885";
                    new GetData().inputDataIntoDatabase(TableCode,agentName,getApplicationContext());
                }


/*

                long elapsedTime = System.currentTimeMillis() - startTime;
                System.out.println("Total elapsed http request/response time in milliseconds: " + elapsedTime+" Incentive ");
                TaskListDatabaseConnector taskListDatabaseConnector = new TaskListDatabaseConnector(AgentSelection.this);
                taskListDatabaseConnector.insertLogs(agentName,TAG,"EoTasks","https://greenlightplanet.looker.com:19999/api/3.0/looks/"+TableCode,String.valueOf(elapsedTime),currentDateTimeString,txt_Country);

                taskListDatabaseConnector.close();

*/




                //  new GetData().inputDataIntoDatabase("410", agentName, getApplicationContext()); //28 of task ihsaan
                // new GetData().inputDataIntoDatabase("1305",agentName,getApplicationContext()); // taz task
            } catch (Exception e) {
                //createErrorDialogBox(getString(R.string.txt_technical_error));

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            preferencesEditor = savedPreferences.edit();
            //  preferencesEditor.putString("agent", agentName);
            //  preferencesEditor.putString("agentCountry", txt_Country);
            preferencesEditor.putString("OTP","Done");
            preferencesEditor.putString("NewIncentive",new_incentive);
            Gson gson = new Gson();
            String json = gson.toJson(agent_score);
            preferencesEditor.putString("finalScore",json);
            preferencesEditor.apply();
            progressDialog.dismiss();
            if (new_incentive.equalsIgnoreCase("Yes")){
                Intent openTaskList = new Intent(AgentSelection.this, Summary.class);
                startActivity(openTaskList);
                AgentSelection.this.finish();
            }else{
                Intent openTaskList = new Intent(AgentSelection.this, TaskList.class);
                startActivity(openTaskList);
                AgentSelection.this.finish();
            }

        }

    }

    public void createErrorDialogBox(String message) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(AgentSelection.this);
        alertDialogBuilder.setTitle(getString(R.string.error_dialog_box_header));
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setPositiveButton(getString(R.string.acknowledgement_button), null);
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setCancelable(false);

        alertDialog.show();


    };





    public static boolean isInteger(String s, int radix) {
        if(s.isEmpty()) return false;
        for(int i = 0; i < s.length(); i++) {
            if(i == 0 && s.charAt(i) == '-') {
                if(s.length() == 1) return false;
                else continue;
            }
            if(Character.digit(s.charAt(i),radix) < 0) return false;
        }
        return true;
    }


    public void createErrorDialogBoxInternetConnection(String message) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(AgentSelection.this);
        alertDialogBuilder.setTitle(getString(R.string.error_dialog_box_header));
        alertDialogBuilder.setMessage(message);


        alertDialogBuilder.setPositiveButton(" OK ", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {
                Toast.makeText(AgentSelection.this,getString(R.string.no_internet_try_again),Toast.LENGTH_SHORT).show();
                finish();
                //dialog.dismiss();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setCancelable(false);

        alertDialog.show();


    };

    @Override
    public void onBackPressed() {
        finish();
    }
    public void volleyR() {
        progressDialog.show();
        String url = "https://greenlightplanet.looker.com:19999/api/3.0/login";
       StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        progressDialog.dismiss();
                        try {
                            JSONObject js = new JSONObject(s);
                            String access_Token = js.getString("access_token");
                            String token_type = js.getString("token_type");
                            String expires_in = js.getString("expires_in");
                            String tableCode = "399";
                            if (!(js == null)) {
                                getTabledata(access_Token, tableCode);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Toast.makeText(AgentSelection.this,"Something went wrong!!"+volleyError,Toast.LENGTH_LONG).show();
                        System.out.println("Something went wrong!");
                        volleyError.printStackTrace();
                        progressDialog.dismiss();
                        createErrorDialogBoxInternetConnection(getString(R.string.no_internet_connection));
                    }
                }){
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("client_id", "QY6DQzQT6NQM3ngbTRxY");
                params.put("client_secret", "PMb9rtjBzrdYgyvqQhfhTjrB");
                return params;
            }
        }  ;
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = NetworkCallSingleton.getInstance(mContext).getRequestQ();
        requestQueue.add(stringRequest);
        // requestQueue.add(nw.MVolleycall(mContext));
        //     NetworkCallSingleton.getInstance().MVolleycall(mContext);
        // NetworkCallSingleton.getInstance().addToRequestQueue(stringRequest);
    }



    public void getTabledata(String accessToken, String tableCode){



        final String token = accessToken;



        progressDialog.show();
        String url = "https://greenlightplanet.looker.com:19999/api/3.0/looks/" + tableCode + "/run/json";
        //   String url = "http://13.126.146.126/Sunking_EB/Kazi_login.php"+"?name="+name+"&password="+password+"&mobileNumber="+Mnumber+"&country="+country;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {

                        ArrayList<String> agentList = new ArrayList<String>();

                        try {
                            JSONArray jsonArray= new JSONArray(s);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                String country = (String) jsonObject.get("accounts.country");
                                agentList.add(country);
                            }


                            country_list = agentList;



                            if (country_list != null) {
                                ArrayAdapter<String> countrySelectionSpinnerAdapter = new ArrayAdapter<String>(AgentSelection.this, android.R.layout.simple_spinner_item, country_list);
                                countrySelectionSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                countrySelectionSpinner.setAdapter(countrySelectionSpinnerAdapter);
                                countrySelectionSpinner.setOnItemSelectedListener(countrySelectionSpinnerListener);
                                //  countrySelectionSpinner.setPrompt("Please Select the Country...");
                                progressDialog.dismiss();
                            } else {
                                createErrorDialogBoxInternetConnection(getString(R.string.no_internet_try_again));
                            }




                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        progressDialog.dismiss();



                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Toast.makeText(AgentSelection.this,"Something went wrong!!"+volleyError,Toast.LENGTH_LONG).show();
                        System.out.println("Something went wrong!");
                        volleyError.printStackTrace();
                        progressDialog.dismiss();
                        createErrorDialogBoxInternetConnection(getString(R.string.no_internet_connection));
                    }
                }){
            @Override
            public Map<String, String> getHeaders()
            //   protected Map<String, String> getParams()throws com.android.volley.AuthFailureError
            {
                Map<String, String  >  headers = new HashMap<String, String>();
                headers.put("Authorization","Bearer " +token);
                return headers;

            }

        }  ;
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = NetworkCallSingleton.getInstance(mContext).getRequestQ();
        requestQueue.add(stringRequest);
        // requestQueue.add(nw.MVolleycall(mContext));
        //     NetworkCallSingleton.getInstance().MVolleycall(mContext);





    }






  /*  public void volleyR() {
        progressDialog.show();
        String url = "https://greenlightplanet.looker.com:19999/api/3.0/login";
        //   String url = "http://13.126.146.126/Sunking_EB/Kazi_login.php"+"?name="+name+"&password="+password+"&mobileNumber="+Mnumber+"&country="+country;
    StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        Log.d("Response",s);


                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Toast.makeText(AgentSelection.this,"Something went wrong!!"+volleyError,Toast.LENGTH_LONG).show();
                        System.out.println("Something went wrong!");
                        volleyError.printStackTrace();
                        progressDialog.dismiss();
                        createErrorDialogBoxInternetConnection(getString(R.string.no_internet_connection));
                    }
                }){
        @Override
        protected Map<String, String> getParams()
        {
            Map<String, String>  params = new HashMap<String, String>();
            params.put("client_id", "QY6DQzQT6NQM3ngbTRxY");
            params.put("client_secret", "PMb9rtjBzrdYgyvqQhfhTjrB");
            return params;
        }
    }  ;
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
         RequestQueue requestQueue = NetworkCallSingleton.getInstance(mContext).getRequestQ();
         requestQueue.add(stringRequest);
       // requestQueue.add(nw.MVolleycall(mContext));
   //     NetworkCallSingleton.getInstance().MVolleycall(mContext);
       // NetworkCallSingleton.getInstance().addToRequestQueue(stringRequest);


    }*/







    private class country_update extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {

            LL_passcode.setVisibility(View.GONE);
            LL_country.setVisibility(View.VISIBLE);
            progressDialog.show();

        }

        @Override
        protected Void doInBackground(Void... params) {
            try {

                // initializeSSLContext(mContext);


                country_list = new GetData().getCountrylist();

            } catch (Exception e) {
                createErrorDialogBox(getString(R.string.txt_technical_error));
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            if (country_list != null) {
                ArrayAdapter<String> countrySelectionSpinnerAdapter = new ArrayAdapter<String>(AgentSelection.this, android.R.layout.simple_spinner_item, country_list);
                countrySelectionSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                countrySelectionSpinner.setAdapter(countrySelectionSpinnerAdapter);
                countrySelectionSpinner.setOnItemSelectedListener(countrySelectionSpinnerListener);
                //  countrySelectionSpinner.setPrompt("Please Select the Country...");
                progressDialog.dismiss();
            } else {
                createErrorDialogBoxInternetConnection(getString(R.string.no_internet_try_again));
            }
        }
    }



    private class login_check extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {

            LL_passcode.setVisibility(View.GONE);
            LL_country.setVisibility(View.VISIBLE);
            progressDialog.show();

        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                country_list = new GetData().getCountrylist();
            } catch (Exception e) {
                createErrorDialogBox(getString(R.string.txt_technical_error));
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            if (country_list != null) {
                ArrayAdapter<String> countrySelectionSpinnerAdapter = new ArrayAdapter<String>(AgentSelection.this, android.R.layout.simple_spinner_item, country_list);
                countrySelectionSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                countrySelectionSpinner.setAdapter(countrySelectionSpinnerAdapter);
                countrySelectionSpinner.setOnItemSelectedListener(countrySelectionSpinnerListener);
                //  countrySelectionSpinner.setPrompt("Please Select the Country...");
                progressDialog.dismiss();
            } else {
                createErrorDialogBoxInternetConnection(getString(R.string.no_internet_try_again));
            }
        }
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull final String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_ALL) {
            //check if all permissions are granted
            boolean allgranted = false;
            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                    allgranted = true;
                } else {
                    allgranted = false;
                    break;
                }
            }
            if (allgranted) {
                // Toast.makeText(getBaseContext(), " you have  got All Permissions", Toast.LENGTH_LONG).show();
            } else if (ActivityCompat.shouldShowRequestPermissionRationale(AgentSelection.this, permissions[0])
                    || ActivityCompat.shouldShowRequestPermissionRationale(AgentSelection.this, permissions[1])
                    || ActivityCompat.shouldShowRequestPermissionRationale(AgentSelection.this, permissions[2])
                    || ActivityCompat.shouldShowRequestPermissionRationale(AgentSelection.this, permissions[3])
                    || ActivityCompat.shouldShowRequestPermissionRationale(AgentSelection.this, permissions[4])
                    || ActivityCompat.shouldShowRequestPermissionRationale(AgentSelection.this, permissions[5])
                    || ActivityCompat.shouldShowRequestPermissionRationale(AgentSelection.this, permissions[6])


                    ) {

                android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(AgentSelection.this);
                builder.setTitle("Need Multiple Permissions");
                builder.setMessage("This app needs CallLog , Setting amd Storage permissions.");
                builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        ActivityCompat.requestPermissions(AgentSelection.this, permissions, PERMISSION_ALL);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Toast.makeText(getBaseContext(), " you have not given any Permissions ", Toast.LENGTH_LONG).show();
                    }
                });
                builder.show();
            } else {
                // Toast.makeText(getBaseContext(), "Unable to get Permission", Toast.LENGTH_LONG).show();
            }
        }
    }




    private class area_filter_task_list extends AsyncTask<Void, Void, Void> {

        String final_score;


        @Override
        protected void onPreExecute() {
            progressDialog.show();
        }


        @Override
        protected Void doInBackground(Void... voids) {
            try {
                // String tablecode = "1976";
                //  String tablecode = "2006"; //
                String tablecode = "2402"; // for testing areacode
                area_list = new GetData().getarealist_task_list(agentName, tablecode);

                //  new GetData().inputDataIntoDatabase("1976",agentName,getApplicationContext());

            } catch (Exception en) {
                createErrorDialogBox(getString(R.string.txt_technical_error));

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            String txt, key = null, value = null;
            String new_incentive = null;
            String user_country, user_area = null;


            try {
                if (area_list == null){
                    area_list.put("No", "NO");


                    new_incentive = "No";

                    preferencesEditor = savedPreferences.edit();
                    preferencesEditor.putString("NewIncentive", new_incentive);
                    preferencesEditor.apply();
                    progressDialog.dismiss();

                    Intent openTaskList = new Intent(AgentSelection.this, TaskList.class);
                    startActivity(openTaskList);
                    AgentSelection.this.finish();


                }

                if (area_list.size() > 0) {


                    String one_day = area_list.get("one_day_score");
                    String two_day = area_list.get("two_day_score");
                    String third_day = area_list.get("third_day_score");
                    String forth_day = area_list.get("forth_day_score");
                    String five_day = area_list.get("fivth_day_score");
                    String six_day = area_list.get("six_day_score");
                    String seven_day = area_list.get("seven_day_score");
                    user_country = area_list.get("accounts.country");

                    txt_Country = user_country;

                    final_score = one_day + two_day + third_day + forth_day + five_day + six_day + seven_day;



                    new_incentive = "Yes";
                    preferencesEditor = savedPreferences.edit();
                    preferencesEditor.putString("agent", agentName);
                    preferencesEditor.putString("agentCountry", txt_Country);
                    preferencesEditor.putString("OTP", "Done");
                    preferencesEditor.putString("incentiveDone", "Done");

                    preferencesEditor.putString("NewIncentive", new_incentive);
                    Gson gson = new Gson();
                    String json = gson.toJson(area_list);
                    preferencesEditor.putString("finalScore", json);
                    preferencesEditor.apply();
                    progressDialog.dismiss();


                    final GoToTaskList goToTaskList = new GoToTaskList("Yes", area_list);
                    goToTaskList.execute();


                /*if (agent_area_check.equals("Yes")) {

                    Intent openTaskList = new Intent(AgentSelection.this, Summary.class);
                    //openTaskList.putExtra("AGENT_SCORE", agent_score);
                    startActivity(openTaskList);
                    AgentSelection.this.finish();
                }*/



             /*   final GoToTaskList goToTaskList= new GoToTaskList("Yes",area_list);
                goToTaskList.execute();*/

                } else {

                    area_list.put("No", "NO");


                    new_incentive = "No";

                    preferencesEditor = savedPreferences.edit();
                    preferencesEditor.putString("NewIncentive", new_incentive);
                    preferencesEditor.apply();
                    progressDialog.dismiss();

                    Intent openTaskList = new Intent(AgentSelection.this, TaskList.class);
                    startActivity(openTaskList);
                    AgentSelection.this.finish();


         /*       final GoToTaskList goToTaskList= new GoToTaskList("No",area_list);
                goToTaskList.execute();*/

                }


            }catch (Exception  ex){

                ex.printStackTrace();

                progressDialog.dismiss();
            }
        }

    }






    public String firstCallqueryFilter(final String tableCode,final String agentName) {
        LoadingDialog.showLoadingDialog(mContext,"Loading...");

       // progressDialog.show();
        String url = "https://greenlightplanet.looker.com:19999/api/3.0/login";
        //   String url = "http://13.126.146.126/Sunking_EB/Kazi_login.php"+"?name="+name+"&password="+password+"&mobileNumber="+Mnumber+"&country="+country;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {

                      //  Toast.makeText(AgentSelection.this,"Starttttttttttttttt"+s,Toast.LENGTH_LONG).show();




                        try {
                            JSONObject js = new JSONObject(s);
                            String access_Token = js.getString("access_token");
                            String token_type = js.getString("token_type");
                            String expires_in = js.getString("expires_in");




                            // String  tableCode = "2239";

                            if (!(js == null)) {
                                //    getTabledata(access_Token, tableCode);
                                secoundcallFilter(access_Token,tableCode,agentName);

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            LoadingDialog.cancelLoading();

                        }


                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Toast.makeText(AgentSelection.this,"Something went wrong!!"+volleyError,Toast.LENGTH_LONG).show();
                        System.out.println("Something went wrong!");
                        volleyError.printStackTrace();
                        LoadingDialog.cancelLoading();
                        //progressDialog.dismiss();
                        createErrorDialogBoxInternetConnection(getString(R.string.no_internet_connection));
                    }
                }){
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("client_id", "QY6DQzQT6NQM3ngbTRxY");
                params.put("client_secret", "PMb9rtjBzrdYgyvqQhfhTjrB");
                return params;
            }
        }  ;
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = NetworkCallSingleton.getInstance(mContext).getRequestQ();
        requestQueue.add(stringRequest);
        // requestQueue.add(nw.MVolleycall(mContext));
        //     NetworkCallSingleton.getInstance().MVolleycall(mContext);
        // NetworkCallSingleton.getInstance().addToRequestQueue(stringRequest);
        return url;
    }


    public void secoundcallFilter(String accessToken, final String tableCode,final String agentName){

        final String token = accessToken;

        String id_query;
        // Log.d(accessToken,tableCode+"@@"+token);

//        progressDialog.show();
        String url = "https://greenlightplanet.looker.com:19999/api/3.0/looks/"+tableCode;
        //   String url = "http://13.126.146.126/Sunking_EB/Kazi_login.php"+"?name="+name+"&password="+password+"&mobileNumber="+Mnumber+"&country="+country;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {

                        int query_id = 0;
                        ArrayList<Integer> query_list = new ArrayList<Integer>();

                        try {
                            //  JSONArray jsonArray= new JSONArray(s);
                            JSONObject jsonObject = new JSONObject(s);

                            for (int i = 0; i < jsonObject.length(); i++) {
                                //JSONObject jsonObject = jsonObject.getJSONObject(i);

                                query_id = (int)jsonObject.get("query_id");
                                query_list.add(query_id);

                                if (query_list.size() > 0){

                                    break;
                                }
                            }


                            if (query_list.size() > 0) {
                                // thirdCallquery1(tableCode, query_id, Mnumber);

                                volleyRFilter(query_id,agentName);

                            }else{

                               // Toast.makeText(AgentSelection.this,"Something went wrong!!",Toast.LENGTH_LONG).show();
LoadingDialog.cancelLoading();

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        LoadingDialog.cancelLoading();
                        }




                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Toast.makeText(AgentSelection.this,"Something went wrong!!"+volleyError,Toast.LENGTH_LONG).show();
                        System.out.println("Something went wrong!");
                        volleyError.printStackTrace();
                        LoadingDialog.cancelLoading();

                        //                        progressDialog.dismiss();
                        createErrorDialogBoxInternetConnection(getString(R.string.no_internet_connection));
                    }
                }){
            @Override
            public Map<String, String> getHeaders()
            //   protected Map<String, String> getParams()throws com.android.volley.AuthFailureError
            {
                Map<String, String  >  headers = new HashMap<String, String>();
                headers.put("Authorization","Bearer " +token);
                return headers;

            }

        }  ;
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = NetworkCallSingleton.getInstance(mContext).getRequestQ();
        requestQueue.add(stringRequest);
        // requestQueue.add(nw.MVolleycall(mContext));
        //     NetworkCallSingleton.getInstance().MVolleycall(mContext);

    }





    public void thirdCallquery1Filter(final String accessToken, final int query_id, final String Mnumber){


        final JSONObject filter_update = new JSONObject();



        // Log.d(accessToken,tableCode+"@@"+token);

     //   progressDialog.show();
        String url = "https://greenlightplanet.looker.com:19999/api/3.0/queries/" + query_id;
        //   String url = "http://13.126.146.126/Sunking_EB/Kazi_login.php"+"?name="+name+"&password="+password+"&mobileNumber="+Mnumber+"&country="+country;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {



                        try {

                            JSONObject finalFilter = new JSONObject();
                            JSONObject jsonObject = new JSONObject(s);
                            for (int i = 0; i < jsonObject.length(); i++) {

                                finalFilter = new JSONObject(String.valueOf(s));


                                String filters = (String) finalFilter.getString("filters");

                                finalFilter.put("client_id", "");
                                JSONObject jsonObject1 = new JSONObject(filters);
                                JSONObject filter_update = jsonObject1.put("accounts.agent",Mnumber);

                                finalFilter.put("filters",filter_update);
                            }

                            QueryIDFilter(finalFilter,Mnumber);




                        } catch (JSONException e) {
                            e.printStackTrace();
                        LoadingDialog.cancelLoading();
                        }


//                        progressDialog.dismiss();


                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Toast.makeText(AgentSelection.this,"Something went wrong!!"+volleyError,Toast.LENGTH_LONG).show();
                        System.out.println("Something went wrong!");
                        volleyError.printStackTrace();

                        LoadingDialog.cancelLoading();
                        //                      progressDialog.dismiss();
                        createErrorDialogBoxInternetConnection(getString(R.string.no_internet_connection));
                    }
                }){


            @Override
            public Map<String, String> getHeaders()
            //   protected Map<String, String> getParams()throws com.android.volley.AuthFailureError
            {
                Map<String, String  >  headers = new HashMap<String, String>();
                headers.put("Authorization","Bearer " +accessToken);
                return headers;

            }

        }  ;
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = NetworkCallSingleton.getInstance(mContext).getRequestQ();
        requestQueue.add(stringRequest);
        // requestQueue.add(nw.MVolleycall(mContext));
        //     NetworkCallSingleton.getInstance().MVolleycall(mContext);





    }



    public void forthCallquery1Filter(final String accessToken, final JSONObject queress, final String agentName){


        // Log.d(accessToken,tableCode+"@@"+token);

//        progressDialog.show();
        final String requestBody,FinalID;

        requestBody = queress.toString();

        String url = "https://greenlightplanet.looker.com:19999/api/3.0/queries";
        //   String url = "http://13.126.146.126/Sunking_EB/Kazi_login.php"+"?name="+name+"&password="+password+"&mobileNumber="+Mnumber+"&country="+country;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {


                        int final_id = 0;
                        ArrayList<Integer> finalq_list = new ArrayList<Integer>();

                        try {
                            //  JSONArray jsonArray= new JSONArray(s);
                            JSONObject jsonObject = new JSONObject(s);

                            for (int i = 0; i < jsonObject.length(); i++) {
                                //JSONObject jsonObject = jsonObject.getJSONObject(i);

                                final_id = (int)jsonObject.get("id");
                                finalq_list.add(final_id);

                                if (finalq_list.size() > 0){

                                    break;
                                }
                            }


                            if (finalq_list.size() > 0) {
                                // thirdCallquery1(tableCode, query_id, Mnumber);

                                FinalQuery_loginFilter(final_id,agentName);

                            }else{

                                Toast.makeText(AgentSelection.this,"Something went wrong!!",Toast.LENGTH_LONG).show();

                                LoadingDialog.cancelLoading();

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                            LoadingDialog.cancelLoading();

                        }





//                        progressDialog.dismiss();


                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Toast.makeText(AgentSelection.this,"Something went wrong!!"+volleyError,Toast.LENGTH_LONG).show();
                        System.out.println("Something went wrong!");
                        volleyError.printStackTrace();
                        LoadingDialog.cancelLoading();
  //                      progressDialog.dismiss();
                        createErrorDialogBoxInternetConnection(getString(R.string.no_internet_connection));
                    }
                }){

            @Override
            public Map<String, String> getHeaders()
            //   protected Map<String, String> getParams()throws com.android.volley.AuthFailureError
            {
                Map<String,String>  headers = new HashMap<String, String>();
                headers.put("Authorization","Bearer " +accessToken);
                headers.put("Content-Type","application/json");
                // headers.put("Query",String.valueOf(queress));
                return headers;
            }

            @Override
            public String getBodyContentType() {
                return String.format("application/json: charset=utf-8");
            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s",
                            requestBody, "utf-8");
                    return null;
                }
            }


        }
                ;
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = NetworkCallSingleton.getInstance(mContext).getRequestQ();
        requestQueue.add(stringRequest);
        // requestQueue.add(nw.MVolleycall(mContext));
        //     NetworkCallSingleton.getInstance().MVolleycall(mContext);





    }


    public void volleyRFilter(final int query_id, final String agentName) {
     //   progressDialog.show();
        String url = "https://greenlightplanet.looker.com:19999/api/3.0/login";
        //   String url = "http://13.126.146.126/Sunking_EB/Kazi_login.php"+"?name="+name+"&password="+password+"&mobileNumber="+Mnumber+"&country="+country;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                      //  Log.d("Response",s);

                   //     progressDialog.dismiss();

                        try {
                            JSONObject js = new JSONObject(s);
                            String access_Token = js.getString("access_token");
                            String token_type = js.getString("token_type");
                            String expires_in = js.getString("expires_in");



                           // String tableCode = "2239";
                            if (!(js == null)) {
                                // getTabledata(access_Token, tableCode);

                                thirdCallquery1Filter(access_Token,query_id,agentName);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            LoadingDialog.cancelLoading();

                        }


                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Toast.makeText(AgentSelection.this,"Something went wrong!!"+volleyError,Toast.LENGTH_LONG).show();
                        System.out.println("Something went wrong!");
                        volleyError.printStackTrace();
                       LoadingDialog.cancelLoading();
                        // progressDialog.dismiss();
                        createErrorDialogBoxInternetConnection(getString(R.string.no_internet_connection));
                    }
                }){
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("client_id", "QY6DQzQT6NQM3ngbTRxY");
                params.put("client_secret", "PMb9rtjBzrdYgyvqQhfhTjrB");
                return params;
            }
        }  ;
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = NetworkCallSingleton.getInstance(mContext).getRequestQ();
        requestQueue.add(stringRequest);
        // requestQueue.add(nw.MVolleycall(mContext));
        //     NetworkCallSingleton.getInstance().MVolleycall(mContext);
        // NetworkCallSingleton.getInstance().addToRequestQueue(stringRequest);
    }




    public void FinalQuery_loginFilter(final int query_ID, final String agentName) {
  //      progressDialog.show();
        String url = "https://greenlightplanet.looker.com:19999/api/3.0/login";
        //   String url = "http://13.126.146.126/Sunking_EB/Kazi_login.php"+"?name="+name+"&password="+password+"&mobileNumber="+Mnumber+"&country="+country;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {


                       // progressDialog.dismiss();

                        try {
                            JSONObject js = new JSONObject(s);
                            String access_Token = js.getString("access_token");
                            String token_type = js.getString("token_type");
                            String expires_in = js.getString("expires_in");




                            if (!(js == null)) {


                                finalquery1oginFilter(access_Token,query_ID,agentName);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        LoadingDialog.cancelLoading();
                        }


                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Toast.makeText(AgentSelection.this,"Something went wrong!!"+volleyError,Toast.LENGTH_LONG).show();
                        System.out.println("Something went wrong!");
                        volleyError.printStackTrace();
                       LoadingDialog.cancelLoading();
                        // progressDialog.dismiss();
                        createErrorDialogBoxInternetConnection(getString(R.string.no_internet_connection));
                    }
                }){
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("client_id", "QY6DQzQT6NQM3ngbTRxY");
                params.put("client_secret", "PMb9rtjBzrdYgyvqQhfhTjrB");
                return params;
            }
        }  ;
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = NetworkCallSingleton.getInstance(mContext).getRequestQ();
        requestQueue.add(stringRequest);
        // requestQueue.add(nw.MVolleycall(mContext));
        //     NetworkCallSingleton.getInstance().MVolleycall(mContext);
        // NetworkCallSingleton.getInstance().addToRequestQueue(stringRequest);
    }




    public void finalquery1oginFilter(final String accessToken, int final_Id, final String agentName){
        String final_score;


         final String   agentname = agentName;
        // Log.d(accessToken,tableCode+"@@"+token);




        String url = "https://greenlightplanet.looker.com:19999/api/3.0/queries/" + final_Id + "/run/json";
        //   String url = "http://13.126.146.126/Sunking_EB/Kazi_login.php"+"?name="+name+"&password="+password+"&mobileNumber="+Mnumber+"&country="+country;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {



                        LoadingDialog.cancelLoading();
                        String final_score;


                        try {

                            JSONArray jsonArray = new JSONArray(s);
                            if (!(jsonArray == null)) {
                                for (int i = 0; i < jsonArray.length(); i++) {

                                    JSONObject agentNameJ = (JSONObject) jsonArray.get(i);


                                    if (agentNameJ.get("accounts.agent").toString().equals(agentname)) {
                                        arealist.put("area", agentNameJ.get("angaza_users_facts.current_area").toString());
                                        arealist.put("one_day_score", agentNameJ.get("portfolio_derived.one_day_average_account_disabled_score").toString());
                                        arealist.put("two_day_score", agentNameJ.get("portfolio_derived.two_day_average_account_disabled_score").toString());
                                        arealist.put("third_day_score", agentNameJ.get("portfolio_derived.three_day_average_account_disabled_score").toString());
                                        arealist.put("forth_day_score", agentNameJ.get("portfolio_derived.four_day_average_account_disabled_score").toString());
                                        arealist.put("fivth_day_score", agentNameJ.get("portfolio_derived.five_day_average_account_disabled_score").toString());
                                        arealist.put("six_day_score", agentNameJ.get("portfolio_derived.six_day_average_account_disabled_score").toString());
                                        arealist.put("seven_day_score", agentNameJ.get("portfolio_derived.seven_day_average_account_disabled_score").toString());
                                        arealist.put("accounts.country", agentNameJ.get("accounts.country").toString());
                                    }
                                }
                            }


                            {
                                String txt,key = null,value=null;
                                String user_area = null;

                                try {


                                    if (arealist.size() > 0) {



                                        String one_day = arealist.get("one_day_score");
                                        String two_day = arealist.get("two_day_score");
                                        String third_day = arealist.get("third_day_score");
                                        String forth_day = arealist.get("forth_day_score");
                                        String five_day = arealist.get("fivth_day_score");
                                        String six_day = arealist.get("six_day_score");
                                        String seven_day = arealist.get("seven_day_score");
                                        user_country = arealist.get("accounts.country");
                                        txt_Country = user_country;

                                        final_score = one_day + two_day + third_day + forth_day + five_day + six_day + seven_day;



                                        // preferencesEditor.putString("agent", agentName);
                                        preferencesEditor = savedPreferences.edit();
                                        preferencesEditor.putString("agent", agentname);
                                        preferencesEditor.putString("agentCountry", txt_Country);
                                        preferencesEditor.putString("OTP", "Done");
                                        preferencesEditor.putString("incentiveDone", "Done");
                                        preferencesEditor.apply();
                                        progressDialog.dismiss();

/*
                                            final GoToTaskList goToTaskList = new GoToTaskList("Yes", arealist);
                                            goToTaskList.execute();*/

                                        CallGoToTaskList("Yes",arealist);


/*

                                        new Handler().postDelayed(new Runnable() {

                                            @Override
                                            public void run() {

                                              //  LoadingDialog.showLoadingDialog(mContext,"Please wait...");

                                                CallGoToTaskList("Yes",arealist);
                                            }
                                        },1000 );

*/


                                   //     CallGoToTaskList("Yes",arealist);



                                    } else {

                                        arealist.put("No", "NO");
                                        preferencesEditor = savedPreferences.edit();
                                        preferencesEditor.putString("agent", agentname);
                                        preferencesEditor.putString("agentCountry", txt_Country);
                                        preferencesEditor.putString("incentiveDone", "Done");
                                        preferencesEditor.apply();




                                        CallGoToTaskList("No",arealist);

                                    /*    new Handler().postDelayed(new Runnable() {

                                            @Override
                                            public void run() {


                                                LoadingDialog.showLoadingDialog(mContext,"Please wait...");
                                                CallGoToTaskList("No",arealist);
                                            }
                                        },10000 );*/


/*

                                            final GoToTaskList goToTaskList = new GoToTaskList("No", arealist);
                                            goToTaskList.execute();
*/

                                    }

                                }catch (Exception ex){
                                    ex.printStackTrace();
                                }
                            }

                        }catch (Exception e){
                            e.printStackTrace();

                        }

                      // LoadingDialog.cancelLoading();










                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Toast.makeText(AgentSelection.this,"Something went wrong!!"+volleyError,Toast.LENGTH_LONG).show();
                        System.out.println("Something went wrong!");
                        volleyError.printStackTrace();
                        progressDialog.dismiss();
                        createErrorDialogBoxInternetConnection(getString(R.string.no_internet_connection));
                    }
                }){


            @Override
            public Map<String, String> getHeaders()
            //   protected Map<String, String> getParams()throws com.android.volley.AuthFailureError
            {
                Map<String, String  >  headers = new HashMap<String, String>();
                headers.put("Authorization","Bearer " +accessToken);
                return headers;

            }

        }
                ;
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = NetworkCallSingleton.getInstance(mContext).getRequestQ();
        requestQueue.add(stringRequest);
        // requestQueue.add(nw.MVolleycall(mContext));
        //     NetworkCallSingleton.getInstance().MVolleycall(mContext);





    }


    public void QueryIDFilter(final JSONObject queress, final String agentName) {
        progressDialog.show();
        String url = "https://greenlightplanet.looker.com:19999/api/3.0/login";
        //   String url = "http://13.126.146.126/Sunking_EB/Kazi_login.php"+"?name="+name+"&password="+password+"&mobileNumber="+Mnumber+"&country="+country;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {

                      //  progressDialog.dismiss();

                        try {
                            JSONObject js = new JSONObject(s);
                            String access_Token = js.getString("access_token");
                            String token_type = js.getString("token_type");
                            String expires_in = js.getString("expires_in");


                       //     Log.d("RequestResss",access_Token+","+token_type+","+expires_in);


                            if (!(js == null)) {


                                forthCallquery1Filter(access_Token,queress,agentName);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                   LoadingDialog.cancelLoading();
                        }


                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Toast.makeText(AgentSelection.this,"Something went wrong!!"+volleyError,Toast.LENGTH_LONG).show();
                        System.out.println("Something went wrong!");
                        volleyError.printStackTrace();
                       LoadingDialog.cancelLoading();
                        // progressDialog.dismiss();
                        createErrorDialogBoxInternetConnection(getString(R.string.no_internet_connection));
                    }
                }){
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("client_id", "QY6DQzQT6NQM3ngbTRxY");
                params.put("client_secret", "PMb9rtjBzrdYgyvqQhfhTjrB");
                return params;
            }
        }  ;
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = NetworkCallSingleton.getInstance(mContext).getRequestQ();
        requestQueue.add(stringRequest);
        // requestQueue.add(nw.MVolleycall(mContext));
        //     NetworkCallSingleton.getInstance().MVolleycall(mContext);
        // NetworkCallSingleton.getInstance().addToRequestQueue(stringRequest);
    }









}