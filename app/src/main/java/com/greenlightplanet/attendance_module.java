package com.greenlightplanet;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Handler;
import android.os.LocaleList;
import android.os.Message;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.gcm.Task;
import com.greenlightplanet.kazi.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;

public class attendance_module extends Activity implements LocationListener {


    Button btn_check_in, btn_check_out, back_btn;
    private List<Locat> LocatList = new ArrayList<>();
    private RecyclerView recyclerView;
    private LocatAdapter mAdapter;
    private LocationManager locationManager;
    private String provider, agentName, current_area, country, formattedDate, formatdate;
    String data_name, data_latlong, data_area, data_location, data_timeStamp, data_country = null;
    private SharedPreferences savedPreferences;
    String locationAddress, latlong;
    GPSTracker gps;
    double latitude;
    double longitude;
    Context mContext;

    private ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance_module);

        mContext = attendance_module.this;
        progressDialog = new ProgressDialog(attendance_module.this);
        progressDialog.setTitle(getString(R.string.progress_bar_title));
        progressDialog.setMessage(getString(R.string.progress_bar_message));
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        btn_check_in = (Button) findViewById(R.id.check_in);
        btn_check_out = (Button) findViewById(R.id.check_out);
        back_btn = (Button) findViewById(R.id.attendace_back_button);


        savedPreferences = getSharedPreferences("preferences", MODE_PRIVATE);
        agentName = savedPreferences.getString("agent", "None");
        Intent i = getIntent();
        current_area = i.getStringExtra("current_area");
        country = i.getStringExtra("country");


        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        mAdapter = new LocatAdapter(LocatList);

        recyclerView.setHasFixedSize(true);

        // vertical RecyclerView
        // keep movie_list_row.xml width to `match_parent`
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());

        // horizontal RecyclerView
        // keep movie_list_row.xml width to `wrap_content`
        // RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);

        recyclerView.setLayoutManager(mLayoutManager);

        // adding inbuilt divider line
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        // adding custom divider line with padding 16dp
        // recyclerView.addItemDecoration(new MyDividerItemDecoration(this, LinearLayoutManager.HORIZONTAL, 16));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.setAdapter(mAdapter);

        // row click listener

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);


        if (ActivityCompat.checkSelfPermission((Activity) mContext, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((Activity) mContext, new String[]{
                    android.Manifest.permission.ACCESS_FINE_LOCATION
            }, 10);


        }

        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return ;
        }

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Locat movie = LocatList.get(position);
                Toast.makeText(getApplicationContext(), movie.getTitle() + " is selected!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (country.equalsIgnoreCase("India")){
                    Intent openTaskList = new Intent(attendance_module.this, TaskList.class);
                    startActivity(openTaskList);
                    finish();

                }else {
                    Intent openTaskList = new Intent(attendance_module.this, Summary.class);
                    startActivity(openTaskList);
                    finish();
                }

            }
        });

        btn_check_out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            /*    Long tsLong = System.currentTimeMillis()/1000;
                String ts = tsLong.toString();

                Calendar cal = Calendar.getInstance(Locale.ENGLISH);
                //cal.setTimeInMillis(time);
                cal.setTimeInMillis(Long.parseLong(String.valueOf(ts)));
                String date = DateFormat.format("yyyy-MM-dd", cal).toString();
*/




                Calendar c = Calendar.getInstance();
                System.out.println("Current time => " + c.getTime());

                SimpleDateFormat df1 = new SimpleDateFormat("yyyy-MM-dd");
                String date = df1.format(c.getTime());


                String eo_name = agentName.replaceAll(" ", "");

                // hardcoded

                // eo_name = "PrudenceWainaina";

                if (new GetData().isNetworkAvailable(attendance_module.this)) {
                    LocatList.clear();
                    getLocation(eo_name, date);
                } else {
                    createErrorDialogBoxInternetConnection(getString(R.string.no_internet_connection));
                }
            }
        });

        btn_check_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // create class object
/*


                if (gps == null) {


                    if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {


                        return ;
                    }
                    Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

                    Location location1 = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

                    Location location2 = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);

                    if (location != null) {
                        double latti = location.getLatitude();
                        double longi = location.getLongitude();
                        latitude = Double.parseDouble(String.valueOf(latti));
                        longitude = Double.parseDouble(String.valueOf(longi));


                    } else if (location1 != null) {
                        double latti = location1.getLatitude();
                        double longi = location1.getLongitude();
                        latitude = Double.parseDouble(String.valueOf(latti));
                        longitude = Double.parseDouble(String.valueOf(longi));


                    } else if (location2 != null) {
                        double latti = location2.getLatitude();
                        double longi = location2.getLongitude();
                        latitude = Double.parseDouble(String.valueOf(latti));
                        longitude = Double.parseDouble(String.valueOf(longi));

                    }

                    Log.d("CASTTTTTTTTTTT",latitude+""+longitude);








                }

*/


                if (new GetData().isNetworkAvailable(attendance_module.this)) {
                    gps = new GPSTracker(attendance_module.this);


                    // check if GPS enabled
                    if (gps.canGetLocation()) {


                        if (gps != null) {
                            latitude = gps.getLatitude();
                            longitude = gps.getLongitude();
                            LocationAddress locationAddress = new LocationAddress();

                            // harcoded lattitude and longitude
                            //  latitude = Double.parseDouble("18.9885912");
                            // longitude = Double.parseDouble("72.8154853");

                            Double doubleInstance = new Double(latitude);
                            Double doubleIns = new Double(longitude);
                            latlong = doubleInstance.toString() + "_" + doubleIns.toString();


                            if (latlong != "0.0") {
                                locationAddress.getAddressFromLocation(latitude, longitude,
                                        getApplicationContext(), new GeocoderHandler());
                            } else {

                                Toast.makeText(getApplicationContext(), "Please check the location settings!!", Toast.LENGTH_LONG).show();
                            }

                        } else {
                            showSettingsAlert();
                        }

                        // prepareMovieData("Andheri","12/04/2018","may");
                        // \n is for new line
                        //  Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
                    } else {
                        // can't get location
                        // GPS or Network is not enabled
                        // Ask user to enable GPS/network in settings
                        gps.showSettingsAlert();
                    }
                }else {
                    createErrorDialogBoxInternetConnection(getString(R.string.no_internet_connection));
                }

            }


        });
    }

    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                attendance_module.this);
        alertDialog.setTitle("SETTINGS");
        alertDialog.setMessage("Enable Location Provider! Go to settings menu?");
        alertDialog.setPositiveButton("Settings",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(
                                Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        attendance_module.this.startActivity(intent);
                    }
                });
        alertDialog.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        alertDialog.show();
    }


    public void prepareMovieData(String locationg,String timeStamp,String month) {





        Locat movie = new Locat(locationg,timeStamp,month);
        LocatList.add(movie);

/*

        SharedPreferences preferences = getSharedPreferences("uerInput", MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        LocatList.clear();

        editor.putInt("count", LocatList.size());
        editor.putString("locationg", locationg);
        editor.putString("timeStamp",timeStamp);
        editor.putString("month",month);
        for(int i=0; i<LocatList.size(); i++){
            editor.putInt("count", LocatList.size());
            editor.putString("locationg"+i, String.valueOf(LocatList.get(i)));
            editor.putString("timeStamp"+i, String.valueOf(LocatList.get(i)));
            editor.putString("month"+i, String.valueOf(LocatList.get(i)));
        }
        editor.commit();




        preferences = getSharedPreferences("uerInput", MODE_PRIVATE);
        int tmpCount = preferences.getInt("count", 10);
        String locationg1 = preferences.getString("locationg", "");
        String timeS = preferences.getString("timeStamp","");
        String mnth = preferences.getString("month","");

        for(int i=0; i<tmpCount; i++){

            Locat movie1 = new Locat(String.valueOf(locationg1),timeS,mnth);
            LocatList.add(movie1);

        }
*/



        /*

*/
/*

        Locat movie = new Locat(locationg,timeStamp,month);
        LocatList.add(movie);
*//*


        SharedPreferences preferences = getSharedPreferences("uerInput", MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        LocatList.clear();

        editor.putInt("count", LocatList.size());
        editor.putString("locationg", locationg);
        editor.putString("timeStamp",timeStamp);
        editor.putString("month",month);
        for(int i=0; i<LocatList.size(); i++){
            editor.putInt("count", LocatList.size());
            editor.putString("locationg"+i, String.valueOf(LocatList.get(i)));
            editor.putString("timeStamp"+i, String.valueOf(LocatList.get(i)));
            editor.putString("month"+i, String.valueOf(LocatList.get(i)));
        }
        editor.commit();



        // restoring the data


//        LocatList.clear();
        preferences = getSharedPreferences("uerInput", MODE_PRIVATE);
        int tmpCount = preferences.getInt("count", 10);
        String locationg1 = preferences.getString("locationg", "");
        String timeS = preferences.getString("timeStamp","");
        String mnth = preferences.getString("month","");

        for(int i=0; i<tmpCount; i++){

            Locat movie1 = new Locat(String.valueOf(locationg1),timeS,mnth);

            LocatList.add(movie1);

        }

*/

        // notify adapter about data set changes
        // so that it will render the list with new data
        mAdapter.notifyDataSetChanged();
    }



    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    private class GeocoderHandler extends Handler {
        @Override
        public void handleMessage(Message message) {

            switch (message.what) {
                case 1:
                    Bundle bundle = message.getData();
                    locationAddress = bundle.getString("address");
                    break;
                default:
                    locationAddress = null;
            }


            Toast.makeText(getBaseContext(),"Check@@"+locationAddress,Toast.LENGTH_LONG).show();

            // tvAddress.setText(locationAddress);

            Long tsLong = System.currentTimeMillis()/1000;
            String ts = tsLong.toString();

            Calendar cal = Calendar.getInstance(Locale.ENGLISH);
            //cal.setTimeInMillis(time);
            cal.setTimeInMillis(Long.parseLong(String.valueOf(ts)));
             String date = DateFormat.format("yyyy-MM-dd", cal).toString();

            Calendar c = Calendar.getInstance();
            System.out.println("Current time =&gt; "+c.getTime());

            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
          // SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy-HH:mm:ss");
            SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
            formatdate =  df2.format(c.getTime());
            formattedDate = df.format(c.getTime());
// Now formattedDate have current date/time
           // Toast.makeText(this, formattedDate, Toast.LENGTH_SHORT).show();


           // prepareMovieData(locationAddress,formattedDate,"May");

            locationUpdate(agentName,current_area,locationAddress,latlong,formattedDate,formatdate,country);




            // adding on adapterview


        }



    }



    public void getLocation(String agentName,String timeStamp) {
        progressDialog.show();



        String url = "http://13.126.146.126/Sunking_EB/location.php"+"?name="+agentName+"&date="+timeStamp;

        //   String url = "http://13.126.146.126/Sunking_EB/Kazi_login.php"+"?name="+name+"&password="+password+"&mobileNumber="+Mnumber+"&country="+country;




        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,

                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        progressDialog.hide();

                        if (s.length()>=3) {
                            try {

                                JSONArray jsonArray = new JSONArray(s);
                                for (int i = 0; i < jsonArray.length(); i++) {

                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    data_name = jsonObject.getString("name");
                                    data_area = jsonObject.getString("area");
                                    data_latlong = jsonObject.getString("latlong");
                                    data_timeStamp = jsonObject.getString("timeStamp");
                                  //  data_timeStamp =  data_timeStamp.replaceAll("_","Time");


                                    data_timeStamp = data_timeStamp.replaceAll("_"," ");

                                    // formattedDate = "2018-06-16 21:06:30";

                                    StringTokenizer tk = new StringTokenizer(data_timeStamp);
                                    String date = tk.nextToken();
                                    String time = tk.nextToken();

                                    SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");
                                    SimpleDateFormat sdfss = new SimpleDateFormat("hh:mm a");
                                    Date dt;
                                    try {
                                        dt = sdf.parse(time);
                                        // Time locTime = Time.valueOf(sdfss.format(dt));

                                        String locTime = String.valueOf(sdfss.format(dt));
                                        System.out.println("Time Display: " + sdfss.format(dt)); // <-- I got result here




                                        data_timeStamp =  date +" "+locTime;



                                        prepareMovieData(data_timeStamp,data_latlong,data_name);


//                                        prepareMovieData(locationAddress,"--",formattedDate);


                                    } catch (ParseException e) {
                                        // TODO Auto-generated catch block
                                        e.printStackTrace();
                                    }












//                                    prepareMovieData(data_timeStamp,data_latlong,data_name);



                                }









                                progressDialog.dismiss();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }else if (s.length() == 2){

                            Toast.makeText(attendance_module.this, "No check in found!!", Toast.LENGTH_LONG).show();
                            progressDialog.dismiss();


                        }
                        else if (s.equals("")){

                            Toast.makeText(attendance_module.this, "Please check your credential!!", Toast.LENGTH_LONG).show();
                            progressDialog.dismiss();
                        }

                    }
                },

                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        Toast.makeText(attendance_module.this,"Something went wrong!!"+volleyError,Toast.LENGTH_LONG).show();
                        System.out.println("Something went wrong!");
                        volleyError.printStackTrace();
                        progressDialog.dismiss();

                        createErrorDialogBoxInternetConnection(getString(R.string.no_internet_connection));

                    }
                });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }











    public void locationUpdate(String agentName,String area,String location,String latlong,String timeStamp,String date,String country) {
        progressDialog.show();

        String noSpaceStr = agentName.replaceAll("\\s", ""); // using built in method

         agentName = noSpaceStr;


         String spce = location.replaceAll("\\s","_");
         location = spce;


         String timeS = timeStamp.replaceAll("\\s","");
         timeStamp = timeS;
        String url = "http://13.126.146.126/Sunking_EB/kazi_attendance.php"+"?name="+agentName+"&area="+area+"&location="+location+"&latlong="+latlong+"&timeStamp="+timeStamp+"&date="+date+"&country="+country;

        //   String url = "http://13.126.146.126/Sunking_EB/Kazi_login.php"+"?name="+name+"&password="+password+"&mobileNumber="+Mnumber+"&country="+country;




        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,

                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        progressDialog.hide();




                        if (s.equalsIgnoreCase("Success")) {



                              //  Log.d("Data url",s.toString());
                            try {

                                //Toast.makeText(attendance_module.this, "Successfully Logged in!!", Toast.LENGTH_LONG).show();


                            formattedDate = formattedDate.replaceAll("_"," ");

                          // formattedDate = "2018-06-16 21:06:30";

                            StringTokenizer tk = new StringTokenizer(formattedDate);
                            String date = tk.nextToken();
                            String time = tk.nextToken();

                            SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:aa");
                            String now = sdf.format(new java.util.Date().getTime());
                          //  SimpleDateFormat sdfss = new SimpleDateFormat("hh:mm a");
                            Date dt;

                               // dt = sdf.parse(time);
                              // Time locTime = Time.valueOf(sdfss.format(dt));

                               /* String locTime = String.valueOf(sdfss.format(dt));
                                System.out.println("Time Display: " + sdfss.format(dt));*/ // <-- I got result here




                            formattedDate =  date +" "+now;


                                LocatList.clear();
                                prepareMovieData(locationAddress,"--",formattedDate);


                            } catch (Exception e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }

                                progressDialog.dismiss();

                        }else{
                            Toast.makeText(attendance_module.this,"Please check your location!!",Toast.LENGTH_LONG).show();
                        }

                    }
                },

                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        Toast.makeText(attendance_module.this,"Something went wrong!!"+volleyError,Toast.LENGTH_LONG).show();
                        System.out.println("Something went wrong!");
                        volleyError.printStackTrace();
                        progressDialog.dismiss();

                        createErrorDialogBoxInternetConnection(getString(R.string.no_internet_connection));

                    }
                });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }



    public void createErrorDialogBoxInternetConnection(String message) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(attendance_module.this);
        alertDialogBuilder.setTitle(getString(R.string.error_dialog_box_header));
        alertDialogBuilder.setMessage(message);


        alertDialogBuilder.setPositiveButton(" OK ", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {
                Toast.makeText(attendance_module.this,getString(R.string.no_internet_try_again),Toast.LENGTH_SHORT).show();
                //finish();
    dialog.dismiss();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setCancelable(false);

        alertDialog.show();


    }



}
