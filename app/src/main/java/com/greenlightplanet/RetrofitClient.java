package com.greenlightplanet;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Url;


/**
 * Created by Prateek on 21/11/16.
 */

public class RetrofitClient {

    private static GitApiInterface gitApiInterface;

    private static String PNR_URL = "https://greenlightplanet.looker.com:19999/";

    public static GitApiInterface Indianrail() {
        if (gitApiInterface == null) {


            Retrofit client = new Retrofit.Builder()
                    .baseUrl(PNR_URL)
                    .addConverterFactory(new ToStringConverterFactory())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            gitApiInterface = client.create(GitApiInterface.class);
        }
        return gitApiInterface;
    }

    public interface GitApiInterface {
        @GET
        Call<String> getdata(@Url String url);
    }


}