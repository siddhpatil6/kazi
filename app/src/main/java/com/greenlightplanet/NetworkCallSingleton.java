package com.greenlightplanet;

import android.content.Context;
import android.os.Build;

import com.android.volley.BuildConfig;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.Volley;
import com.greenlightplanet.kazi.R;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;

public class NetworkCallSingleton {
    private static NetworkCallSingleton networkCallSingleton = null;
    private RequestQueue requestQueue;
   /* private CallApi callApi;
    private SharedPreferenceHelper sharedPreferenceHelper;*/


    public NetworkCallSingleton(Context context) {

        //requestQueue = new Volley().newRequestQueue(context);


       MVolleycall(context);

        //callApi = new CallApi(requestQueue);
        //sharedPreferenceHelper = SharedPreferenceHelper.getInstance();
    }

    public String MVolleycall(Context context) {
        try {


            if (BuildConfig.FLAVOR.equalsIgnoreCase("dev"))
                requestQueue = new Volley().newRequestQueue(context);
            else
                requestQueue = getPinnedRequestQueue(context);
        } catch (CertificateException e) {
            //  LogUtil.printException(e);
        } catch (IOException e) {
            //LogUtil.printException(e);
        } catch (NoSuchAlgorithmException e) {
            //LogUtil.printException(e);
        } catch (KeyStoreException e) {
            //LogUtil.printException(e);
        } catch (KeyManagementException e) {
            //LogUtil.printException(e);
        }


        return null;
    }

    public static void initNetworkCallSingleton(Context context) {
        networkCallSingleton = new NetworkCallSingleton(context);
    }

    public static NetworkCallSingleton getInstance(Context context) {
        initNetworkCallSingleton(context);
        if (networkCallSingleton == null)
            throw new IllegalStateException("Init first !");
        return networkCallSingleton;
    }

   /* public CallApi getCallApi() {
        return callApi;
    }*/


    private RequestQueue getPinnedRequestQueue(Context context) throws CertificateException, IOException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException {

        CertificateFactory cf = CertificateFactory.getInstance("X.509");

        // Generate the certificate using the certificate file under res/raw/cert.cer
        InputStream caInput = new BufferedInputStream(context.getResources().openRawResource(R.raw.looker_cert));
        final Certificate ca = cf.generateCertificate(caInput);
        caInput.close();

        // Create a KeyStore containing our trusted CAs
        String keyStoreType = KeyStore.getDefaultType();
        KeyStore trusted = KeyStore.getInstance(keyStoreType);
        trusted.load(null, null);
        trusted.setCertificateEntry("ca", ca);

        // Create a TrustManager that trusts the CAs in our KeyStore
        String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
        TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
        tmf.init(trusted);

        // Create an SSLContext that uses our TrustManager
//        SSLContext sslContext = SSLContext.getInstance("TLS");
//        sslContext.init(null, tmf.getTrustManagers(), null);
        SSLContext sslContext = SSLContext.getInstance("TLSV1.2");
        sslContext.init(null, tmf.getTrustManagers(), null);

        SSLSocketFactory sf = sslContext.getSocketFactory();

        if (Build.VERSION.SDK_INT >= 17 && Build.VERSION.SDK_INT < 22) {
            sf = new TLS12SocketFactory(sf);
        }


//        SSLSocketFactory sf = sslContext.getSocketFactory();
        HurlStack hurlStack = new HurlStack(null, sf) {
            @Override
            protected HttpURLConnection createConnection(URL url) throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super.createConnection(url);

                httpsURLConnection.setHostnameVerifier(new HostnameVerifier() {
                    @Override
                    public boolean verify(String hostName, SSLSession sslSession) {
                       /* String certificateDomainName = ((X509Certificate) ca).getSubjectDN().toString();
                        //Log.i(TAG, "Index : " + certificateDomainName.indexOf("CN=") + " Len : " + certificateDomainName.codePointCount(certificateDomainName.indexOf("CN="), certificateDomainName.indexOf(",")));
                        String certificateName = certificateDomainName.substring(certificateDomainName.indexOf("CN="), certificateDomainName.codePointCount(certificateDomainName.indexOf("CN="), certificateDomainName.indexOf(",")));
                        certificateName = certificateName.replace("CN=", "");
                        //LogUtil.info(TAG, "hostName : " + hostName + " certificateName : " + certificateName);
                        if (certificateName.isEmpty())
                            return false;
                        return certificateName.equals(hostName);*/

return true;
                    }
                });
                return httpsURLConnection;
            }
        };

        return new Volley().newRequestQueue(context, hurlStack);
    }

    public RequestQueue getRequestQ() {
        return requestQueue;
    }
}